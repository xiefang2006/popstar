//
//  GameManager.h
//  Game
//
//  Created by Xie Fang on 9/14/12.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import <GameKit/GameKit.h>
//#import <Social/Social.h>
//#import "WeixinActivity.h"
//#import "TimelineActivity.h"

#import "cocos2d.h"


// MMU ads

//#import "MMUBanners.h"
//#import "MMUInterstitials.h"

// Old ads
/*
#import "AdMoGoView.h"
#import "AdMoGoInterstitial.h"
#import "AdMoGoInterstitialDelegate.h"
#import "AdMoGoDelegateProtocol.h"
#import "AdMoGoWebBrowserControllerUserDelegate.h"
#import "AdMoGoInterstitialManager.h"
#import "AdMoGoLogCenter.h"
*/
// New ads
//#import <ChartBoost/Chartboost.h>

// Domob video
//#import "AssetZoneVideoManager.h"
//#import "DMOfferWallViewController.h"
//#import "DMVideoViewController.h"
//#import "DMOfferWallManager.h"

// Admob ads
#import <GoogleMobileAds/GADInterstitial.h>
#import <GoogleMobileAds/GADBannerView.h>



@interface GameManagerPlatform : NSObject <

    /*MMUBannersDelegate, MMUBrowserDelegate, MMUInterstitialDelegate,*/

    /*AdMoGoDelegate, AdMoGoInterstitialDelegate,AdMoGoWebBrowserControllerUserDelegate,ChartboostDelegate, */
    GADBannerViewDelegate, GADInterstitialDelegate,

    GKMatchDelegate, GKMatchmakerViewControllerDelegate,
    SKProductsRequestDelegate, SKPaymentTransactionObserver,
    SKStoreProductViewControllerDelegate,
    UIAlertViewDelegate>
{
    // 0 for nothing
    // 1 for rating
    // 2 for promo
    int alertType;
    
    int dmPoint;
    NSArray *products;
    
    //MMUBanners *banner;
    //MMUInterstitials *interstitial;
    //AdMoGoView *banner;
    //AdMoGoInterstitial *interstitial;
    GADBannerView *banner;
    GADInterstitial *interstitial;
    
    GADInterstitial *admobInterstitial;
        
    unsigned int nextWall;
        
    GKMatch* myMatch;
    BOOL matchStarted;
    
}

+(GameManagerPlatform*)sharedGameManagerPlatform;


// Ads by adsmogo
-(void)showBanner;
-(void)hideBanner;
-(void)startBanner;
-(void)stopBanner;
-(void)cacheInterstitial;
-(void)showInterstitial;

-(void)loadInAppStore;
-(void)puchaseProduct:(NSString*)productId;
-(const char*)getProductPrice:(NSString*)productId;
-(void)restorePurchases;

-(void)loadGameCenter;
-(void)sendScore:(int)score forId:(NSString*)scoreId;
-(void)showGameCenter:(NSString*)scoreId;


-(void)shareSocialNetwork:(int)score;


// Ads with guomob wall and chartboost
-(void)showRandomWall;
-(void)showGuomobWall;
-(void)showInterstitialNew;
-(void)cacheInterstitialNew;

// Ads with admob
-(void)showInterstitialAdmob;
-(void)cacheInterstitialAdmob;

// Ads with domob wall
-(void)showDomobWall;
-(void)showVideoWall;



// GameCener Match related
-(void)hostMatch;
-(void)disconnectMatch;
-(void)sendScoreRT: (int)score;


- (void)presentAppStoreForID:(NSString *)appStoreID;
-(void)presentCoinAlert;
-(void)presentPromoPopAlert;
-(void)presentRatePopAlert;



//@property (nonatomic, retain) AdMoGoView *banner;
//@property (nonatomic, retain) AdMoGoInterstitial *interstitial;
@property (nonatomic, retain) NSArray* products;

//@property (nonatomic, retain) AssetZoneVideoManager *videoWall;

@property (nonatomic, retain) GKMatch *myMatch;
//@property (nonatomic, retain) MMUBanners *banner;
//@property (nonatomic, retain) MMUInterstitials *interstitial;

@property (nonatomic, retain) GADBannerView *banner;
@property (nonatomic, retain) GADInterstitial *interstitial;

/*
@property (nonatomic, retain) DMOfferWallViewController *domobWall;
@property (nonatomic, retain) DMVideoViewController *videoWall;
@property (nonatomic, retain) DMOfferWallManager *wallManager;
 */
@end


