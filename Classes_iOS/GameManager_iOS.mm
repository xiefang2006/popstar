//
//  GameManager.cpp
//  minesweeperx
//
//  Created by SimplyGame on 8/3/13.
//
//

#include "GameManager.h"
#import "GameManagerPlatform.h"
//#import "Flurry.h"

using namespace cocos2d;

void GameManager::startBanner() {
    [[GameManagerPlatform sharedGameManagerPlatform] startBanner];
}

void GameManager::showBanner() {
    [[GameManagerPlatform sharedGameManagerPlatform] showBanner];
}

void GameManager::hideBanner() {
    [[GameManagerPlatform sharedGameManagerPlatform] hideBanner];

}

void GameManager::cacheInterstitial() {
    [[GameManagerPlatform sharedGameManagerPlatform] cacheInterstitial];
    
    /*
    switch (getServerInterger("interType2")) {
        case 0:
            break;
        case 1:
            // Guomob and Chartboost
            [[GameManagerPlatform sharedGameManagerPlatform] cacheInterstitialNew];
            break;
        case 2:
            [[GameManagerPlatform sharedGameManagerPlatform] cacheInterstitial];
            //[[GameManagerPlatform sharedGameManagerPlatform] cacheInterstitialAdmob];

            break;
        case 3:
            [[GameManagerPlatform sharedGameManagerPlatform] cacheInterstitialAdmob];
            break;
        default:
            break;
    
    }
     */
    
}
void GameManager::showInterstitial() {
    
    [[GameManagerPlatform sharedGameManagerPlatform] showInterstitial];

    
    /*
    switch (getServerInterger("interType2")) {  // for 2.0 and later
        case 0:
            break;
        case 1:
            // Guomob and Chartboost
            [[GameManagerPlatform sharedGameManagerPlatform] showInterstitialNew];
            break;
        case 2:
            // AdsMogo
            [[GameManagerPlatform sharedGameManagerPlatform] showInterstitial];
            //[[GameManagerPlatform sharedGameManagerPlatform] cacheInterstitialAdmob];

            break;
        case 3:
            // Admob direct
            [[GameManagerPlatform sharedGameManagerPlatform] showInterstitialAdmob];
            break;
        default:
            break;
            
    }
     */
    
}

void GameManager::loadInAppStore() {
    [[GameManagerPlatform sharedGameManagerPlatform] loadInAppStore];
}
void GameManager::purchaseProuct(const char* productId) {
    NSString* str = [NSString stringWithCString:productId encoding:NSASCIIStringEncoding];
    [[GameManagerPlatform sharedGameManagerPlatform] puchaseProduct:str];
}

const char* GameManager::getProductPrice(const char* productId) {
    NSString* str = [NSString stringWithCString:productId encoding:NSASCIIStringEncoding];
    return [[GameManagerPlatform sharedGameManagerPlatform] getProductPrice:str];
}

void GameManager::restorePurchases() {
    [[GameManagerPlatform sharedGameManagerPlatform] restorePurchases];
}

void GameManager::loadGameCenter() {
    [[GameManagerPlatform sharedGameManagerPlatform] loadGameCenter];
}
     
void GameManager::sendScore(int score, const char* scoreId) {
    NSString* str = [NSString stringWithCString:scoreId encoding:NSASCIIStringEncoding];
    [[GameManagerPlatform sharedGameManagerPlatform] sendScore:score forId: str];
}

void GameManager::showGameCenter(const char* scoreId) {
    
    NSString* str = nil;
    
    if (scoreId != NULL) {
        str = [NSString stringWithCString:scoreId encoding:NSASCIIStringEncoding];
    }
    
    [[GameManagerPlatform sharedGameManagerPlatform] showGameCenter:str];
}

void GameManager::shareSocialNetwork(int score) {
    [[GameManagerPlatform sharedGameManagerPlatform] shareSocialNetwork:score];
}


void GameManager::showVideoAd() {
    [[GameManagerPlatform sharedGameManagerPlatform] showVideoWall];

}

void GameManager::showWall() {
    switch (getServerInterger("wallType")) {
        case 0:
            break;
        case 1:
            // Domob
            [[GameManagerPlatform sharedGameManagerPlatform] showDomobWall];
            break;
        case 2:
            // Guomob
            [[GameManagerPlatform sharedGameManagerPlatform] showGuomobWall];
            break;
        case 3:
            [[GameManagerPlatform sharedGameManagerPlatform] showRandomWall];
            break;
        default:
            break;
            
    }
}




void GameManager::openURL(const char* url) {
    NSString* urlString = [NSString stringWithUTF8String:url];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: urlString]];

    CCLog("opening url...");
}


void GameManager::logEvent(const char* info) {
    
    if (GameManager::sharedGameManager()->getServerBoolean("enableFlurry") == TRUE) {
        NSString* flurryInfo = [NSString stringWithUTF8String:info];
        //[Flurry logEvent:flurryInfo];
        NSLog(flurryInfo);
    }
}


void GameManager::presendAppstore(const char* appId) {
    [[GameManagerPlatform sharedGameManagerPlatform] presentAppStoreForID:[NSString stringWithUTF8String:appId]];
}


void GameManager::presendCoinAlert() {
    [[GameManagerPlatform sharedGameManagerPlatform] presentCoinAlert];
}

void GameManager::presendPromoPopAlert(bool rate) {
    if (rate == true) {
        [[GameManagerPlatform sharedGameManagerPlatform] presentRatePopAlert];
    }
    else {
        [[GameManagerPlatform sharedGameManagerPlatform] presentPromoPopAlert];
    }
}


void GameManager::hostMatch() {
    [[GameManagerPlatform sharedGameManagerPlatform] hostMatch];

}
void GameManager::disconnectMatch() {
    [[GameManagerPlatform sharedGameManagerPlatform] disconnectMatch];

}
void GameManager::sendScoreRT(int score) {
    [[GameManagerPlatform sharedGameManagerPlatform] sendScoreRT:score];

}


float GameManager::getScreenScale() {
    UIScreen *mainScreen = [UIScreen mainScreen];
    float scale = mainScreen.scale;
    NSLog(@"[Screen scale: %f]", scale);
    return scale;
}



