//
//  MMUNativePromoterManager.h
//  MMUSDK
//
//  Created by liuyu on 16/1/24.
//  Copyright 2007-2016 Alimama.com. All rights reserved.
//
//  Support Email: afpsupport@list.alibaba-inc.com


#import <Foundation/Foundation.h>

@interface MMUMamaPromoter : NSObject <NSCopying>

@property (nonatomic, readonly, copy) NSString *promoterId; // 创意ID

- (instancetype)initWithAttributes:(NSDictionary *)attributes;
- (NSDictionary *)materials; // 创意元信息，key会根据创意的类型有所变化
- (NSDictionary *)effects; // 创意渲染/交互相关的控制信息
- (NSDictionary *)providerInfo; // 角标信息

@end
