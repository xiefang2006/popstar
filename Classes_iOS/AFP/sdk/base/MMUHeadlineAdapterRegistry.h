//
//  MMUHeadlineAdapterRegistry.h
//  MMULibrary
//
//  Created by liuyu on 1/24/16.
//  Copyright © 2016 alimama. All rights reserved.
//

#import "MMUAdAPISDKNetworkRegistry.h"

@interface MMUHeadlineAdapterRegistry : MMUAdAPISDKNetworkRegistry

+ (MMUHeadlineAdapterRegistry *)sharedRegistry;

@end
