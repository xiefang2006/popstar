//
//  MMUSDK.h
//  MMUSDK
//
//  Created by liuyu on 12/23/14.
//  Updated by liufuyin 16/4/25.
//  Copyright 2007-2016 Alimama.com. All rights reserved.
//
//  Support Email: afpsupport@list.alibaba-inc.com

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, MMULogLevel) {
    MMULogLevelNone     = 0,      //不不输出日志
    MMULogLevelInfo     = 1 << 0, //输出常规日志，如轮换、平台相关信息等，一般在开发阶段使用此MASK即可
    MMULogLevelDebug    = 1 << 1, //输出调试信息，联调阶段使用，一般不需要使用此MASK
};

@interface MMUSDK : NSObject

@property (nonatomic, assign) MMULogLevel logLevelMask; //设置日志输出过滤，为MASK掩码形式，默认为MMULogLevelNone，即不输出日志。

+ (MMUSDK *)sharedInstance;

/**
 
 全局初始化，请在App启动时调用
 
 */

- (void)globalInitializeWithCompletionHandler:(void (^)(BOOL finished))handler;

/**
 
 初始化免登功能
 
 @param appkey 如使用的是精简版免登SDK，传入nil即可；否则，需传入对应的appkey
 
 */

- (void)initializeTBLoginWithAppkey:(NSString *)appkey;

/**
 
 双11红包初始化，如果不需要红包功能，则无需调用此接口
 
 */

- (void)initializeTBAppLinkWithAppkey:(NSString *)appkey andAppSecret:(NSString *)appSecret;

/**
 
 用于处理从其它App回跳当前App的case
 
 @return 是否经过了MMUSDK的处理
 
 */

- (BOOL)handleOpenURL:(NSURL *)url;

/**
 
 当前版本SDK的版本信息，示例：5.6.0.20150520
 
 @return 返回SDK版本号
 
 */

- (NSString *)sdkVersionStr;

/**
 
 注册百川免登协议，用于定制广告二跳页面加载逻辑，由开发者自定义登录流程
 该方法会替代默认的免登实现，请保证在MMUSDK初始化之前调用此方法
 具体需要实现的方法参见MMUFramework/MMUTBLoginProtocol.h文件
 @param TBLoginProtocol 自定义协议的类，必须为MMUTBLoginProtocol的子类
 
 */
- (void)registerTBLoginProtocol:(Class)TBLoginProtocol;

@end
