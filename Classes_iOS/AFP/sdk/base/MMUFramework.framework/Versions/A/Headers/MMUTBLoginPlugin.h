//
//  MMUTBLoginPlugin.h
//  MMUSDK
//
//  Created by GaoBin on 14/12/22.
//  Copyright (c) 2014年 alimama. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MMUTBLoginProtocol.h"

@interface MMUTBLoginPlugin : MMUTBLoginProtocol

+ (BOOL)isEnabled;
+ (void)setAppKey:(NSString *)appkey;
+ (void)globalInitialize;
+ (BOOL)handleOpenURL:(NSURL *)URL;

- (instancetype)initWithWebView:(UIWebView *)webView sourceViewController:(UIViewController *)viewController;
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request;

@end
