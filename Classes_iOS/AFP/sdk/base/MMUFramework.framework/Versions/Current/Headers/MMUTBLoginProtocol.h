//
//  Header.h
//  MMUFoundation
//
//  Created by GaoBin on 2016/11/16.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMUTBLoginProtocol : NSObject

/**
 
 是否启动百川免登功能，默认为YES
 
 */
+ (BOOL)isEnabled;

/**
 百川免登初始化，该方法只会调用一次
 
 */
+ (void)globalInitialize;

/**
 设置appkey 只有使用的是精简版免登SDK会调用此方法
 @param appkey 百川使用的appkey
 
 */
+ (void)setAppKey:(NSString *)appkey;

/**
 用于处理从手淘授权后跳回app的情况
 @param 只有使用的是精简版免登SDK会调用此方法
 
 */
+ (BOOL)handleOpenURL:(NSURL *)URL;

/**
 每次二跳页webview初始化的时候会调用此方法
 @param webView 二跳页所用的webview
 @param viewController 该UIViewController用于present其他页面
 */
- (instancetype)initWithWebView:(UIWebView *)webView sourceViewController:(UIViewController *)viewController;

/**
 处理页面跳转逻辑，在此处理具体的免登逻辑
 @param webView 二跳页所用的webview
 @param request 页面将要加载的请求
 @return 是否允许继续加载界面，如果免登处理了此逻辑，可走免登相关逻辑，并且返回NO即可，其他时刻返回YES，将逻辑回归广告SDK
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request;

@end
