//
//  MMUDISplashNetworkRegistry.h
//  MMULibrary
//
//  Created by liufuyin on 16/2/17.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUAdAPISDKNetworkRegistry.h"

@interface MMUDISplashNetworkRegistry : MMUAdAPISDKNetworkRegistry

+ (MMUDISplashNetworkRegistry *)sharedRegistry;

@end
