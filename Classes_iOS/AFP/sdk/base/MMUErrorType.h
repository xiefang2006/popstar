//
//  MMUErrorType.h
//  MMULibrary
//
//  Created by liufuyin on 16/6/1.
//  Copyright © 2016年 alimama. All rights reserved.
//

typedef enum {
    MMUE_Null,                              //null
    
    MMUE_AdPlaceIDEmpty = 1001,             //平台广告位ID为空
    MMUE_DataLoadFailed,                    //平台数据列表加载失败
    MMUE_AllPlatfromFailed,                 //广告平台全部失败
    
    MMUE_PFReqTimeOut = 2001,               //平台广告请求超时
    MMUE_PFAdFailed,                        //平台广告加载失败
    MMUE_PFAdDisplayFailed,                 //平台广告展示失败
    MMUE_PFAdObjError,                      //平台广告对象创建错误
    MMUE_PFAdTimeInvalid,                   //没有适时的创意
    
    MMUE_Banner_SizeInvalid = 3001,         //banner尺寸类型错误
    
    MMUE_Splash_LoadOutTime = 4001,         //开屏加载时长超过设置时间
    MMUE_Splash_WindowNeed,                 //开屏需要添加在window上
    MMUE_Splash_SizeInvalid,                //Splash_尺寸类型错误
    MMUE_Splash_ReserveViewSuper,           //开屏预留视图不能有父试图
    MMUE_Splash_ReserveViewSizeInvalid,     //开屏预留视图尺寸错误
    
    MMUE_Interstitial_ReqNotReady = 5001,   //插屏平台广告尚未请求完成，无法展示
    
    MMUE_DI_DataError = 6001,               //平台广告返回数据错误
    
    MMUE_ZIP_DOWNLOAD = 7001,                      //离线包下载错误
    MMUE_ZIP_UnzipOpen,                     //解压离线包打开文件错误
    MMUE_ZIP_Unzip,                         //解压离线包错误
    MMUE_ZIP_UnzipWrite,                    //解压离线包写入文件错误
    MMUE_ZIP_FileNotExist,                  //解压离线包后文件找不到
    
    MMUE_VIDEO_NOCACHED = 8001,              //视频文件未缓存
    
    MMUE_H5Feeds_InvalidCreatives = 9001,     //信息流Creatives数组空
    MMUE_H5Feeds_JSToolNotReady,              //计算cell高度失败
} MMUErrorType;
