//
//  MMUFeedsItem.h
//  MMUSDK
//
//  Created by liuyu on 16/1/20.
//  update by shiyi.sy on 16/11/29.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMUFeedsItem : NSObject

- (CGFloat)heightForPromoterCell;
- (UITableViewCell *)cellWithTableView:(UITableView *)tableView;

- (void)willDisplayCell:(UITableViewCell *)cell;
- (void)didEndDisplayingCell:(UITableViewCell *)cell;

@end
