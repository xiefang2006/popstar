//
//  MMUFeeds.h
//  MMUSDK
//
//  Created by shiyi.sy on 16/8/24.
//  Updated by shiyi.sy on 16/11/29.
//  Copyright 2007-2016 Alimama.com. All rights reserved.
//
//  Support Email: afpsupport@list.alibaba-inc.com


#import <Foundation/Foundation.h>

@class CLLocation;
@class MMUFeedsItem;

@protocol MMUFeedsDelegate;
@protocol MMUBrowserDelegate;

@interface MMUFeeds : NSObject

@property (nonatomic, copy) NSString *mTags; // 设置当前用户的标签信息
@property (nonatomic, copy) NSString *mSize; // 设置推广位的实际大小（in point）：推广位宽度x推广位高度，如 320x100
@property (nonatomic) NSInteger mAdcnt; //  设置期望返回的创意数

@property (nonatomic, weak) id<MMUFeedsDelegate> delegate;
@property (nonatomic, weak) id<MMUBrowserDelegate>  browserDelegate;

- (NSArray<MMUFeedsItem*>*)getFeedsItems;

- (instancetype)initWithSlotId:(NSString*)slotId viewController:(UIViewController*)controller;

- (void)requestPromoterDataInBackground;

- (void)setLocation:(CLLocation *)location;

@end

@protocol MMUFeedsDelegate <NSObject>

@optional

- (void)feeds:(MMUFeeds *)feeds didLoadDataFinished:(NSInteger)promotersAmount; //called when promoter list loaded
- (void)feeds:(MMUFeeds *)feeds didLoadDataFailedWithError:(NSError *)error; //called when promoter list loaded failed for some reason
- (void)feeds:(MMUFeeds *)feeds didClickedPromoterAtIndex:(NSInteger)promoterIndex; //called when table cell clicked

@end
