//
//  MMUBaiduInterstitialAdapter.m
//  MMUSDK
//
//  Created by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUBaiduInterstitialAdapter.h"
#import "MMUSDKInterstitialNetworkRegistry.h"
#import "BaiduMobAdSDK/BaiduMobAdInterstitial.h"


@interface MMUBaiduInterstitialAdapter() <UIGestureRecognizerDelegate, BaiduMobAdInterstitialDelegate>

@property (nonatomic, strong) BaiduMobAdInterstitial *interstitial;
@property (nonatomic, copy) NSString *appid;
@property (nonatomic, copy) NSString *adPlaceID;

@end


@implementation MMUBaiduInterstitialAdapter
+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeBaiduMobAd;
}

+ (void)load
{
    [[MMUSDKInterstitialNetworkRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adInterstitialWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:@"netset"];
    self.appid = [config mmuStringValueForKey:@"AppID"];
    self.adPlaceID = [config mmuStringValueForKey:@"AdPlaceID"];
    if ([self.appid length] == 0 || [self.adPlaceID length] == 0) {
        [self adInterstitialFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    self.interstitial = [[BaiduMobAdInterstitial alloc] init];
    self.interstitial.AdUnitTag = [self AdPlaceId];
    self.interstitial.delegate = self;
    self.interstitial.interstitialType = BaiduMobAdViewTypeInterstitialOther;
    [self.interstitial load];
    [self adInterstitialDidStartRequest];
}

- (void)stopBeingDelegate
{
    self.interstitial.delegate = nil;
}

- (void)presentInterstitial
{
    if (!self.interstitial.isReady) {
        return;
    }
    
    UIViewController *viewController = [self.delegate viewControllerForPresentingInterstitialModalView];
    [self.interstitial presentFromRootViewController:viewController];
}

#pragma mark - BaiduMobAdInterstitialDelegate
/**
 *  应用在mounion.baidu.com上的id
 */
- (NSString *)publisherId
{
    return self.appid;
}

- (NSString *)AdPlaceId
{
    return self.adPlaceID;
}

/**
 *  渠道id
 */
- (NSString *)channelId
{
    return @"13b50d6f";
}

/**
 *  广告预加载成功
 */
- (void)interstitialSuccessToLoadAd:(BaiduMobAdInterstitial *)interstitial
{
    NSLog(@"百度插屏加载成功");
    [self adInterstitiaDidReceiveWithInfo:nil];
}

/**
 *  广告预加载失败
 */
- (void)interstitialFailToLoadAd:(BaiduMobAdInterstitial *)interstitial
{
    NSLog(@"百度插屏加载失败");
    [self adInterstitialFailWithError:MMUE_PFAdFailed];
}

/**
 *  广告即将展示
 */
- (void)interstitialWillPresentScreen:(BaiduMobAdInterstitial *)interstitial
{
    [self adInterstitiaWillPresentWithInfo:@{KEY_INTERST_OBJ:self.interstitial}];
}

/**
 *  广告展示成功
 */
- (void)interstitialSuccessPresentScreen:(BaiduMobAdInterstitial *)interstitial
{
    NSLog(@"百度插屏展示成功");

    [self adInterstitiaDidPresentWithInfo:@{KEY_INTERST_OBJ:self.interstitial}];
}

/**
 *  广告展示失败
 */
- (void)interstitialFailPresentScreen:(BaiduMobAdInterstitial *)interstitial withError:(BaiduMobFailReason)reason
{
    NSLog(@"百度插屏展示失败");

    [self adInterstitialFailWithError:MMUE_PFAdDisplayFailed];
}

/**
 *  广告展示结束
 */
- (void)interstitialDidDismissScreen:(BaiduMobAdInterstitial *)interstitial
{
    [self adInterstitialDismissScreenWithInfo:@{KEY_INTERST_OBJ:self.interstitial}];
}

/**
 *  广告展示被用户点击时的回调
 */
- (void)interstitialDidAdClicked:(BaiduMobAdInterstitial *)interstitial
{
    [self adInterstitialClickedWithInfo:@{}];
}

@end
