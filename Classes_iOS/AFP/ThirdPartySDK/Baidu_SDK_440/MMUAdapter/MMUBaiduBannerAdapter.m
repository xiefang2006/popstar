//
//  MMUBaiduBannerAdapter.m
//  MMUSDK
//
//  Created by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//
#import "MMUBaiduBannerAdapter.h"
#import "MMUSDKBannerNetworkRegistry.h"
#import "MMUFramework/NSDictionary+MMUExtension.h"
#import "BaiduMobAdSDK/BaiduMobAdView.h"
#import "MMUErrorType.h"

@interface MMUBaiduBannerAdapter() <BaiduMobAdViewDelegate>

@property (nonatomic, strong) BaiduMobAdView *bannerView;
@property (nonatomic, copy) NSString *appid;
@property (nonatomic, copy) NSString *adPlaceID;

@end

@implementation MMUBaiduBannerAdapter

+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeBaiduMobAd;
}

+ (void)load
{
    [[MMUSDKBannerNetworkRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adBannerWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:@"netset"];
    self.appid = [config mmuStringValueForKey:@"AppID"];
    self.adPlaceID = [config mmuStringValueForKey:@"AdPlaceID"];
    if ([self.appid length] == 0 || [self.adPlaceID length] == 0) {
        [self adBannerFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    NSString *sizeInfoStr = [[self.ration mmuDictionaryValueForKey:KEY_AZSET] mmuStringValueForKey:KEY_BANNER_SIZE];
    NSDictionary *sizeInfo = [self typeInfoWithDescription:sizeInfoStr];
    NSNumber *typeInfo = [sizeInfo mmuNumberValueForKey:KEY_BANNER_TYPE];
    MMUBannerType type = (typeInfo != nil) ? (MMUBannerType)[typeInfo intValue] : MMUBannerUnknow;
    
    CGSize size = CGSizeZero;
    switch (type) {
        case MMUBannerNormal:
            size = kBaiduAdViewBanner320x48;
            break;
        case MMUBannerRectangle:
            size = kBaiduAdViewSquareBanner300x250;
            break;
        case MMUBannerMedium:
            size = kBaiduAdViewBanner468x60;
            break;
        case MMUBannerLarger:
            size = kBaiduAdViewBanner728x90;
            break;
        case MMUBannerCustomSize: {
            CGFloat theWidth = [[sizeInfo mmuNumberValueForKey:KEY_BANNER_WIDTH] intValue];
            CGFloat theHeight = [[sizeInfo mmuNumberValueForKey:KEY_BANNER_HEIGHT] intValue];
            size = CGSizeMake(theWidth, theHeight);
            break;
        }
        default: {
            [self adBannerFailWithError:MMUE_Banner_SizeInvalid];
            return;
        }
    }

    self.bannerView = [[BaiduMobAdView alloc] init];
    self.bannerView.AdUnitTag = self.adPlaceID;
    self.bannerView.AdType = BaiduMobAdViewTypeBanner;
    self.bannerView.frame = CGRectMake(0, 0, size.width, size.height);
    self.bannerView.delegate = self;
    [self.bannerView start];
    
    [self adBannerDidStartRequest];
}

- (void)stopBeingDelegate
{
    self.bannerView.delegate = nil;
}

- (BOOL)isSupportClickDelegate
{
    return YES;
}

#pragma mark - BaiduMobAdViewDelegate
/**
 *  应用在mounion.baidu.com上的id
 */
- (NSString *)publisherId
{
    return self.appid;
}

- (NSString *)AdPlaceId
{
    return self.adPlaceID;
}

/**
 *  广告将要被载入
 */
- (void)willDisplayAd:(BaiduMobAdView *) adview
{
    [self adBannerLoadSuccess];
}

//展现，
- (void)didAdImpressed
{
    [self adBannerDidReceiveBannerView:@{KEY_BANNEROBJECT:self.bannerView}];
}

- (void)didAdClicked
{
    [self adBannerClicked:@{KEY_BANNEROBJECT:self.bannerView}];
    [self adBannerSuspend:@{KEY_BANNEROBJECT:self.bannerView}];
}

- (void)didDismissLandingPage
{
    [self adBannerResumed:@{KEY_BANNEROBJECT:self.bannerView}];
}

/**
 *  广告载入失败
 */
- (void)failedDisplayAd:(BaiduMobFailReason)reason
{
    [self adBannerFailWithError:MMUE_PFAdFailed];
}

@end
