//
//  MMUBaiduInterstitialAdapter.h
//  MMUSDK
//
//  Created by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMUInterstitialAdNetworkAdapter.h"

@interface MMUBaiduInterstitialAdapter : MMUInterstitialAdNetworkAdapter

+ (MMUAdNetworkType)networkType;

@end
