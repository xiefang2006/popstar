//
//  MMUBaiduSplashAdapter.m
//  MMUSDK
//
//  Created by liufuyin on 16/3/8.
//  update by liufuyin on 16/8/31.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUBaiduSplashAdapter.h"
#import "BaiduMobAdSDK/BaiduMobAdSplash.h"
#import "BaiduMobAdSDK/BaiduMobAdSplashDelegate.h"
#import "MMUFramework/NSDictionary+MMUExtension.h"
#import "MMUSDKSplashNetworkRegistry.h"
#import "MMUErrorType.h"

@implementation MMUSplashsBgView

- (BOOL)isHidden
{
    return NO;
}

@end

@interface MMUBaiduSplashAdapter() <BaiduMobAdSplashDelegate>

@property (nonatomic,strong) BaiduMobAdSplash *splash;
@property (nonatomic, copy) NSString *appid;
@property (nonatomic, copy) NSString *adPlaceID;
@property (nonatomic, strong) UIView *mBgView;

@end

@implementation MMUBaiduSplashAdapter

+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeBaiduMobAd;
}

+ (void)load
{
    [[MMUSDKSplashNetworkRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    //请求开始
    [self adSplashWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:@"netset"];
    self.appid = [config mmuStringValueForKey:@"AppID"];
    self.adPlaceID = [config mmuStringValueForKey:@"AdPlaceID"];
    if ([self.appid length] == 0 || [self.adPlaceID length] == 0) {
        [self adSplashFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    self.splash = [[BaiduMobAdSplash alloc] init];
    self.splash.AdUnitTag = [self AdPlaceId];
    self.splash.delegate = self;
    self.splash.canSplashClick = [object_validate([self.ration mmuNumberValueForKey:@"ec"],@(YES)) boolValue];
    
    NSDictionary *customReqPams = [self.ration valueForKey:ADAPTER_ADDTION_INFO];
    UIWindow *window = [customReqPams objectForKey:KEY_SPLASH_SUPERWINDOW];
    UIView *splashSuperView = [customReqPams objectForKey:KEY_SPLASH_SUPERVIEW];
    UIView *defaultView = [customReqPams objectForKey:KEY_SPLASH_DEFAULTVIEW];
    
    if (splashSuperView.window) {
        if (window && window != splashSuperView.window) {
            [self adSplashFailWithError:MMUE_Splash_WindowNeed];
            return;
        }
        if (!window) {
            window = splashSuperView.window;
        }
    }
    
    if (!window || ![window isKindOfClass:[UIWindow class]]) {
        [self adSplashFailWithError:MMUE_Splash_WindowNeed];
        return;
    }
    
    //如果defaultView和splashSuperView两者都为nil
    if (!defaultView && !splashSuperView) {
        [_splash loadAndDisplayUsingKeyWindow:window];
        [self adSplashDidStartRequest];
        return;
    }
    
    //不能有父视图
    if (defaultView && [defaultView superview]) {
        [self adSplashFailWithError:MMUE_Splash_ReserveViewSuper];
        return;
    }
    
    CGFloat dWidth = 0;
    CGFloat dHeight = 0;
    //尺寸必须合法
    if (defaultView) {
        dWidth = defaultView.bounds.size.width;
        dHeight = defaultView.bounds.size.height;
        if (dWidth <= 0 || dHeight <= 0) {
            [self adSplashFailWithError:MMUE_Splash_ReserveViewSizeInvalid];
            return;
        }
    }
    
    self.mBgView = [[MMUSplashsBgView alloc] initWithFrame:window.bounds];
    self.mBgView.backgroundColor = [UIColor whiteColor];
    self.mBgView.hidden = YES;
    
    UIView *containerView = nil;
    if (splashSuperView) {
        containerView = splashSuperView;
        if (!splashSuperView.window) {
            splashSuperView.frame = splashSuperView.bounds;
            [_mBgView addSubview:splashSuperView];
        }
    } else if (defaultView) {
        defaultView.frame = CGRectMake(0,_mBgView.bounds.size.height - dHeight*(_mBgView.bounds.size.width/dWidth),_mBgView.bounds.size.width,dHeight*(_mBgView.bounds.size.width/dWidth));
        [_mBgView addSubview:defaultView];
        
        CGRect containerFrame = CGRectMake(0,0 , _mBgView.bounds.size.width, _mBgView.bounds.size.height - defaultView.bounds.size.height);
        containerView = [[UIView alloc] initWithFrame:containerFrame];
        [_mBgView addSubview:containerView];
        [window addSubview:_mBgView];
    }
    
    if (containerView) {
        [self adSplashDidStartRequest];
        [_splash loadAndDisplayUsingContainerView:containerView];
    } else {
        [self adSplashFailWithError:MMUE_Splash_ReserveViewSizeInvalid];
        return;
    }
}

- (void)stopBeingDelegate
{
    if ([_mBgView superview]) {
        [_mBgView removeFromSuperview];
    }
    
    self.splash.delegate = nil;
}

#pragma mark - BaiduMobAdSplashDelegate
/**
 *  应用在union.baidu.com上的APPID
 */
- (NSString *)publisherId
{
    return self.appid;
}

- (NSString *)AdPlaceId
{
    return self.adPlaceID;
}

/**
 *  渠道id
 */
- (NSString *)channelId
{
    return @"13b50d6f";
}

/**
 *  广告展示成功
 */
- (void)splashSuccessPresentScreen:(BaiduMobAdSplash *)splash
{
    _mBgView.hidden = NO;
    
    [self adSplashSuccessWithInfo:@{KEY_SPLASHOBJECT:self.splash}];
    if ([self.splashDelegate respondsToSelector:@selector(splashSuccess:)]) {
        [self.splashDelegate splashSuccess:splash];
    }
    
    [self adSplashDidPresentWithInfo:@{KEY_SPLASHOBJECT:self.splash}];
    if ([self.splashDelegate respondsToSelector:@selector(splashDidPresent:)]) {
        [self.splashDelegate splashDidPresent:splash];
    }
}

/**
 *  广告展示失败
 */
- (void)splashlFailPresentScreen:(BaiduMobAdSplash *)splash withError:(BaiduMobFailReason) reason
{
    if (_mBgView.superview) [_mBgView removeFromSuperview];
    [self adSplashFailWithError:MMUE_PFAdDisplayFailed];
}

/**
 *  广告展示结束
 */
- (void)splashDidDismissScreen:(BaiduMobAdSplash *)splash
{
    if (_mBgView.superview) [_mBgView removeFromSuperview];
    if ([self.splashDelegate respondsToSelector:@selector(splashDidDismiss:)]) {
        [self.splashDelegate splashDidDismiss:splash];
    }
}

/**
 *  广告被点击
 */
- (void)splashDidClicked:(BaiduMobAdSplash *)splash
{
    [self adSplashClickedWithInfo:(splash ? @{KEY_SPLASHOBJECT:splash} : nil)];
    if ([self.splashDelegate respondsToSelector:@selector(splashDidClicked:)]) {
        [self.splashDelegate splashDidClicked:splash];
    }
}

@end
