//
//  MMUInMobiNativeAdapter.m
//  MMUSDK
//
//  Created by GaoBin on 16/2/29.
//  update by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUInMobiNativeAdapter.h"
#import "InMobiSDK/IMNative.h"
#import "InMobiSDK/IMSdk.h"
#import "MMUFeedsSDKAdapterRegistry.h"
#import <MMUFramework/NSDictionary+MMUExtension.h>
#import "MMUMamaResponse.h"
#import "MMUMamaPromoter.h"

@interface MMUInMobiNativeAdapter() <IMNativeDelegate>

@property (nonatomic, strong) IMNative *native;
@property (nonatomic, strong) NSDictionary *keyMap;

@end

@implementation MMUInMobiNativeAdapter

+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeInMobi;
}

+ (void)load
{
    [[MMUFeedsSDKAdapterRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adDidWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:@"netset"];
    self.keyMap = [self dictionaryFromJsonString:[config mmuStringValueForKey:@"attr"]];
    NSString *accountID = [config mmuStringValueForKey:@"ACCOUNT_ID"];
    NSString *placementID = [config mmuStringValueForKey:@"PLACEMENT_ID"];
    if ([accountID length] == 0 || [placementID length] == 0) {
        [self adDidFailed:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    [IMSdk setLogLevel:kIMSDKLogLevelNone];
    [IMSdk initWithAccountID:accountID];
    
    self.native = [[IMNative alloc] initWithPlacementId:[placementID longLongValue]];
    self.native.delegate = self;
    [self.native load];
    
    [self adDidStartRequest];
}

//展示广告
- (void)adDidPresentWithPromoters:(NSArray<MMUMamaPromoter *> *)promoters fromResponse:(MMUMamaResponse *)response
{
    [super adDidPresentWithPromoters:promoters fromResponse:response];
    [IMNative bindNative:self.native toView:[self getRenderView]];
}

//点击广告
- (void)adDidClickWithPromoter:(MMUMamaPromoter *)promoter fromResponse:(MMUMamaResponse *)response
{
    [super adDidClickWithPromoter:promoter fromResponse:response];
    
    [self.native reportAdClickAndOpenLandingURL:nil];
}

- (void)stopBeingDelegate
{
    self.native.delegate = nil;
}

#pragma mark - IMNativeDelegate
- (void)nativeDidFinishLoading:(IMNative *)native
{
    NSString *adContent = native.adContent;
    NSData *adDatas = [adContent dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:adDatas
                                                               options:NSJSONReadingMutableContainers
                                                                 error:nil];
    if (![dictionary isKindOfClass:[NSDictionary class]] || [dictionary count] == 0) {
        [self adDidFailed:MMUE_DI_DataError];
        return;
    }
    
    NSMutableDictionary *mamaDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *mater = [NSMutableDictionary dictionary];
    
    NSDictionary *netset = [self.ration mmuDictionaryValueForKey:@"netset"];
    [mamaDictionary addEntriesFromDictionary:netset];

    NSArray *mapKeys = [[_keyMap keyEnumerator] allObjects];
    //可识别属性key集合
    NSArray *mapDefaultKeys = @[@"title",@"icon",@"description",@"screenshots",@"url"];
    //平台特有key集合
    NSMutableArray *pKeys = [NSMutableArray arrayWithCapacity:0];
    //分类
    NSMutableDictionary *clickSet = [NSMutableDictionary dictionaryWithCapacity:0];
    for (NSObject *key in mapKeys) {
        if ([key isKindOfClass:[NSString class]] && [mapDefaultKeys containsObject:key]) {
            NSString *keyStr = (NSString*)key;
            //标题
            if ([keyStr isEqualToString:@"title"]) {
                [mater setValue:object_validate([dictionary mmuStringValueForKey:_keyMap[keyStr]],@"") forKey:@"title"];
            }
            //描述
            if ([keyStr isEqualToString:@"description"]) {
                [mater setValue:object_validate([dictionary mmuStringValueForKey:_keyMap[keyStr]],@"") forKey:@"sub_title"];
            }
            //click_url
            if ([keyStr isEqualToString:@"url"]) {
                [clickSet setValue:object_validate([dictionary mmuStringValueForKey:_keyMap[keyStr]], @"") forKey:@"click_url"];
                [pKeys addObject:_keyMap[key]];
            }
            
            //icon
            if ([keyStr isEqualToString:@"icon"]) {
                NSString *iconUrl = object_validate([[dictionary mmuDictionaryValueForKey:_keyMap[keyStr]] mmuStringValueForKey:@"url"], @"");
                [mater setValue:iconUrl forKey:@"icon_url"];
                //icon还有子节点，当作平台特有信息
                [pKeys addObject:_keyMap[key]];
            }
            //img
            if ([keyStr isEqualToString:@"screenshots"]) {
                NSString *iconUrl = object_validate([[dictionary mmuDictionaryValueForKey:_keyMap[keyStr]] mmuStringValueForKey:@"url"], @"");
                [mater setValue:iconUrl forKey:@"img_url"];
                //img还有子节点，当作平台特有信息
                [pKeys addObject:_keyMap[key]];
            }
        } else {
            [pKeys addObject:_keyMap[key]];
        }
        //设置click_setting
        if ([clickSet count] > 0) {
            [mater setValue:clickSet forKey:@"click_setting"];
        }
    }
    
    //添加平台特有属性，属性key以p_开头
    for (NSObject *pkey in pKeys) {
        NSString *pKeyStr = [NSString stringWithFormat:@"p_%@",pkey];
        [mater setValue:dictionary[pkey] forKey:pKeyStr];
    }
    
    NSDictionary *addInfo = [self platformInfoWithType:MMUAdNetworkTypeInMobi name:@"InMobi" addInfo:nil];
    [mamaDictionary setObject:addInfo forKey:@"pInfo"];
    [mamaDictionary setObject:@[@{@"mater":mater}] forKey:@"creatives"];
    [self adDidSuccess:[[MMUMamaResponse alloc] initWithAttributes:mamaDictionary]];
}

- (void)native:(IMNative *)native didFailToLoadWithError:(IMRequestStatus *)error
{
    [self adDidFailed:MMUE_PFAdFailed];
}

- (void)nativeWillDismissScreen:(IMNative *)native
{
    if ([self.mPDelegate respondsToSelector:@selector(nativeWillDismissScreen:)]) {
        [self.mPDelegate performSelector:@selector(nativeWillDismissScreen:)
                              withObject:native];
    }
}

- (void)nativeWillPresentScreen:(IMNative *)native
{
    if ([self.mPDelegate respondsToSelector:@selector(nativeWillPresentScreen:)]) {
        [self.mPDelegate performSelector:@selector(nativeWillPresentScreen:)
                              withObject:native];
    }
}

- (void)nativeDidPresentScreen:(IMNative *)native
{
    if ([self.mPDelegate respondsToSelector:@selector(nativeDidPresentScreen:)])
    {
        [self.mPDelegate performSelector:@selector(nativeDidPresentScreen:)
                              withObject:native];
    }
}

- (void)nativeDidDismissScreen:(IMNative *)native
{
    if ([self.mPDelegate respondsToSelector:@selector(nativeDidDismissScreen:)]) {
        [self.mPDelegate performSelector:@selector(nativeDidDismissScreen:)
                              withObject:native];
    }
}

- (void)userWillLeaveApplicationFromNative:(IMNative*)native
{
    if ([self.mPDelegate respondsToSelector:@selector(nativeDidDismissScreen:)]) {
        [self.mPDelegate performSelector:@selector(nativeDidDismissScreen:)
                              withObject:native];
    }
}

-(void)nativeAdImpressed:(IMNative*)native {
    NSLog(@"Inmobi native ad impressed!");
}


#pragma mark utility method
- (NSDictionary *)dictionaryFromJsonString:(NSString *)string
{
    if (string == nil)
        return nil;
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingMutableContainers
                                                           error:nil];
    
    if (dict == nil || ![dict isKindOfClass:[NSDictionary class]])
        return nil;
    
    return dict;
}

@end
