//
//  MMUInMobiBannerAdapter.m
//  MMUSDK
//
//  Created by GaoBin on 16/2/29.
//  update by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUInMobiBannerAdapter.h"
#import "MMUSDKBannerNetworkRegistry.h"
#import <MMUFramework/NSDictionary+MMUExtension.h>
#import "InMobiSDK/IMSdk.h"
#import "InMobiSDK/IMBanner.h"
#import "InMobiSDK/IMBannerDelegate.h"
#import "InMobiSDK/IMCommonConstants.h"
#import "MMUErrorType.h"

@interface MMUInMobiBannerAdapter() <IMBannerDelegate>

@property (nonatomic, strong) IMBanner *bannerView;

@end

@implementation MMUInMobiBannerAdapter
+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeInMobi;
}

+ (void)load
{
    [[MMUSDKBannerNetworkRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adBannerWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:KEY_NETSET];
    NSString *accountID = [config mmuStringValueForKey:@"ACCOUNT_ID"];
    NSString *placementID = [config mmuStringValueForKey:@"PLACEMENT_ID"];
    if ([accountID length] == 0 || [placementID length] == 0) {
        [self adBannerFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    NSString *sizeInfoStr = [[self.ration mmuDictionaryValueForKey:KEY_AZSET] mmuStringValueForKey:KEY_BANNER_SIZE];
    NSDictionary *sizeInfo = [self typeInfoWithDescription:sizeInfoStr];
    NSNumber *typeInfo = [sizeInfo mmuNumberValueForKey:KEY_BANNER_TYPE];
    MMUBannerType type = (typeInfo != nil) ? (MMUBannerType)[typeInfo intValue] : MMUBannerUnknow;

    CGRect rect = CGRectZero;
    switch (type) {
        case MMUBannerNormal:
            rect = CGRectMake(0, 0, 320, 48);
            break;
        case MMUBannerRectangle:
            rect = CGRectMake(0, 0, 300, 250);
            break;
        case MMUBannerMedium:
            rect = CGRectMake(0, 0, 468, 60);
            break;
        case MMUBannerLarger:
            rect = CGRectMake(0, 0, 728, 90);
            break;
        case MMUBannerCustomSize: {
            CGFloat theWidth = [[sizeInfo mmuNumberValueForKey:KEY_BANNER_WIDTH] intValue];
            CGFloat theHeight = [[sizeInfo mmuNumberValueForKey:KEY_BANNER_HEIGHT] intValue];
            rect = CGRectMake(0, 0, theWidth, theHeight);
            break;
        }
        default:
            [self adBannerFailWithError:MMUE_Banner_SizeInvalid];
            return;
            break;
    }
    
    [IMSdk setLogLevel:kIMSDKLogLevelNone];
    [IMSdk initWithAccountID:accountID];
    
    self.bannerView = [[IMBanner alloc] initWithFrame:rect placementId:[placementID longLongValue]];
    [self.bannerView shouldAutoRefresh:NO];
    self.bannerView.delegate = self;
    [self.bannerView load];
    [self adBannerDidStartRequest];
}

- (void)stopBeingDelegate
{
    self.bannerView.delegate = nil;
}

- (BOOL)isSupportClickDelegate
{
    return YES;
}

#pragma mark - IMBannerDelegate
/**
 * Callback sent when an ad request loaded an ad. This is a good opportunity
 * to add this view to the hierarchy if it has not yet been added.
 * @param banner The IMBanner instance which finished loading the ad request.
 */
- (void)bannerDidFinishLoading:(IMBanner*)banner
{
    NSLog(@"inmobi banner已经显示");
    [self adBannerLoadSuccess];
    [self adBannerDidReceiveBannerView:@{KEY_BANNEROBJECT:self.bannerView}];
}

/**
 * Callback sent when an ad request failed. Normally this is because no network
 * connection was available or no ads were available (i.e. no fill).
 * @param banner The IMBanner instance that failed to load the ad request.
 * @param error The error that occurred during loading.
 */
- (void)banner:(IMBanner *)banner didFailToLoadWithError:(IMRequestStatus *)error
{
    NSLog(@"inmobi banner无法显示");
    [self adBannerFailWithError:MMUE_PFAdFailed];
}
/**
 * Called when the banner is tapped or interacted with by the user
 * Optional data is available to publishers to act on when using
 * monetization platform to render promotional ads.
 * @param banner The IMBanner instance that presents the screen.
 * @param dictionary The NSDictionary containing the parameters as passed by the creative
 */
- (void)banner:(IMBanner *)banner didInteractWithParams:(NSDictionary *)params
{
    NSLog(@"inmobi banner被点击");
    [self adBannerClicked:@{}];
}

/**
 * Callback sent just before when the banner is presenting a full screen view
 * to the user. Use this opportunity to stop animations and save the state of
 * your application in case the user leaves while the full screen view is on
 * screen (e.g. to visit the App Store from a link on the full screen view).
 * @param banner The IMBanner instance that presents the screen.
 */
- (void)bannerWillPresentScreen:(IMBanner*)banner {

}

- (void)bannerDidPresentScreen:(IMBanner*)banner {

}

/**
 * Callback sent just before dismissing the full screen view.
 * @param banner The IMBanner instance that dismisses the screen.
 */
- (void)bannerWillDismissScreen:(IMBanner *)banner { }

/**
 * Callback sent just after dismissing the full screen view.
 * Use this opportunity to restart anything you may have stopped as part of
 * bannerWillPresentScreen: callback.
 * @param banner The IMBanner instance that dismissed the screen.
 */
- (void)bannerDidDismissScreen:(IMBanner*)banner { }

/**
 * Callback sent just before the application goes into the background because
 * the user clicked on a link in the ad that will launch another application
 * (such as the App Store). The normal UIApplicationDelegate methods like
 * applicationDidEnterBackground: will immediately be called after this.
 * @param banner The IMBanner instance that is launching another application.
 */
- (void)userWillLeaveApplicationFromBanner:(IMBanner *)banner { }

- (void)banner:(IMBanner *)banner rewardActionCompletedWithRewards:(NSDictionary *)rewards
{
    if ([self.delegate respondsToSelector:@selector(banner:rewardActionCompletedWithRewards:)]) {
        [self.delegate performSelector:@selector(banner:rewardActionCompletedWithRewards:)
                            withObject:banner
                            withObject:rewards];
    }
}

@end
