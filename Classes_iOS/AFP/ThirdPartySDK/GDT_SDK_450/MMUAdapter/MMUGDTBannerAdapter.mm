//
//  MMUGDTBannerAdapter.m
//  MMUSDK
//
//  Created by GaoBin on 16/2/29.
//  update by liufuyin on 16/3/8..
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "GDTMobBannerView.h"
#import "MMUSDKBannerNetworkRegistry.h"
#import <MMUFramework/NSDictionary+MMUExtension.h>
#import "MMUGDTBannerAdapter.h"
#import "MMUErrorType.h"

#include "GameManager.h"


@interface MMUGDTBannerAdapter() <GDTMobBannerViewDelegate>

@property (nonatomic, strong) GDTMobBannerView *bannerView;

@end

@implementation MMUGDTBannerAdapter
+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeGDT;
}

+ (void)load
{
    [[MMUSDKBannerNetworkRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adBannerWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:KEY_NETSET];
    NSString *appid = [config mmuStringValueForKey:@"appid"];
    NSString *pid = [config mmuStringValueForKey:@"pid"];
    if ([appid length] == 0 || [pid length] == 0) {
        [self adBannerFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    NSString *sizeInfoStr = [[self.ration mmuDictionaryValueForKey:KEY_AZSET] mmuStringValueForKey:KEY_BANNER_SIZE];
    NSDictionary *sizeInfo = [self typeInfoWithDescription:sizeInfoStr];
    NSNumber *typeInfo = [sizeInfo mmuNumberValueForKey:KEY_BANNER_TYPE];
    MMUBannerType type = (typeInfo != nil) ? (MMUBannerType)[typeInfo intValue] : MMUBannerUnknow;
    
    
    //TODO: hack for GDT no ipad banner, should ask server if we want smaller size
    if (type == MMUBannerLarger) {
        if (GameManager::sharedGameManager()->getServerBoolean("smallPadBannerForGDT") == true) {
            type = MMUBannerNormal;
        }
    }
    
    CGSize size = CGSizeZero;
    switch (type) {
        case MMUBannerNormal:
            size =  GDTMOB_AD_SUGGEST_SIZE_320x50;
            break;
        case MMUBannerLarger:
            size = GDTMOB_AD_SUGGEST_SIZE_728x90;
            break;
        case MMUBannerMedium:
            size = GDTMOB_AD_SUGGEST_SIZE_468x60;
            break;
        case MMUBannerCustomSize: {
            CGFloat theWidth = [[sizeInfo mmuNumberValueForKey:KEY_BANNER_WIDTH] intValue];
            CGFloat theHeight = [[sizeInfo mmuNumberValueForKey:KEY_BANNER_HEIGHT] intValue];
            size = CGSizeMake(theWidth, theHeight);
            break;
        }
        default: {
            [self adBannerFailWithError:MMUE_Banner_SizeInvalid];
            return;
        }
    }
    
    self.bannerView = [[GDTMobBannerView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)
                                                       appkey:appid
                                                  placementId:pid];
    self.bannerView.delegate = self;
    self.bannerView.currentViewController = [self.delegate bannerViewControllerForPresentingModalView]; //设置当前的ViewController
    self.bannerView.interval = 0; //【可选】设置刷新频率;默认30秒,0s 不刷新
    self.bannerView.showCloseBtn = NO;
    self.bannerView.isAnimationOn = NO;
    [self.bannerView loadAdAndShow];
    [self adBannerDidStartRequest];
}

- (BOOL)isSupportClickDelegate
{
    return YES;
}

- (void)stopBeingDelegate
{
    self.bannerView.delegate = nil;
}

#pragma mark - GDTMobBannerViewDelegate
// 请求广告条数据成功后调用
//
// 详解:当接收服务器返回的广告数据成功后调用该函数
- (void)bannerViewDidReceived
{
    NSLog(@"广点通横幅接受返回数据成功");
    [self adBannerLoadSuccess];
    [self adBannerDidReceiveBannerView:@{KEY_BANNEROBJECT:self.bannerView}];
}

// 请求广告条数据失败后调用
//
// 详解:当接收服务器返回的广告数据失败后调用该函数
- (void)bannerViewFailToReceived:(NSError *)error
{
    NSLog(@"广点通横幅接受返回数据失败");

    //广点通在二跳页展示失败的时候也会调用这个回调，，而SDK是不需要处理二跳页失败的情况的
    if ([self getAdapterState] >= ST_MMUAapterReady)
        return;
    
    [self adBannerFailWithError:MMUE_PFAdFailed];
}

/**
 *  banner条点击回调
 */
- (void)bannerViewClicked
{
    [self adBannerClicked:@{}];
}

@end
