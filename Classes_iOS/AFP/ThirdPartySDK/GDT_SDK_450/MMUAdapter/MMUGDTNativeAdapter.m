//
//  MMUGDTNativeAdapter.m
//  MMUSDK
//
//  Created by GaoBin on 16/2/29.
//  update by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUGDTNativeAdapter.h"
#import "MMUFeedsSDKAdapterRegistry.h"
#import <MMUFramework/NSDictionary+MMUExtension.h>
#import "MMUMamaPromoter.h"
#import "MMUMamaResponse.h"
#import "MMUErrorType.h"
#import "GDTNativeAd.h"

@interface MMUGDTNativeAdapter() <GDTNativeAdDelegate>

@property (nonatomic, strong) GDTNativeAd *native;
@property (nonatomic, strong) NSMutableDictionary *mNativeAdDatas;

@end

@implementation MMUGDTNativeAdapter

+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeGDT;
}

+ (void)load
{
    [[MMUFeedsSDKAdapterRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adDidWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:@"netset"];
    NSString *appid = [config mmuStringValueForKey:@"appid"];
    NSString *pid = [config mmuStringValueForKey:@"pid"];
    if ([appid length] == 0 || [pid length] == 0) {
        [self adDidFailed:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    NSDictionary *customReqPams = [self.ration valueForKey:ADAPTER_ADDTION_INFO];
    int adcount = [object_validate([customReqPams mmuNumberValueForKey:@"adcnt"],@(1)) intValue];
    
    self.native = [[GDTNativeAd alloc] initWithAppkey:appid placementId:pid];
    self.native.controller = [self.mPDelegate viewControllerForPresentingModalView];
    self.native.delegate = self;
    [self.native loadAd:adcount];
    [self adDidStartRequest];
}

//展示广告
- (void)adDidPresentWithPromoters:(NSArray<MMUMamaPromoter *> *)promoters fromResponse:(MMUMamaResponse *)response
{
    for (MMUMamaPromoter *promoter in promoters) {
        if ([promoter.promoterId length] == 0)
            continue;
        
        [self.native attachAd:_mNativeAdDatas[promoter.promoterId] toView:[self getRenderView]];
    }
    [super adDidPresentWithPromoters:promoters fromResponse:response];
}

//点击广告
- (void)adDidClickWithPromoter:(MMUMamaPromoter *)promoter fromResponse:(MMUMamaResponse *)response
{
    if (!promoter || !promoter.promoterId) return;
    
    [self.native clickAd:_mNativeAdDatas[promoter.promoterId]];
    
    [super adDidClickWithPromoter:promoter fromResponse:response];
}

- (void)stopBeingDelegate
{
    self.native.delegate = nil;
}

#pragma mark - GDTNativeAdDelegate
/**
 *  原生广告加载广告数据成功回调，返回为GDTNativeAdData对象的数组
 */
- (void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray
{
    if (nativeAdDataArray == nil || [nativeAdDataArray count] == 0) {
        [self adDidFailed:MMUE_DI_DataError];
        return;
    }
    
    NSMutableDictionary *mamaDictionary = [NSMutableDictionary dictionary];
    NSMutableArray *matersArray = [NSMutableArray array];
    self.mNativeAdDatas = [NSMutableDictionary dictionary];
    
    NSDictionary *netset = [self.ration mmuDictionaryValueForKey:@"netset"];
    [mamaDictionary addEntriesFromDictionary:netset];

    for (GDTNativeAdData *adData in nativeAdDataArray) {
        NSDictionary *gdtDictionary = adData.properties;
        NSMutableDictionary *mater = [NSMutableDictionary dictionary];
        
        [mater setValue:object_validate([gdtDictionary mmuStringValueForKey:GDTNativeAdDataKeyImgUrl], @"") forKey:@"img_url"];
        [mater setValue:object_validate([gdtDictionary mmuStringValueForKey:GDTNativeAdDataKeyIconUrl], @"") forKey:@"icon_url"];
        [mater setValue:object_validate([gdtDictionary mmuStringValueForKey:GDTNativeAdDataKeyTitle], @"") forKey:@"title"];
        [mater setValue:object_validate([gdtDictionary mmuStringValueForKey:GDTNativeAdDataKeyDesc], @"") forKey:@"sub_title"];
        [mater setValue:object_validate([gdtDictionary mmuStringValueForKey:GDTNativeAdDataKeyAppPrice], @"") forKey:@"price"];
        [mater setValue:object_validate([gdtDictionary mmuStringValueForKey:GDTNativeAdDataKeyAppPrice], @"") forKey:@"p_appRating"];
        
        NSDictionary *creativeInfo = @{@"mater":mater, @"cid":[NSString stringWithFormat:@"%ld", (long)index]};
        [matersArray addObject:creativeInfo];
        //对adData做记录
        [self.mNativeAdDatas setValue:adData forKey:[NSString stringWithFormat:@"%ld",(long)index]];
    }
    
    if ([matersArray count] > 0) {
        NSDictionary *addInfo = [self platformInfoWithType:MMUAdNetworkTypeGDT name:@"广点通" addInfo:nil];
        [mamaDictionary setObject:addInfo forKey:@"pInfo"];
        [mamaDictionary setObject:matersArray forKey:@"creatives"];
        [self adDidSuccess:[[MMUMamaResponse alloc] initWithAttributes:mamaDictionary]];
    } else {
        [self adDidFailed:MMUE_DI_DataError];
    }
}

/**
 *  原生广告加载广告数据失败回调
 */
- (void)nativeAdFailToLoad:(NSError *)error
{
    [self adDidFailed:MMUE_PFAdFailed];
}

@end
