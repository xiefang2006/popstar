//
//  MMUGDTSplashAdapter.m
//  MMUSDK
//
//  Created by GaoBin on 16/2/29.
//  update by liufuyin on 16/8/31.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUGDTSplashAdapter.h"
#import <MMUFramework/NSDictionary+MMUExtension.h>
#import "MMUSDKSplashNetworkRegistry.h"
#import "GDTSplashAd.h"

@interface MMUGDTSplashAdapter() <GDTSplashAdDelegate>

@property (nonatomic, strong) GDTSplashAd *splash;

@end

@implementation MMUGDTSplashAdapter

+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeGDT;
}

+ (void)load
{
    [[MMUSDKSplashNetworkRegistry sharedRegistry] registerClass:self];
}

//这里两个步骤分开写是因为需要adSplashStartRequestWithInfo来转换状态，要不然不会调用startTimeoutTimerWithInterval
- (void)getAd
{
    [self adSplashWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:@"netset"];
    NSString *appid = [config mmuStringValueForKey:@"appid"];
    NSString *pid = [config mmuStringValueForKey:@"pid"];
    if ([appid length] == 0 || [pid length] == 0) {
        [self adSplashFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    self.splash = [[GDTSplashAd alloc] initWithAppkey:appid placementId:pid];
    self.splash.delegate = self;
    self.splash.backgroundColor = [self launchImgColor];
    
    [self adSplashDidStartRequest];
}

//适配GDT的特殊逻辑
- (UIColor*)launchImgColor
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    if ([self.splashDelegate respondsToSelector:@selector(splashBgImageName)]) {
        NSString *imgName = [self.splashDelegate performSelector:@selector(splashBgImageName)];
        UIImage *bgImg = nil;
        if (imgName && (bgImg = [UIImage imageNamed:imgName])) {
            return  [UIColor colorWithPatternImage:bgImg];
        }
    }
#pragma clang diagnostic pop
    return [UIColor clearColor];
}

//这里用来代替getAd，因为广点通的开屏需要一个超时时间，他没有自己的取消方法
- (void)startTimeoutTimerWithInterval:(NSTimeInterval)time
{
    NSDictionary *customReqPams = [self.ration valueForKey:ADAPTER_ADDTION_INFO];
    UIWindow *window = [customReqPams objectForKey:KEY_SPLASH_SUPERWINDOW];
    if (!self.splash || !window || ![window isKindOfClass:[UIWindow class]]) {
        [self adSplashFailWithError:MMUE_Splash_WindowNeed];
        return;
    }
    
    NSTimeInterval timeDelay = self.rationEndTime - [[NSDate date] timeIntervalSince1970];
    timeDelay = timeDelay > 0 ? timeDelay : 0;
    self.splash.fetchDelay = (int)((timeDelay > time) ? time :timeDelay);
    
    UIView *defaultView = [customReqPams objectForKey:KEY_SPLASH_DEFAULTVIEW];
    if (defaultView)  {
        [self.splash loadAdAndShowInWindow:window withBottomView:defaultView];
    } else {
        [self.splash loadAdAndShowInWindow:window];
    }
}

- (void)stopBeingDelegate
{
    self.splash.delegate = nil;
}

#pragma mark - GDTSplashAdDelegate

/**
 *  开屏广告成功展示
 */
- (void)splashAdSuccessPresentScreen:(GDTSplashAd *)splashAd
{
    [self adSplashSuccessWithInfo:@{}];
    if ([self.splashDelegate respondsToSelector:@selector(splashSuccess:)]) {
        [self.splashDelegate splashSuccess:splashAd];
    }
    
    [self adSplashDidPresentWithInfo:@{}];
    if ([self.splashDelegate respondsToSelector:@selector(splashDidPresent:)]) {
        [self.splashDelegate splashDidPresent:splashAd];
    }
}

/**
 *  开屏广告展示失败
 */
- (void)splashAdFailToPresent:(GDTSplashAd *)splashAd withError:(NSError *)error
{
    [self adSplashFailWithError:MMUE_PFAdDisplayFailed];
    NSLog(@"%s, 广点通返回的失败原因：%@", __PRETTY_FUNCTION__, error);
}

/**
 *  应用进入后台时回调
 *  详解: 当点击下载应用时会调用系统程序打开，应用切换到后台
 */
- (void)splashAdApplicationWillEnterBackground:(GDTSplashAd *)splashAd { }

/**
 *  开屏广告点击回调
 */
- (void)splashAdClicked:(GDTSplashAd *)splashAd
{
    [self adSplashClickedWithInfo:@{}];
    if ([self.splashDelegate respondsToSelector:@selector(splashDidClicked:)]) {
        [self.splashDelegate splashDidClicked:splashAd];
    }
}

/**
 *  开屏广告关闭回调
 */
- (void)splashAdClosed:(GDTSplashAd *)splashAd
{
    if ([self.splashDelegate respondsToSelector:@selector(splashDidDismiss:)]) {
        [self.splashDelegate splashDidDismiss:splashAd];
    }
}

/**
 *  开屏广告点击以后即将弹出全屏广告页
 */
- (void)splashAdWillPresentFullScreenModal:(GDTSplashAd *)splashAd
{
    if ([self.browserDelegate respondsToSelector:@selector(browserWillLoad)]) {
        [self.browserDelegate browserWillLoad];
    }
}
/**
 *  点击以后全屏广告页已经被关闭
 */
- (void)splashAdDidDismissFullScreenModal:(GDTSplashAd *)splashAd
{
    if ([self.browserDelegate respondsToSelector:@selector(browserWillDismiss)]) {
        [self.browserDelegate browserWillDismiss];
    }
}

@end
