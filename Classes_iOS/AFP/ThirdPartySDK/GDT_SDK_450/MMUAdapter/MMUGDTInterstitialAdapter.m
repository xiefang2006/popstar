//
//  MMUGDTInterstitialAdapter.m
//  MMUSDK
//
//  Created by GaoBin on 16/2/29.
//  update by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUGDTInterstitialAdapter.h"
#import "MMUSDKInterstitialNetworkRegistry.h"
#import <MMUFramework/NSDictionary+MMUExtension.h>
#import "GDTMobInterstitial.h"

@interface MMUGDTInterstitialAdapter() <GDTMobInterstitialDelegate>

@property (nonatomic, strong) GDTMobInterstitial *interstitial;

@end

@implementation MMUGDTInterstitialAdapter

+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeGDT;
}

+ (void)load
{
    [[MMUSDKInterstitialNetworkRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adInterstitialWillStartRequest];
    
    NSDictionary *config = [self.ration mmuDictionaryValueForKey:@"netset"];
    NSString *appid = [config mmuStringValueForKey:@"appid"];
    NSString *pid = [config mmuStringValueForKey:@"pid"];
    if ([appid length] == 0 || [pid length] == 0) {
        [self adInterstitialFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    self.interstitial = [[GDTMobInterstitial alloc] initWithAppkey:appid placementId:pid];
    self.interstitial.delegate = self; //设置委托
    //预加载广告
    [self.interstitial loadAd];
    [self adInterstitialDidStartRequest];
}

- (void)stopBeingDelegate
{
    self.interstitial.delegate = nil;
}

- (void)presentInterstitial
{
    if (!self.interstitial.isReady) {
        NSLog(@"%s ad is not ready",__FUNCTION__);
        return;
    }
    
    UIViewController *vc = [self.delegate viewControllerForPresentingInterstitialModalView];
    [self.interstitial presentFromRootViewController:vc];
}

#pragma mark - GDTMobInterstitialDelegate
/**
 *  广告预加载成功回调
 *  详解:当接收服务器返回的广告数据成功后调用该函数
 */
- (void)interstitialSuccessToLoadAd:(GDTMobInterstitial *)interstitial
{
    [self adInterstitiaDidReceiveWithInfo:@{}];
}

/**
 *  广告预加载失败回调
 *  详解:当接收服务器返回的广告数据失败后调用该函数
 */
- (void)interstitialFailToLoadAd:(GDTMobInterstitial *)interstitial error:(NSError *)error
{
    [self adInterstitialFailWithError:MMUE_PFAdFailed];
}

/**
 *  插屏广告将要展示回调
 *  详解: 插屏广告即将展示回调该函数
 */
- (void)interstitialWillPresentScreen:(GDTMobInterstitial *)interstitial
{
    if ([self getAdapterState] >= ST_MMUAapterStop) {
        return;
    }
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    if ([self.delegate respondsToSelector:@selector(MMUEXT_InterstitialAdWillPresent)]) {
        [self.delegate performSelector:@selector(MMUEXT_InterstitialAdWillPresent)];
    }
    
#pragma clang diagnostic pop
}

/**
 *  插屏广告视图展示成功回调
 *  详解: 插屏广告展示成功回调该函数
 */
- (void)interstitialDidPresentScreen:(GDTMobInterstitial *)interstitial
{
    NSLog(@"广点通插屏已经显示在屏幕");
    [self adInterstitiaDidPresentWithInfo:@{}];
}

/**
 *  插屏广告展示结束回调
 *  详解: 插屏广告展示结束回调该函数
 */
- (void)interstitialDidDismissScreen:(GDTMobInterstitial *)interstitial
{
    NSLog(@"广点通插屏已经消失在屏幕");
    if ([self getAdapterState] >= ST_MMUAapterStop) {
        return;
    }
    
    [self adInterstitialDismissScreenWithInfo:@{KEY_INTERST_OBJ:interstitial}];
}

/**
 *  应用进入后台时回调
 *  详解: 当点击下载应用时会调用系统程序打开，应用切换到后台
 */
- (void)interstitialApplicationWillEnterBackground:(GDTMobInterstitial *)interstitial
{
    NSLog(@"广点通插屏将要进入后台");
}

/**
 *  插屏广告曝光回调
 */
- (void)interstitialWillExposure:(GDTMobInterstitial *)interstitial
{
    NSLog(@"广点通插屏将要被显示");
}

/**
 *  插屏广告点击回调
 */
- (void)interstitialClicked:(GDTMobInterstitial *)interstitial
{
    NSLog(@"广点通插屏被点击");
    [self adInterstitialClickedWithInfo:@{}];
}

@end
