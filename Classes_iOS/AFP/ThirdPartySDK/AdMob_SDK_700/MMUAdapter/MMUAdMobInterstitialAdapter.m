//
//  MMUAdapterGoogleAdMobFullAds.m
//  MMUSDK
//
//  Created by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUAdMobInterstitialAdapter.h"
#import "MMUSDKInterstitialNetworkRegistry.h"
#import <GoogleMobileAds/GADInterstitial.h>
#import <GoogleMobileAds/GADInterstitialDelegate.h>

@interface MMUAdMobInterstitialAdapter() <GADInterstitialDelegate>

@property (nonatomic, strong) GADInterstitial *interstitial;

@end

@implementation MMUAdMobInterstitialAdapter

+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeAdMob;
}

+ (void)load
{
    [[MMUSDKInterstitialNetworkRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adInterstitialWillStartRequest];
    
    NSString *adPlaceID = [[self.ration mmuDictionaryValueForKey:@"netset"] mmuStringValueForKey:@"ID"];
    if ([adPlaceID length] == 0) {
        [self adInterstitialFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:adPlaceID];
    self.interstitial.delegate = self;
    [self.interstitial loadRequest:[GADRequest request]];
    [self adInterstitialDidStartRequest];
}

- (void)stopBeingDelegate
{
    self.interstitial.delegate = nil;
}

- (void)presentInterstitial
{
    if (!self.interstitial.isReady) {
        NSLog(@"%s ad is not ready",__FUNCTION__);
        return;
    }
    [self.interstitial presentFromRootViewController:[self.delegate viewControllerForPresentingInterstitialModalView]];
}

#pragma mark - GADInterstitialDelegate
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    NSLog(@"admob插屏接收成功");
    [self adInterstitiaDidReceiveWithInfo:@{KEY_INTERST_OBJ:ad}];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"admob插屏接收失败");
    [self adInterstitialFailWithError:MMUE_PFAdFailed];
}

- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad
{
    [self adInterstitialClickedWithInfo:@{KEY_INTERSTITIAL_ADAPTER:self}];
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    [self adInterstitialDismissScreenWithInfo:@{KEY_INTERST_OBJ:ad}];
}

- (void)interstitialWillPresentScreen:(GADInterstitial *)ad
{
    NSLog(@"admob插屏展示成功");
    NSDictionary *info = @{KEY_INTERST_OBJ:ad};
    [self adInterstitiaWillPresentWithInfo:info];
    [self adInterstitiaDidPresentWithInfo:info];
}

@end
