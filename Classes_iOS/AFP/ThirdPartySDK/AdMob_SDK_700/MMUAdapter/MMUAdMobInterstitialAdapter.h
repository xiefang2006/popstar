//
//  MMUAdMobInterstitialAdapter.h
//  MMUSDK
//
//  Created by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//


#import "MMUInterstitialAdNetworkAdapter.h"

@interface MMUAdMobInterstitialAdapter : MMUInterstitialAdNetworkAdapter

+ (MMUAdNetworkType)networkType;

@end
