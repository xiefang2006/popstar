//
//  MMUAdMobBannerAdapter.m
//  MMUSDK
//
//  Created by liufuyin on 16/3/8.
//  Copyright © 2016年 alimama. All rights reserved.
//

#import "MMUAdMobBannerAdapter.h"
#import <GoogleMobileAds/GADBannerView.h>
#import "MMUSDKBannerNetworkRegistry.h"
#import <GoogleMobileAds/GADBannerViewDelegate.h>
#import "MMUBanners.h"
#import <MMUFramework/NSDictionary+MMUExtension.h>
#import "MMUErrorType.h"

@interface MMUAdMobBannerAdapter() <GADBannerViewDelegate>

@property (nonatomic, strong) GADBannerView *bannerView;

@end

@implementation MMUAdMobBannerAdapter

+ (MMUAdNetworkType)networkType
{
    return MMUAdNetworkTypeAdMob;
}

+ (void)load
{
    [[MMUSDKBannerNetworkRegistry sharedRegistry] registerClass:self];
}

- (void)getAd
{
    [self adBannerWillStartRequest];
    
    NSString *adPlaceID = [[self.ration mmuDictionaryValueForKey:@"netset"] mmuStringValueForKey:@"ID"];
    if ([adPlaceID length] == 0) {
        [self adBannerFailWithError:MMUE_AdPlaceIDEmpty];
        return;
    }
    
    GADRequest *request = [GADRequest request];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    NSString *gender = [self.delegate respondsToSelector:@selector(gender)]?(NSString*)[self.delegate performSelector:@selector(gender)]:nil;
    if ([gender isEqualToString:@"m"]) {
        request.gender = kGADGenderMale;
    } else if([gender isEqualToString:@"f"]) {
        request.gender = kGADGenderFemale;
    } else {
        request.gender = kGADGenderUnknown;
    }
    
    NSDate *date = [self.delegate respondsToSelector:@selector(dateOfBirth)]?(NSDate*)[self.delegate performSelector:@selector(dateOfBirth)]:nil;
    if (date) request.birthday = date;
    
    NSArray *keywords = [self.delegate respondsToSelector:@selector(keywords)]?(NSArray*)[self.delegate performSelector:@selector(keywords)]:nil;
    if (keywords) request.keywords = [NSMutableArray arrayWithArray:(NSArray *)keywords];
    
#pragma clang diagnostic pop
    
    BOOL testMode = [[self.ration objectForKey:@"testmodel"] intValue];
    if (testMode) {
        request.testDevices = [NSArray arrayWithObjects:@"Simulator",nil];
    }
    
    NSString *sizeInfoStr = [[self.ration mmuDictionaryValueForKey:KEY_AZSET] mmuStringValueForKey:KEY_BANNER_SIZE];
    NSDictionary *sizeInfo = [self typeInfoWithDescription:sizeInfoStr];
    NSNumber *typeInfo = [sizeInfo mmuNumberValueForKey:KEY_BANNER_TYPE];
    MMUBannerType theType = (typeInfo != nil)?(MMUBannerType)[typeInfo intValue]:MMUBannerUnknow;
    
    GADAdSize size = kGADAdSizeBanner;
    switch (theType) {
        case MMUBannerNormal:
            size = kGADAdSizeBanner;
            break;
        case MMUBannerRectangle:
            size = kGADAdSizeMediumRectangle;
            break;
        case MMUBannerMedium:
            size = kGADAdSizeFullBanner;
            break;
        case MMUBannerLarger:
            size = kGADAdSizeLeaderboard;
            break;
        default: {
            [self adBannerFailWithError:MMUE_Banner_SizeInvalid];
            return;
        }
            break;
    }
    
    self.bannerView = [[GADBannerView alloc] initWithAdSize:size];
    self.bannerView.adUnitID = adPlaceID;
    self.bannerView.delegate = self;
    self.bannerView.rootViewController = [self.delegate bannerViewControllerForPresentingModalView];
    [self.bannerView loadRequest:request];
    [self adBannerDidStartRequest];
}

- (void)stopBeingDelegate
{
    self.bannerView.delegate = nil;
    self.bannerView.rootViewController = nil;
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)adView
{
    NSLog(@"admob banner已经显示");
    [self adBannerLoadSuccess];
    [self adBannerDidReceiveBannerView:@{KEY_BANNEROBJECT:adView}];
}

- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"admob banner无法显示");
    [self adBannerFailWithError:MMUE_PFAdFailed];
}

- (void)adViewWillPresentScreen:(GADBannerView *)adView
{
    if ([self getAdapterState] >= ST_MMUAapterStop) {
        return;
    }
    
    if ([self.browserDelegate respondsToSelector:@selector(browserWillLoad)]) {
        [self.browserDelegate browserWillLoad];
    }
}

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView
{
    if ([self getAdapterState] >= ST_MMUAapterStop) {
        return;
    }
    if ([self.browserDelegate respondsToSelector:@selector(browserWillDismiss)]) {
        [self.browserDelegate browserWillDismiss];
    }
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"

- (void)adViewDidDismissScreen:(GADBannerView *)adView
{
    if ([self getAdapterState] >= ST_MMUAapterStop) {
        return;
    }
    
    if ([self.browserDelegate respondsToSelector:@selector(MMUEXT_BrowserDidDismiss)]) {
        [self.browserDelegate performSelector:@selector(MMUEXT_BrowserDidDismiss)];
    }
}

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView
{
    if ([self getAdapterState] >= ST_MMUAapterStop) {
        return;
    }
    
    if ([self.browserDelegate respondsToSelector:@selector(MMUEXT_AdViewWillLeaveApplication)]) {
        [self.browserDelegate performSelector:@selector(MMUEXT_AdViewWillLeaveApplication)];
    }
}

#pragma clang diagnostic pop
@end
