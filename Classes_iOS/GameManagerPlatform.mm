//
//  GameManager.m
//  Game
//
//  Created by Xie Fang on 9/14/12.
//
//
#include "Popstar.h"


#include "cocos2d.h"
#import "GameManagerPlatform.h"
#import "EAGLView.h"
//#import "AdsMOGOContent.h"


#include "GameManager.h"
#include "GameScene.h"
#include "StoreScene.h"
#include "BonusScene.h"

//#include <Chartboost/Chartboost.h>

//#import "WXApi.h"


using namespace cocos2d;
static GameManagerPlatform* _sharedGameManagerPlatform = nil;         

@implementation GameManagerPlatform
@synthesize banner;
@synthesize interstitial;

@synthesize products;
//@synthesize videoWall;

@synthesize myMatch;



+(GameManagerPlatform*)sharedGameManagerPlatform {
    @synchronized([GameManagerPlatform class])                             
    {
        if(!_sharedGameManagerPlatform)  {                                  
            [[self alloc] init];
        }
        return _sharedGameManagerPlatform;                                
    }
    return nil;
}

+(id)alloc
{
    @synchronized ([GameManagerPlatform class])                           
    {
        NSAssert(_sharedGameManagerPlatform == nil,
                 @"Attempted to allocated a second instance of the Ad Manager singleton");                                          
        _sharedGameManagerPlatform = [super alloc];
        return _sharedGameManagerPlatform;                                
    }
    return nil;
}


-(void)dealloc {

    /*
    if (videoWall != nil) {
        videoWall.delegate = nil;
        [videoWall release];
        self.videoWall = nil;
    }

     */
    
    [super dealloc];
}



#pragma mark -
#pragma mark Advertisement

-(void)showInterstitial {
    
    
    if (GameManager::sharedGameManager()->getServerBoolean("noInterstitialInGame") == true) {
        NSLog(@"Simplygame interstitial disabled");
        return;
    }
    
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:mainViewController];
        
        // prepare next in delegate method dismiss and exit
       
    }
    else {
        NSLog(@"Admob interstitial not ready");
        
        // TODO: this might be wrong, double loading
        [self cacheInterstitial];
    }
    

}

-(void)cacheInterstitial {
    
    if (GameManager::sharedGameManager()->getServerBoolean("noInterstitialInGame") == true) {
        NSLog(@"Simplygame interstitial disabled");
        return;
    }
    
    const char* admobId = GameManager::sharedGameManager()->getServerString("kAdmobInterId");
    if (admobId == NULL) {
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            
            // iphone default id for admob inter
            self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-2115356302075581/7749285015"];
        }
        else {
            
            // ipad default id for admob inter
            self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-2115356302075581/7358294972"];
        }
    }
    else {
        // server overwritten id for admob inter
        self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:[NSString stringWithUTF8String:admobId]];
    }

    
    self.interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    [self.interstitial loadRequest:request];
    

    //[interstitial interstitialLoad];
    
    
    
    //return;
    
    /*
    if (interstitial != nil) {
        return;
    }
    
    [[AdMoGoLogCenter shareInstance] setLogLeveFlag:AdMoGoLogDebug];
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        
        self.interstitial = [[AdMoGoInterstitialManager shareInstance] adMogoInterstitialByAppKey:kAdsmogoIdPhone isManualRefresh:NO];
        self.interstitial.delegate = self;
        
        

    }
    else {
        self.interstitial = [[AdMoGoInterstitial alloc] initWithAppKey:kAdsmogoIdPad
                                                             isRefresh:YES adInterval:0 adType:AdViewTypeiPadFullScreen adMoGoViewDelegate:self];
    }
     */

}

-(void)startBanner {
    
    
    //if (GameManager::sharedGameManager()->getServerBoolean("bannerInGame") == false) {
    if (GameManager::sharedGameManager()->getServerBoolean("noBannerInGame") == true) {
        NSLog(@"Simplygame banner disabled");
        [self stopBanner];
        return;
    }
    
    if (banner != nil) {
        [self showBanner];
        return;
    }

    const char* admobId = GameManager::sharedGameManager()->getServerString("kAdmoBannerId");
    if (admobId == NULL) {
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            
            // iphone default id for admob banner
            self.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
            self.banner.adUnitID = @"ca-app-pub-2115356302075581/5637917564";
            
        }
        else {
            
            // ipad default id for admob banner
            self.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLeaderboard];
            self.banner.adUnitID = @"ca-app-pub-2115356302075581/3592996707";
        }
    }
    else {
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            self.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        }
        else {
            self.banner = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLeaderboard];
        }
            
        // server overwritten id for admob inter
        self.banner.adUnitID = [NSString stringWithUTF8String:admobId];
    }
    
    self.banner.delegate = self;
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    self.banner.rootViewController = mainViewController;
    [self.banner loadRequest:[GADRequest request]];
    
    [self showBanner];    
    [[EAGLView sharedEGLView] addSubview:banner];
    
}

-(void)stopBanner {
    
    
    if (banner == nil) return;
    
    [banner removeFromSuperview];
    //[banner setDelegate:nil];
    //[banner setAdWebBrowswerDelegate:nil];
    banner = nil;
    
}

-(void)showBanner
{
    
    if (banner == nil)  return;



    
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        
        if (screenSize.height == 693.0) {
            
            // iPhone X from 2017, stay in safe area
            banner.frame = CGRectMake(27.0f,728.0f, 320.0f, 50.0f);
        }
        else {
            banner.frame = CGRectMake(0.0f,screenSize.height - 50.0f, 320.0f, 50.0f);
        }
    }
    else {
        banner.frame = CGRectMake(20.0f, 1024.0 - 100.0, 728.0f, 90.0f);
    }
    
}

-(void)hideBanner {
    
    // TODO: hack, make sure banner not showing when not approperiate to show
    // TODO: should refactor remove show and hide banner, only start and stop is needed
    [self stopBanner];
    return;
    
    /*
    if (banner == nil)  return;

    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        banner.frame = CGRectMake(0.0f,screenSize.height + 50.0f, 320.0f, 50.0f);
    }
    else {
        banner.frame = CGRectMake(20.0f, 1024.0+ 100.0f, 728.0f, 90.0f);
    }
     */
}




#pragma mark - admob banner delegate

/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"adViewDidReceiveAd");
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}
/// Tells the delegate an ad request succeeded.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(GADInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that an interstitial will be presented.
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

/// Tells the delegate the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

/// Tells the delegate the interstitial had been animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    [self cacheInterstitial];
    NSLog(@"interstitialDidDismissScreen");

}

/// Tells the delegate that a user click will open another app
/// (such as the App Store), backgrounding the current app.
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
    
}












/*
 返回广告rootViewController
 */

/*
- (UIViewController *)viewControllerForPresentingModalView{
    NSLog(@"ViewControllerForPresenting");
    return [[[UIApplication sharedApplication] delegate] getViewController];
}

- (UIViewController *)viewControllerForPresentingInterstitialModalView {
    NSLog(@"ViewControllerForPresenting");
    return [[[UIApplication sharedApplication] delegate] getViewController];
}
 */


/*
返回广告rootViewController
*/
- (UIViewController *)viewControllerForPresentingInterstitialModalView{
    return [[[UIApplication sharedApplication] delegate] getViewController];
}

/*
 全屏请求成功
 */
- (void)interstitialRequestAdSuccess{
    NSLog(@"Inter request success");
    
}

/*
 全屏失败
 */
- (void)interstitialFailWithError:(NSError *)error{
    NSLog(@"Inter request failed");

}

/*
 插屏展现
 */
- (void)interstitialAdDidPresent
{
    NSLog(@"Inter displayed");

}

/*
 插屏点击
 */
- (void)interstitialAdClick
{
    NSLog(@"Inter clicked");

}

/*
 插屏关闭
 */
- (void)interstitialViewClose
{
    NSLog(@"Inter closed");

}





#pragma mark - MMUBannerView delegate methods

- (UIViewController *)bannerViewControllerForPresentingModalView{
    return [[[UIApplication sharedApplication] delegate] getViewController];
}


/*
// 横幅成功
- (void)bannerAdsSuccess:(MMUBanners *)bannerAds{
    NSLog(@"Banner Success");
}

// 横幅失败
- (void)bannerAdsAllAdFail:(MMUBanners *)bannerAds withError:(NSError *)err{
    NSLog(@"Banner Fail");
}

// 横幅点击
- (void)bannerClick:(MMUBanners *)bannerAds{
    NSLog(@"Banner Click");
}

// 横幅展现
- (void)bannerAdsAppear:(MMUBanners *)bannerAds
{
    NSLog(@"Banner Appear");
}

// 横幅关闭
- (void)bannerClosed:(MMUBanners *)bannerAds{
    NSLog(@"Banner Close");
}

 */
/**
 *You can get notified when the user delete the ad
 返回 YES 表示由开发者处理关闭广告事件
 返回 NO 表示由sdk处理关闭广告事件
 */

/*
- (BOOL)dealCloseAd:(MMUBanners *)bannerAds{
    return NO;
}


#pragma mark - browserDelegate 支持平台：阿里妈妈、谷歌admob
// 浏览器将要展示
- (void)browserWillLoad{
    NSLog(@"%s",__func__);
}

// 浏览器将要消失
- (void)browserWillDismiss{
    NSLog(@"%s",__func__);
}
*/


/**
 * 广告开始请求回调

- (void)adMoGoDidStartAd:(AdMoGoView *)adMoGoView{
    NSLog(@"广告开始请求回调");
}
/**
 * 广告接收成功回调

- (void)adMoGoDidReceiveAd:(AdMoGoView *)adMoGoView{
    NSLog(@"广告接收成功回调");
}
/**
 * 广告接收失败回调

- (void)adMoGoDidFailToReceiveAd:(AdMoGoView *)adMoGoView didFailWithError:(NSError *)error{
    NSLog(@"广告接收失败回调");
}
/**
 * 点击广告回调

- (void)adMoGoClickAd:(AdMoGoView *)adMoGoView{
    NSLog(@"点击广告回调");
}
/**
 *You can get notified when the user delete the ad
 广告关闭回调

- (void)adMoGoDeleteAd:(AdMoGoView *)adMoGoView{
    NSLog(@"广告关闭回调");
}
 */



/*
- (void)adsMoGoInterstitialAdDidDismiss
{
    NSLog(@"全屏广告关闭回调");

    //UIButton *btn = (UIButton *)[self.view viewWithTag:1001];
    //[btn setBackgroundColor:[UIColor greenColor]];
    //[btn setTitle:@"show Interstitial" forState:UIControlStateNormal];
    

   // AdMoGoInterstitial *ins = isVideo?[[AdMoGoInterstitialManager shareInstance] adMogoVideoInterstitialByAppKey:self.appkey]:[[AdMoGoInterstitialManager shareInstance] adMogoInterstitialByAppKey:self.appkey];

    
    AdMoGoInterstitial *ins = [[AdMoGoInterstitialManager shareInstance] defaultInterstitial];
     
    [ins interstitialCancel];
    //isShowTime = !isShowTime;
}
 */

#pragma mark -
#pragma mark AdMoGoWebBrowserControllerUserDelegate delegate

/*
 浏览器将要展示

- (void)webBrowserWillAppear{
    NSLog(@"浏览器将要展示");
}

/*
 浏览器已经展示

- (void)webBrowserDidAppear{
    NSLog(@"浏览器已经展示");
}

/*
 浏览器将要关闭

- (void)webBrowserWillClosed{
    NSLog(@"浏览器将要关闭");
}

/*
 浏览器已经关闭

- (void)webBrowserDidClosed{
    NSLog(@"浏览器已经关闭");
}

- (void)adMoGoFullScreenAdReceivedRequest{
    NSLog(@"全屏广告 接收成功");
}
- (void)adMoGoFullScreenAdFailedWithError:(NSError *) error{
    NSLog(@"全屏广告 接收失败");

}
- (void)adMoGoWillPresentFullScreenAdModal{
    NSLog(@"全屏广告展示");
}
- (void)adMoGoDidDismissFullScreenAdModal{
    NSLog(@"全屏广告消失");
}
-(void)adMogoFullScreenAdClosed{
    NSLog(@"全屏广告关闭");
}
*/


/*
- (void)presentAppStoreForID:(NSString *)appStoreID
{
    if(NSClassFromString(@"SKStoreProductViewController")) { // Checks for iOS 6 feature.
        
        SKStoreProductViewController *storeController = [[SKStoreProductViewController alloc] init];
        storeController.delegate = self; // productViewControllerDidFinish
        
        NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier : appStoreID };
        
        
        [storeController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *error) {
            if (result) {
                UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] viewController];
                [mainViewController presentViewController:storeController animated:YES completion:nil];
            } else {
                // Gracefully do nothing
                //[[[UIAlertView alloc] initWithTitle:@"Uh oh!" message:@"There was a problem displaying the app" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            }
        }];
        
        
    } else { // Before iOS 6, we can only open the URL
        NSURL* fullURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", appStoreID]];
        [[UIApplication sharedApplication] openURL:fullURL];
    }
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController { 
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] viewController];
    [mainViewController dismissViewControllerAnimated:YES completion:^{  
        [viewController release];
    }];
}
*/



#pragma mark -
#pragma mark InAppPurchase

-(void)loadInAppStore {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    NSSet *productIdentifiers = nil;

    productIdentifiers = [NSSet setWithObjects:
                          [NSString stringWithUTF8String:kInAppPurchaseCoin1Id],
                          [NSString stringWithUTF8String:kInAppPurchaseCoin2Id],
                          [NSString stringWithUTF8String:kInAppPurchaseCoin3Id],
                          [NSString stringWithUTF8String:kInAppPurchaseCoin4Id],
                          [NSString stringWithUTF8String:kInAppPurchaseCoin5Id],

                          nil];
    
    SKProductsRequest* productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}
-(void)puchaseProduct:(NSString*)productId {
    for (SKProduct* product in products) {
        if ([product.productIdentifier isEqualToString:productId]) {
            SKPayment *payment = [SKPayment paymentWithProduct:product];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
        }
    }
}

-(const char*)getProductPrice:(NSString*)productId {
    for (SKProduct* product in products) {
        if ([product.productIdentifier isEqualToString:productId]) {
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [numberFormatter setLocale:product.priceLocale];
            NSString *formattedString = [numberFormatter stringFromNumber:product.price];
            [numberFormatter release];
            NSLog(formattedString);
            return [formattedString UTF8String];
        }
    }
    return "BUY NOW";
}


-(void)restorePurchases {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}


#pragma mark -
#pragma mark SKProductsRequestDelegate methods

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    self.products = response.products;
    
	// For debug only
    for (NSString *invalidProductId in response.invalidProductIdentifiers)
    {
        NSLog(@"Invalid product id: %@" , invalidProductId);
    }
    
    // Mirror the alloc in loadStore
    [request release];
}


#pragma mark -
#pragma mark SKPaymentTransactionObserver methods

//
// called when the transaction status is updated
//
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
		//NSLog(@"Transcation: %d", [transactions count]);
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}


#pragma mark -
#pragma mark InAppPurchase help functions

- (void)provideContent:(NSString *)productId
{
    
    // Do something to provide content
    /*
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Buy Sucess" message:productId delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
     */
    
    if ([productId isEqualToString:[NSString stringWithUTF8String:kInAppPurchaseCoin1Id]]) {
        GameManager::sharedGameManager()->gameSetting.coins += 30;
        GameManager::sharedGameManager()->logEvent("IAP: 1");

    }
    else if ([productId isEqualToString:[NSString stringWithUTF8String:kInAppPurchaseCoin2Id]]) {
        GameManager::sharedGameManager()->gameSetting.coins += 80;
        GameManager::sharedGameManager()->logEvent("IAP: 2");

    }
    else if ([productId isEqualToString:[NSString stringWithUTF8String:kInAppPurchaseCoin3Id]]) {
        GameManager::sharedGameManager()->gameSetting.coins += 180;
        GameManager::sharedGameManager()->logEvent("IAP: 4");

    }
    else if ([productId isEqualToString:[NSString stringWithUTF8String:kInAppPurchaseCoin4Id]]) {
        GameManager::sharedGameManager()->gameSetting.coins += 380;
        GameManager::sharedGameManager()->logEvent("IAP: 7");

    }
    else if ([productId isEqualToString:[NSString stringWithUTF8String:kInAppPurchaseCoin5Id]]) {
        GameManager::sharedGameManager()->gameSetting.coins += 780;
        GameManager::sharedGameManager()->logEvent("IAP: 12");
        
    }
    
    GameManager::sharedGameManager()->saveGameSetting();
    GameManager::sharedGameManager()->playSoundEffect("coinsin.wav");

    
    
    
    CCScene* currentScene = CCDirector::sharedDirector()->getRunningScene();
    
    StoreScene* storeScene = (StoreScene*)currentScene;
    
    if (storeScene) {
        storeScene->updateCoins();
    }
    

    
}

- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    // Notify the UI that transaction was sucessful or not, probably a NSAlert view is fine.
}

//
// called when the transaction was successful
//
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{    
    [self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has been restored and and successfully completed
//
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{    
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has failed
//
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{	
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
		NSLog(@"%@", [transaction.error localizedDescription]);
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else
    {
		NSLog(@"Canceled by user");
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
}


-(BOOL)isGameCenterAvailable {
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    
    if (gcClass == nil)
        return NO;
    else
        return YES;

}

-(void)loadGameCenter {
    if ([self isGameCenterAvailable] == NO)
        return;
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    [localPlayer authenticateWithCompletionHandler:^(NSError *error) {
        if (localPlayer.isAuthenticated)
        {
            // Player was successfully authenticated.
            // Perform additional tasks for the authenticated player.
            [self installInvitationHandler];
        }
    }];
}

-(void)sendScore:(int)score forId:(NSString*)scoreId {
    if ([self isGameCenterAvailable] == NO)
        return;
    
    GKScore *gkScore = [[[GKScore alloc]
                       initWithCategory:scoreId] autorelease];
    gkScore.value = score;
    gkScore.context = 15;
    
    [gkScore reportScoreWithCompletionHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^(void)
                       {
                           if (error == NULL) {
                               NSLog(@"Successfully sent score!");
                           } else {
                               NSLog(@"Score failed to send... will try again later.  Reason: %@", error.localizedDescription);
                           }
                       });
    }];
}


-(void)showGameCenter:(NSString*)scoreId {
    if ([self isGameCenterAvailable] == NO)
        return;
    
    GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init];
    leaderboardController.category = scoreId;
    
    if (leaderboardController != NULL)
    {
        leaderboardController.leaderboardDelegate = (id)self;
        UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
        [mainViewController presentModalViewController:leaderboardController animated:YES];
    }
}

- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    [mainViewController dismissModalViewControllerAnimated: YES];
}


-(void)shareSocialNetwork:(int)score {
    Class avcClass = (NSClassFromString(@"UIActivityViewController"));
    if (avcClass == nil)
        return;
    else {
        
        /* Get screen shot */
        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
        CCRenderTexture * tex =  CCRenderTexture::create(winSize.width, winSize.height);
        tex->begin();
        CCDirector::sharedDirector()->getRunningScene()->visit();
        tex->end();
        std::string path = CCFileUtils::sharedFileUtils()->getWritablePath();
        path += "screenshot.png";
        tex->saveToFile(path.c_str());
        
        /* Setup weixin */
        NSArray* activities = nil;
        
        /* 64 bit conflict
        if ( [WXApi isWXAppInstalled] == YES) {
            WeixinActivity* weixin = [[[WeixinActivity alloc] init] autorelease];
            TimelineActivity* timeline = [[[TimelineActivity alloc] init] autorelease];
            activities = [NSArray arrayWithObjects:weixin, timeline, nil];
        }
         */
        
        /* Get the image and share it */
        UIImage* image = [UIImage imageWithContentsOfFile:[NSString stringWithUTF8String:path.c_str()]];
        NSString* text = [NSString stringWithFormat:[NSString stringWithUTF8String:GameManager::sharedGameManager()->getLocalizedString("shareText")], score];
        NSString* url = [NSString stringWithUTF8String:kAppUrl];
        
        NSArray* items = [NSArray arrayWithObjects: image, text, url, nil];
        UIActivityViewController *vc = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:activities];
        NSArray* excluded = [NSArray arrayWithObjects:/*@"com.apple.UIKit.activity.Mail", @"com.apple.UIKit.activity.Message",*/@"com.apple.UIKit.activity.SaveToCameraRoll", @"com.apple.UIKit.activity.CopyToPasteboard", @"com.apple.UIKit.activity.AssignToContact", @"com.apple.UIKit.activity.Print", nil];
        vc.excludedActivityTypes = excluded;
        
        
        vc.completionHandler = ^(NSString *activityType, BOOL completed) {
            if (completed == YES) {
                NSString* info = [NSString stringWithFormat:@"Social: %@", activityType];
                GameManager::sharedGameManager()->logEvent([info UTF8String]);
                
                GameScene* gameScene = dynamic_cast<GameScene*>(CCDirector::sharedDirector()->getRunningScene());
                if (gameScene != NULL) {
                    gameScene->rewardBonusCoin();
                }
            }
        };
        
        UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
        [mainViewController presentViewController:vc animated:NO completion:nil];
    }
}

-(void)showRandomWall {
    if ((nextWall != 1) && (nextWall != 2)) {
        nextWall = rand() % 2 + 1;
    }

    if (nextWall == 1) {
        [self showDomobWall];
    }
    else {
        [self showGuomobWall];
    }
    
    nextWall = 3 - nextWall;
}

-(void)showDomobWall {
    
    /*
    if (domobWall == nil) {
        const char* domobId = GameManager::sharedGameManager()->getServerString("kDomobId");
        if (domobId == NULL) {
            self.domobWall=[[DMOfferWallViewController alloc] initWithPublisherID:kDomobId];
            self.wallManager = [[DMOfferWallManager alloc] initWithPublishId:kDomobId];
        }
        else {
            self.domobWall=[[DMOfferWallViewController alloc] initWithPublisherID:[NSString stringWithUTF8String:domobId]];
            self.wallManager=[[DMOfferWallManager alloc] initWithPublishId:[NSString stringWithUTF8String:domobId]];
        }
        
        domobWall.delegate = self;
        wallManager.delegate = self;
    }
    
    [domobWall presentOfferWall];
     */
}

-(void)showVideoWall {
    
    /*
    if (videoWall == nil) {
        const char* domobId = GameManager::sharedGameManager()->getServerString("kDomobId");
        if (domobId == NULL) {
            
            self.videoWall = [[AssetZoneVideoManager alloc] initWithPublisherID:kDomobId
                                                                andUserID:nil];
        }
        else {
            self.videoWall = [[AssetZoneVideoManager alloc] initWithPublisherID:[NSString stringWithUTF8String:domobId]
                                                                      andUserID:nil];
        }
        
        videoWall.delegate = self;
    }
    
    [videoWall presentVideoAssetZone];
    GameManager::sharedGameManager()->stopMusic();
    
*/

    /*
    if (domobWall == nil) {
        const char* domobId = GameManager::sharedGameManager()->getServerString("kDomobId");
        if (domobId == NULL) {
            self.videoWall=[[DMVideoViewController alloc] initWithPublisherID:kDomobId];
        }
        else {
            self.videoWall=[[DMVideoViewController alloc] initWithPublisherID:[NSString stringWithUTF8String:domobId]];
        }
        
        videoWall.delegate = self;
    }
    
    [videoWall presentVideoAdView];
     */
}

-(void)showGuomobWall {

}


/*
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    GameManager::sharedGameManager()->saveGameSetting();
    GameManager::sharedGameManager()->updateCoins();
}
 */



// Video wall

/*
- (void)azManager:(AssetZoneVideoManager *)manager
failedLoadWithError:(NSError *)error {
    
}

- (void)azManagerUncompleteVideoAsset:(AssetZoneVideoManager *)manager
                            withError:(NSError *)error {
}




- (void)azManagerDidClosed:(AssetZoneVideoManager *)manager {
    GameManager::sharedGameManager()->continueMusic();

}

- (void)azCompleteVideoAsset:(AssetZoneVideoManager *)manager
              withTotalPoint:(NSNumber *)totalPoint
               consumedPoint:(NSNumber *)consumedPoint {

    
    NSLog(@"DomobPoints: %d, ConsumedPoints: %d", [totalPoint intValue], [consumedPoint intValue]);

    dmPoint = 0;

    int point = [totalPoint intValue] - [consumedPoint intValue];
    if (point <= 0) return; //TODO: remind them to come back and check
    
    dmPoint = point; // positive
    [videoWall consumeWithPointNumber:dmPoint];
    
}


- (void)azManager:(AssetZoneVideoManager *)manager
consumedWithStatusCode:(VideoAssetZoneConsumeStatus)statusCode
       totalPoint:(NSNumber *)totalPoint
totalConsumedPoint:(NSNumber *)consumedPoint {
    
    NSLog(@"DomobPoints: %d, ConsumedPoints: %d", [totalPoint intValue], [consumedPoint intValue]);

    if (statusCode == eVideoAssetZoneConsumeSuccess && dmPoint > 0) {
        GameManager::sharedGameManager()->gameSetting.coins += dmPoint;
        
        
        NSString* congrat = @"恭喜你获得%d金币";
        NSString* confirm = @"确认";
        
        ccLanguageType currentLanguage = CCApplication::sharedApplication()->getCurrentLanguage();
        if (currentLanguage != kLanguageChinese) {
            congrat = @"Congrats, you get %d coins";
            confirm = @"OK";
        }
        
        NSString *tempMsg = [NSString stringWithFormat:congrat, dmPoint];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:tempMsg delegate:self cancelButtonTitle:confirm otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        dmPoint = 0;
    }
    
}
 */

// Domob wall delegate

/* DOMOB REMOVED
- (void)offerWallDidClosed {
    [wallManager requestOnlinePointCheck];
}


- (void)offerWallDidFinishCheckPointWithTotalPoint:(NSInteger)totalPoint
                             andTotalConsumedPoint:(NSInteger)consumed
{
    
    dmPoint = 0;
    int point = totalPoint - consumed;
    if (point <= 0) return; //TODO: remind them to come back and check
    
    dmPoint = point; // positive
    [wallManager requestOnlineConsumeWithPoint:dmPoint];


}


- (void)offerWallDidFinishConsumePointWithStatusCode:(DMOfferWallConsumeStatusCode)statusCode
                                          totalPoint:(NSInteger)totalPoint
                                  totalConsumedPoint:(NSInteger)consumed {
    NSLog(@"DomobPoints: %d, ConsumedPoints: %d", totalPoint, consumed);
    
    if (statusCode == DMOfferWallConsumeStatusCodeSuccess) {
        GameManager::sharedGameManager()->gameSetting.coins += dmPoint;

        
        NSString *tempMsg = [NSString stringWithFormat:@"恭喜你通过积分墙获得%d金币", dmPoint];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:tempMsg delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        dmPoint = 0;
    }
    
}
*/

-(void)cacheInterstitialNew {
    
    /*

        Chartboost *cb = [Chartboost sharedChartboost];
        
        const char* appId = GameManager::sharedGameManager()->getServerString("kChartBoostId");
        if (appId == NULL) {
            cb.appId = kChartBoostId;
        }
        else {
            cb.appId = [NSString stringWithUTF8String:appId];
        }
        

        const char* appSignature = GameManager::sharedGameManager()->getServerString("kChartBoostSignature");
        if (appSignature == NULL) {
            cb.appSignature = kChartBoostSignature;
        }
        else {
            cb.appSignature = [NSString stringWithUTF8String:appSignature];

        }

        cb.delegate = self;
        [cb startSession];
        [cb cacheInterstitial];
     */
}

-(void)showInterstitialNew {
    

    /*
        Chartboost *cb = [Chartboost sharedChartboost];
        [cb showInterstitial];
     */


}


- (void)didDismissInterstitial:(NSString *)location {
    NSLog(@"CHARTBOOST: dismissInterstitial");
    /*
    [[Chartboost sharedChartboost] cacheInterstitial];
    GameManager::sharedGameManager()->logEvent("Chartboost: inter showed");
     */

}

-(void)didClickInterstitial:(NSString *)location {
    
    //GameManager::sharedGameManager()->logEvent("Chartboost: inter clicked");

}

- (void)didFailToLoadInterstitial:(NSString *)location {
    NSLog(@"CHARTBOOST: failToLoad");
    
    //GameManager::sharedGameManager()->logEvent("Chartboost: inter failed");


}





// load appstore
- (void)presentAppStoreForID:(NSString *)appStoreID
{
    if(NSClassFromString(@"SKStoreProductViewController")) { // Checks for iOS 6 feature.
        
        SKStoreProductViewController *storeController = [[SKStoreProductViewController alloc] init];
        storeController.delegate = self; // productViewControllerDidFinish
        
        NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier : appStoreID };
        
        
        [storeController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *error) {
            if (result) {
                UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
                [mainViewController presentViewController:storeController animated:YES completion:nil];
            } else {
                // Gracefully do nothing
                //[[[UIAlertView alloc] initWithTitle:@"Uh oh!" message:@"There was a problem displaying the app" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            }
        }];
        
        
    } else { // Before iOS 6, we can only open the URL
        NSURL* fullURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", appStoreID]];
        [[UIApplication sharedApplication] openURL:fullURL];
    }
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    [mainViewController dismissViewControllerAnimated:YES completion:^{
        [viewController release];
    }];
}

-(void)presentPromoPopAlert {
    

    
    if (GameManager::sharedGameManager()->getServerBoolean("promoPop") == true) {
        
        const char* url = GameManager::sharedGameManager()->getServerString("promoPopUrl");
        
        // no promo url
        if (( url == NULL ) || (strlen(url) == 0)){
            return;
        }
        
        // promo same
        std::string lastOpen = CCUserDefault::sharedUserDefault()->getStringForKey("lastOpenedPromo");
        if (lastOpen.length() !=0) {
            if (lastOpen.compare(std::string(url)) == 0) {
                return;
            }
        }
        
        
        
        
        NSString* pTitle = [NSString stringWithUTF8String:GameManager::sharedGameManager()->getServerString("promoPopTitle")];
        NSString* pText = [NSString stringWithUTF8String:GameManager::sharedGameManager()->getServerString("promoPopText")];
        
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:pTitle message:pText delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"download", nil), nil];
        
        alertType = 2;
        
        [alert show];
        [alert release];
    }
}

-(void)presentRatePopAlert {
    
    if (GameManager::sharedGameManager()->getServerBoolean("ratePop") == true) {
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"rateTitle",nil) message:NSLocalizedString(@"rateText", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"download", nil), nil];
        
        alertType = 1;
        
        [alert show];
        [alert release];
    }
}


-(void)presentCoinAlert
{
    const char* text = GameManager::sharedGameManager()->getLocalizedString("vsInfoDetail2");
    
    NSString* confirm = @"确认";
    ccLanguageType currentLanguage = CCApplication::sharedApplication()->getCurrentLanguage();
    if (currentLanguage != kLanguageChinese) {
        confirm = @"OK";
    }
    
    NSString *tempMsg = [NSString stringWithUTF8String:text];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:tempMsg delegate:nil cancelButtonTitle:confirm otherButtonTitles:nil];

    
    [alertView show];
    [alertView release];
}




// Admob interstitial

// Ads with admob
-(void)showInterstitialAdmob {
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    
    if (admobInterstitial) {
        if ([admobInterstitial isReady]) {
            [admobInterstitial presentFromRootViewController:mainViewController];
        }
    }
    
    //admobInterstitial.delegate = nil;
    if (admobInterstitial) {
        [admobInterstitial release];
    }
    
    admobInterstitial = nil;
    
    [self cacheInterstitialAdmob];
    
    
}
-(void)cacheInterstitialAdmob {
    admobInterstitial = [[GADInterstitial alloc] init];

    const char* admobId = GameManager::sharedGameManager()->getServerString("kAdmobId");
    if (admobId == NULL) {

        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            admobInterstitial.adUnitID = kAdmobIdPhone;
        }
        else {
            admobInterstitial.adUnitID = kAdmobIdPad;
        }

        
    }
    else {
        admobInterstitial.adUnitID = [NSString stringWithUTF8String:admobId];
    }
    
    //admobInterstitial.delegate = self;
    [admobInterstitial loadRequest:[GADRequest request]];
    
}






// Game center match

-(void)installInvitationHandler {
    [GKMatchmaker sharedMatchmaker].inviteHandler = ^(GKInvite *acceptedInvite, NSArray *playersToInvite) {
        // Insert game-specific code here to clean up any game in progress.
        
        
        if (GameManager::sharedGameManager()->gameSetting.coins < kVSCoinNeed) {
            [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(presentCoinAlert) userInfo:nil repeats:NO];
            return;
        }
        
        
        
        if (acceptedInvite)
        {
            matchStarted = NO;
            GameManager::sharedGameManager()->inBattle = false;
            GKMatchmakerViewController *mmvc = [[GKMatchmakerViewController alloc] initWithInvite:acceptedInvite];
            mmvc.matchmakerDelegate = self;
            UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
            [mainViewController presentViewController:mmvc animated:YES completion:nil];
        }
        else if (playersToInvite)
        {
            matchStarted = NO;
            GameManager::sharedGameManager()->inBattle = false;

            GKMatchRequest *request = [[GKMatchRequest alloc] init];
            request.minPlayers = 2;
            request.maxPlayers = 4;
            request.playersToInvite = playersToInvite;
            
            GKMatchmakerViewController *mmvc = [[GKMatchmakerViewController alloc] initWithMatchRequest:request];
            mmvc.matchmakerDelegate = self;
            
            UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
            [mainViewController presentViewController:mmvc animated:YES completion:nil];
        }
    };

    
}

-(void)hostMatch {
    
    
    if (GameManager::sharedGameManager()->gameSetting.coins < kVSCoinNeed) {
        
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(presentCoinAlert) userInfo:nil repeats:NO];
        return;
    }
    
    
    matchStarted = NO;
    GameManager::sharedGameManager()->inBattle = false;

    
    GKMatchRequest *request = [[GKMatchRequest alloc] init];
    request.minPlayers = 2;
    request.maxPlayers = 4;
    
    GKMatchmakerViewController *mmvc = [[GKMatchmakerViewController alloc] initWithMatchRequest:request];
    mmvc.matchmakerDelegate = self;
    
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    [mainViewController presentViewController:mmvc animated:YES completion:nil];
}


- (void)matchmakerViewControllerWasCancelled:(GKMatchmakerViewController *)viewController
{
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    [mainViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)matchmakerViewController:(GKMatchmakerViewController *)viewController didFailWithError:(NSError *)error
{
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    [mainViewController dismissViewControllerAnimated:YES completion:nil];
}


- (UIImage *)imageWithImage:(UIImage *)largeImage scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [largeImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



- (void)matchmakerViewController:(GKMatchmakerViewController *)viewController didReceiveAcceptFromHostedPlayer:(NSString *)playerID {
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    [viewController setHostedPlayer:playerID connected:YES];

}


- (void)match:(GKMatch *)match player:(NSString *)playerID didChangeState:(GKPlayerConnectionState)state
{
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
}






- (void)matchmakerViewController:(GKMatchmakerViewController *)viewController didFindMatch:(GKMatch *)match
{
    UIViewController* mainViewController = [[[UIApplication sharedApplication] delegate] getViewController];
    [mainViewController dismissViewControllerAnimated:YES completion:^{
        self.myMatch = match; // Use a retaining property to retain the match.
        match.delegate = self;
        if (!matchStarted && match.expectedPlayerCount == 0)
        {
            matchStarted = YES;
            GameManager::sharedGameManager()->inBattle = true;

            
            // Force the game mode to be battle
            GameManager::sharedGameManager()->gameMode = 4;
        
            
            // Insert game-specific code to start the match.
            [GKPlayer loadPlayersForIdentifiers:myMatch.playerIDs withCompletionHandler:
             ^(NSArray *players, NSError *error){
                 
                 NSMutableArray* myPlayers = [[NSMutableArray alloc] init];
                 [myPlayers addObjectsFromArray:players];
                 [myPlayers addObject: [GKLocalPlayer localPlayer]];

                 
                 GameManager::sharedGameManager()->removeAllPlayers();
                 
                 __block int  expectedLoadingCount = [myPlayers count];
                 

                 for (int i=0; i<[myPlayers count]; i++) {
                     GKPlayer* player = [myPlayers objectAtIndex:i];
                     [player loadPhotoForSize:GKPhotoSizeSmall withCompletionHandler:
                      ^(UIImage *photo, NSError *error){
                          if (photo == nil) {
                              photo = [UIImage imageNamed:@"avatar.png"];
                          }
                          
                          photo = [self imageWithImage:photo scaledToSize:CGSizeMake(30, 30)];
                          
                              
                              CCImage *imf =new CCImage();
                              NSData *imgData = UIImagePNGRepresentation(photo);
                              NSUInteger len = [imgData length];
                              Byte *byteData = (Byte*)malloc(len);
                              memcpy(byteData, [imgData bytes], len);
                              imf->initWithImageData(byteData,imgData.length);
                              imf->autorelease();
                              CCTexture2D* pTexture = new CCTexture2D();
                              pTexture->initWithImage(imf);
                              pTexture->autorelease();
                            CCSprite *sprit = CCSprite::createWithTexture(pTexture);

                          sprit->setScale(15.0 / sprit->boundingBox().size.width);

                          
                          NSString* displayName = [player displayName];
                          if ([displayName length] > 12) {
                              displayName = [NSString stringWithFormat: @"%@...", [displayName substringToIndex:10]];
                          }
                          
                              GameManager::sharedGameManager()->addPlayer(i,[[player playerID] UTF8String], [displayName UTF8String], sprit);
  
                          
                          expectedLoadingCount--;
                          
                          if (expectedLoadingCount == 0) {
                              GameScene* gameScene = GameScene::create();
                              CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, gameScene));
                              
                              
                              //GameManager::sharedGameManager()->debugPlayers();
                              [myPlayers release];
                              
                          }
                      }];
                     //NSLog(@"%@: %@", [player playerID], [player displayName]);
                 }
             }];
            
            

        
        }
    }];
    

}


-(void)disconnectMatch {
    [myMatch disconnect];
    matchStarted = NO;
    GameManager::sharedGameManager()->inBattle = false;

    
}

-(void)sendScoreRT: (int)score {
    NSError *error;
    
    int scoreRT = score;
    
    GameManager::sharedGameManager()->setScore([[[GKLocalPlayer localPlayer] playerID] UTF8String], scoreRT);
    

    NSData *packet = [NSData dataWithBytes:&scoreRT length:sizeof(int)];
    [myMatch sendDataToAllPlayers: packet withDataMode: GKMatchSendDataReliable error:&error];
    if (error != nil)
    {
        // Handle the error.
    }
}

- (void)match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID
{
    
    int* pScoreRT = (int*)[data bytes];
    int scoreRT = (*pScoreRT);
    
    GameManager::sharedGameManager()->setScore([playerID UTF8String], scoreRT);
    
    //GameManager::sharedGameManager()->debugPlayers();

    // Update the score for player
}


- (void)presentBonusScene {
    CCScene* pScene = BonusScene::create();
    CCDirector::sharedDirector()->pushScene(CCTransitionFadeTR::create(0.7, pScene));
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        
    }
    else {
        
        
        if (alertType == 2) {
            //promo
            
            const char* url = GameManager::sharedGameManager()->getServerString("promoPopUrl");
            
            if (( url == NULL ) || (strlen(url) == 0)){
                alertType = 0;
                return;
            }
            else {
                CCUserDefault::sharedUserDefault()->setStringForKey("lastOpenedPromo", std::string(url));
                GameManager::sharedGameManager()->saveGameSetting();
                GameManager::sharedGameManager()->openURL(url);

            }
        }
        
        // Alert Type = 1
        else {
            // rate
            CCUserDefault::sharedUserDefault()->setBoolForKey("didRating25", true);
            GameManager::sharedGameManager()->saveGameSetting();
            GameManager::sharedGameManager()->openURL(kRateUrl);
        }
        [self presentBonusScene];
        
        /*
        if ([promoUrl hasPrefix:@"http"]) {
            // Open url
            GameManager::sharedGameManager()->openURL([promoUrl UTF8String]);
            [self presentBonusScene];
        }
        else {
            // url is appId
            [self presentAppStoreForID:promoUrl];
        }
         */
    }
    
    // reset alertType;
    alertType = 0;
    
}


@end
