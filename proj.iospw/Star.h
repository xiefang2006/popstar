//
//  Star.h
//  SimpleGame
//
//  Created by Game on 14-8-13.
//
//

#ifndef SimpleGame_Star_h
#define SimpleGame_Star_h

//#define __Xmas__

// Platforms, pick one of them, should match app id
#define __Platform_iOS__
//#define __Platform_Mac__
//#define __Platform_Android__

#define kHintTime 3
#define kVSCoinNeed 5

#define __PW__
#define __ChineseOnly__


// Platforms, pick one of them
#ifdef __Platform_iOS__

#ifdef __Star__
#define kInAppPurchaseCoin1Id "com.pw.star.coin1"
#define kInAppPurchaseCoin2Id "com.pw.star.coin2"
#define kInAppPurchaseCoin3Id "com.pw.star.coin3"
#define kInAppPurchaseCoin4Id "com.pw.star.coin4"
#define kInAppPurchaseCoin5Id "com.pw.star.coin5"

#define kServerSettingURL "https://www.simplygame.net/web/pwpopstar.plist"
#define kServerSettingURLHD "https://www.simplygame.net/web/pwpopstarhd.plist"
#define kAppUrl "https://itunes.apple.com/app/id1186849610"
#define kRateUrl "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1186849610&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8"

// FOR AFP (alimama)
#define kAdsAFPBannerPhone @"68428534"
#define kAdsAFPBannerPad @"68422787"
#define kAdsAFPInterPhone @"68424770"
#define kAdsAFPInterPad @"68436268"

#define kLeaderboardClassic "grp.com.pw.star.classicmode"
#define kLeaderboardStage "grp.com.pw.star.stage"
#define kLeaderboardTime "grp.com.pw.star.timedmode"
#define kLeaderboardMove "grp.com.pw.star.movesmode"
#define kLeaderboardAdvStar "grp.com.pw.star.advstar"
#define kLeaderboardAdvStage "grp.com.pw.star.advstage"
#define kLeaderboardCoin "grp.com.pw.star.coin"
#define kLeaderboardBattle "grp.com.pw.star.battle"

#define kAdmobIdPhone @"a15203ccf34cbc7"
#define kAdmobIdPad @"a1528254c91e3b3"





#endif

#endif



#ifdef __Platform_Mac__

#ifdef __Star__
#define kInAppPurchaseCoin1Id "com.sg.popstarmac.coin1"
#define kInAppPurchaseCoin2Id "com.sg.popstarmac.coin2"
#define kInAppPurchaseCoin3Id "com.sg.popstarmac.coin3"
#define kInAppPurchaseCoin4Id "com.sg.popstarmac.coin4"
#define kInAppPurchaseCoin5Id "com.sg.popstarmac.coin5"

#define kServerSettingURL "https://www.simplygame.net/web/sgpopstarmac.plist"
#define kRateUrl "macappstore://itunes.apple.com/app/id720910624?mt=12"
#define kLeaderboardClassic "grp.com.sg.popstar.classicmode"
#define kLeaderboardStage "grp.com.sg.popstar.stage"
#define kLeaderboardTime "grp.com.sg.popstar.timedmode"
#define kLeaderboardMove "grp.com.sg.popstar.movesmode"
#define kLeaderboardAdvStar "grp.com.sg.popstar.advstar"
#define kLeaderboardAdvStage "grp.com.sg.popstar.advstage"

#endif

#endif





#ifdef __Platform_Android__

#ifdef __Star__
#define kInAppPurchaseCoin1Id "com.sg.androidpopstar.coin1"
#define kInAppPurchaseCoin2Id "com.sg.androidpopstar.coin2"
#define kInAppPurchaseCoin3Id "com.sg.androidpopstar.coin3"
#define kInAppPurchaseCoin4Id "com.sg.androidpopstar.coin4"
#define kServerSettingURL "https://www.simplygame.net/web/sgpopstarandroid.plist"
#define kServerSettingURLHD "https://www.simplygame.net/web/sgpopstarandroidhd.plist"
#define kRateUrl "" // "macappstore://itunes.apple.com/app/id725833455?mt=12"       // no rate for android now.
#define kLeaderboardClassic "" // "grp.com.f7.popstar.classicmode"
#define kLeaderboardStage "" // "grp.com.f7.popstar.stage"
#define kLeaderboardTime "" // "grp.com.f7.popstar.timedmode"
#define kLeaderboardMove "" // "grp.com.f7.popstar.movesmode"


#endif

#endif



#endif
