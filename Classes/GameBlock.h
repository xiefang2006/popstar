#include "cocos2d.h"
using namespace cocos2d;

class GameBlock : public CCSprite
{
private:

public:
    int x, y;
    int color; 

    // #-color
    // -------
    // 0-blue
    // 1-green
    // 2-purple
    // 3-red
    // 4-yellow

    virtual bool init();
    static GameBlock* create(int color);
    void setXY(int x, int y);
    void setX(int x);
    void setY(int y);
    int getX();
    int getY();
    int getBlockColor();
    std::string getColorStr();
    void setBlockColor(int c);
    static void printColor(int color);
    static std::string color2str(int color);
    
    CREATE_FUNC(GameBlock);
};
