//
//  StageLayer.cpp
//  SimpleGame
//
//  Created by Game on 14-1-9.
//
//

#include "StageLayer.h"
#include "GameManager.h"
#include "MenuScene.h"
#include "GameScene.h"

#define kRowDistance 0.15

void StageLayer::stageSelected(CCNode* node) {
    GameManager::sharedGameManager()->playSoundEffect("select.wav");
    int tag = node->getTag();
    
    if (tag <= lastEnabledStage) {
        GameManager::sharedGameManager()->selectedStage = tag;
        CCLog("Entering stage %d", tag);
        GameScene* gameScene = GameScene::create();
        CCDirector::sharedDirector()->replaceScene(CCTransitionSlideInR::create(0.5, gameScene));
    }
}

void StageLayer::exitTouched(CCNode* node) {
    GameManager::sharedGameManager()->playSoundEffect("select.wav");
    CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, MenuScene::create()));
}

void StageLayer::gameCenterTouched(CCNode* node) {
    GameManager::sharedGameManager()->playSoundEffect("select.wav");
    GameManager::sharedGameManager()->showGameCenter();
}


void StageLayer::layoutControls() {
    CCMenuItemImage* back = CCMenuItemImage::create("exit.png", "exit.png", this, menu_selector(StageLayer::exitTouched));
    CCMenu* menu = CCMenu::create(back, NULL);
    menu->setPosition(ccp(screenSize.width * 0.1, screenSize.height * 0.9));
    this->addChild(menu);
    
    CCMenuItemImage* gamecenter = CCMenuItemImage::create("ranking.png", "ranking.png", this, menu_selector(StageLayer::gameCenterTouched));
    CCMenu* menu2 = CCMenu::create(gamecenter, NULL);
    menu2->setPosition(ccp(screenSize.width * 0.9, screenSize.height * 0.9));
    this->addChild(menu2);
    
    
    CCLabelBMFont* title = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    title->setString(GameManager::sharedGameManager()->getLocalizedString("selectStage"));
    title->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.92));
    this->addChild(title);
    
    CCSprite* starImg = CCSprite::create("star_gold.png");
    starImg->setPosition(ccp(screenSize.width * 0.33, screenSize.height * 0.85));
    this->addChild(starImg);
    starImg->setScale(0.8);
    
    CCLabelBMFont* star = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    star->setAnchorPoint(ccp(0.0, 0.5));
    star->setString((to_string(totalStars) + " / " + to_string(3 * GameManager::sharedGameManager()->getAdventureStageCount())).c_str());
    star->setPosition(ccp(screenSize.width * 0.40, screenSize.height * 0.85));
    this->addChild(star);
    star->setScale(0.8);
    


    

    
    
    
}

void StageLayer::layoutButtons() {
    int stageCount = GameManager::sharedGameManager()->getAdventureStageCount();
    
    
    int columnCount = 4;
    int rowCount = stageCount / columnCount;
    
    bool enabled = true;
    
    bool isChinese = (CCApplication::sharedApplication()->getCurrentLanguage() == kLanguageChinese);
    
    for (int i=0; i<rowCount; i++) {
        CCMenu* menu = CCMenu::create();
        
        // React to touch up, and cancel when scroll view moved
        //menu->setActionAtTouchUp(true);
        
        for (int j=0; j<columnCount; j++) {
            

            CCMenuItemImage* item = CCMenuItemImage::create(enabled?"button.png":"button_gray.png", enabled?"button.png":"button_gray.png", this, menu_selector(StageLayer::stageSelected));
            
            int stageNumber = i*columnCount + j + 1;
            item->setTag(stageNumber);
            CCLabelBMFont* label = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
            label->setString(to_string(stageNumber).c_str());
            label->setPosition(ccp(buttonSizeW * 0.5, buttonSizeH * 0.5));
            //label->setColor(ccc3(0,0,80));
            item->addChild(label);
            
            if (isChinese) {
                label->setScale(0.7);
            }
            else {
                label->setScale(0.8);
            }
            
            
            // Get the stars
            int stars = 0;
            if (enabled == true) {
                std::string key = "star_" + to_string(stageNumber);
                stars = CCUserDefault::sharedUserDefault()->getIntegerForKey(key.c_str());
                totalStars += stars;
                
                if (stars == 0) {
                    enabled = false;
                }
                lastEnabledStage = stageNumber;
            }
            
            
            CCSprite* star1 = CCSprite::create(stars>=1?"star_gold.png":"star.png");
            CCSprite* star2 = CCSprite::create(stars>=2?"star_gold.png":"star.png");
            CCSprite* star3 = CCSprite::create(stars>=3?"star_gold.png":"star.png");
            
            star2->setScale(2.0 * 0.3);
            star1->setScale(1.5 * 0.3);
            star3->setScale(1.5 * 0.3);
            star1->setPosition(ccp(buttonSizeW * 0.1, - buttonSizeH * 0.1));
            star2->setPosition(ccp(buttonSizeW * 0.5, - buttonSizeH * 0.3));
            star3->setPosition(ccp(buttonSizeW * 0.9, - buttonSizeH * 0.1));

            
            item->addChild(star1);
            item->addChild(star3);
            item->addChild(star2);
            
            
            menu->addChild(item);
        }
        
        menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.1);
        menu->setPosition(ccp(screenSize.width * 0.5, scrollView->getContentSize().height - screenSize.height * kRowDistance * (i+2)));
        scrollView->addChild(menu);
        
    }
}

bool StageLayer::init() {
    
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    setKeypadEnabled(true);
    GameManager::sharedGameManager()->hideBanner();


    lastEnabledStage = 0;
    totalStars = 0;
    
    screenSize = CCDirector::sharedDirector()->getWinSize();
    buttonSizeH = screenSize.width * 0.1;
    buttonSizeW = screenSize.width * 0.122;
    
    int stageCount = GameManager::sharedGameManager()->getAdventureStageCount();
    int rowCount = stageCount / 4;

    
    scrollView = CCScrollView::create(ccp(screenSize.width, screenSize.height));
    scrollView->setContentSize(CCSize(screenSize.width, (rowCount + 2) * screenSize.height * kRowDistance));
    scrollView->setDirection(kCCScrollViewDirectionVertical);
    //scrollView->setAnchorPoint(ccp(0.0, 1.0));
    scrollView->setPosition(ccp(0.0, 0.0));
    scrollView->setContentOffset(ccp(0.0, screenSize.height - scrollView->getContentSize().height ));
    scrollView->setBounceable(true);
    
    this->addChild(scrollView);
    

    CCSprite *bg = CCSprite::create("iphone_bg2.png");
    bg->setPosition(ccp(screenSize.width * 0.5,screenSize.height * 0.5));
    bg->setScaleY(screenSize.height / 568.0);
    this->addChild(bg);
    
    layoutButtons();
    
    layoutControls();
    
    
    return true;
}

void StageLayer::keyBackClicked() {
    exitTouched(NULL);
}
