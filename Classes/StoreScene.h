//
//  StoreScene.h
//  SimpleGame
//
//  Created by SimplyGame on 9/6/13.
//
//

#ifndef __SimpleGame__StoreScene__
#define __SimpleGame__StoreScene__


#include "cocos2d.h"
// #define kStoreLayerTag 50

using namespace cocos2d;

class  StoreScene : public CCScene
{
public:
    virtual bool init();
    void updateCoins();

    CREATE_FUNC(StoreScene);
};

#endif /* defined(__SimpleGame__StoreScene__) */
