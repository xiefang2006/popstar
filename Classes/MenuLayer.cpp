//
//  MenuLayer.cpp
//  SimpleGame
//
//  Created by SimplyGame on 9/3/13.
//
//


#include "Popstar.h"
#include "MenuLayer.h"
#include "GameManager.h"
#include "GameScene.h"
#include "PauseScene.h"
#include "StoreScene.h"
#include "StageScene.h"

#define kModeMenuTag 501
#define kInfoTag 502
#define kDetailTag 503

#define kPopupMenuTag 504

#define kLCImgTag 550
#define kRCImgTag 551
#define kLCMenuTag 552
#define kRCMenuTag 553
#define kLCPopupMenuTag 554
#define kRCPopupMenuTag 555
#define kLCMenuBGTag 556
#define kSoundMenuTag 557
#define kMusicMenuTag 558
#define kGCMenuTag 559
#define kSunBusrtTag 560

#define kZTitle 4

#define kZLC 3
#define kZSound 2
#define kZMusic 2
#define kZGC 2
#define kZLCBG 1
#define kZQ 4

// Coins
#define kCoinsX     0.92
#define kCoinsY     0.95
#define kCCountX    0.85
#define kCCountY    0.95
#define kCoinsTag 1001
#define kCoinsMenuTag 1002


void MenuLayer::continueSelected(cocos2d::CCNode *pNode) {

    if (pNode->getTag() == 2) {
        GameManager::sharedGameManager()->gameState.stage = 0;
    }
    this->removeChildByTag(kPopupMenuTag);
    playStartGameAnimation();
}


void MenuLayer::useCoinSelected(cocos2d::CCNode *pNode) {
    
    if (pNode->getTag() == 1) {
        GameManager::sharedGameManager()->hostMatch();
    }
    else {
        GameManager::sharedGameManager()->gameMode = 0;
    }
    this->removeChildByTag(kPopupMenuTag);
    this->setTouchEnabled(true);
}


void MenuLayer::rateSelected(cocos2d::CCNode *pNode) {
    
    if (pNode->getTag() == 1) {
        GameManager::sharedGameManager()->openURL(kRateUrl);
        CCUserDefault::sharedUserDefault()->setBoolForKey("didRating25", true);
        GameManager::sharedGameManager()->saveGameSetting();
    }
    this->removeChildByTag(kPopupMenuTag);
    this->setTouchEnabled(true);
}

/*
void MenuLayer::popupFirstTimeInfo() {
    this->setTouchEnabled(false);
    
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kPopupMenuTag);
    this->addChild(background, kZQ);
    
    CCSprite *background_img = CCSprite::create("menu_bg.png");
    background_img->setAnchorPoint(ccp(0.0f, 0.5f));
    background_img->setPosition(ccp(0.0, screenSize.height * 0.45));
    background->addChild(background_img);
    
    CCLabelBMFont* mode = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    mode->setScale(0.7);
    
    CCLabelBMFont* detail = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    detail->setScale(0.6);
    
    CCSprite* modeImage = NULL;
    
    const char* filename;
    const char* key;
    const char* keyDetail;
    switch (GameManager::sharedGameManager()->gameMode) {
        case 0:
            key = "targetInfo";
            keyDetail = "targetInfoDetail";
            filename = "target.png";
            break;
        case 1:
            key = "timeInfo";
            keyDetail = "timeInfoDetail";
            filename = "timer.png";
            
            break;
        case 2:
            key = "moveInfo";
            keyDetail = "moveInfoDetail";
            filename = "moves.png";
            
            break;
    }
    modeImage = CCSprite::create(filename);
    mode->setString(GameManager::sharedGameManager()->getLocalizedString(key));
    detail->setString(GameManager::sharedGameManager()->getLocalizedString(keyDetail));
    
    mode->setAnchorPoint(ccp(0.0, 0.5));
    detail->setAnchorPoint(ccp(0.0, 0.5));
    modeImage->setAnchorPoint(ccp(0.0, 0.5));
    
    modeImage->setPosition(ccp(screenSize.width * 0.1, 100 * 0.7));
    mode->setPosition(ccp(screenSize.width * 0.25, 100 * 0.7));
    detail->setPosition(ccp(screenSize.width * 0.1, 100 * 0.3));
    
    background_img->addChild(modeImage);
    background_img->addChild(mode);
    background_img->addChild(detail);

}
 */


void MenuLayer::popupUseCoinQuery() {
    this->setTouchEnabled(false);
    
    
    
    
    
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kPopupMenuTag);
    this->addChild(background, kZQ);
    
    CCSprite *background_img = CCSprite::create("menu_bg.png");
    background_img->setAnchorPoint(ccp(0.0f, 0.5f));
    background_img->setPosition(ccp(0.0, screenSize.height * 0.45));
    background->addChild(background_img);
    
    CCLabelBMFont* query = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    query->setScale(0.7);
    
    GameManager* gm = GameManager::sharedGameManager();
    CCString* queryStr = CCString::createWithFormat(gm->getLocalizedString("vsInfoDetail"),
                                                    gm->gameState.stage);
    
    query->setString(queryStr->getCString());
    query->setAnchorPoint(ccp(0.5f, 0.5f));
#ifdef __Halloween__
    query->setPosition(screenSize.width * 0.5, 120 * 0.7);
#else
    query->setPosition(screenSize.width * 0.5, 105 * 0.7);
#endif
    background_img->addChild(query);
    
    CCMenuItemImage* yesLabel = CCMenuItemImage::create("tick.png", "tick.png", this, menu_selector(MenuLayer::useCoinSelected));
    CCMenuItemImage* noLabel = CCMenuItemImage::create("cross.png", "cross.png", this, menu_selector(MenuLayer::useCoinSelected));
    yesLabel->setTag(1);
    noLabel->setTag(2);
    
    CCMenu* menu = CCMenu::create(yesLabel, noLabel, NULL);
    menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.2);
    
    menu->setAnchorPoint(ccp(0.5f, 0.5f));
#ifdef __Halloween__
    menu->setPosition(screenSize.width * 0.5, 105 * 0.4);
#else
    menu->setPosition(screenSize.width * 0.5, 105 * 0.3);
#endif
    background_img->addChild(menu);
    
    /*
     CCSprite* arrow = CCSprite::create("arrow.png");
     arrow->setScale(32.0 / 128.0);
     
     arrow->setPosition(ccp(screenSize.width * 0.5, 0));
     background->addChild(arrow);
     */
}


void MenuLayer::popupContinueQuery() {
    this->setTouchEnabled(false);
    

    
   
  
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kPopupMenuTag);
    this->addChild(background, kZQ);

    CCSprite *background_img = CCSprite::create("menu_bg.png");
    background_img->setAnchorPoint(ccp(0.0f, 0.5f));
    background_img->setPosition(ccp(0.0, screenSize.height * 0.45));
    background->addChild(background_img);
      
    CCLabelBMFont* query = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    query->setScale(0.7);
    
    GameManager* gm = GameManager::sharedGameManager();
    CCString* queryStr = CCString::createWithFormat(gm->getLocalizedString("continueQuery"),
                                                    gm->gameState.stage);
    
    query->setString(queryStr->getCString());
    query->setAnchorPoint(ccp(0.5f, 0.5f));
#ifdef __Halloween__
    query->setPosition(screenSize.width * 0.5, 120 * 0.7);
#else
    query->setPosition(screenSize.width * 0.5, 105 * 0.7);
#endif
    background_img->addChild(query);
    
    CCMenuItemImage* yesLabel = CCMenuItemImage::create("tick.png", "tick.png", this, menu_selector(MenuLayer::continueSelected));
    CCMenuItemImage* noLabel = CCMenuItemImage::create("cross.png", "cross.png", this, menu_selector(MenuLayer::continueSelected));
    yesLabel->setTag(1);
    noLabel->setTag(2);
    
    CCMenu* menu = CCMenu::create(yesLabel, noLabel, NULL);
    menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.2);
    
    menu->setAnchorPoint(ccp(0.5f, 0.5f));
#ifdef __Halloween__
    menu->setPosition(screenSize.width * 0.5, 105 * 0.4);
#else
    menu->setPosition(screenSize.width * 0.5, 105 * 0.3);
#endif
    background_img->addChild(menu);
    
    /*
    CCSprite* arrow = CCSprite::create("arrow.png");
    arrow->setScale(32.0 / 128.0);
    
    arrow->setPosition(ccp(screenSize.width * 0.5, 0));
    background->addChild(arrow);
    */
}

void MenuLayer::popupRateQuery() {
    this->setTouchEnabled(false);
    
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kPopupMenuTag);
    this->addChild(background, kZQ);
    
    CCSprite *background_img = CCSprite::create("menu_bg.png");
    background_img->setAnchorPoint(ccp(0.0f, 0.5f));
    background_img->setPosition(ccp(0.0, screenSize.height * 0.45));
    background->addChild(background_img);
    
    CCLabelBMFont* query = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    query->setScale(0.7);
    
    GameManager* gm = GameManager::sharedGameManager();
    CCString* queryStr = CCString::create(gm->getLocalizedString("askRate"));
    
    query->setString(queryStr->getCString());
    query->setAnchorPoint(ccp(0.5f, 0.5f));
#ifdef __Halloween__
    query->setPosition(screenSize.width * 0.5, 120 * 0.7);
#else
    query->setPosition(screenSize.width * 0.5, 105 * 0.7);
#endif
    background_img->addChild(query);
    
    CCMenuItemImage* yesLabel = CCMenuItemImage::create("tick.png", "tick.png", this, menu_selector(MenuLayer::rateSelected));
    CCMenuItemImage* noLabel = CCMenuItemImage::create("cross.png", "cross.png", this, menu_selector(MenuLayer::rateSelected));
    yesLabel->setTag(1);
    noLabel->setTag(2);
    
    CCMenu* menu = CCMenu::create(yesLabel, noLabel, NULL);
    menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.2);
    
    menu->setAnchorPoint(ccp(0.5f, 0.5f));
#ifdef __Halloween__
    menu->setPosition(screenSize.width * 0.5, 105 * 0.4);
#else
    menu->setPosition(screenSize.width * 0.5, 105 * 0.3);
#endif
    background_img->addChild(menu);
    
    /*
     CCSprite* arrow = CCSprite::create("arrow.png");
     arrow->setScale(32.0 / 128.0);
     
     arrow->setPosition(ccp(screenSize.width * 0.5, 0));
     background->addChild(arrow);
     */
}


void MenuLayer::startGame() {

    // TEST END
    
    if (GameManager::sharedGameManager()->gameMode == 3) {
        StageScene* stageScene = StageScene::create();
        CCDirector::sharedDirector()->replaceScene(CCTransitionSlideInR::create(0.5, stageScene));

    }
    else {
        GameScene* gameScene = GameScene::create();
        CCDirector::sharedDirector()->replaceScene(CCTransitionSlideInR::create(0.5, gameScene));
    }
}

void MenuLayer::playStartGameAnimation() {
    
#ifdef __Fruit__
    GameManager::sharedGameManager()->playSoundEffect("select.wav");
#else
    
    GameManager::sharedGameManager()->playSoundEffect("select.wav");

    /*
    if ((GameManager::sharedGameManager()->gameMode == 1) || (GameManager::sharedGameManager()->gameMode == 2)) {
        if (GameManager::sharedGameManager()->gameSetting.coins < 5) {
            this->addCoinButtonTouched();
            return;
        }
        else {
            GameManager::sharedGameManager()->gameSetting.coins-=5;
            GameManager::sharedGameManager()->saveGameSetting();
            updateCoins();
            GameManager::sharedGameManager()->playSoundEffect("coin.mp3");
        }
    }
    else {
        GameManager::sharedGameManager()->playSoundEffect("select.wav");
    }
     */
#endif
    
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        //this->scheduleOnce(schedule_selector(MenuLayer::startGame), 0.3);

        
        if (CCUserDefault::sharedUserDefault()->getBoolForKey("didShowVSDetail") == false) {
            CCUserDefault::sharedUserDefault()->setBoolForKey("didShowVSDetail", true);
            CCUserDefault::sharedUserDefault()->flush();
            popupUseCoinQuery();
            return;
        }

        GameManager::sharedGameManager()->hostMatch();
        return;
    }
    
    
#ifdef __Fruit__
    // do nothing with the sunburst
#else
    
    CCSprite* sunburst = (CCSprite*)this->getChildByTag(kSunBusrtTag);
    CCFadeOut* fadeOut = CCFadeOut::create(0.2);
    sunburst->runAction(fadeOut);
#endif
    
    

    CCMenu* rcMenu = (CCMenu*)this->getChildByTag(kRCMenuTag);
    if (rcMenu) {
        CCFadeOut* fadeOut2 = CCFadeOut::create(0.2);
        rcMenu->runAction(fadeOut2);
    }

    
    /*
    CCSprite* coin = CCSprite::create("coin.png");
    coin->setPosition(ccp(screenSize.width * kCoinsX, screenSize.height * kCoinsY));
    CCMoveTo* move = CCMoveTo::create(0.3, ccp(screenSize.width * 0.5, screenSize.height * 0.3));
    this->addChild(coin, -5);
    coin->runAction(move);
    */
    this->scheduleOnce(schedule_selector(MenuLayer::startGame), 0.3);
}

void MenuLayer::startGameSelected(CCNode* pNode) {
    
    if (this->isTouchEnabled() == false) {
        return;
    }
    // Has unfinished game
    if ((GameManager::sharedGameManager()->gameMode == 0) &&
        (GameManager::sharedGameManager()->gameState.stage != 0)) {
        GameManager::sharedGameManager()->playSoundEffect("select.wav");
        popupContinueQuery();
    }
    // play adventure mode, but not rated 1.5 version and server side shows new
    else if ((GameManager::sharedGameManager()->gameMode == 3) &&
             (GameManager::sharedGameManager()->getServerBoolean("lockAdventure") == true) &&
             (CCUserDefault::sharedUserDefault()->getBoolForKey("didRating25") == false)) {
        popupRateQuery();
    }
    else {
        playStartGameAnimation();
    }
    
    

}

void MenuLayer::scaleUpBG() {
        CCScaleTo* scaleUp = CCScaleTo::create(0.2, 1);
        CCSprite * lcMenuBG = (CCSprite*)this->getChildByTag(kLCMenuBGTag);
        lcMenuBG->runAction(scaleUp);
}

void MenuLayer::scaleDownBG() {
        CCScaleTo* scaleDown = CCScaleTo::create(0.2, 0);
        CCSprite * lcMenuBG = (CCSprite*)this->getChildByTag(kLCMenuBGTag);
        lcMenuBG->runAction(scaleDown);
}

void MenuLayer::fiLCMenu() {

        CCFadeIn* fi1 = CCFadeIn::create(0.2);
        CCMenu* soundMenu = (CCMenu*)this->getChildByTag(kSoundMenuTag);
        soundMenu->runAction(fi1);

        CCFadeIn* fi2 = CCFadeIn::create(0.2);
        CCMenu* musicMenu = (CCMenu*)this->getChildByTag(kMusicMenuTag);
        musicMenu->runAction(fi2);

    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CCFadeTo* fi3 = CCFadeTo::create(0.2, 128);
#else
    CCFadeIn* fi3 = CCFadeIn::create(0.2);
#endif
        CCMenu* gcMenu = (CCMenu*)this->getChildByTag(kGCMenuTag);
        gcMenu->runAction(fi3);
}

void MenuLayer::foLCMenu() {

        CCFadeOut* fo1 = CCFadeOut::create(0.2);
        CCFadeOut* fo2 = CCFadeOut::create(0.2);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CCFadeTo* fo3 = CCFadeTo::create(0.2,0);
#else
        CCFadeOut* fo3 = CCFadeOut::create(0.2);
#endif

        CCMenu* soundMenu = (CCMenu*)this->getChildByTag(kSoundMenuTag);
        CCMenu* musicMenu = (CCMenu*)this->getChildByTag(kMusicMenuTag);
        CCMenu* gcMenu = (CCMenu*)this->getChildByTag(kGCMenuTag);

        soundMenu->runAction(fo1);
        musicMenu->runAction(fo2);
        gcMenu->runAction(fo3);
}

void MenuLayer::fiLC() {
        CCFadeTo * ft = CCFadeTo::create(0.2, 255);
        CCMenu* lcMenu = (CCMenu*)this->getChildByTag(kLCMenuTag);
        lcMenu->runAction(ft);
}

void MenuLayer::foLC() {
        CCFadeTo * ft = CCFadeTo::create(0.2, 160);
        CCMenu* lcMenu = (CCMenu*)this->getChildByTag(kLCMenuTag);
        lcMenu->runAction(ft);
}

void MenuLayer::removeLCMenu() {
        // this->removeChildByTag(kLCPopupMenuTag);
        this->removeChildByTag(kLCMenuBGTag);
        this->removeChildByTag(kSoundMenuTag);
        this->removeChildByTag(kMusicMenuTag);
        this->removeChildByTag(kGCMenuTag);
}

void MenuLayer::leftCornerSelected(CCNode* pNode) {
    
    if (this->isTouchEnabled() == false) {
        return;
    }

    GameManager::sharedGameManager()->playSoundEffect("select.wav");

    // GameManager::sharedGameManager()->playSoundEffect("select.wav");

    if(isLCOn) {
        /*
        CCMenu* lcPopupMenu = (CCMenu*)this->getChildByTag(kLCPopupMenuTag);
        CCFadeOut* fadeout = CCFadeOut::create(0.5);
        CCCallFunc* rm = CCCallFunc::create(this, callfunc_selector(MenuLayer::removeLCMenu));
        CCSequence* sequence1= CCSequence::createWithTwoActions(fadeout, rm);
        lcPopupMenu->runAction(sequence1);
        */

/*
        CCMenu* lcPopupMenu = (CCMenu*)this->getChildByTag(kLCPopupMenuTag);
        CCFadeOut* fadeout = CCFadeOut::create(0.5);
        CCCallFunc* rm = CCCallFunc::create(this, callfunc_selector(MenuLayer::removeLCMenu));
        CCSequence* sequence1= CCSequence::createWithTwoActions(fadeout, rm);
        lcPopupMenu->runAction(sequence1);
        */

        CCCallFunc* bg = CCCallFunc::create(this, callfunc_selector(MenuLayer::scaleDownBG));
        CCCallFunc* fo = CCCallFunc::create(this, callfunc_selector(MenuLayer::foLCMenu));
        CCCallFunc* flc = CCCallFunc::create(this, callfunc_selector(MenuLayer::foLC));
        CCCallFunc* rm = CCCallFunc::create(this, callfunc_selector(MenuLayer::removeLCMenu));
        CCDelayTime* dly1 = CCDelayTime::create(0.25);
        CCDelayTime* dly2 = CCDelayTime::create(0.2);
        CCSequence* hide = CCSequence::create(fo, dly1, bg, dly2, rm, flc, NULL);
        this->runAction(hide);
        isLCOn = false;

/*
        CCMenu* lcMenu = (CCMenu*)this->getChildByTag(kLCMenuTag);
        lcMenu->setOpacity(160);
        */
    }
    else {

        CCSprite * lcMenuBG = CCSprite::create("lcMenu_bg.png");
        lcMenuBG->setAnchorPoint(ccp(0, 0));
        lcMenuBG->setPosition(ccp(0,0));
        lcMenuBG->setTag(kLCMenuBGTag);
        lcMenuBG->setScale(0.0f);
        lcMenuBG->setOpacity(192);
        this->addChild(lcMenuBG, kZLCBG);

        CCMenuItemImage* sound;
        if (GameManager::sharedGameManager()->gameSetting.isSoundOff) {
            sound = CCMenuItemImage::create("no_sound.png", "no_sound.png", this, menu_selector(MenuLayer::buttonTouched));

        }
        else {
            sound = CCMenuItemImage::create("sound.png", "sound.png", this, menu_selector(MenuLayer::buttonTouched));
        }
        sound->setAnchorPoint(ccp(0.5f, 0.5f));

        CCMenuItemImage* music;
        if (GameManager::sharedGameManager()->gameSetting.isMusicOff) {
            music = CCMenuItemImage::create("no_music.png", "no_music.png", this, menu_selector(MenuLayer::buttonTouched));
        }
        else {
            music = CCMenuItemImage::create("music.png", "music.png", this, menu_selector(MenuLayer::buttonTouched));
        }
        music->setAnchorPoint(ccp(0.5f, 0.5f));


        /*
        CCMenuItemImage* help;
        help = CCMenuItemImage::create("help.png", "help.png", this, menu_selector(MenuLayer::buttonTouched));
        help->setAnchorPoint(ccp(0, 0));
        */

        CCMenuItemImage* gamecenter = CCMenuItemImage::create("ranking.png", "ranking.png", this, menu_selector(MenuLayer::buttonTouched));
        gamecenter->setAnchorPoint(ccp(0.5f, 0.5f));

        sound->setTag(0);
        music->setTag(1);
        // help->setTag(2);
        gamecenter->setTag(3);

// TO DO : add zoom ratio for positions and bg

        CCMenu* soundMenu = CCMenu::create(sound, NULL);
        soundMenu->setAnchorPoint(ccp(0.5f, 0.5f));
        soundMenu->setPosition(20,  72);
        soundMenu->setTag(kSoundMenuTag);
        soundMenu->setOpacity(0);
        this->addChild(soundMenu, kZSound);

        CCMenu* musicMenu = CCMenu::create(music, NULL);
        musicMenu->setAnchorPoint(ccp(0.5f, 0.5f));
        musicMenu->setPosition(53, 53);
        musicMenu->setTag(kMusicMenuTag);
        musicMenu->setOpacity(0);
        this->addChild(musicMenu, kZMusic);

        CCMenu* gcMenu = CCMenu::create(gamecenter, NULL);
        gcMenu->setAnchorPoint(ccp(0.5f, 0.5f));
        gcMenu->setPosition(72, 20);
        gcMenu->setTag(kGCMenuTag);
        gcMenu->setOpacity(0);
        this->addChild(gcMenu, kZGC);


        // CCMenu* lcPopupMenu = CCMenu::create(sound, music, help, NULL);
        /*
        CCMenu* lcPopupMenu = CCMenu::create(sound, music, gamecenter, NULL);
        lcPopupMenu->alignItemsHorizontallyWithPadding(screenSize.width * 0.05);
        lcPopupMenu->setAnchorPoint(ccp(0, 0));
        lcPopupMenu->setPosition(64 + screenSize.width * 0.10, 4);
        lcPopupMenu->setTag(kLCPopupMenuTag);
        lcPopupMenu->setOpacity(0);
        this->addChild(lcPopupMenu);

        CCFadeIn* fadein = CCFadeIn::create(0.5);
        lcPopupMenu->runAction(fadein);

         */

        CCCallFunc* bg = CCCallFunc::create(this, callfunc_selector(MenuLayer::scaleUpBG));
        CCCallFunc* fi = CCCallFunc::create(this, callfunc_selector(MenuLayer::fiLCMenu));
        CCCallFunc* flc = CCCallFunc::create(this, callfunc_selector(MenuLayer::fiLC));
        CCDelayTime* dly1 = CCDelayTime::create(0.05);
        CCDelayTime* dly2 = CCDelayTime::create(0.25);
        CCSequence* show = CCSequence::create(flc, dly1, bg, dly2, fi, NULL);
        this->runAction(show);


        isLCOn = true;
    }


}



void MenuLayer::rightCornerSelected(CCNode* pNode) {
    // VideoAd
    if (GameManager::sharedGameManager()->getServerBoolean("videoInMenu") == true) {
        GameManager::sharedGameManager()->showVideoAd();
        return;
    }

    // Promo
    const char* url = GameManager::sharedGameManager()->getServerString("promoUrl");
    
    if (( url == NULL ) || (strlen(url) == 0)){
        GameManager::sharedGameManager()->openURL("http://fleetseven.com");
    }
    else if (strcmp(url, "wall") == 0) {
        GameManager::sharedGameManager()->showWall();
    }
    else if ((url[0] <= '9') && (url[0] >= '0')) {
        GameManager::sharedGameManager()->presendAppstore(url);
    }
    else {
        GameManager::sharedGameManager()->openURL(url);
    }

    pNode->removeFromParent();

}

void MenuLayer::buttonTouched(CCNode *pNode) {

    if (this->isTouchEnabled() == false) {
        return;
    }

    CCMenuItemImage* item = (CCMenuItemImage*)pNode;
    if (item) {
        int tag = item->getTag();

        switch (tag) {
            case 0:
                {
                    bool isOff = GameManager::sharedGameManager()->gameSetting.isSoundOff;
                    isOff = !isOff;

                    CCSprite* newImage;
                    if (isOff) {
                        newImage = CCSprite::create("no_sound.png");
                    }
                    else {
                        newImage = CCSprite::create("sound.png");
                    }

                    item->setNormalImage(newImage);
                    GameManager::sharedGameManager()->gameSetting.isSoundOff = isOff;
                }
                break;
            case 1:
                {
                    bool isOff = GameManager::sharedGameManager()->gameSetting.isMusicOff;
                    isOff = !isOff;

                    CCSprite* newImage;
                    if (isOff) {
                        newImage = CCSprite::create("no_music.png");
                    }
                    else {
                        newImage = CCSprite::create("music.png");
                    }

                    item->setNormalImage(newImage);
                    GameManager::sharedGameManager()->gameSetting.isMusicOff = isOff;
                    if (isOff) {
                        GameManager::sharedGameManager()->stopMusic();
                    }
                    else {
                        GameManager::sharedGameManager()->continueMusic();
                    }
                }
                break;
            case 2:
                {
                    CCDirector::sharedDirector()->pushScene(CCTransitionSlideInT::create(0.5, PauseScene::create()));
                }
                break;
            case 3:
                {
                    // GameManager::sharedGameManager()->playSoundEffect("select.wav");
                    GameManager::sharedGameManager()->showGameCenter();
                }
                break;
            default:
                break;
        }
    }

    GameManager::sharedGameManager()->playSoundEffect("select.wav");
    GameManager::sharedGameManager()->saveGameSetting();
}


void MenuLayer::menuSelected(CCNode* pNode) {
    
    if (this->isTouchEnabled() == false) {
        return;
    }
    
    //playCoinsAnimation(5);
    
    

    
    CCMenuItemLabel* item = (CCMenuItemLabel*)pNode;
    if (item) {
        
//#ifdef __Fruit__
        int tag = item->getTag();
        GameManager::sharedGameManager()->gameMode = tag;
        GameManager::sharedGameManager()->saveGameSetting();
        startGameSelected(NULL);
  
        /*
#else
        
        GameManager::sharedGameManager()->playSoundEffect("select.wav");
        int tag = item->getTag();
        GameManager::sharedGameManager()->gameMode = tag;
        GameManager::sharedGameManager()->saveGameSetting();
        
        setItemSeleted(0, false);
        setItemSeleted(1, false);
        setItemSeleted(2, false);
        setItemSeleted(3, false);
        setItemSeleted(4, false);

        setItemSeleted(tag, true);
        
        CCScaleTo* scaleUp = CCScaleTo::create(0.15, 1.3);
        CCScaleTo* scaleDown = CCScaleTo::create(0.15, 1.2);
        CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleUp, scaleDown);
        item->runAction(scaleUpAndDown);
        
#endif
         */

        /*
        CCLabelBMFont *text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
        text_info->stopAllActions();
        const char* key;
        switch (tag) {
            case 0:
                key = "targetInfo";
                break;
            case 1:
                key = "timeInfo";
                break;
            case 2:
                key = "moveInfo";
                break;
        }
        text_info->setString(GameManager::sharedGameManager()->getLocalizedString(key));
        CCFadeIn* fadein = CCFadeIn::create(0.5);
        CCDelayTime* delay = CCDelayTime::create(2.0);
        CCFadeOut* fadeout = CCFadeOut::create(0.5);
        text_info->runAction(CCSequence::create(fadein, delay, fadeout, NULL)); //(fadein, fadeout));
         */

        
        //CCDirector::sharedDirector()->replaceScene(GameScene::create());
    }
    

}

void MenuLayer::setItemSeleted(int itemTag, bool selected) {
    
    CCMenuItemImage* item = (CCMenuItemImage*)this->getChildByTag(kModeMenuTag)->getChildByTag(itemTag);
    if (item) {
        
        item->setOpacity(96);
        item->stopAllActions();
        item->setScale(1.0);
        
        /*
        item->setScale(1.0);
        */
        if (selected) {
            
            /*
            CCBlink* blink = CCBlink::create(1.0, 1);
            CCRepeatForever* repeatAction = CCRepeatForever::create(blink);
            item->runAction(repeatAction);
             */
            
            item->setOpacity(255);
            item->setScale(1.2);
            /*
            CCScaleBy* scaleUp = CCScaleBy::create(0.5, 1.25);
            CCScaleBy* scaleDown = CCScaleBy::create(0.5, 0.8);
            CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleUp, scaleDown);
            CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);
            
            item->runAction(repeatAction);
             */
            
            CCLabelBMFont *text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
            CCLabelBMFont *text_detail = (CCLabelBMFont*)this->getChildByTag(kDetailTag);

            text_info->stopAllActions();
            text_detail->stopAllActions();
            
            const char* key;
            const char* keyDetail;
            switch (itemTag) {
                case 0:
                    key = "targetInfo";
                    keyDetail = "targetInfoDetail";
                    break;
                case 1:
                    key = "timeInfo";
                    keyDetail = "timeInfoDetail";

                    break;
                case 2:
                    key = "moveInfo";
                    keyDetail = "moveInfoDetail";
                
                    break;
                case 3:
                    key = "adventureInfo";
                    keyDetail = "adventureInfoDetail";
                    break;
                case 4:
                    key = "timeInfo";
                    keyDetail = "timeInfoDetail";
                    break;
            }
            text_info->setString(GameManager::sharedGameManager()->getLocalizedString(key));
            CCFadeIn* fadein = CCFadeIn::create(0.5);
            CCFadeOut* fadeout = CCFadeOut::create(1.0);
            CCDelayTime* delay = CCDelayTime::create(3.0);
            CCSequence* action = CCSequence::create(fadein, delay, fadeout, NULL);
            text_info->runAction(action);
            
            
            text_detail->setString(GameManager::sharedGameManager()->getLocalizedString(keyDetail));
            CCFadeIn* fadein2 = CCFadeIn::create(0.5);
            CCFadeOut* fadeout2 = CCFadeOut::create(1.0);
            CCDelayTime* delay2 = CCDelayTime::create(3.0);
            CCSequence* action2 = CCSequence::create(fadein2, delay2, fadeout2, NULL);
            text_detail->runAction(action2);
            
            
            
            //CCDelayTime* delay = CCDelayTime::create(2.0);
            //CCFadeOut* fadeout = CCFadeOut::create(0.5);
            //text_info->runAction(CCSequence::create(fadein, delay, fadeout, NULL)); //(fadein, fadeout));
            
        }
    }
}

bool MenuLayer::init() {
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    GameManager::sharedGameManager()->gameMode = 0;
    
    setKeypadEnabled(true);

    GameManager::sharedGameManager()->hideBanner();
    screenSize = CCDirector::sharedDirector()->getWinSize();
    
    
    // CCSprite *bg = CCSprite::create("front.png");
    CCSprite *bg = CCSprite::create("iphone_bg.png");
    bg->setPosition(ccp(screenSize.width * 0.5,screenSize.height * 0.5));
    bg->setScaleY(screenSize.height / 568.0);
    // bg->setFlipX(true);
    this->addChild( bg , -10 );
    
    /*
    CCLayerColor* backgroundLayer = CCLayerColor::create(ccc4(0, 0, 0, 192));
    this->addChild(backgroundLayer, -5);
    */
    
    const char* titlefilename = GameManager::sharedGameManager()->getLocalizedString("title.png");
    CCSprite *title = CCSprite::create(titlefilename);
    title->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.75));
    this->addChild(title, kZTitle);

    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        title->setScale(1.0);
    }
    else {
        title->setScale(1.2);
    }


#ifdef __Fruit__
    
#else
    
    
    CCSprite* sunburst = CCSprite::create("bg_sunburst.png");
#ifdef __Star__
    sunburst->setPosition(ccp (screenSize.width * 0.5, screenSize.height * 0.75));
    
    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        sunburst->setScale(1.5);
    }
    else {
        sunburst->setScale(2.0);
    }
#else
    sunburst->setPosition(ccp (screenSize.width * 0.5, screenSize.height * 0.68));
    sunburst->setScale(1.32);

#endif
    

    sunburst->setOpacity(192);
    //sunburst->setTag(kGameOverBackgroundTag);
    this->addChild(sunburst, -1);
    sunburst->setTag(kSunBusrtTag);
    CCRotateBy* rotate = CCRotateBy::create(5.0, 10.0);
    CCRepeatForever* repeat = CCRepeatForever::create(rotate);
    sunburst->runAction(repeat);
    
#endif
    
    CCLabelBMFont *text_info = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    text_info->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.54));
    text_info->setTag(kInfoTag);
    text_info->setScale(0.7);
    text_info->setOpacity(0);
    this->addChild(text_info);
    
    
    CCLabelBMFont *text_detail = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    text_detail->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.49));
    text_detail->setTag(kDetailTag);
    text_detail->setScale(0.6);
    text_detail->setOpacity(0);
    this->addChild(text_detail);
    

    
    CCMenuItemImage *classic = CCMenuItemImage::create("target.png", "target.png", this, menu_selector(MenuLayer::menuSelected));
    CCMenuItemImage *time = CCMenuItemImage::create("timer.png", "timer.png", this, menu_selector(MenuLayer::menuSelected));
    CCMenuItemImage *move = CCMenuItemImage::create("moves.png", "moves.png", this, menu_selector(MenuLayer::menuSelected));
    CCMenuItemImage *adventure = CCMenuItemImage::create("adventures.png", "adventures.png", this, menu_selector(MenuLayer::menuSelected));
    CCMenuItemImage *vs = CCMenuItemImage::create("battle.png", "battle.png", this, menu_selector(MenuLayer::menuSelected));



    /*
    CCLabelTTF* classicMode = CCLabelTTF::create("Classic - Meet Target", kGameFont, 24);
    CCLabelTTF* timeMode = CCLabelTTF::create("Time - 60 Secs", kGameFont, 24);
    CCLabelTTF* moveMode = CCLabelTTF::create("Move - 30 Moves", kGameFont, 24);
    
    CCMenuItemLabel* classic = CCMenuItemLabel::create(classicMode, this, menu_selector(MenuLayer::menuSelected));
    CCMenuItemLabel* time = CCMenuItemLabel::create(timeMode, this, menu_selector(MenuLayer::menuSelected));
    CCMenuItemLabel* move = CCMenuItemLabel::create(moveMode, this, menu_selector(MenuLayer::menuSelected));
    */
    
    classic->setTag(0);
    time->setTag(1);
    move->setTag(2);
    adventure->setTag(3);
    vs->setTag(4);

    CCMenu* menu = CCMenu::create(classic, time, move, adventure, vs, NULL);
    
    

    

#ifdef __Fruit__
    
    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        menu->alignItemsVerticallyWithPadding(screenSize.height * (-0.02));
    }
    else if ((screenSize.height / screenSize.width) == 1.5) {
        menu->alignItemsVerticallyWithPadding(screenSize.height * (-0.01));

    }
    else {
        menu->alignItemsVerticallyWithPadding(screenSize.height * 0.01);
    }
    CCLabelBMFont* classicLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("targetInfo"),
                                                        GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    classic->addChild(classicLabel);
    classicLabel->setScale(0.7);
    classicLabel->setPosition(classic->getContentSize().width * 0.55, classic->getContentSize().height * 0.5);
    
    CCLabelBMFont* timeLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("timeInfo"),
                                                        GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    time->addChild(timeLabel);
    timeLabel->setScale(0.7);
    timeLabel->setPosition(time->getContentSize().width * 0.55, time->getContentSize().height * 0.5);
    
    CCLabelBMFont* moveLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("moveInfo"),
                                                     GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    move->addChild(moveLabel);
    moveLabel->setScale(0.7);
    moveLabel->setPosition(move->getContentSize().width * 0.55, move->getContentSize().height * 0.5);
    
    CCLabelBMFont* adventureLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("adventureInfo"),
                                                     GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    adventure->addChild(adventureLabel);
    adventureLabel->setScale(0.7);
    adventureLabel->setPosition(adventure->getContentSize().width * 0.55, adventure->getContentSize().height * 0.5);
    
    CCLabelBMFont* vsLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("vsInfo"),
                                                          GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    vs->addChild(vsLabel);
    vsLabel->setScale(0.7);
    vsLabel->setPosition(vs->getContentSize().width * 0.55, vs->getContentSize().height * 0.5);
    
    menu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.35));
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    vs->setScale(0.0);
    menu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.3));
#endif

#else
    //menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.1);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    classic->setPosition(ccp(screenSize.width * 0.3, screenSize.height * 0.5));
    time->setPosition(ccp(screenSize.width * 0.7, screenSize.height * 0.5));
    move->setPosition(ccp(screenSize.width * 0.3, screenSize.height * 0.3));
    adventure->setPosition(ccp(screenSize.width * 0.7, screenSize.height * 0.3));
    vs->setPosition(ccp(screenSize.width * 1.0, screenSize.height * 0.3));
    vs->setScale(0.0);
    
    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        classic->setScale(0.7);
        time->setScale(0.7);
        move->setScale(0.7);
        adventure->setScale(0.7);
    }
    else {
        classic->setScale(0.8);
        time->setScale(0.8);
        move->setScale(0.8);
        adventure->setScale(0.8);
    }
    
#else
    classic->setPosition(ccp(screenSize.width * 0.23, screenSize.height * 0.5));
    time->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.5));
    move->setPosition(ccp(screenSize.width * 0.77, screenSize.height * 0.5));
    adventure->setPosition(ccp(screenSize.width * 0.23, screenSize.height * 0.3));
    vs->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.3));
    
    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        classic->setScale(0.7);
        time->setScale(0.7);
        move->setScale(0.7);
        adventure->setScale(0.7);
        vs->setScale(0.7);
    }
    else {
        classic->setScale(0.8);
        time->setScale(0.8);
        move->setScale(0.8);
        adventure->setScale(0.8);
        vs->setScale(0.8);
    }
#endif
    

    
    CCLabelBMFont* classicLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("targetInfo"),
                                                        GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    classic->addChild(classicLabel);
    classicLabel->setScale(0.7);
    classicLabel->setPosition(classic->getContentSize().width * 0.5, classic->getContentSize().height * (- 0.2));
    
    CCLabelBMFont* timeLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("timeInfo"),
                                                     GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    time->addChild(timeLabel);
    timeLabel->setScale(0.7);
    timeLabel->setPosition(time->getContentSize().width * 0.5, time->getContentSize().height * (- 0.2));
    
    CCLabelBMFont* moveLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("moveInfo"),
                                                     GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    move->addChild(moveLabel);
    moveLabel->setScale(0.7);
    moveLabel->setPosition(move->getContentSize().width * 0.5, move->getContentSize().height * (- 0.2));
    
    CCLabelBMFont* adventureLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("adventureInfo"),
                                                          GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    adventure->addChild(adventureLabel);
    adventureLabel->setScale(0.7);
    adventureLabel->setPosition(adventure->getContentSize().width * 0.5, adventure->getContentSize().height * (- 0.2));
    
    
    CCLabelBMFont* vsLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("vsInfo"),
                                                          GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    vs->addChild(vsLabel);
    vsLabel->setScale(0.7);
    vsLabel->setPosition(vs->getContentSize().width * 0.5, adventure->getContentSize().height * (- 0.2));
    
    menu->setPosition(ccp(0, 0));
    
    

    

#endif
    
    this->addChild(menu);
    menu->setTag(kModeMenuTag);
    
    

   
    
    
    /*
#ifdef __Fruit__

    
#else
    
    
    setItemSeleted(0, false);
    setItemSeleted(1, false);
    setItemSeleted(2, false);
    setItemSeleted(3, false);
    setItemSeleted(4, false);


    setItemSeleted(GameManager::sharedGameManager()->gameMode, true);

    //CCMenuItemImage* currentItem = (CCMenuItemImage*)menu->getChildByTag(GameManager::sharedGameManager()->gameMode);
    //currentItem->setOpacity(255);
    
    const char* filename = GameManager::sharedGameManager()->getLocalizedString("play.png");
    CCMenuItemImage* play = CCMenuItemImage::create(filename, filename, this, menu_selector(MenuLayer::startGameSelected));
    //play->setScale(1.2);
    
    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        play->setScale(0.8);
    }
    else {
        play->setScale(1);
    }
    
    
    CCScaleBy* scaleUp = CCScaleBy::create(1.0, 1.25);
    CCScaleBy* scaleDown = CCScaleBy::create(1.0, 0.8);
    CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleUp, scaleDown);
    CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);
    play->runAction(repeatAction);
    
    CCMenu* playMenu = CCMenu::create(play, NULL);
    

    
#ifdef __Star__
    playMenu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.25));
#else
    playMenu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.28));
#endif
    this->addChild(playMenu);
#endif
    */
    
    
    
    this->setTouchEnabled(true);

    


    const char* lcfn = "leftCorner.png";
    CCMenuItemImage* lcImg = CCMenuItemImage::create(lcfn, lcfn, this, menu_selector(MenuLayer::leftCornerSelected));
    lcImg->setAnchorPoint(ccp(0, 0));
    lcImg->setTag(kLCImgTag);
 
    CCScaleBy* scaleUpLC = CCScaleBy::create(1.5, 1.25);
    CCScaleBy* scaleDownLC = CCScaleBy::create(1.5, 0.8);
    
    if (screenSize.height == 693.0) { // iphone x make sure arrow is viewable
        scaleUpLC = CCScaleBy::create(1.5, 1.8);
        scaleDownLC = CCScaleBy::create(1.5, 1.0/1.8);
    }

    CCSequence* scaleUpAndDownLC = CCSequence::createWithTwoActions(scaleUpLC, scaleDownLC);
    CCRepeatForever* repeatActionLC = CCRepeatForever::create(scaleUpAndDownLC);
    lcImg->runAction(repeatActionLC);
    
    CCMenu* lcMenu = CCMenu::create(lcImg, NULL);
    lcMenu->setAnchorPoint(ccp(0, 0));
    // lcMenu->setPosition(ccp(screenSize.width * 0, screenSize.height * 0));
    lcMenu->setPosition(ccp(0, 0));
    lcMenu->setTag(kLCMenuTag);
    lcMenu->setOpacity(160);
    this->addChild(lcMenu, kZLC);
 
    isLCOn = false;
    




    

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CCMenuItemImage* addCoinButton = CCMenuItemImage::create("coins.png", "coins.png", this, menu_selector(MenuLayer::addCoinButtonTouched));
#else
    CCMenuItemImage* addCoinButton = CCMenuItemImage::create("money_plus.png", "money_plus.png", this, menu_selector(MenuLayer::addCoinButtonTouched));
#endif
    // addCoinButton->setScale(0.4);
    CCMenu* addCoinMenu = CCMenu::create(addCoinButton, NULL);
    addCoinMenu->setPosition(ccp(screenSize.width * kCoinsX, screenSize.height * kCoinsY));
    addCoinMenu->setTag(kCoinsMenuTag);
    //addCoinMenu->setOpacity(0);
    this->addChild(addCoinMenu);
    
    int coinsCount = GameManager::sharedGameManager()->gameSetting.coins;
    // CCLabelBMFont* coinsLabel = CCLabelBMFont::create(to_string(coinsCount).c_str(), kGameFont, 15);
    CCLabelBMFont* coinsLabel = CCLabelBMFont::create(to_string(coinsCount).c_str(), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    coinsLabel->setScale(0.7);
    coinsLabel->setAnchorPoint(ccp(1,0.5f));
    coinsLabel->setPosition(ccp(screenSize.width * kCCountX, screenSize.height * kCCountY));
    coinsLabel->setTag(kCoinsTag);
    //coinsLabel->setOpacity(0);
    this->addChild(coinsLabel);
    
    
    
    
    
    
    //playCloudAnimation();
    //playGrassAnimation();
    
    this->scheduleOnce(schedule_selector(MenuLayer::updateRightCorner), 1.0);
    return true;
}


void MenuLayer::updateCoins() {
    int coinsCount = GameManager::sharedGameManager()->gameSetting.coins;
    CCLabelBMFont* coinsLabel = (CCLabelBMFont*)this->getChildByTag(kCoinsTag);
    if (coinsLabel) {
        coinsLabel->setString(to_string(coinsCount).c_str());
    }
}

void MenuLayer::addCoinButtonTouched() {
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    return;
#endif

    if (this->isTouchEnabled() == false) return;
    GameManager::sharedGameManager()->playSoundEffect("select.wav");
    
    CCDirector::sharedDirector()->pushScene(CCTransitionSlideInT::create(0.5, StoreScene::create()));
    return;
}

void MenuLayer::playCoinsAnimation(int number) {
    
    for (int i=0; i< number; i++) {
        CCSprite* coin = CCSprite::create("coin.png");
        coin->setPosition(ccp(screenSize.width * kCoinsX, screenSize.height * kCoinsY));
        this->addChild(coin);
        
        CCMoveTo* move = CCMoveTo::create(0.1, ccp(screenSize.width * kCoinsX, screenSize.height * (kCoinsY - 0.1)));
        CCFadeOut* fadeOut = CCFadeOut::create(0.2);
        
        CCSpawn* action = CCSpawn::createWithTwoActions(move, fadeOut);
        coin->runAction(action);
        GameManager::sharedGameManager()->playSoundEffect("coin.mp3");
    }

    
    
}


void MenuLayer::updateRightCorner() {
    
    if (GameManager::sharedGameManager()->getServerBoolean("showNew") == true) {
        CCSprite *newIcon = CCSprite::create("new.png");
        
        CCMenuItemImage* image = (CCMenuItemImage*)(this->getChildByTag(kModeMenuTag)->getChildByTag(4));
        
#ifdef __Fruit__
        newIcon->setPosition(ccp(image->getContentSize().width * 1.0, image->getContentSize().height * 0.2));
#else
        newIcon->setPosition(ccp(image->getContentSize().width, image->getContentSize().height));
#endif
        image->addChild(newIcon);
    }
    
    
    if (GameManager::sharedGameManager()->getServerBoolean("promoPop") == true) {
        // false means not rating alert but promo alert
        GameManager::sharedGameManager()->presendPromoPopAlert(false);
    }
    
    if (getChildByTag(kRCMenuTag) != NULL) return;
    
    
    if (GameManager::sharedGameManager()->getServerBoolean("videoInMenu") == true) {
        
        
        // Most likely we don't show out of China
        ccLanguageType currentLanguage = CCApplication::sharedApplication()->getCurrentLanguage();
        if (currentLanguage != kLanguageChinese) {
            if (GameManager::sharedGameManager()->getServerBoolean("videoNonChina") == false) {
                return;
            }
        }
        
        const char* rcfn = "free_button.png";
        CCMenuItemImage* rcImg = CCMenuItemImage::create(rcfn, rcfn, this, menu_selector(MenuLayer::rightCornerSelected));
        rcImg->setAnchorPoint(ccp(1.0f, 0.0f));
        rcImg->setTag(kRCImgTag);
        rcImg->setScale(0.9);
        
        
        CCLabelBMFont* freeCoinsLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("freeCoins"), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        freeCoinsLabel->setScale(0.8);
        freeCoinsLabel->setPosition(rcImg->getContentSize().width * 0.5, rcImg->getContentSize().height * 0.5);
        rcImg->addChild(freeCoinsLabel);
        
        
        
        CCMenu* rcMenu = CCMenu::create(rcImg, NULL);
        rcMenu->setAnchorPoint(ccp(1.0f, 0.0f));
        rcMenu->setPosition(ccp(screenSize.width , 0));
        rcMenu->setTag(kRCMenuTag);
        rcMenu->setOpacity(0);
        this->addChild(rcMenu);
        
        CCFadeIn* fadeIn = CCFadeIn::create(1.0);
        rcMenu->runAction(fadeIn);
        
        isRCOn = false;
        return;
    }
    
    const char* promoUrl = GameManager::sharedGameManager()->getServerString("promoUrl");
    if ((promoUrl == NULL) || (strlen(promoUrl) == 0)) return;
    

    if (GameManager::sharedGameManager()->getServerBoolean("promoInMenu2")) {
        
        const char* rcfn = "badge.png";
        CCMenuItemImage* rcImg = CCMenuItemImage::create(rcfn, rcfn, this, menu_selector(MenuLayer::rightCornerSelected));
        rcImg->setAnchorPoint(ccp(1.0f, 0.0f));
        rcImg->setTag(kRCImgTag);
        
        
        CCScaleBy* scaleUpRC = CCScaleBy::create(1.5, 1.25);
        CCScaleBy* scaleDownRC = CCScaleBy::create(1.5, 0.8);
        CCSequence* scaleUpAndDownRC = CCSequence::createWithTwoActions(scaleUpRC, scaleDownRC);
        CCRepeatForever* repeatActionRC = CCRepeatForever::create(scaleUpAndDownRC);
        rcImg->runAction(repeatActionRC);
        
        
        
        
        CCMenu* rcMenu = CCMenu::create(rcImg, NULL);
        rcMenu->setAnchorPoint(ccp(1.0f, 0.0f));
        rcMenu->setPosition(ccp(screenSize.width , 0));
        rcMenu->setTag(kRCMenuTag);
        rcMenu->setOpacity(0);
        this->addChild(rcMenu);
        
        CCFadeIn* fadeIn = CCFadeIn::create(1.0);
        rcMenu->runAction(fadeIn);
        
        isRCOn = false;
        
    }
}


void MenuLayer::keyBackClicked() {
    CCDirector::sharedDirector()->end();
}

