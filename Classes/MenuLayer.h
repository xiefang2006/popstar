//
//  MenuLayer.h
//  SimpleGame
//
//  Created by SimplyGame on 9/3/13.
//
//

#ifndef __SimpleGame__MenuLayer__
#define __SimpleGame__MenuLayer__

#include "cocos2d.h"

using namespace cocos2d;

class  MenuLayer : public CCLayer
{
private:
    CCSize screenSize;
    bool    isLCOn;
    bool    isRCOn;
    
public:
    virtual bool init();
    void menuSelected(CCNode* pNode);
    void startGameSelected(CCNode* pNode);

    void setItemSeleted(int itemTag, bool selected);
    void popupContinueQuery();
    void continueSelected(CCNode* pNode);
    
    void popupRateQuery();
    void rateSelected(CCNode* pNode);

    void popupUseCoinQuery();
    void useCoinSelected(CCNode* pNode);


    void startGame();

    void leftCornerSelected(CCNode* pNode);
    void rightCornerSelected(CCNode* pNode);
    void buttonTouched(CCNode* pNode);
    void scaleUpBG();
    void scaleDownBG();
    void fiLCMenu();
    void foLCMenu();
    void fiLC();
    void foLC();
    void removeLCMenu();

    void addCoinButtonTouched();

    void updateCoins();
    void playCoinsAnimation(int number);
    
    
    void popupFirstTimeInfo();
    void playStartGameAnimation();
    
    
    void updateRightCorner();
    
    void keyBackClicked();

    
    
    CREATE_FUNC(MenuLayer);
};
#endif /* defined(__SimpleGame__MenuLayer__) */
