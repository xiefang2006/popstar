//
//  GameLayer.cpp
//  SampleGame
//
//  Created by SimplyGame on 7/25/13.
//  Copyright (c) 2013 Bullets in a Burning Box, Inc. All rights reserved.
//

#include "Popstar.h"
#include "stdlib.h"
#include "time.h"
#include "GameLayer.h"
#include "GameManager.h"
#include "GameScene.h"
#include "MenuScene.h"
#include "PauseScene.h"
#include "StoreScene.h"
#include "StageScene.h"
//#include "GameOverScene.h"

#define kColorCount 5

// 0-100 reserved, could be used by blocks
#define kStartLabelContainerTag 101
#define kStageLabelTag 102
#define kTargetLabelTag 103

#define kEndLabelContainerTag 201
#define kBonusLabelTag 202
#define kRemainLabelTag 203

#define kBGTag 301
#define kBGMaskTag 302

#define kBestTag 401
#define kStageTag 402
#define kTargetTag 403
#define kScoreTag 404
#define kInfoTag 104

#define kCoinsTag 1001
#define kCoinsMenuTag 1002
#define kPauseMenuTag 1003

#define kStageClearTag 1004

#define kPopupMenuTag 501
#define kItemMenuTag 502
#define kGameOverMenuTag 503
#define kGameOverBackgroundTag 504
#define kBGGameOverTag 510


#define kGameOverTitleTag 505
#define kGameOverModeTag 506
#define kGameOverHintTag 507


#define kRestartMenuTag 601
#define kExitBattleMenuTag 604

#define kRCImgTag 602
#define kRCMenuTag 603

#define kStageTargetTag 701
#define kTargetMenuTag 705
#define kBlockCountTag 710



#define kScoreLabelTagBegin 2000

#define kGCScoreLayerTag 3000
#define kGCScoreLayerTagStart 3100
#define kGCPlayerScoreLabelTag 4000

// all the label positions
//#define kBestX      0.30
//#define kBestY      0.95

#define kStageX     0.02
#define kStageY     0.95

#define kTargetX    0.46
#define kTargetY    0.95

#define kScoreX     0.45
#define kScoreY     0.88

#define kCheatX     0.82 
#define kCheatY     0.88

#define kCoinsX     0.92 
#define kCoinsY     0.95

#define kCCountX    0.85 
#define kCCountY    0.95 

#define kContainerY 0.47 

#define kPauseX     0.08
#define kPauseY     0.88

#define kInfoX      0.5
#define kInfoY      0.81

#define kClearX     0.12
#define kClearY     0.81


#define kGCScoreY   0.78

#define kStageTargetY 0.78

#define kGreenColor ccc3(128, 255, 128)
#define kRedColor ccc3(255, 128, 128)
#define kBlueColor ccc3(128, 128, 255)
#define kYellowColor ccc3(128, 172, 172)



void GameLayer::playEndAnimation(int remainingBlocks) {
    CCSprite* container = (CCSprite*)this->getChildByTag(kEndLabelContainerTag);
    
    if (container == NULL) {
        container = CCSprite::create();
        CCLabelBMFont* bonusLabel = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        bonusLabel->setScale(1.2);
        bonusLabel->setPosition(ccp(0, screenSize.height * 0.05));
        bonusLabel->setTag(kBonusLabelTag);
#ifdef __Halloween__
        // bonusLabel->setColor( ccc3(255, 173, 2) );
        // bonusLabel->setColor( ccc3(241, 112, 8) );
#endif
        
        CCLabelBMFont* remainLabel = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        remainLabel->setScale(1.0);
        remainLabel->setPosition(ccp(0, screenSize.height * (-0.05)));
        remainLabel->setTag(kRemainLabelTag);
#ifdef __Halloween__
        // remainLabel->setColor( ccc3(255, 173, 2) );
        // remainLabel->setColor( ccc3(241, 112, 8) );
#endif
        
        container->addChild(bonusLabel);
        container->addChild(remainLabel);
        container->setTag(kStartLabelContainerTag);
        this->addChild(container, 10);
    }
    
    // Set the labels
    CCLabelBMFont* bonusLabel = (CCLabelBMFont*)container->getChildByTag(kBonusLabelTag);
    bonusLabel->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("bonus"), genBonusScore(remainingBlocks))->getCString());
    
    CCLabelBMFont* remainLabel = (CCLabelBMFont*)container->getChildByTag(kRemainLabelTag);
    remainLabel->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("remain"), remainingBlocks)->getCString());

    container->setPosition(ccp(screenSize.width * 1.5, screenSize.height * kContainerY));

    // Play animation, right to left, then init the blocks
    CCMoveBy* moveIn = CCMoveBy::create(1.0, ccp(screenSize.width * (-1.0), 0));
    CCMoveBy* moveOut = CCMoveBy::create(1.0, ccp(screenSize.width * (-1.0), 0));
    
    CCCallFunc* bonusScore = CCCallFunc::create(this, callfunc_selector(GameLayer::playBonusScoreAnimation));
    
    CCCallFunc* clearStage = CCCallFunc::create(this, callfunc_selector(GameLayer::popRemainingBlocks));
    CCDelayTime* stay = CCDelayTime::create(remainingBlocks * 0.08);
    CCCallFunc* finishStage = CCCallFunc::create(this, callfunc_selector(GameLayer::finishStage));
    
    CCSequence* animation = CCSequence::create(moveIn, clearStage, bonusScore, stay, moveOut, finishStage, NULL);
    
    container->runAction(animation);
}


void GameLayer::playStartAnimation(int currentStage) {
    
    CCSprite* container = (CCSprite*)this->getChildByTag(kStartLabelContainerTag);
    
    // Init the labels
    if (container == NULL) {
        container = CCSprite::create();
        CCLabelBMFont* stageLabel = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        stageLabel->setScale(1.2);
        stageLabel->setPosition(ccp(0, screenSize.height * 0.05));
        stageLabel->setTag(kStageLabelTag);
#ifdef __Halloween__
        // stageLabel->setColor( ccc3(255, 173, 2) );
        // stageLabel->setColor( ccc3(241, 112, 8) );
#endif
 
        
        CCLabelBMFont* targetLabel = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        targetLabel->setScale(1.0);
        targetLabel->setPosition(ccp(0, screenSize.height * (-0.05)));
        targetLabel->setTag(kTargetLabelTag);
#ifdef __Halloween__
        // targetLabel->setColor( ccc3(255, 173, 2) );
        // targetLabel->setColor( ccc3(241, 112, 8) );
#endif
        
        container->addChild(stageLabel);
        container->addChild(targetLabel);
        container->setTag(kStartLabelContainerTag);
        this->addChild(container, 10);
    }

    // Set the labels
    CCLabelBMFont* stageText = (CCLabelBMFont*)container->getChildByTag(kStageLabelTag);
    CCLabelBMFont* targetText = (CCLabelBMFont*)container->getChildByTag(kTargetLabelTag);
    
    switch (GameManager::sharedGameManager()->gameMode) {
        case 0:
            stageText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("stage"), currentStage)->getCString());
            targetText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("target"), targetScore)->getCString());
            break;
        case 1:
        case 4:
            stageText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("getReady"), currentStage)->getCString());
            targetText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("timeText"), remainingTime)->getCString());
            break;

        case 2:
            stageText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("getReady"), currentStage)->getCString());
            targetText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("moveText"), remainingMoves)->getCString());
            break;
            
        case 3:
            if (subGameMode == 0) {
                stageText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("stage"), currentStage)->getCString());
                targetText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("moveText"), remainingMoves)->getCString());
            }
            else {
                stageText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("stage"), currentStage)->getCString());
                targetText->setString(CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("timeText"), remainingTime)->getCString());
            }
            
            break;
            
    }

    
    container->setPosition(ccp(screenSize.width * 1.5, screenSize.height * kContainerY));
    
    
    // Play animation, right to left, then init the blocks
    CCMoveBy* moveIn = CCMoveBy::create(0.5, ccp(screenSize.width * (-1.0), 0));
    CCEaseIn* easeIn = CCEaseIn::create(moveIn, 2);
    
    //CCMoveBy* moveCenter = CCMoveBy::create(0.1, ccp(screenSize.width * 0.05, 0));
    //CCEaseInOut* easeCenter = CCEaseInOut::create(moveCenter, 2);
    
    //CCMoveBy* moveCenter = CCMoveBy::create(0.1, ccp(screenSize.width * 0.1, 0));
    CCMoveBy* moveOut = CCMoveBy::create(0.15, ccp(screenSize.width * (-1.0), 0));
    CCEaseIn* easeOut = CCEaseIn::create(moveOut, 2);

    
    CCDelayTime* stay = CCDelayTime::create(0.8);
    CCCallFunc* playReadyGo = CCCallFunc::create(this, callfunc_selector(GameLayer::playReadyGo));
    CCCallFunc* initBlocks = CCCallFunc::create(this, callfunc_selector(GameLayer::initBlocks));
    
    CCSequence* animation = CCSequence::create(easeIn, playReadyGo, stay, easeOut, initBlocks, NULL);

    container->runAction(animation);

}

void GameLayer::playReadyGo() {
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        GameManager::sharedGameManager()->playSoundEffect("coin.mp3");
        
        GameManager::sharedGameManager()->gameSetting.coins-= kVSCoinNeed;
        GameManager::sharedGameManager()->saveGameSetting();
        updateCoins();
    }
    else {
        GameManager::sharedGameManager()->playSoundEffect("readygo.wav");
    }
}

void GameLayer::playLandingSound() {
    GameManager::sharedGameManager()->playSoundEffect("landing.wav");
    
}


bool GameLayer::init() {

    if ( !CCLayer::init() )
    {
        return false;
    }
    
    setKeypadEnabled(true);
    
    /*
    for (int i=1; i<100; i++) {
        CCLOG("%d - %d \t%d", i, genTargetScore2(i), genTargetScore2(i) - genTargetScore2(i-1));
    }
    
    
    for (int i=1; i<100; i++) {
        CCLOG("%d - %d \t%d", i, genTargetScore(i), genTargetScore(i) - genTargetScore(i-1));
    }
    */

    
    screenSize = CCDirector::sharedDirector()->getWinSize();
    

    
    
    needFadeIn = true;
    this->layoutControls();

    selectedItem = 0;
    sNum = 0;
    selected = false;
    totalScore = 0;
    networkScore = 0;
    bestScore = 0;

    blockSize = screenSize.width / 10 ;
    sizeX = 10;
    sizeY = 10;
    
    if ((GameManager::sharedGameManager()->gameMode == 3) || (GameManager::sharedGameManager()->gameMode == 4)){
        sizeY = 9;
    }

    adX = ADX * screenSize.width;
    adY = ADY * screenSize.height;
    
    // for long screen
    if (screenSize.height == 568.0) {
        adY += (screenSize.height - 480.0) / 2.0;
    }
    else if (screenSize.height == 693.0) {
        adY += (screenSize.height - 550.0) / 2.0;
    }

    // Speical for game mode 3
    /*
    if (GameManager::sharedGameManager()->gameMode == 3) {
        adY = 0;
        if (screenSize.height >= 568.0) {
            adY = 50;
        }
    }
     */
    
    stage = 0;

    duration = 0.2;

    srand(time(NULL));

    //fontName = GameManager::sharedGameManager()->getLocalizedString("mm.fnt");
    fontSize = 64;
    
    //this->addChild(text_best);

    columns = CCArray::createWithCapacity(sizeX);
    columns->retain();
    
    this->initStage();
    this->scheduleOnce(schedule_selector(GameLayer::updateRightCorner), 2.0);

    
    //int score = genTargetScore2(539);
    //int score2 = genTargetScore2(327);
    
    return true;
}

void GameLayer::keyBackClicked() {
    // up one level
    if (GameManager::sharedGameManager()->gameMode == 3) {
        CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, StageScene::create()));
    }
    else {
        CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, MenuScene::create()));
    }
}

void GameLayer::registerWithTouchDispatcher(void) {
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 10, true);
}

bool GameLayer::ccTouchBegan (CCTouch *pTouch, CCEvent *pEvent) {
   
    //stopHint();

    // CCLog("Touch Began");
    CCArray* columnX;
    GameBlock* blockC; // central block being touched
    
    int touchX, touchY; // x index, y index
    int touchColor;
    
    // CCLog("Touch Ended");
    
    CCPoint location = pTouch->getLocation();
    // CCLog("x:%f, y:%f", location.x, location.y);
    
    // touchX = (int) location.x / blockSize;
    // touchY = (int) location.y / blockSize;
    touchX = getTouchedX( location.x );
    touchY = getTouchedY( location.y );
    // CCLog("touchX:%d, touchY:%d", touchX, touchY);
    
    // bool tcm[sizeX][sizeY]; // touch connection map
    if( (touchX < columns->count()) && (columns->count() != 0)) {
        columnX = getCol(columns, touchX);
        if((touchY < columnX->count()) && (columnX->count() != 0)) {
            blockC = getGameBlk(columnX, touchY);
            touchColor = blockC->getBlockColor();
            // CCLog("touched color is: %s", GameBlock::color2str(touchColor).c_str());
            
            switch (selectedItem) {
                case 0:
                    if ( selected && tcm[touchX][touchY] ) {
                        CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                        if (GameManager::sharedGameManager()->gameMode == 3) {
                            text_info->setString(defaultInfoText.c_str());
                        }
                        else {
                            text_info->setString("");
                        }
                        
                        
                        //touch a block inside last selection
                        popBlocks();

                    }
                    // no connection was selected, or
                    // touch a block outside last connection
                    else {
                        // recover last selection images
                        if ( selected ) {
                            recoverSelection();
                            
                            CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                            if (GameManager::sharedGameManager()->gameMode == 3) {
                                text_info->setString(defaultInfoText.c_str());
                            }
                            else {
                                text_info->setString("");
                            }
                            
                        }
                        // find new tcm
                        clearTCM();
                        // CCLog("finding connection");
                        if( findConnection ( columns, touchX, touchY, touchColor ) ) {
                            // CCLog("connection found.");
                            // tcm[touchX][touchY] = true;
                            // printCM();
                            updateSelection();
                            selected = true;
                            
                            CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                            int n = this->getConnectedCount();
                            int s = this->genScore();
                            CCString* info_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("selectInfo"), n, s);
                            if (GameManager::sharedGameManager()->gameMode == 3) {
                                text_info->setString(defaultInfoText.c_str());
                            }
                            else {
                                text_info->setString(info_str->getCString());
                            }
                            
                            
                            
                            // TODO remove One touch try
                            // text_info->setString("");
                            // popBlocks();
                            
                            
                            if (GameManager::sharedGameManager()->gameSetting.isSingleTapOff) {
                                GameManager::sharedGameManager()->playSoundEffect("select.wav");
                            }
                            else {
                                popBlocks();
                            }
                        }
                        
                    }
                    break;
                case 1:
                {
                    if ( selected ) {
                        recoverSelection();
                    }
                    
                    bombBlocks(touchX, touchY);

                    
                    // unselect
                    setItemSeleted(1, false);
                    selectedItem = 0;
                    
                    break;
                }
                    
                case 2:
                    // update hints
                    break;
                    
                case 3:
                {
                    if (selected) {
                        recoverSelection();
                    }
                    popupColorSelection(touchX, touchY);
                    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                    text_info->setString(GameManager::sharedGameManager()->getLocalizedString("selectColorInfo"));
                    break;
                }
                default:
                    break;
                    
            }
            
        }
    }
    
    
    
    
    /*
     CCLog("---------------------------------");
     CCLog(" solve map...");
     CCLog("---------------------------------");
     
     int score = 0;
     CCArray *DataColumns = this->cloneGameColumns( columns );
     score = solveMap( DataColumns, 0 );
     CCLog("Final best score = %d", score);
     CCLog("The best block to start with is (x, y) = (%d, %d)", bestX, bestY);
     
     //--------------------------------------------
     // show the best connection
     
     // get the color
     columnX = getCol(columns, bestX);
     blockC = getGameBlk(columnX, bestY);
     touchColor = blockC->getBlockColor();
     // update tcm
     clearTCM();
     findConnection ( columns, bestX, bestY, touchColor );
     updateSelection();
     selected = true;
     //--------------------------------------------
     
     */
    

    
    return true;
}

void GameLayer::ccTouchMoved (CCTouch *pTouch, CCEvent *pEvent) {
    // CCLog("Touch Moved");

}

void GameLayer::ccTouchEnded (CCTouch *pTouch, CCEvent *pEvent) {


}

void GameLayer::initBlocks() {
    int initColor;
    CCArray* columnX; // column at X.
    GameBlock* block;
    
    this->setTouchEnabled(true);
    isPoping = false;
    isGameOver = false;
    
    zoomRatio = 1.0;
    

    CCLayerColor * bg_mask = CCLayerColor::create(ccc4(0,0,0,0), screenSize.width, screenSize.width);
    bg_mask->setTag(kBGMaskTag);
    bg_mask->setAnchorPoint(ccp(0, 0));
    bg_mask->setPosition(ccp(adX, adY));
    this->addChild( bg_mask, 0 );


    
    


    
    /*
    CCSprite *bg_mask = CCSprite::create("bg_mask.png");
    bg_mask->setTag(kBGMaskTag);
    bg_mask->setAnchorPoint(ccp(0, 0));
    bg_mask->setPosition(ccp(adX, adY));
*/
    
    
    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        
        
        // TODO: make this pixel perfect for iPad
        zoomRatio = screenSize.height / screenSize.width / 1.5;
        CCLOG("Zoom ratio: %f", zoomRatio);
        adX = screenSize.width * (1-zoomRatio) * 0.5;
        
        if (adX % 2 == 1) {
            adX++;
        }
        
        /*
        if (GameManager::sharedGameManager()->gameMode == 3) {
            adY = adX * 0.5;
        }
         */

        //CCLOG("Resolution: %f x %f", screenSize.width, screenSize.height);
        bg_mask->setPosition(ccp(adX, adY));
        bg_mask->setScale(zoomRatio);
    }

    
    CCSprite *bg_mask_img = CCSprite::create("bg_mask.png");
    bg_mask_img->setAnchorPoint(ccp(0,0));
    bg_mask_img->setPosition(ccp(-1, -1));
    bg_mask->addChild(bg_mask_img);
    
#if defined(__Star__)
    bg_mask_img->setOpacity(0);
#endif
    
    
    GameManager* gm = GameManager::sharedGameManager();
    if (initFromSavedGame == true) {
        
        for (int x = 0; x < sizeX; x++) {
            columns->addObject(CCArray::createWithCapacity(sizeY));
            for (int y = 0; y < sizeY; y++) {
                int color = gm->gameState.matrix[x][y] - 1;
                if (color < 0) break;
                
                block = GameBlock::create(color);
                
                block->setXY(x, y);
                block->setTag(x * sizeX + y);
                block->setAnchorPoint(ccp(0,0));
                block->setPosition( genPosition( x, y ));
                bg_mask->addChild(block);
                columnX = getCol(columns, x);
                columnX->addObject(block);
            }
            
            if (columnX->count() == 0) {
                columns->removeObjectAtIndex(x); //(columnX);
                break;
            }
        }
        
        initFromSavedGame = false;
        //clearColumns();
        
    }
    else {
    // init the columns of blocks
        for (int x = 0; x < sizeX; x++) {
            columns->addObject(CCArray::createWithCapacity(sizeY));
            for (int y = 0; y < sizeY; y++) {
                initColor = rand() % kColorCount;
                block = GameBlock::create(initColor);
                
                block->setXY(x, y);
                block->setTag(x * sizeX + y);
                block->setAnchorPoint(ccp(0,0));
                block->setPosition( genPosition( x, y ));
                bg_mask->addChild(block);
                columnX = getCol(columns, x);
                columnX->addObject(block);
            }
        }
    }
    
    
    // Set the clock only for time mode
    if (GameManager::sharedGameManager()->gameMode == 1 || GameManager::sharedGameManager()->gameMode == 4) {
        this->schedule(schedule_selector(GameLayer::countDown), 1.0);
    }
    
    // layoutStageTarget only for adventure mode
    if (GameManager::sharedGameManager()->gameMode == 3) {
        layoutStageTarget();
        
        if (remainingTime != 0) {
            this->schedule(schedule_selector(GameLayer::countDown), 1.0);
        }
    }
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        layoutGCScore();
    }
    
    saveStateAndScore();
    GameManager::sharedGameManager()->saveGameState();

    if(testStageEnd() == false) {
        this->scheduleOnce(schedule_selector(GameLayer::autoHint), kHintTime);
    }
    
}

void GameLayer::countDown() {
    remainingTime--;
    
    bool isAdventure = (GameManager::sharedGameManager()->gameMode == 3);
    
    CCString* stage_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelTime"), remainingTime);
    CCLabelBMFont* text_stage = (CCLabelBMFont*)this->getChildByTag(isAdventure?kTargetTag:kStageTag);
    text_stage->setString(stage_str->getCString());
    
    
    if (remainingTime < 10) {
        GameManager::sharedGameManager()->playSoundEffect("beep.wav");
        CCSprite* countdownSprite = CCSprite::create("countdown.png");
        countdownSprite->setScaleY(screenSize.height / 568.0);
        countdownSprite->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.5));
        this->addChild(countdownSprite, 50);
        countdownSprite->scheduleOnce(schedule_selector(CCSprite::removeFromParent), 0.5);
    }
    
    if (remainingTime == 0) {
        // Game Over
        this->unschedule(schedule_selector(GameLayer::countDown));
        stopHint();

        
        if (isPoping == false) {
            // Delay in case score is flying
            isGameOver = true;
            setTouchEnabled(false);
            
            CCDelayTime* delay = CCDelayTime::create(0.81);
            CCCallFunc* popupGameOver = CCCallFunc::create(this, isAdventure?callfunc_selector(GameLayer::popupAdventureGameOver):callfunc_selector(GameLayer::popupGameOver));
            CCSequence* fullAction = CCSequence::create(delay, popupGameOver, NULL);
            this->runAction(fullAction);            
        }
    }

    
}


void GameLayer::autoHint() {

    if ( columns->count() == 0 ) {
        return;
    }

    hintOn = true;

    setTouchEnabled(false);

    // this->unschedule(schedule_selector(GameLayer::autoHint));

    // auto hint the longest connected blocks

    CCArray *DataColumns = this->cloneGameColumns( columns );
    int score = solveMap( DataColumns, 0 );

    // show the best connection

    CCArray* columnX = getCol(columns, bestX);
    GameBlock* blockC = getGameBlk(columnX, bestY);
    int touchColor = blockC->getBlockColor();

    // find new tcm
    clearTCM();
    if( findConnection ( columns, bestX, bestY, touchColor ) ) {

        showHint();

        /*
        updateSelection();
        selected = true;

        CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
        int n = this->getConnectedCount();
        int s = this->genScore();
        CCString* info_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("selectInfo"), n, s);
        text_info->setString(info_str->getCString());
        */
    }

    setTouchEnabled(true);

}

 
void GameLayer::stopHint() {

    this->unschedule(schedule_selector(GameLayer::autoHint));

    // stop hint action if it is on
    if ( hintOn ) {
        hintOn = false;
        CCArray *col;
        GameBlock *blk;
        for(int x = 0; x < columns->count(); x++) {
            col = getCol(columns, x);
            for(int y = (col->count() - 1); y >= 0; y--) {
                if(true) {
                    blk = getGameBlk( col, y );
                    blk->stopAllActions();
                    blk->setScale(1.0);
                    blk->setPosition( genPosition( blk->getX(), blk->getY() ));
                }
            }
        }
    }
}

void GameLayer::showHint() {

    CCArray *col;
    GameBlock *blk;

    float d = 0.5;

    for(int x = 0; x < columns->count(); x++) {
        col = getCol(columns, x);
        for(int y = (col->count() - 1); y >= 0; y--) {
            if(tcm[x][y] == true) {

                CCScaleBy* scaleDown = CCScaleBy::create(d, 0.5);
                CCMoveBy* moveDown = CCMoveBy::create(d, ccp(blockSize * 0.25, blockSize * 0.25));
                CCSpawn* spawnDown = CCSpawn::createWithTwoActions(scaleDown, moveDown);
                CCScaleBy* scaleUp = CCScaleBy::create(d, 2.0);
                CCMoveBy* moveUp = CCMoveBy::create(d, ccp(-blockSize * 0.25, -blockSize * 0.25));
                CCSpawn* spawnUp = CCSpawn::createWithTwoActions(scaleUp, moveUp);
                CCDelayTime* delay = CCDelayTime::create(4);
                CCSequence* scaleUpAndDown = CCSequence::create(spawnDown, spawnUp, spawnDown, spawnUp, spawnDown, spawnUp, delay, NULL);
                CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);

                blk = getGameBlk( col, y );
                blk->runAction(repeatAction);
            }
        }
    }

}


void GameLayer::layoutStageTarget() {
    CCSprite *background_img = CCSprite::create("paint_bg_extended.png");
    background_img->setAnchorPoint(ccp(0.5f, 0.5f));
    background_img->setPosition(ccp(screenSize.width * 0.5, screenSize.height * kStageTargetY));
    background_img->setScale(zoomRatio);
    background_img->setOpacity(0);
    
    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        background_img->setPosition(ccp(screenSize.width * 0.5, screenSize.height * (kStageTargetY + 0.01)));
    }
    

    
    this->addChild(background_img, -5);
    background_img->setTag(kStageTargetTag);
    

    CCMenu* menu = CCMenu::create();
    menu->setTag(kTargetMenuTag);
    
    int gameType = 0;
    
    for (int i=0; i<5; i++) {
        std::string filename = GameBlock::color2str(i) + ".png";
        CCMenuItemImage* block = CCMenuItemImage::create(filename.c_str(), filename.c_str());
        CCLabelBMFont* count = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        block->setScale(0.8);
        count->setString(to_string(currentStage.target[i]).c_str());
        count->setScale(0.8);
        count->setAnchorPoint(ccp(0.0, 0.5));
        count->setTag(kBlockCountTag);
        count->setPosition(ccp(blockSize * 1.1, blockSize * 0.5));
        block->addChild(count);
        block->setTag(i);
    
        if (currentStage.target[i] > 0) {
            menu->addChild(block);
            gameType = 1;
        }
    }
    
    
    if (currentStage.target[5] != 0) {
        
        CCMenuItemImage* block = CCMenuItemImage::create("red.png", "red.png");
        CCLabelBMFont* count = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        block->setOpacity(0);
        block->setScale(0.8);
        count->setString(("0/" + to_string(currentStage.target[5])).c_str());
        count->setScale(0.8);
        count->setAnchorPoint(ccp(0.0, 0.5));
        count->setTag(kBlockCountTag);
        count->setPosition(ccp(blockSize * 0.0, blockSize * 0.5));
        block->addChild(count);
        block->setTag(5);
        menu->addChild(block);
        
        gameType += 2;
        
        /*
        CCLabelBMFont* scoreTarget = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        scoreTarget->setScale(0.8);
        scoreTarget->setString(("0/" + to_string(targetBlocks[5])).c_str());
        scoreTarget->setTag(kBlockCountTag);
        scoreTarget->setAnchorPoint(ccp(0.0, 0.5));
        scoreTarget->setPosition(ccp(0, blockSize * 0.5));
        
        
        CCMenuItemLabel* scoreTargetItem = CCMenuItemLabel::create(scoreTarget);
        scoreTargetItem->setTag(5);
        scoreTargetItem->setScale(0.8);
        menu->addChild(scoreTargetItem);
         */
    }
    
    
    menu->setAnchorPoint(ccp(0.5, 0.5));
    menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.12);
    menu->setPosition(ccp(400 * 0.48, blockSize * 0.7));
    background_img->addChild(menu);
    
    CCLabelBMFont* text_info = (CCLabelBMFont*) this->getChildByTag(kInfoTag);
    
    switch (gameType) {
        case 1:
            defaultInfoText = GameManager::sharedGameManager()->getLocalizedString("collectColors");
            break;
        case 2:
            defaultInfoText = GameManager::sharedGameManager()->getLocalizedString("reachScore");
            break;
        case 3:
            defaultInfoText = GameManager::sharedGameManager()->getLocalizedString("colorAndScore");
            break;
        default:
            defaultInfoText = "";
            break;

    }
    text_info->setString(defaultInfoText.c_str());
    text_info->setPosition(ccp(screenSize.width * kInfoX, screenSize.height* (kInfoY + 0.005)));
    
    if (screenSize.height == 693.0) { // TODO: iphone x
        background_img->setPositionY(background_img->getPositionY() - 20.0);
        text_info->setPositionY(text_info->getPositionY() - 20.0);
    }
    


}
void GameLayer::initStage() {

    // TODO: remove, no need to do twice
    GameManager::sharedGameManager()->startBanner();
    
    selected = false;
    
    CCString* stage_str;
    CCString* target_str;
    CCString* score_str;
    
    initFromSavedGame = false;
    
    switch (GameManager::sharedGameManager()->gameMode) {
        case 0: {
            // loading
            if ((stage == 0) && (GameManager::sharedGameManager()->gameState.stage != 0)) {
                stage = GameManager::sharedGameManager()->gameState.stage;
                totalScore = GameManager::sharedGameManager()->gameState.score;
                stageBeginScore = GameManager::sharedGameManager()->gameState.beginScore;
                initFromSavedGame = true;
            }
            else {
                stage++;
                stageBeginScore = totalScore;
            }
            
            
            if (GameManager::sharedGameManager()->getServerInterger("targetLevel") == 1) {
                // Hard
                targetScore = genTargetScore(stage);
            }
            else {
                // Easy default
                targetScore = genTargetScore2(stage);
            }
            
            stage_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelStage"), stage);
            target_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelTarget"), targetScore);
            score_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelScore"), totalScore);
            //best_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelBest"), bestScore);
            
            break;
        }
        case 1:
            remainingTime = 60;
            stage_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelTime"), remainingTime);
            target_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelBest"), GameManager::sharedGameManager()->gameSetting.bestScoreTime);
            score_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelScore"), totalScore);
            //best_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelBest"), bestScore);
            
            
            break;
        case 4:
            remainingTime = 60;
            stage_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelTime"), remainingTime);
            target_str = CCString::createWithFormat("");
            score_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelScore"), totalScore);
            break;
            
        case 2:
            remainingMoves = 30;
            stage_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelMove"), remainingMoves);
            target_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelBest"), GameManager::sharedGameManager()->gameSetting.bestScoreMove);
            score_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelScore"), totalScore);
            //best_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelBest"), bestScore);
            break;
            
        case 3:
            // Entered from stage selection menu
            if (stage == 0)
                stage = GameManager::sharedGameManager()->selectedStage;
            
            currentStage = GameManager::sharedGameManager()->getAdventureStage(stage);
            remainingMoves = currentStage.moves[2];
            remainingTime = currentStage.time[2];
            
            itemUsed = false;
            
            if (remainingMoves != 0 && remainingTime != 0) {
                subGameMode = 2;
            }
            else if (remainingTime != 0) {
                subGameMode = 1;
            }
            else {
                subGameMode = 0;
            }
            
            
            totalScore = 0;

            stage_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelStage"), stage);
            
            CCMenuItemImage* item = (CCMenuItemImage*)this->getChildByTag(kItemMenuTag)->getChildByTag(4);
            
            if (subGameMode == 1) {
                target_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelTime"), remainingTime);
                
                if (subGameMode != lastSubGameMode) {
                    if (item) {
                        item->setNormalImage(CCSprite::create("addtime.png"));
                        item->setSelectedImage(CCSprite::create("addtime.png"));
                        
                        if (needFadeIn) {
                            ((CCMenu*)item->getParent())->setOpacity(0);
                        }
                    }
                    lastSubGameMode = subGameMode;
                }
            }
            else {
                target_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelMove"), remainingMoves);
                
                if (subGameMode != lastSubGameMode) {
                    if (item) {
                        item->setNormalImage(CCSprite::create("addmoves.png"));
                        item->setSelectedImage(CCSprite::create("addmoves.png"));
                    }
                    lastSubGameMode = subGameMode;
                    //    this->setTexture(CCTextureCache::sharedTextureCache()->addImage(ss.c_str()));

                }
            }
            
            score_str = CCString::create(""); // CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelTime"), remainingTime);
            
            break;
            
            
            
            



    }
    
    // First stage is 1

    


    CCLabelBMFont* text_stage = (CCLabelBMFont*)this->getChildByTag(kStageTag);
    text_stage->setString(stage_str->getCString());

    CCLabelBMFont* text_target = (CCLabelBMFont*)this->getChildByTag(kTargetTag);
    text_target->setString(target_str->getCString());

    /*
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        text_stage->setAnchorPoint(ccp(0.5, 0.5));
        text_stage->setPosition(text_target->getPosition());
    }
     */
    

    CCLabelBMFont* text_score = (CCLabelBMFont*)this->getChildByTag(kScoreTag);
    text_score->setString(score_str->getCString());
    text_score->setPosition(ccp(screenSize.width * kScoreX, screenSize.height * kScoreY));
    
    if (screenSize.height == 693.0) {  // iphone x
          text_score->setPositionY(text_score->getPositionY() - 10.0);
    }
    text_score->setScale(1.0);
    text_score->setOpacity(0);
    
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        text_score->setPosition(ccp(screenSize.width * kScoreX, screenSize.height * (kScoreY + 0.02)));
        text_score->setScale(1.5);
        schedule(schedule_selector(GameLayer::updateGCScore), 0.5);
    }

    this->removeChildByTag(kStageClearTag);
    text_score->setColor(ccWHITE);
    cheered = false;
    
    if (needFadeIn) {
        fadeInControls();
    }

    //CCLabelBMFont* text_best = (CCLabelBMFont*)this->getChildByTag(kBestTag);
    //text_best->setString(best_str->getCString());

    playStartAnimation(stage);
    //clearColumns( );

}

int GameLayer::genBonusScore ( int num) {

    int bonusScore = 0;

    if ( num < 10 ) {
        bonusScore = ( 2000 - num * num * 20);
    } 
    else {
        bonusScore = 0;
    }

    return bonusScore;
}

int GameLayer::genBonusScore ( CCArray *columns ) {
    int num = 0;
    int bonusScore = 0;
    CCArray *columnX;

    for ( int x = 0; x < columns->count(); x++ ) {
        columnX = getCol(columns, x);
        num = num + columnX->count();
    }

    if ( num < 10 ) {
        bonusScore = ( 2000 - num * num * 20);
    } 
    else {
        bonusScore = 0;
    }

    return bonusScore;
}

// return a score for a step/stage further
int GameLayer::solveMap( CCArray *columns, int depth ) {

    sNum++;

    depth++;
    // CCLog("%d : %d", sNum, depth);

    // bonus score is 2000 for an empty final stage.
    if ( columns->count() == 0 ) {
        return 2000; 
    }

    int sizeX, sizeY;

    bool **solvedCM;                    // mark the position has been tested as true 
    int **solvedScore;                  // score of each position

    int bestScore = 0;                  // best score of previous position
    int cScore = 0;                     // score of current position
    int cColor;                         // current core
    bool **cCM;                         // current connection map
    bool iRCM = true;                   // Ir-Reducible Connection Map, i.e., the last stage, no more same-color nighborhood.
    int rScore = 0;                     // score of recursive deeper stage
    int bonusScore = 0;

    CCArray *reCols;
    CCArray *columnX;
    DataBlock *cBlock;

    sizeX = this->getMaxX( columns );
    sizeY = this->getMaxY( columns );

    solvedScore = new int*[sizeX];
    for ( int x = 0; x < sizeX; x++ ) {
        solvedScore[x] = new int[sizeY];
    }
    for ( int x = 0; x < sizeX; x++ ) {
        for ( int y = 0; y < sizeY; y++ ) {
            solvedScore[x][y] = 0;
        }
    }

    solvedCM = new bool*[sizeX];
    for ( int x = 0; x < sizeX; x++ ) {
        solvedCM[x] = new bool[sizeY];
    }
    for ( int x = 0; x < sizeX; x++ ) {
        for ( int y = 0; y < sizeY; y++ ) {
            solvedCM[x][y] = false;
        }
    }

    cCM = new bool*[sizeX];
    for ( int x = 0; x < sizeX; x++ ) {
        cCM[x] = new bool[sizeY];
    }

    for ( int x = 0; x < columns->count(); x++ ) {
        columnX = getCol(columns, x);
        for ( int y = 0; y < columnX->count(); y++ ) {
            // reset current CM
            for ( int i = 0; i < sizeX; i++ ) {
                for ( int j = 0; j < sizeY; j++ ) {
                    cCM[i][j] = false;
                }
            }

            if ( solvedCM[x][y] ) {
                // this block is solved.
            }
            else {
                cBlock = getBlk(columnX, y);
                cColor = cBlock->getBlockColor();
                testConnection ( columns, x, y, cColor, cCM );
                cScore = genScore( cCM, sizeX, sizeY );
                solvedScore[x][y] += cScore;

                if ( cScore == 0 ) {
                    // no connection, go to next one.
                    solvedCM[x][y] = true;
                }
                else {
                    // has connections, need to recurse one more round
                    iRCM = false;   
                    // update solvedCM with the connected blocks.
                    copyCM( cCM, solvedCM, sizeX, sizeY );
                    reCols = cloneColumns(columns);
                    updateColumnsData( reCols, cCM );

                    // recurse deeper
                    // if ( depth <= REC_DEPTH ) {
                    if ( depth <= searchDepth ) {
                        rScore = solveMap( reCols, depth );
                        solvedScore[x][y] += rScore;
                    }

                    
                    /* This crashes the app, double check for memory leak
                    if ( reCols != NULL ) {
                        deleteColumns ( reCols );
                    }
                     */

                }
            }
        }
    }

    // find the best score in sovledScore
    for ( int x = 0; x < sizeX; x++ ) {
        for ( int y = 0; y < sizeY; y++ ) {
            if( solvedScore[x][y] > bestScore ) {
                bestScore = solvedScore[x][y]; 
                bestX = x;
                bestY = y;
            }
        }
    }

    if( cCM != NULL ) {
        for ( int x = 0; x < sizeX; x++ ) {
            delete cCM[x];
        }
        delete cCM;
    }

    if( solvedCM != NULL ) {
        for ( int x = 0; x < sizeX; x++ ) {
            delete solvedCM[x];
        }
        delete solvedCM;
    }

    if( solvedScore != NULL ) {
        for ( int x = 0; x < sizeX; x++ ) {
            delete solvedScore[x];
        }
        delete solvedScore;
    }

    // if current columns have no conenctions
    if ( iRCM ) {
        bonusScore = genBonusScore(columns);
        bestScore += bonusScore;
    }
    // if there are connections
    else {
    }

    return bestScore;
}

void GameLayer::copyCM (bool **a, bool **b, int sizeX, int sizeY ) {
    for(int x = 0; x < sizeX; x++) {
        for(int y = 0; y < sizeY; y++) {
            if ( a[x][y] ) {
                b[x][y] = true;
            }
        }
    }
}

int GameLayer::countBlocks () {
    CCArray *columnX;
    int cnt = 0;

    for(int x = 0; x < columns->count(); x++) {
        columnX = getCol(columns, x);
        cnt += columnX->count();
    }

    return cnt;
}

void GameLayer::setSearchDepth ( int cnt ) {
    /*
    if ( cnt < 30 ) {
        searchDepth = 2;
    }
    else if ( cnt < 60 ) {
        searchDepth = 1;
    }
    else {
        searchDepth = 0;
    }
    */
    searchDepth = 0;
}

void GameLayer::updateColumnsData (CCArray *columns, bool **tcm) {

    CCArray *columnX;
    CCArray *columnR;
    DataBlock *blockC;
    DataBlock *blockM; // block to be moved down

    if(columns->count() == 0) {
        return;
    }
    else {
        // remove connected blocks
        // x: index for current column
        for(int x = 0; x < columns->count(); x++) {
            columnX = getCol(columns, x);
            // y: index for the current veritcal position in TCM[x][y]
            for(int y = (columnX->count() - 1); y >= 0; y--) {
                if(tcm[x][y] == true) {
                    // remove connected block
                    blockC = getBlk(columnX, y);
                    columnX->removeObjectAtIndex(y);
                }
            }
        }

        // clear the empty columns
        for(int x = (columns->count() - 1); x >= 0; x--) {
            columnX = getCol(columns, x);
            if(columnX->count() == 0) {
                // columns->removeObject(columnX);
                columns->removeObjectAtIndex(x);
            }
        }

        // move blocks to correct positons
        // merge move down & left
        if(columns->count() == 0) {
            return;
        }
        for(int x = (columns->count() - 1); x >= 0; x--) {
            columnX = getCol(columns, x);
            // j: index for the moveTo position of the upper blocks
            for (int y = 0; y < columnX->count(); y++) {
                blockM = getBlk(columnX, y);
                // if the current position match its index
                if((blockM->getX() == x) && (blockM->getY() == y)) {
                }
                else { // if((blockM->getX() != x) && (blockM->getY() != y)) {
                    blockM->setXY(x, y);
                }
            }
        }
    }
}

void GameLayer::updateColumns () {

    CCArray *columnX;
    CCArray *columnR;
    GameBlock *blockC;
    GameBlock *blockM; // block to be moved down

    if(columns->count() == 0) {
        return;
    }
    else {
        // remove connected blocks
        // x: index for current column
        for(int x = 0; x < columns->count(); x++) {
            columnX = getCol(columns, x);
            // y: index for the current veritcal position in TCM[x][y]
            for(int y = (columnX->count() - 1); y >= 0; y--) {
                if(tcm[x][y] == true) {
                    // remove connected block
                    blockC = getGameBlk(columnX, y);
                    this->getChildByTag(kBGMaskTag)->removeChild(blockC);
                    columnX->removeObjectAtIndex(y);

                }
            }
        }

        // clear the empty columns
        for(int x = (columns->count() - 1); x >= 0; x--) {
            columnX = getCol(columns, x);
            if(columnX->count() == 0) {
                columns->removeObjectAtIndex(x);
            }
        }

        // move blocks to correct positons
        // merge move down & left
        if(columns->count() == 0) {
            return;
        }

        // move _|
        for(int x = (columns->count() - 1); x >= 0; x--) {
            columnX = getCol(columns, x);
            // j: index for the moveTo position of the upper blocks
            for (int y = 0; y < columnX->count(); y++) {
                blockM = getGameBlk(columnX, y);
                // if the current position match its index
                if((blockM->getX() == x) && (blockM->getY() == y)) {
                }
                else { 
                    CCFiniteTimeAction* actionMoveDown = CCMoveTo::create(duration,
                            // ccp( (blockM->getX() * blockSize), (y * blockSize) ) );
                            genPosition( blockM->getX(), y ) );
                    CCFiniteTimeAction* actionMoveLeft = CCMoveTo::create(duration,
                            // ccp( (x * blockSize), (y * blockSize) ) );
                            genPosition( x, y ) );
                    blockM->runAction( CCSequence::create(actionMoveDown, actionMoveLeft, NULL) );
                    blockM->setXY(x, y);
                }
            }
        }
    }
}

int GameLayer::testConnection ( CCArray *columns, int x, int y, int testColor, bool **cCM ) {
    CCArray *columnX, *columnL, *columnR; // at X, left, right
    DataBlock *blockC, *blockL, *blockR, *blockU, *blockD;    // center, left, right, up, down
    bool flagFound = false;

    if( (0 <= x) && (x < columns->count()) && (columns->count() > 0) ) {
        columnX = getCol(columns, x);
    }
    else {
        return 0;
    }

    // test left
    if((x > 0) && (cCM[x - 1][y] == false)) {
        columnL = getCol(columns, (x - 1));
        if(y < columnL->count()) {
            blockL = getBlk(columnL, y);
            if(blockL->getBlockColor() == testColor) {
                cCM[x - 1][y] = true;
                flagFound = true;
                testConnection(columns, (x - 1), y, testColor, cCM);
            }
        }
    }

    // test right
    if( ((x + 1) < columns->count()) && (cCM[x + 1][y] == false) ) {
        columnR = getCol(columns, (x + 1));
        if(y < columnR->count()) {
            blockR = getBlk(columnR, y);
            if(blockR->getBlockColor() == testColor) {
                cCM[x + 1][y] = true;
                flagFound = true;
                testConnection(columns, (x + 1), y, testColor, cCM);
            }
        }
    }

    // test down
    if((y > 0) && (cCM[x][y - 1] == false)) {
        blockD = getBlk(columnX, (y - 1));
        if(blockD->getBlockColor() == testColor) {
            cCM[x][y - 1] = true;
            flagFound = true;
            testConnection(columns, x, (y - 1), testColor, cCM);
        }
        // printCM( cCM, sizeX, sizeY );
    }

    // test up
    if( ((y + 1) < columnX->count()) && (cCM[x][y + 1] == false) ) {
        blockU = getBlk(columnX, (y + 1));
        if(blockU->getBlockColor() == testColor) {
            cCM[x][y + 1] = true;
            flagFound = true;
            testConnection(columns, x, (y + 1), testColor, cCM);
        }
        // printCM( cCM, sizeX, sizeY );
    }

    // return flagFound;
    return 0;

}

bool GameLayer::findConnection ( CCArray *columns, int x, int y, int touchColor ) {
    CCArray *columnX, *columnL, *columnR; // at X, left, right
    GameBlock *blockC, *blockL, *blockR, *blockU, *blockD;    // center, left, right, up, down
    bool flagFound = false;

    if( (0 <= x) && (x < columns->count()) && (columns->count() > 0) ) {
        columnX = getCol(columns, x);
    }
    else {
        return false;
    }

    // find left
    if((x > 0) && (tcm[x - 1][y] == false)) {
        columnL = getCol(columns, (x - 1));
        if(y < columnL->count()) {
            blockL = getGameBlk(columnL, y);
            if(blockL->getBlockColor() == touchColor) {
                tcm[x - 1][y] = true;
                flagFound = true;
                findConnection(columns, (x - 1), y, touchColor);
            }
        }
    }

    // find right
    if( ((x + 1) < columns->count()) && (tcm[x + 1][y] == false) ) {
        columnR = getCol(columns, (x + 1));
        if(y < columnR->count()) {
            blockR = getGameBlk(columnR, y);
            if(blockR->getBlockColor() == touchColor) {
                tcm[x + 1][y] = true;
                flagFound = true;
                findConnection(columns, (x + 1), y, touchColor);
            }
        }
    }

    // find down
    if((y > 0) && (tcm[x][y - 1] == false)) {
        blockD = getGameBlk(columnX, (y - 1));
        if(blockD->getBlockColor() == touchColor) {
            tcm[x][y - 1] = true;
            flagFound = true;
            findConnection(columns, x, (y - 1), touchColor);
        }
    }

    // find up
    if( ((y + 1) < columnX->count()) && (tcm[x][y + 1] == false) ) {
        blockU = getGameBlk(columnX, (y + 1));
        if(blockU->getBlockColor() == touchColor) {
            tcm[x][y + 1] = true;
            flagFound = true;
            findConnection(columns, x, (y + 1), touchColor);
        }
    }

    return flagFound;

}

void GameLayer::printCM ( ) {
    std::string row;
    for(int y = (sizeY - 1); y >= 0; y--) {
        row.clear();
        for ( int x = 0; x < sizeX; x++ ) {
            row = row + (B2S(this->tcm[x][y])) + " ";
        }
        CCLog(" %s", row.c_str());
    }
}

void GameLayer::printCM ( bool **cm, int sizeX, int sizeY ) {
    std::string row;
    for(int y = (sizeY - 1); y >= 0; y--) {
        row.clear();
        for ( int x = 0; x < sizeX; x++ ) {
            row = row + (B2S(cm[x][y])) + " ";
        }
        CCLog(" %s", row.c_str());
    }
}

void GameLayer::printSS ( int **s, int sizeX, int sizeY ) {
    std::stringstream ss;
    for(int y = (sizeY - 1); y >= 0; y--) {
        ss.str(" ");
        for ( int x = 0; x < sizeX; x++ ) {
            ss << s[x][y];
            ss << " ";
        }
        CCLog(" %s", ss.str().c_str());
    }
}

/*
void GameLayer::printColumns ( CCArray *columns ) {
    std::string s;
    CCArray *col;
    DataBlock *blk;
    for ( int x = 0; x < columns->count(); x++ ) {
        col = getCol( columns, x );
        s.clear();
        for(int y = 0; y < col->count() ; y++) {
            blk = getBlk( col, y );
            s =  s + GameBlock->color2str(blk->getBlockColor()) + " ";
        }
        CCLog("col %d: %s", x, s.c_str());
    }
}
*/

int GameLayer::getMaxX ( CCArray* columns ) {
    return columns->count();
}

int GameLayer::getMaxY ( CCArray* columns ) {

    CCArray* columnX;
    int maxY = 0;
    int y;

    for(int x = 0; x < columns->count(); x++) {
        columnX = getCol(columns, x);
        y = columnX->count();
        if ( maxY < y ) {
            maxY = y;
        }
    }

    return maxY;
}

CCArray* GameLayer::cloneGameColumns( CCArray *columns) {

    CCArray *reCols;
    CCArray *col, *reCol;
    DataBlock *db;
    GameBlock *gb;

    reCols = CCArray::createWithCapacity( columns->count() );

    for ( int x = 0; x < columns->count(); x++ ) {
        col = getCol(columns, x);
        reCol = CCArray::createWithCapacity( col->count() );
        for ( int y = 0; y < col->count(); y++ ) {
            gb = getGameBlk(col, y);
            db = DataBlock::create( gb->getX(), gb->getY(), gb->getBlockColor() );
            /*
            db = new DataBlock();
            db->setBlockColor( gb->getBlockColor() );
            db->setXY( gb->getX(), gb->getY() );
            */
            reCol->addObject( db );
        }
        reCols->addObject( reCol );
    }

    return reCols;
}

CCArray* GameLayer::cloneColumns( CCArray *columns) {

    CCArray *reCols;
    CCArray *col, *reCol;
    DataBlock *db;
    DataBlock *rb;

    reCols = CCArray::createWithCapacity( columns->count() );

    for ( int x = 0; x < columns->count(); x++ ) {
        col = getCol(columns, x);
        reCol = CCArray::createWithCapacity( col->count() );
        for ( int y = 0; y < col->count(); y++ ) {
            db = getBlk(col, y);
            rb = DataBlock::create( db->getX(), db->getY(), db->getBlockColor() );
            /*
            rb = new DataBlock();
            rb->setBlockColor( db->getBlockColor() );
            rb->setXY( db->getX(), db->getY() );
            */
            reCol->addObject( rb );
        }
        reCols->addObject( reCol );
    }

    return reCols;
}

void GameLayer::deleteColumns( CCArray *columns ) {

    CCArray *col;
    DataBlock *blk;

    for ( int x = 0; x < columns->count(); x++ ) {
        col = getCol(columns, x);
        if ( col->count() > 0 ) { 
            col->removeAllObjects();
        }
        // delete col; // --> this could be the root cause for Mac crash
    }

    if ( columns->count() > 0 ) { 
        columns->removeAllObjects();
    }
    // delete columns; // --> this could be the root cause for Mac crash

}

void GameLayer::clearColumns( ) {

    CCArray *col;
    GameBlock *blk;

    for ( int x = 0; x < columns->count(); x++ ) {
        col = getCol(columns, x);
        for ( int y = 0; y < col->count(); y++ ) {
            blk = getGameBlk( col, y );
            this->getChildByTag(kBGMaskTag)->removeChild( blk );
        }
        if ( col->count() > 0 ) { 
            col->removeAllObjects();
        }
    }

    if ( columns->count() > 0 ) { 
        columns->removeAllObjects();
    }

}

int GameLayer::genScore () {
    int n = 0;
    for ( int x = 0; x < sizeX; x++ ) {
        for ( int y = 0; y < sizeY; y++ ) {
            if ( tcm[x][y] ) {
                n++;
            }
        }
    }

    if ( n < 2 ) {
        return 0;
    }
    else {
        return ( n * n * 5 );
    }
}


int GameLayer::genScore ( bool **m, int sizeX, int sizeY ) {
    int n = 0;
    for ( int x = 0; x < sizeX; x++ ) {
        for ( int y = 0; y < sizeY; y++ ) {
            if ( m[x][y] ) {
                n++;
            }
        }
    }

    if ( n < 2 ) {
        return 0;
    }
    else {
        return ( n * n * 5 );
    }
}

void GameLayer::updateSelection ( ) {
    CCArray *col;
    GameBlock *blk;
    std::string img;

    for(int x = 0; x < columns->count(); x++) {
        col = getCol(columns, x);
        for(int y = (col->count() - 1); y >= 0; y--) {
            if(tcm[x][y] == true) {
                // update connected block
                blk = getGameBlk( col, y );
                img = blk->getColorStr() + "_heart.png";
                blk->setTexture( 
                        CCTextureCache::sharedTextureCache()->addImage(
                            img.c_str()) );
                
                CCMoveBy* moveUp = CCMoveBy::create(0.05, ccp(0, 3));
                CCMoveBy* moveDown = CCMoveBy::create(0.1, ccp(0, -3));
                CCSequence* moveUpAndDown = CCSequence::createWithTwoActions(moveUp, moveDown);
                blk->runAction(moveUpAndDown);
            }
        }
    }
}

void GameLayer::recoverSelection ( ) {
    CCArray *col;
    GameBlock *blk;
    std::string img;
    
    selected = false;

    for(int x = 0; x < columns->count(); x++) {
        col = getCol(columns, x);
        for(int y = (col->count() - 1); y >= 0; y--) {
            if(tcm[x][y] == true) {
                // recover connected block
                blk = getGameBlk( col, y );
                img = blk->getColorStr() + ".png";
                blk->setTexture( 
                        CCTextureCache::sharedTextureCache()->addImage(
                            img.c_str()) );
            }
        }
    }
}

void GameLayer::updateScore ( ) {
    
    if (GameManager::sharedGameManager()->gameMode == 3) return;
    
    // should replace it with CCLabelBMFont
    CCString* score_str =CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelScore"), totalScore);
    CCLabelBMFont* text_score = (CCLabelBMFont*)this->getChildByTag(kScoreTag);
    text_score->setString(score_str->getCString());
    
    
    switch (GameManager::sharedGameManager()->gameMode) {
        case 0:
            if (totalScore >= targetScore) {
                
                if (cheered == false) {
                    /*
                    CCSprite* stageClear = CCSprite::create("stage_clear.png");
                    stageClear->setPosition(ccp(screenSize.width * kClearX, screenSize.height * kClearY));
                    stageClear->setTag(kStageClearTag);
                    this->addChild(stageClear);
                    stageClear->setOpacity(0);
                     */
                    
                    /*
                    CCFadeIn* fadein = CCFadeIn::create(0.5);
                    stageClear->runAction(fadein);
                     */
                    
                    CCLabelBMFont* score = (CCLabelBMFont*)this->getChildByTag(kScoreTag);
                    score->setColor(ccc3(128, 255, 128));
                    
                    CCScaleBy* scaleUp = CCScaleBy::create(0.25, 1.25);
                    CCScaleBy* scaleDown = CCScaleBy::create(0.25, 0.8);
                    CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleUp, scaleDown);
                    CCRepeat* repeat = CCRepeat::create(scaleUpAndDown, 3);
                    score->runAction(repeat);
                    
                    CCLabelBMFont* info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                    info->setString(GameManager::sharedGameManager()->getLocalizedString("stageClear"));
                    
                    GameManager::sharedGameManager()->playSoundEffect("cheer.wav");
                    cheered = true;

                }
            }
            
            break;
        case 1:

            if (totalScore > GameManager::sharedGameManager()->gameSetting.bestScoreTime) {
                
                if (cheered == false) {
                    if (GameManager::sharedGameManager()->gameSetting.bestScoreTime != 0) {
                        GameManager::sharedGameManager()->playSoundEffect("cheer.wav");
                    }
                    cheered = true;
                }
                GameManager::sharedGameManager()->gameSetting.bestScoreTime = totalScore;
                GameManager::sharedGameManager()->saveGameSetting();
                
                CCString* target_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelBest"),totalScore);
                CCLabelBMFont* target_score = (CCLabelBMFont*)this->getChildByTag(kTargetTag);
                target_score->setString(target_str->getCString());
            }
            break;
        case 2:
            if (totalScore > GameManager::sharedGameManager()->gameSetting.bestScoreMove) {
                
                if (cheered == false) {
                    if (GameManager::sharedGameManager()->gameSetting.bestScoreMove != 0) {
                        GameManager::sharedGameManager()->playSoundEffect("cheer.wav");
                    }
                    cheered = true;
                }
                
                GameManager::sharedGameManager()->gameSetting.bestScoreMove = totalScore;
                GameManager::sharedGameManager()->saveGameSetting();
                
                CCString* target_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelBest"),totalScore);
                CCLabelBMFont* target_score = (CCLabelBMFont*)this->getChildByTag(kTargetTag);
                target_score->setString(target_str->getCString());
            }
            break;
        case 3:
            break;
        default:
            break;
    }
    // should replace it with CCLabelBMFont
}

bool GameLayer::testStageEnd ( ) {
    int num = 0; // number of blocks for bonus
    CCArray *col;
    GameBlock *blk;

    for(int x = 0; x < columns->count(); x++) {
        col = getCol(columns, x);
        for(int y = 0; y < col->count(); y++) {
            blk = getGameBlk( col, y );
            clearTCM();
            if( findConnection ( columns, x, y, blk->getBlockColor() ) ) {
                // not end
                
                // save the state after 0.8 second, to ensure the score is updated
                saveStateThenScore();
                this->setTouchEnabled(true);
                return false;
            }
            else {
                num++;
            }
        }
    }

    
    // End of stage
    playEndAnimation(num);
    
    //totalScore += genBonusScore( num );

    return true;

}

void GameLayer::exitBattleSelected(CCNode* pNode) {
    switch (pNode->getTag()) {
        case 1:  {   // exit
            
            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            this->removeFromParent();
            GameManager::sharedGameManager()->disconnectMatch();
            CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, MenuScene::create()));
            
            break;
        }
        case 2:     // resume
            this->removeChildByTag(kExitBattleMenuTag);
            
            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            setTouchEnabled(true);

            
            break;
        default:
            break;
            
            
    }
}


void GameLayer::continueSelected(cocos2d::CCNode *pNode) {
    

    switch (pNode->getTag()) {
        case 1:  {   // continue
            
            if (GameManager::sharedGameManager()->gameSetting.coins < 5) {
                this->addCoinButtonTouched();
            }
            else {
                this->removeChildByTag(kRestartMenuTag);

                GameManager::sharedGameManager()->gameSetting.coins-=5;
                GameManager::sharedGameManager()->saveGameSetting();
                updateCoins();
                
                GameManager::sharedGameManager()->logEvent("Item: restart");

                
                stage--;
                totalScore = stageBeginScore;

                GameManager::sharedGameManager()->playSoundEffect("select.wav");
                initStage();
            }
            break;
        }
        case 2:     // end game
            this->removeChildByTag(kRestartMenuTag);

            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            isGameOver = true;
            setTouchEnabled(false);
            
            // No need to save stage
            GameManager::sharedGameManager()->gameState.stage = 0;
            GameManager::sharedGameManager()->saveGameState();
            
            popupGameOver();
            
            break;
        default:
            break;
            
            
    }
    
    /*
    if (pNode->getTag() == 2) {
        GameManager::sharedGameManager()->gameState.stage = 0;
    }
    this->removeChildByTag(kPopupMenuTag);
    playStartGameAnimation();
     */
}

void GameLayer::popupExitBattleAlert() {
    this->setTouchEnabled(false);
    unschedule(schedule_selector(GameLayer::updateGCScore));
    unschedule(schedule_selector(GameLayer::countDown));
    GameManager::sharedGameManager()->disconnectMatch();

    
    
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kExitBattleMenuTag);
    this->addChild(background, 15);
    
    CCSprite *background_img = CCSprite::create("menu_bg.png");
    background_img->setAnchorPoint(ccp(0.0f, 0.5f));
    background_img->setPosition(ccp(0.0, screenSize.height * 0.45));
    background->addChild(background_img);
    
    CCLabelBMFont* query = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    query->setScale(0.7);
    
    GameManager* gm = GameManager::sharedGameManager();
    CCString* queryStr = CCString::create(gm->getLocalizedString("exitBattleAlert"));
    
    
    query->setString(queryStr->getCString());
    query->setAnchorPoint(ccp(0.5f, 0.5f));
    
    query->setPosition(screenSize.width * 0.5, background_img->getContentSize().height * 0.7);
    background_img->addChild(query);
    
    CCMenuItemImage* yesLabel = CCMenuItemImage::create("tick.png", "tick.png", this, menu_selector(GameLayer::exitBattleSelected));
    CCMenuItemImage* noLabel = CCMenuItemImage::create("cross.png", "cross.png", this, menu_selector(GameLayer::exitBattleSelected));
    yesLabel->setTag(1);
    noLabel->setTag(2);
    //noLabel->setVisible(false);
    
    CCMenu* menu = CCMenu::create(yesLabel, NULL);
    menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.2);
    
    menu->setAnchorPoint(ccp(0.5f, 0.5f));
    
    menu->setPosition(screenSize.width * 0.5, background_img->getContentSize().height * 0.3);
    background_img->addChild(menu);
}

void GameLayer::popupExitBattleQuery() {
    this->setTouchEnabled(false);
    
    
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kExitBattleMenuTag);
    this->addChild(background, 15);
    
    CCSprite *background_img = CCSprite::create("menu_bg.png");
    background_img->setAnchorPoint(ccp(0.0f, 0.5f));
    background_img->setPosition(ccp(0.0, screenSize.height * 0.45));
    background->addChild(background_img);
    
    CCLabelBMFont* query = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    query->setScale(0.7);
    
    GameManager* gm = GameManager::sharedGameManager();
    CCString* queryStr = CCString::create(gm->getLocalizedString("exitBattleQuery"));

    
    query->setString(queryStr->getCString());
    query->setAnchorPoint(ccp(0.5f, 0.5f));

    query->setPosition(screenSize.width * 0.5, background_img->getContentSize().height * 0.7);
    background_img->addChild(query);
    
    CCMenuItemImage* yesLabel = CCMenuItemImage::create("tick.png", "tick.png", this, menu_selector(GameLayer::exitBattleSelected));
    CCMenuItemImage* noLabel = CCMenuItemImage::create("cross.png", "cross.png", this, menu_selector(GameLayer::exitBattleSelected));
    yesLabel->setTag(1);
    noLabel->setTag(2);
    
    CCMenu* menu = CCMenu::create(yesLabel, noLabel, NULL);
    menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.2);
    
    menu->setAnchorPoint(ccp(0.5f, 0.5f));

    menu->setPosition(screenSize.width * 0.5, background_img->getContentSize().height * 0.3);
    background_img->addChild(menu);
}



void GameLayer::popupRestartQuery() {
    this->setTouchEnabled(false);
    
    
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kRestartMenuTag);
    this->addChild(background, 15);
    
    CCSprite *background_img = CCSprite::create("menu_bg.png");
    background_img->setAnchorPoint(ccp(0.0f, 0.5f));
    background_img->setPosition(ccp(0.0, screenSize.height * 0.45));
    background->addChild(background_img);
    
    CCLabelBMFont* query = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    query->setScale(0.7);
    
    GameManager* gm = GameManager::sharedGameManager();
    CCString* queryStr = CCString::createWithFormat(gm->getLocalizedString("restartQuery"),
                                                    gm->gameState.stage);
    
    query->setString(queryStr->getCString());
    query->setAnchorPoint(ccp(0.5f, 0.5f));
#ifdef __Halloween__
    query->setPosition(screenSize.width * 0.5, 120 * 0.7);
#else
    query->setPosition(screenSize.width * 0.5, 105 * 0.7);
#endif
    background_img->addChild(query);
    
    CCMenuItemImage* yesLabel = CCMenuItemImage::create("tick.png", "tick.png", this, menu_selector(GameLayer::continueSelected));
    CCMenuItemImage* noLabel = CCMenuItemImage::create("cross.png", "cross.png", this, menu_selector(GameLayer::continueSelected));
    yesLabel->setTag(1);
    noLabel->setTag(2);
    
    CCMenu* menu = CCMenu::create(yesLabel, noLabel, NULL);
    menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.2);
    
    menu->setAnchorPoint(ccp(0.5f, 0.5f));
#ifdef __Halloween__
    menu->setPosition(screenSize.width * 0.5, 120 * 0.35);
#else
    menu->setPosition(screenSize.width * 0.5, 105 * 0.3);
#endif
    background_img->addChild(menu);
}


void GameLayer::finishStage() {
    updateScore();
    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
    text_info->setString("");
    
    clearColumns();
    CCLayerColor *bg_mask = (CCLayerColor*)this->getChildByTag(kBGMaskTag);
    this->removeChild(bg_mask);
    
    
    
    if ( totalScore < targetScore ) {       // game over
        
        if (stage >= 2) {
            GameManager::sharedGameManager()->playSoundEffect("clear.wav");

            popupRestartQuery();
        }
        else {      // No continue option if it's stage 1
            isGameOver = true;
            setTouchEnabled(false);
            
            // No need to save stage
            GameManager::sharedGameManager()->gameState.stage = 0;
            GameManager::sharedGameManager()->saveGameState();
            
            popupGameOver();
        }
        /*
        isGameOver = true;
        setTouchEnabled(false);
        
        // No need to save stage
        GameManager::sharedGameManager()->gameState.stage = 0;
        popupGameOver();
         */
    }
    else {
        int gap = GameManager::sharedGameManager()->getServerInterger("interBetweenStage");
        
        // 2018: always show
        if (gap == 0) {
            gap = 3;
        }
        
        if (gap != 0) {
            if (stage % gap == 0) {
                
                if ((GameManager::sharedGameManager()->getServerBoolean("ratePop") == true) &&
                    (CCUserDefault::sharedUserDefault()->getBoolForKey("didRating25") == false)) {
                    GameManager::sharedGameManager()->presendPromoPopAlert(true);
                }
                else {
                    GameManager::sharedGameManager()->showInterstitial();
                }
            }
        }
        
        GameManager::sharedGameManager()->playSoundEffect("clear.wav");

        // Send score before going to next stage
        sendScore();
        initStage();
    }
}


void GameLayer::clearTCM ( ) {
    // clear tcm
    for(int x = 0; x < 10; x++) {
        for(int y = 0; y < 10; y++) {
            tcm[x][y] = false;
        }
    }
}


int GameLayer::genTargetScore2 (int stage) {
    int ts;
    int delta;
    
    ts = 2500;
    
    if ( stage == 0 ) {
        return 0;
    }
    else if ( stage == 1 ) {
        return 1000;
    }
    else if ( stage == 2 ) {
        return 2500;
    }
    else {
        for( int i = 3; i <= stage; i++ ) {
            
            
            delta = 2000;
            
            delta += 20 * (i-3);
            
            ts += delta;
        }
    }
    
    return ts;
}
int GameLayer::genTargetScore( int stage ) {
    
    int ts;
    int delta;
    float w;
    int i;

    ts = 1000;

    if ( stage == 0 ) {
        return 0;
    }
    else if ( stage == 1 ) {
        return 1000;
    }
    else { 
        for( int i = 2; i <= stage; i++ ) {

            delta = 2000;

            w = 1 + 0.10 * floor( i / 10 );

            if ( i % 3 == 0 ) {
                delta += 1000 * w;
            }
            else if ( i % 10 == 0 ) {
                delta += 1000 * w;
            }

            ts += delta;
        }
    }

    return ts;
}

/*
CCPoint GameLayer::genPosition ( int x, int y ) {
    return ccp(( x * blockSize + adX ), ( y * blockSize + adY ));
}
*/

CCPoint GameLayer::genPosition ( float x, float y ) {
    return ccp(( x * blockSize ), ( y * blockSize ));
}
int GameLayer::getTouchedX ( float x ) {
    
    if (x < adX) return -1;
    
    return ( (int) ( x - adX ) / (blockSize * zoomRatio));
}

int GameLayer::getTouchedY ( float y ) {
    if (y < adY) return -1;
    
    return ( (int) ( y - adY ) / (blockSize * zoomRatio));
}

void GameLayer::hearts( GameBlock *blk ) {
    
#ifdef __Star__
    
#ifdef __Fruit__
    std::string fn =  "water.png";
#else
    std::string fn =  "star.png";
#endif
#else
    std::string fn =  blk->getColorStr()+ "_heart.png";
#endif
    
    CCParticleSystem* h = CCParticleExplosion::create();
    // CCParticleSystem* h = CCParticleFlower::createWithTotalParticles(8);
    h->setTexture( 
            // CCTextureCache::sharedTextureCache()->addImage("red_heart.png") );
            CCTextureCache::sharedTextureCache()->addImage( fn.c_str() ) );
    h->setTotalParticles(16);
    h->setGravity(ccp(0, -350));
    // h->setGravity(ccp(blk->getX(), blk->getY()-100));
    h->setSpeed( 300 );

    h->setStartSize( 12 );
    h->setStartSizeVar( 6 );
    h->setEndSize( 16 );
    h->setEndSizeVar( 10 );

    h->setBlendAdditive( false );
    // h->setBlendAdditive( true );
    // h->setBlendFunc ( (ccBlendFunc){GL_ONE, GL_ZERO} );
    // h->setBlendFunc ( (ccBlendFunc){GL_SRC_COLOR, GL_SRC_COLOR} );
    
    switch ( blk->getBlockColor() ) {
        /*
        // old color, not in use.
        case 1 : // blue
            h->setStartColor( (ccColor4F){23.0/255.0, 124.0/255.0, 254.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){23.0/255.0, 124.0/255.0, 254.0/255.0, 1.0} );
            break;
        case 2 : // green
            h->setStartColor( (ccColor4F){61.0/255.0, 208.0/255.0, 12.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){61.0/255.0, 208.0/255.0, 12.0/255.0, 1.0} );
            break;
        case 4 : // purple
            h->setStartColor( (ccColor4F){212.0/255.0, 16.0/255.0, 255.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){212.0/255.0, 16.0/255.0, 255.0/255.0, 1.0} );
            break;
        case 0 : // red
            h->setStartColor( (ccColor4F){251.0/255.0, 0.0/255.0, 8.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){251.0/255.0, 0.0/255.0, 8.0/255.0, 1.0} );
            break;
        case 3 : // yellow
            h->setStartColor( (ccColor4F){248.0/255.0, 177.0/255.0, 9.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){248.0/255.0, 177.0/255.0, 9.0/255.0, 1.0} );
            break;
        default:
            h->setStartColor( (ccColor4F){255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0} );
            break;
            */
#ifdef __Halloween__
        case 1 : // blue - ghost - white
            h->setStartColor( (ccColor4F){255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0} );
            break;
        case 2 : // green - cat
            h->setStartColor( (ccColor4F){133.0/255.0, 226.0/255.0, 31.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){133.0/255.0, 226.0/255.0, 31.0/255.0, 1.0} );
            break;
        case 4 : // purple - hat
            h->setStartColor( (ccColor4F){186.0/255.0,   0.0/255.0, 186.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){186.0/255.0,   0.0/255.0, 186.0/255.0, 1.0} );
            break;
        case 0 : // red - bat
            h->setStartColor( (ccColor4F){255.0/255.0,  0.0/255.0,   0.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){255.0/255.0,  0.0/255.0,   0.0/255.0, 1.0} );
            break;
        case 3 : // orange - pumpkin
            h->setStartColor( (ccColor4F){255.0/255.0, 173.0/255.0,  2.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){255.0/255.0, 173.0/255.0,  2.0/255.0, 1.0} );
            break;
        default:
            h->setStartColor( (ccColor4F){255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0} );
            break;
#else
            
        case 1 : // blue
            h->setStartColor( (ccColor4F){77.0/255.0, 206.0/255.0, 254.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){77.0/255.0, 206.0/255.0, 254.0/255.0, 1.0} );
            break;
        case 2 : // green
            h->setStartColor( (ccColor4F){121.0/255.0, 206.0/255.0, 73.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){121.0/255.0, 206.0/255.0, 73.0/255.0, 1.0} );
            break;
        case 4 : // purple
            h->setStartColor( (ccColor4F){182.0/255.0, 100.0/255.0, 221.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){182.0/255.0, 100.0/255.0, 221.0/255.0, 1.0} );
            break;
        case 0 : // red
            h->setStartColor( (ccColor4F){252.0/255.0, 71.0/255.0, 119.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){252.0/255.0, 71.0/255.0, 119.0/255.0, 1.0} );
            break;
        case 3 : // yellow
            h->setStartColor( (ccColor4F){251.0/255.0, 206.0/255.0, 40.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){251.0/255.0, 206.0/255.0, 40.0/255.0, 1.0} );
            break;
        default:
            h->setStartColor( (ccColor4F){255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0} );
            h->setEndColor( (ccColor4F){255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0} );
            break;
            
#endif

    }

    
    h->setStartColorVar( (ccColor4F){0.0, 0.0, 0.0, 0.0} );
    h->setEndColorVar( (ccColor4F){0.0, 0.0, 0.0, 0.0} );

    /* Fix
     CCPoint blockPosition = genPosition(blk->getX()+0.5, blk->getY()+0.5 );
     blockPosition = ccpMult(blockPosition, zoomRatio);
     blockPosition = ccpAdd(blockPosition, ccp(adX, adY));
     h->setPosition( blockPosition );
     */
    h->setPosition( genPosition( blk->getX()+0.5, blk->getY()+0.5 ) );
    h->setAutoRemoveOnFinish( true );

    // h->
    this->getChildByTag(kBGMaskTag)->addChild( h );
}











// The following replaced updateColumn()
int GameLayer::getConnectedCount() {
    int n = 0;
    for ( int x = 0; x < sizeX; x++ ) {
        for ( int y = 0; y < sizeY; y++ ) {
            if ( tcm[x][y] ) {
                n++;
            }
        }
    }
    return n;
}

int GameLayer::getRemainingCount() {
    int num = 0;
    CCArray *columnX;
    
    for ( int x = 0; x < columns->count(); x++ ) {
        columnX = getCol(columns, x);
        num = num + columnX->count();
    }
    
    return num;
}

void GameLayer::playPopSoundOnce() {
    GameManager::sharedGameManager()->stopSoundEffect(lastPopSound);
    lastPopSound = GameManager::sharedGameManager()->playSoundEffect("pop.wav");
}

void GameLayer::playPopSound() {
    int n = this->getConnectedCount();
    
    CCCallFunc* playSound = CCCallFunc::create(this, callfunc_selector(GameLayer::playPopSoundOnce));
    CCDelayTime* delay = CCDelayTime::create(0.08);
    CCSequence* actionOnce = CCSequence::createWithTwoActions(playSound, delay);
    CCRepeat* repeatAction = CCRepeat::create(actionOnce, n);
    this->runAction(repeatAction);
}

void GameLayer::popOneRemainingBlock() {
    CCArray *columnX;
    
    for(int x = 0; x < columns->count(); x++) {
        columnX = getCol(columns, x);

        for(int y = (columnX->count() - 1); y >= 0; y--) {
            this->popBlock(0, x,y);
            tcm[x][y] = false;
            return;
        }
    }
}

void GameLayer::popOneBlock() {
    CCArray *columnX;

    for(int x = 0; x < columns->count(); x++) {
        columnX = getCol(columns, x);
        // y: index for the current veritcal position in TCM[x][y]
        for(int y = (columnX->count() - 1); y >= 0; y--) {
            if(tcm[x][y] == true) {
                this->popBlock(popOrder, x,y);
                tcm[x][y] = false;
                //totalScore += (popOrder * 10 -5);
                popOrder++;
                return;

            }
        }
    }
}

void GameLayer::popBlock(int order, int column, int row){
    //CCLOG("Poping: %d, %d, %d", order, column, row);
    CCArray* columnX = getCol(columns, column);
    GameBlock* blockC = getGameBlk(columnX, row);
    hearts( blockC );
    playPopSoundOnce();
    
    if (order != 0) {
        if (GameManager::sharedGameManager()->gameMode == 3) {
            updateTargetAnimation(blockC, order);
            
            //playScoreAnimation(blockC, order);
        }
        else {
            playScoreAnimation(blockC, order * 10 -5);
        }
    }
    
    // clean up
    this->getChildByTag(kBGMaskTag)->removeChild(blockC);
    columnX->removeObjectAtIndex(row);
}

void GameLayer::adventureGameOverItemTouched(CCNode* pSender) {
    CCMenuItemImage* item = (CCMenuItemImage*)pSender;
    if (item == NULL) return;
    
    // Clear the screen
    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
    text_info->setString("");
    
    this->removeChildByTag(kStageTargetTag);

    CCLayerColor* bg_mask = (CCLayerColor*)this->getChildByTag(kBGMaskTag);

    if (bg_mask) {
        CCArray* allBlocks = bg_mask->getChildren();
        for (int i=0; i<allBlocks->count(); i++) {
            GameBlock* block = dynamic_cast <GameBlock*> (allBlocks->objectAtIndex(i));
            if (block) {
                hearts(block);
            }
        }
        
        clearColumns();
        bg_mask->removeAllChildren();
        this->removeChild(bg_mask);
    }
    
    
    int tag = item->getTag();
    switch (tag) {
        case 0:
            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            GameManager::sharedGameManager()->hideBanner();

            this->removeFromParent();
            CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, StageScene::create()));
            break;
            
        case 1:
            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            
            // Fix bug, choose restart game but not enough coins
            this->removeChildByTag(kPopupMenuTag);
            totalScore = 0;
            updateScore();
            
            needFadeIn = false;
            this->scheduleOnce(schedule_selector(GameLayer::initStage), 0.3);
            break;
        case 2:
            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            
            // Fix bug, choose restart game but not enough coins
            this->removeChildByTag(kPopupMenuTag);
            stage++;
            totalScore = 0;
            updateScore();
            
            needFadeIn = false;
            this->scheduleOnce(schedule_selector(GameLayer::initStage), 0.3);
            break;
            
        default:
            break;
            
    }
    

    
}


void GameLayer::shareTouched(CCNode* pSender) {
    CCMenu* menu = (CCMenu*)this->getChildByTag(kGameOverMenuTag)->getChildByTag(kGameOverHintTag);
    if (menu) {
        menu->removeFromParentAndCleanup(true);
    }
    GameManager::sharedGameManager()->playSoundEffect("select.wav");
    GameManager::sharedGameManager()->shareSocialNetwork(totalScore);
    
}
void GameLayer::gameOverItemTouched(CCNode* pSender) {
    
    CCMenuItemImage* item = (CCMenuItemImage*)pSender;
    if (item == NULL) return;
    
    CCScaleTo* scaleUp = CCScaleTo::create(0.15, 1.2);
    CCScaleTo* scaleDown = CCScaleTo::create(0.15, 1.0);
    CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleUp, scaleDown);
    item->runAction(scaleUpAndDown);
    
    int tag = item->getTag();
    switch (tag) {
        case 0:
            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            this->removeFromParent();
            CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, MenuScene::create()));
            break;
            
        case 1: {

            if (GameManager::sharedGameManager()->gameMode == 4) {
                GameManager::sharedGameManager()->playSoundEffect("select.wav");
                this->removeFromParent();
                CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, MenuScene::create()));
                GameManager::sharedGameManager()->hostMatch();
                break;
            }
            

            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            
            /*
            if ((GameManager::sharedGameManager()->gameMode != 0) && (GameManager::sharedGameManager()->gameMode != 3)) {
                

                if (GameManager::sharedGameManager()->gameSetting.coins < 5) {
                    this->addCoinButtonTouched();
                    return;
                }
                else {
                    GameManager::sharedGameManager()->gameSetting.coins-=5;
                    GameManager::sharedGameManager()->saveGameSetting();
                    updateCoins();
                    GameManager::sharedGameManager()->playSoundEffect("coin.mp3");
                }
            }
            else {
                GameManager::sharedGameManager()->playSoundEffect("select.wav");
            }
             */
            
            // Fix bug, choose restart game but not enough coins
            this->removeChildByTag(kGameOverMenuTag);
            CCLabelBMFont* score = (CCLabelBMFont*)this->getChildByTag(kScoreTag);
            score->setOpacity(0);
            
            totalScore = 0;
            stage = 0;
            updateScore();
            
            
            this->scheduleOnce(schedule_selector(GameLayer::initStage), 0.3);
            break;
        }
        case 2: {
            CCMenu* menu = (CCMenu*)this->getChildByTag(kGameOverMenuTag)->getChildByTag(kGameOverHintTag);
            if (menu) {
                menu->removeFromParentAndCleanup(true);
            }
            /*
            CCLabelBMFont* label = (CCLabelBMFont*)this->getChildByTag(kGameOverMenuTag)->getChildByTag(kGameOverHintTag);
            if (label) {
                label->setString(""); //GameManager::sharedGameManager()->getLocalizedString("beatMe"));
                //CCFadeOut* fadeout = CCFadeOut::create(2);
                //label->runAction(fadeout);
            }
             */
            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            GameManager::sharedGameManager()->shareSocialNetwork(totalScore);
            break;
        }
        case 3:
            GameManager::sharedGameManager()->playSoundEffect("select.wav");
            GameManager::sharedGameManager()->showGameCenter();
            break;
        default:
            break;
            
    }
    


}

void GameLayer::playGameOverAnimation() {
    
    
    // TODO: disable menu when playing animation
    
    fadeOutControls();
    
    // Move score
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        
        CCLabelBMFont* text_score = (CCLabelBMFont*)this->getChildByTag(kScoreTag);
        CCFadeOut* fadeout = CCFadeOut::create(0.5);
        text_score->runAction(fadeout);
       
        unschedule(schedule_selector(GameLayer::updateGCScore));
        moveGCScore();
        
        GameManager::sharedGameManager()->disconnectMatch();
    }
    else {
        CCLabelBMFont* text_score = (CCLabelBMFont*)this->getChildByTag(kScoreTag);
        CCScaleTo* scaleUp = CCScaleTo::create(0.5, 2.0);
        CCMoveTo* moveTo = CCMoveTo::create(0.5, ccp(screenSize.width * 0.5, screenSize.height * 0.5));
        CCSpawn* spawn = CCSpawn::createWithTwoActions(scaleUp, moveTo);
        text_score->runAction(spawn);
    }
    
    // Scale up background
    
#ifdef __Fruit__
#else
    CCSprite* gameOverBackground = (CCSprite*)this->getChildByTag(kGameOverMenuTag)->getChildByTag(kGameOverBackgroundTag);
    CCDelayTime* delay2 = CCDelayTime::create(0.5);
    CCScaleTo* scaleUp2 = CCScaleTo::create(0.5, 1.32);
    CCSequence* sequence2= CCSequence::createWithTwoActions(delay2, scaleUp2);
    gameOverBackground->runAction(sequence2);
    
    CCRotateBy* rotate = CCRotateBy::create(5.0, 10.0);
    CCRepeatForever* repeat = CCRepeatForever::create(rotate);
    gameOverBackground->runAction(repeat);
    
#endif
    
    // Fade in menu, title and mode text
    int tags[2] = {kGameOverModeTag, kGameOverTitleTag};
    for (int i=0; i<2; i++) {
        CCNode* obj = this->getChildByTag(kGameOverMenuTag)->getChildByTag(tags[i]);
        CCDelayTime* delay = CCDelayTime::create(0.5);
        CCFadeIn* fadein = CCFadeIn::create(1.0);
        CCSequence* sequence= CCSequence::createWithTwoActions(delay, fadein);
        obj->runAction(sequence);
    }
    
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        this->scheduleOnce(schedule_selector(GameLayer::rewardBattleCoins), 0.5);
 
    }
    else {
        this->scheduleOnce(schedule_selector(GameLayer::rewardCoins), 0.5);
    }
    //this->scheduleOnce(schedule_selector(GameLayer::enableGameOverMenu), 1.5);

    
    // Gives reward, TODO: animation
    /*
    if (GameManager::sharedGameManager()->gameMode != 0) {
        
        int reward = this->lookupReward(totalScore);

        for (int i=0 ; i < reward; i++) {
            CCSprite* coin = CCSprite::create("coin.png");
            coin->setPosition(ccp((i + 1) * screenSize.width / (reward + 1), screenSize.height * 0.4));
            this->addChild(coin);
        }
    }
    
    this->scheduleOnce(schedule_selector(GameLayer::enableGameOverMenu), 1.5);
    this->scheduleOnce(schedule_selector(GameLayer::updateCoins), 1.5);
    */
    
    /*
    CCMenu* gameOverMenu = (CCMenu*)this->getChildByTag(kGameOverMenuTag)->getChildByTag(kGameOverMenuTag);
    CCDelayTime* delay1 = CCDelayTime::create(0.5);
    CCFadeIn* fadeIn1 = CCFadeIn::create(1.0);
    CCSequence* sequence1= CCSequence::createWithTwoActions(delay1, fadeIn1);
    gameOverMenu->runAction(sequence1);
     */
}

void GameLayer::playRewardCoinsAnimation2(float delay, int reward, float x, float y) {
    if (reward == 0) return;
    
    CCSprite* coin = CCSprite::create("coin.png");
    coin->setTag(reward);
    coin->setPosition(ccp(x,y));
    this->addChild(coin, -1);
    coin->setOpacity(0);
    
    CCDelayTime* delayTime = CCDelayTime::create(delay);
    CCFadeIn* fadeIn = CCFadeIn::create(0.1);
    CCMoveTo* move = CCMoveTo::create(0.5, ccp(screenSize.width * kCoinsX, screenSize.height * kCoinsY));
    CCCallFuncN* remove = CCCallFuncN::create(this, callfuncN_selector(GameLayer::removeCoin2));
    CCCallFunc* sound2 = CCCallFunc::create(this, callfunc_selector(GameLayer::playCoinsInSound));

    CCSequence* action = CCSequence::create(delayTime, fadeIn, move, remove, sound2, NULL);
    
    coin->runAction(action);
    

}
void GameLayer::playRewardCoinsAnimation(int reward) {
    for (int i=0; i<reward; i++) {
        CCSprite* coin = CCSprite::create("coin.png");
        coin->setPosition(ccp((i + 1) * screenSize.width / (reward + 1), screenSize.height * 0.3));
        coin->setOpacity(0);
        //coin->setTag(kRewardCoinTagBegin + i);
        this->addChild(coin, -1);
        
        CCFadeIn* fadein = CCFadeIn::create(0.5);
        CCDelayTime* delay = CCDelayTime::create(i * 0.2);
        CCCallFunc* sound = CCCallFunc::create(this, callfunc_selector(GameLayer::playCoinSound));
        CCDelayTime* delay2 = CCDelayTime::create((reward-i) * 0.15);
        CCMoveTo* move = CCMoveTo::create(0.5, ccp(screenSize.width * kCoinsX, screenSize.height * kCoinsY));
        CCCallFuncN* remove = CCCallFuncN::create(this, callfuncN_selector(GameLayer::removeCoin));
        CCSequence* action = NULL;
        
        CCCallFunc* sound2 = CCCallFunc::create(this, callfunc_selector(GameLayer::playCoinsInSound));
        
        if (i== 0) {
            action = CCSequence::create(delay, sound, fadein, delay2, move, remove, sound2, NULL);
        }
        else {
            action = CCSequence::create(delay, sound, fadein, delay2, move, remove, NULL);
            
        }
        
        
        coin->runAction(action);
    }
    

}

void GameLayer::rewardBonusCoin() {
    
    if (isScoreShared == false) {
        isScoreShared = true;
        
        int rewardCoin = GameManager::sharedGameManager()->getServerInterger("socialCoin");
    
        if (rewardCoin <= 0 || rewardCoin >=10) rewardCoin = 1;
        playRewardCoinsAnimation(rewardCoin);

    }
}

void GameLayer::rewardBattleCoins() {
    int playerCount = GameManager::sharedGameManager()->getPlayerCount();
    
    int rewardMatrix[][4] = {{10,0,0,0}, {10,5,0,0}, {10,7,3,0}};
    const char* medalName[4] = {"medal_gold.png", "medal_silver.png", "medal_bronze.png", NULL};
    
    float delay = 0.5 * playerCount;
    
    for (int i=0; i<playerCount; i++) {
        CCSprite* player = (CCSprite*)this->getChildByTag(kGCScoreLayerTagStart + i);
        int rank = GameManager::sharedGameManager()->getRank(i);
        
        CCSprite* coin = CCSprite::create("coin.png");

        
        
        
        coin->setPosition(ccp(screenSize.width * 0.65, player->getPosition().y));
        this->addChild(coin);
        
        int coinCount = 0;
        if ((playerCount < 2) || (playerCount > 4) || (rank < 0) || (rank > 3)) {
            coinCount = 0;
        }
        else {
            coinCount = rewardMatrix[playerCount - 2][rank];
        }
        
        CCString* str = CCString::createWithFormat("x %d", coinCount);
        CCLabelBMFont* label = CCLabelBMFont::create(str->getCString(), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        label->setScale(0.8);

        label->setAnchorPoint(ccp(0.0, 0.5));
        label->setPosition(ccp(screenSize.width * 0.1, coin->getContentSize().height * 0.5));
        
        if (medalName[rank] != NULL) {
            CCSprite* medal = CCSprite::create(medalName[rank]);
            medal->setPosition(ccp(screenSize.width * (-0.45), coin->getContentSize().height * 0.5));
            coin->addChild(medal);
        }
        
        coin->setCascadeOpacityEnabled(true);
        //label->setPosition(ccp(screenSize.width * 0.7, player->getPosition().y));
        coin->addChild(label);
        coin->setOpacity(0);
        
        
        
        CCFadeIn* fadein = CCFadeIn::create(0.1);
        CCDelayTime* delayin = CCDelayTime::create(rank * 0.5);
        CCCallFunc* sound = CCCallFunc::create(this, callfunc_selector(GameLayer::playCoinSound));
        CCSequence* action = CCSequence::create(delayin, fadein, sound, NULL);
        coin->runAction(action);

        
        if (i == playerCount - 1) {
            // it's me
            playRewardCoinsAnimation2(delay, coinCount, coin->getPosition().x, coin->getPosition().y);
            
            sendBattleScore(coinCount);
            if (coinCount != 0) delay += 0.8;
        }
    }
    
    delay += 0.2;

    isScoreShared = false;
    
    this->scheduleOnce(schedule_selector(GameLayer::fadeInMenu), delay);
    
    
}
void GameLayer::rewardCoins() {
    
    int reward = this->lookupReward(totalScore);
    
    if (reward != 0) {
        if (GameManager::sharedGameManager()->gameMode == 1) {
            CCString* flurryInfo = CCString::createWithFormat("Time reward: %d", reward);
            GameManager::sharedGameManager()->logEvent(flurryInfo->getCString());
        }
        else if (GameManager::sharedGameManager()->gameMode == 2) {
            CCString* flurryInfo = CCString::createWithFormat("Move reward: %d", reward);
            GameManager::sharedGameManager()->logEvent(flurryInfo->getCString());
        }
    }
    
    playRewardCoinsAnimation(reward);
    
    float delay = 0.0;
    if (reward != 0) {
        delay = reward * 0.2 + 1.0;
    }
    
    isScoreShared = false;
    

    
    this->scheduleOnce(schedule_selector(GameLayer::fadeInMenu), delay);
}

void GameLayer::fadeInMenu() {
    CCMenu* menu = (CCMenu*)this->getChildByTag(kGameOverMenuTag)->getChildByTag(kGameOverMenuTag);
    menu->setEnabled(true);
    CCFadeIn* fadein = CCFadeIn::create(0.5);
    menu->runAction(fadein);
    
    /*
    CCMenu* menu2 = (CCMenu*)this->getChildByTag(kGameOverMenuTag)->getChildByTag(kGameOverHintTag);
    menu2->setEnabled(true);
    CCFadeIn* fadein2 = CCFadeIn::create(0.5);
    menu2->runAction(fadein2);
*/
    
    if (GameManager::sharedGameManager()->getServerBoolean("noInterAfterGame") == false) {
        GameManager::sharedGameManager()->showInterstitial();
    }

    //this->setTouchEnabled(true);
}

void GameLayer::playCoinSound() {
    GameManager::sharedGameManager()->playSoundEffect("coin.mp3");
}


void GameLayer::playCoinsInSound() {
    GameManager::sharedGameManager()->playSoundEffect("coinsin.wav");
}


void GameLayer::removeCoin(CCNode* coin) {
    GameManager::sharedGameManager()->gameSetting.coins ++;
    GameManager::sharedGameManager()->saveGameSetting();
    updateCoins();
    coin->removeFromParent();
}

void GameLayer::removeCoin2(CCNode* coin) {
    GameManager::sharedGameManager()->gameSetting.coins += (coin->getTag());
    GameManager::sharedGameManager()->saveGameSetting();
    updateCoins();
    coin->removeFromParent();
}

void GameLayer::enableGameOverMenu() {
    CCMenu* menu = (CCMenu*)this->getChildByTag(kGameOverMenuTag)->getChildByTag(kGameOverMenuTag);
    if (menu) {
        menu->setEnabled(true);
    }
}

void GameLayer::popupAdventureGameOver() {
    
    int gap = GameManager::sharedGameManager()->getServerInterger("interBetweenStage2");
    
    // 2018: always show
    if (gap == 0) {
        gap = 4;
    }
    
    
    if (gap != 0) {
        if (stage % gap == 0) {
            GameManager::sharedGameManager()->showInterstitial();
        }
    }
    
    if (cheered == false) {
        GameManager::sharedGameManager()->playSoundEffect("clear.wav");
    }
    
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kPopupMenuTag);
    this->addChild(background);
    background->setOpacity(0);
    background->runAction(CCFadeTo::create(0.5, 160));
    
    CCSprite *background_img = CCSprite::create("paint_bg_extended.png");
    background_img->setAnchorPoint(ccp(0.5f, 0.5f));
    background_img->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.5));
    background_img->setScale(zoomRatio);
    
    if ((screenSize.height / screenSize.width) <= 1.5) { // ipad and iphone 3.5
        background_img->setScaleY(zoomRatio * 5.0);
    }
    else {
        // iphone x
        if (screenSize.height == 693.0) {
            background_img->setScaleY(zoomRatio * 6.0);

        }
        else {
            background_img->setScaleY(zoomRatio * 5.5);
        }
    }
    background->addChild(background_img);
    background_img->setOpacity(0);
    background_img->runAction(CCFadeTo::create(0.5, 220));
    
    
    // Stars
    int stars = 0;
    int moveUsed = currentStage.moves[2] - remainingMoves;
    if (currentStage.moves[2] == 0) moveUsed = -1;
    
    int timeUsed = currentStage.time[2] - remainingTime;
    if (currentStage.time[2] == 0) timeUsed = -1;
    
    if (cheered == true) {  // otherwise, 0 star
        if (moveUsed <= currentStage.moves[2] && timeUsed <= currentStage.time[2]){
            stars = 1;
        }
        if (moveUsed <= currentStage.moves[1] && timeUsed <= currentStage.time[1]) {
            stars = 2;
        }
        if (moveUsed <= currentStage.moves[0] && timeUsed <= currentStage.time[0]) {
            stars = 3;
        }
        
        // always star = 1 if used the add time or add moves item
        if (itemUsed == true) {
            stars = 1;
        }
        
        std::string key = "star_" + to_string(stage);
        int oldStars = CCUserDefault::sharedUserDefault()->getIntegerForKey(key.c_str());
        if (stars > oldStars) {
            CCUserDefault::sharedUserDefault()->setIntegerForKey(key.c_str(), stars);
            CCUserDefault::sharedUserDefault()->flush();
            
            sendAdventureScore();
        }
    }
    
    
    CCSprite* star1 = CCSprite::create(stars>=1?"star_gold.png":"star.png");
    CCSprite* star2 = CCSprite::create(stars>=2?"star_gold.png":"star.png");
    CCSprite* star3 = CCSprite::create(stars>=3?"star_gold.png":"star.png");
    
    star2->setScale(2.0);
    star1->setScale(1.5);
    star3->setScale(1.5);
    star1->setPosition(ccp(screenSize.width * 0.3, screenSize.height * 0.68));
    star2->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.72));
    star3->setPosition(ccp(screenSize.width * 0.7, screenSize.height * 0.68));
    
    background->addChild(star1);
    background->addChild(star3);
    background->addChild(star2);
    
    star1->setOpacity(0);
    star1->runAction(CCFadeIn::create(0.5));
    star2->setOpacity(0);
    star2->runAction(CCFadeIn::create(0.5));
    star3->setOpacity(0);
    star3->runAction(CCFadeIn::create(0.5));
    

    const char* text1 = GameManager::sharedGameManager()->getLocalizedString(stars==0?"tryAgain":"youWon");
    
    const char* text2 = NULL;
    switch (subGameMode) {
        case 0:
            if (stars == 0) {
                text2 = GameManager::sharedGameManager()->getLocalizedString("outOfMoves");
            }
            else if (itemUsed == true) {
                text2 = GameManager::sharedGameManager()->getLocalizedString("addMovesItemUsed");
            }
            else {
                text2 = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("usedMoves"), moveUsed)->getCString();
            }
            break;
        case 1:
            if (stars == 0) {
                text2 = GameManager::sharedGameManager()->getLocalizedString("outOfTime");
            }
            else if (itemUsed == true) {
                text2 = GameManager::sharedGameManager()->getLocalizedString("addTimeItemUsed");
            }
            else {
                text2 = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("usedSeconds"), timeUsed)->getCString();
            }
            break;
        default:
            text2 = "Default";
            break;
    }
    
    
    CCLabelBMFont* result = CCLabelBMFont::create(text1, GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont* detail = CCLabelBMFont::create(text2, GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    detail->setScale(0.8);
    
    result->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.55));
    detail->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.45));
    
    background->addChild(result);
    background->addChild(detail);
    
    result->setOpacity(0);
    result->runAction(CCFadeIn::create(0.5));
    detail->setOpacity(0);
    detail->runAction(CCFadeIn::create(0.5));

    

    
    CCMenuItemImage* play = CCMenuItemImage::create("next2.png", "next2.png", this, menu_selector(GameLayer::adventureGameOverItemTouched));
    CCMenuItemImage* retry = CCMenuItemImage::create("retry.png", "retry.png", this, menu_selector(GameLayer::adventureGameOverItemTouched));
    CCMenuItemImage* back = CCMenuItemImage::create("stage.png", "stage.png", this, menu_selector(GameLayer::adventureGameOverItemTouched));
    //CCMenuItemImage* gamecenter = CCMenuItemImage::create("ranking.png", "ranking.png", this, menu_selector(GameLayer::adventureGameOverItemTouched));
    
    
    back->setTag(0);
    retry->setTag(1);
    play->setTag(2);
    

    
    
    CCMenu* gameOverMenu = NULL;
    if ((stars == 0) || (stage >= GameManager::sharedGameManager()->getAdventureStageCount())){
        gameOverMenu = CCMenu::create(back, retry, NULL);
        
        if (stars == 0) {
            retry->setScale(1.5);
            CCScaleBy* scaleUp = CCScaleBy::create(0.5, 1.25);
            CCScaleBy* scaleDown = CCScaleBy::create(0.5, 0.8);
            CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleDown, scaleUp);
            CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);
            retry->runAction(repeatAction);
        }
    }
    else {
        gameOverMenu = CCMenu::create(back, play, retry, NULL);
        play->setScale(1.5);
        CCScaleBy* scaleUp = CCScaleBy::create(0.5, 1.25);
        CCScaleBy* scaleDown = CCScaleBy::create(0.5, 0.8);
        CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleDown, scaleUp);
        CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);
        play->runAction(repeatAction);
    }
    
    
    gameOverMenu->alignItemsHorizontallyWithPadding(screenSize.width * 0.1);
    gameOverMenu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.3));
    background->addChild(gameOverMenu);
    gameOverMenu->setTag(kGameOverMenuTag);
    gameOverMenu->setOpacity(0);
    gameOverMenu->runAction(CCFadeIn::create(0.5));

    //gameOverMenu->setEnabled(false);

    
    
    
}

void GameLayer::popupGameOver() {
    /* Use a new scene ?
    //GameOverScene* gameOverScene = GameOverScene::create();
    //CCDirector::sharedDirector()->replaceScene(CCTransitionSlideInR::create(0.5, gameOverScene));
    //return;
     */
    GameManager::sharedGameManager()->hideBanner();

    
    this->sendScore();
    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
    text_info->setString("");
    
    CCLayerColor* bg_mask = (CCLayerColor*)this->getChildByTag(kBGMaskTag);
    
    if (bg_mask) {
        CCArray* allBlocks = bg_mask->getChildren();
        for (int i=0; i<allBlocks->count(); i++) {
            GameBlock* block = dynamic_cast <GameBlock*> (allBlocks->objectAtIndex(i));
            if (block) {
                hearts(block);
            }
        }
    
        clearColumns();
        bg_mask->removeAllChildren();
        this->removeChild(bg_mask);
    }
    
    //float hsize = 0.3;

    GameManager::sharedGameManager()->playSoundEffect("clear.wav");

    // Background
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,0), screenSize.width, screenSize.height);
    background->setPosition(0.0, 0.0);
    background->setTag(kGameOverMenuTag);
    this->addChild(background, -5);

    // Foreground
    CCSprite* title = CCSprite::create(GameManager::sharedGameManager()->getLocalizedString("title.png"));
    title->setScale(0.7);
    title->setOpacity(0);
    title->setPosition(ccp(screenSize.width * 0.3, screenSize.height * 0.85));
    title->setTag(kGameOverTitleTag);
    background->addChild(title);
    
    
    CCLabelBMFont* text_mode = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    text_mode->setScale(0.6);
    text_mode->setPosition(ccp(screenSize.width * 0.3, screenSize.height * 0.73));
    switch (GameManager::sharedGameManager()->gameMode) {
        case 0: {
            CCString* targetInfoString = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("targetInfo2"), stage);
            text_mode->setString(targetInfoString->getCString());
            
            CCString* flurryInfo = CCString::createWithFormat("Mode: stage %d", stage);
            GameManager::sharedGameManager()->logEvent(flurryInfo->getCString());
            
            break;
        }
        case 1:
            text_mode->setString(GameManager::sharedGameManager()->getLocalizedString("timeInfo"));
            GameManager::sharedGameManager()->logEvent("Mode: time");

            break;
        case 2:

            text_mode->setString(GameManager::sharedGameManager()->getLocalizedString("moveInfo"));
            GameManager::sharedGameManager()->logEvent("Mode: move");

            break;
        case 3: {
            text_mode->setString(GameManager::sharedGameManager()->getLocalizedString("adventureInfo"));
            CCString* flurryInfo = CCString::createWithFormat("Mode: adventure %d", stage);
            GameManager::sharedGameManager()->logEvent(flurryInfo->getCString());
            break;
        }
            
        case 4:
            text_mode->setString(GameManager::sharedGameManager()->getLocalizedString("vsInfo"));
            GameManager::sharedGameManager()->logEvent("Mode: vs");
            
            break;
            
        default:
            text_mode->setString("");
            break;
    }
    text_mode->setOpacity(0);
    text_mode->setTag(kGameOverModeTag);
    background->addChild(text_mode);
    
#ifdef __Fruit__
#else
    CCSprite* sunburst = CCSprite::create("bg_sunburst.png");
    sunburst->setPosition(ccp (screenSize.width * 0.5, screenSize.height * 0.5));
    sunburst->setOpacity(192);
    sunburst->setScale(0.0);
    sunburst->setTag(kGameOverBackgroundTag);
    background->addChild(sunburst, -1);
#endif
  
    /*
    CCLabelBMFont* totalClear = CCLabelBMFont::create("Total Cleared: 35", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont* mostClear = CCLabelBMFont::create("Best Pop: 15", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    totalClear->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.5));
    mostClear->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.45));
    
    totalClear->setScale(0.35);
    mostClear->setScale(0.35);
    
    this->addChild(totalClear);
    this->addChild(mostClear);
    
    */

    
    
    
    
    
    
    
    CCMenuItemImage* back = CCMenuItemImage::create("exit.png", "exit.png", this, menu_selector(GameLayer::gameOverItemTouched));
    CCMenuItemImage* retry = CCMenuItemImage::create("retry.png", "retry.png", this, menu_selector(GameLayer::gameOverItemTouched));
    CCMenuItemImage* share = CCMenuItemImage::create("share.png", "share.png", this, menu_selector(GameLayer::gameOverItemTouched));
    CCMenuItemImage* gamecenter = CCMenuItemImage::create("ranking.png", "ranking.png", this, menu_selector(GameLayer::gameOverItemTouched));
    
    
    back->setTag(0);
    retry->setTag(1);
    share->setTag(2);
    gamecenter->setTag(3);
    
    // NO sharing for mac
#if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    CCMenu* gameOverMenu = CCMenu::create(back, retry, gamecenter,NULL);
    // No game center for Android
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CCMenu* gameOverMenu = CCMenu::create(back, retry, NULL);
#else
    CCMenu* gameOverMenu = CCMenu::create(back, retry, /*share,*/ gamecenter,NULL);
#endif
    
    gameOverMenu->alignItemsHorizontallyWithPadding(screenSize.width * 0.1);
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        gameOverMenu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.22));

    }
    else {
        gameOverMenu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.28));
    }
    background->addChild(gameOverMenu);
    gameOverMenu->setTag(kGameOverMenuTag);
    gameOverMenu->setEnabled(false);
    gameOverMenu->setOpacity(0);
    

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CCLabelBMFont* label = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
#else
    CCLabelBMFont* label = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("shareScore"), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
#endif 
    
    
    /* TODO: hack no sharing to social */
    
    /*
    CCMenuItemLabel* labelItem = CCMenuItemLabel::create(label, this, menu_selector(GameLayer::shareTouched));
    CCMenu* menu = CCMenu::create(labelItem, NULL);
    menu->alignItemsHorizontally();
    
    
    labelItem->setScale(0.6);
    //label->setTag(kGameOverHintTag);
    labelItem->runAction(CCRepeatForever::create(CCSequence::createWithTwoActions(CCScaleTo::create(0.5,0.7), CCScaleTo::create(0.5,0.6))));
    
    menu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.1));
    menu->setTag(kGameOverHintTag);
    menu->setEnabled(false);
    menu->setOpacity(0);

    background->addChild(menu);
     */
    
    
    playGameOverAnimation();
    /*
    CCLabelBMFont *text_score = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCString* score_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelScore"), totalScore);
    text_score->setString(score_str->getCString());
    text_score->setPosition(ccp(screenSize.width * 0.5, screenSize.height * hsize * 0.70));
    // text_score->setColor(ccc3( 23,124,254));
    background->addChild(text_score);
     */
}


void GameLayer::popupColorSelection( int column, int row) {
    setTouchEnabled(false);
    GameManager::sharedGameManager()->playSoundEffect("select.wav");

    changeColorX = column;
    changeColorY = row;
    
    CCArray* columnX = getCol(columns, column);
    GameBlock* blockC = getGameBlk(columnX, row);
    
    // Highlight the block, workaround for anchor point (0,0)
    CCScaleBy* scaleDown = CCScaleBy::create(0.5, 0.8);
    CCMoveBy* moveDown = CCMoveBy::create(0.5, ccp(blockSize * 0.1, blockSize * 0.1));
    CCSpawn* spawnDown = CCSpawn::createWithTwoActions(scaleDown, moveDown);
    CCScaleBy* scaleUp = CCScaleBy::create(0.5, 1.25);
    CCMoveBy* moveUp = CCMoveBy::create(0.5, ccp(-blockSize * 0.1, -blockSize * 0.1));
    CCSpawn* spawnUp = CCSpawn::createWithTwoActions(scaleUp, moveUp);
    CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(spawnDown, spawnUp);
    CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);
    blockC->runAction(repeatAction);
    
    float y = blockC->getPosition().y;
    y = (y + blockSize * 2.50)* zoomRatio + adY;
    
    float ybottom = (y - blockSize * zoomRatio);
    

    /*
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,220), screenSize.width, blockSize * 2.0 * zoomRatio);
    background->setPosition(0.0, y);
    background->setTag(kPopupMenuTag);
    this->addChild(background,15);
    
    CCLayerColor* background1 = CCLayerColor::create(ccc4(30,30,30,128), screenSize.width, screenSize.height);
    background1->setPosition(0.0, background->getPosition().y * (-1.0));
    background->addChild(background1, -5);
    
    CCSprite* arrow = CCSprite::create("arrow.png");
    arrow->setScale(blockSize / 128.0 * zoomRatio);
    
    float x = blockC->getPosition().x;
    x = (x + blockSize * 0.5) * zoomRatio + adX;
    arrow->setPosition(ccp(x, 0));
    background->addChild(arrow);
    */
    
    CCLayerColor* background = CCLayerColor::create(ccc4(0,0,0,160), screenSize.width, screenSize.height);
    background->ignoreAnchorPointForPosition(false);
    background->setAnchorPoint(ccp(0.5f, 0.5f));
    background->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    background->setTag(kPopupMenuTag);
    this->addChild(background, 15);
 
    CCSprite *background_img = CCSprite::create("paint_bg_extended.png");
    background_img->setAnchorPoint(ccp(0.5f, 0.5f));
    background_img->setPosition(ccp(screenSize.width * 0.5, y));
    background_img->setScale(zoomRatio);
    background->addChild(background_img);

    /*
    CCLayerColor* background_img = CCLayerColor::create(ccc4(  2, 96,204,256), screenSize.width, blockSize * 2.0 * zoomRatio);
    background_img->ignoreAnchorPointForPosition(false);
    background_img->setPosition(0.0, y);
    background_img->setAnchorPoint(ccp(0.0f, 0.0f));
    background_img->setPosition(ccp(0, y));
    background->addChild(background_img);
    */

    CCSprite* arrow = CCSprite::create("arrow.png");
    arrow->setScale(zoomRatio);
    
    float x = blockC->getPosition().x;
    x = (x + blockSize * 0.5) * zoomRatio + adX;
    
    
    arrow->setAnchorPoint(ccp(0.5f, 0.5f));
    arrow->setPosition(ccp(x, ybottom));
    background->addChild(arrow);

  
    CCMenuItemImage* red = CCMenuItemImage::create("red.png", "red.png", this, menu_selector(GameLayer::colorSelected));
    CCMenuItemImage* yellow = CCMenuItemImage::create("orange.png", "orange.png", this, menu_selector(GameLayer::colorSelected));
    CCMenuItemImage* green = CCMenuItemImage::create("green.png", "green.png", this, menu_selector(GameLayer::colorSelected));
    CCMenuItemImage* blue = CCMenuItemImage::create("blue.png", "blue.png", this, menu_selector(GameLayer::colorSelected));
    CCMenuItemImage* purple = CCMenuItemImage::create("purple.png", "purple.png", this, menu_selector(GameLayer::colorSelected));
    CCMenuItemImage* back = CCMenuItemImage::create("back.png", "back.png", this, menu_selector(GameLayer::colorSelected));
    
    
    red->setScale(zoomRatio);
    yellow->setScale(zoomRatio);
    green->setScale(zoomRatio);
    blue->setScale(zoomRatio);
    purple->setScale(zoomRatio);
    back->setScale(zoomRatio);
    
    
    red->setTag(0);
    yellow->setTag(3);
    green->setTag(2);
    blue->setTag(1);
    purple->setTag(4);
    back->setTag(-1);
    
    CCMenu* colorSelectionMenu = CCMenu::create(red,yellow,green,blue,purple,back,NULL);
    colorSelectionMenu->alignItemsHorizontallyWithPadding(screenSize.width * 0.05);
    colorSelectionMenu->setPosition(screenSize.width * 0.5, y);
    background->addChild(colorSelectionMenu);
}

void GameLayer::changeColor(int column, int row, int color) {
    
    stopHint();

    CCArray* columnX = getCol(columns, column);
    GameBlock* blockC = getGameBlk(columnX, row);
    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);

    if (blockC->getBlockColor() != color) {
        blockC->setBlockColor(color);
    
        GameManager::sharedGameManager()->gameSetting.coins -= 2;
        GameManager::sharedGameManager()->saveGameSetting();
        updateCoins();
        
        text_info->setString(GameManager::sharedGameManager()->getLocalizedString("paintUsed"));
        
        GameManager::sharedGameManager()->logEvent("Item: paint");


    }
    else {
        if (GameManager::sharedGameManager()->gameMode == 3) {
            text_info->setString(defaultInfoText.c_str());
        }
        else {
            text_info->setString("");
        }
    }

    this->scheduleOnce(schedule_selector(GameLayer::autoHint), kHintTime);

}

void GameLayer::bombBlocks(int column, int row) {
    // bomb 3x3
    
    stopHint();

    this->setTouchEnabled(false);
    isPoping = true;
    
    if (GameManager::sharedGameManager()->gameMode == 2) {
        remainingMoves++;
    }

    CCArray* columnX;
    GameBlock* blockC;
    
    for (int i = column-1; i <= column+1; i++) {
        if (i >= columns->count()) continue;
        columnX = getCol(columns, i);
        for (int j= row +1; j>= row-1; j--) {
            if (j >= columnX->count()) continue;
            blockC = getGameBlk(columnX, j);
            hearts(blockC);
            this->getChildByTag(kBGMaskTag)->removeChild(blockC);
            columnX->removeObjectAtIndex(j);
        }
    }
    GameManager::sharedGameManager()->playSoundEffect("clear.wav");

    GameManager::sharedGameManager()->gameSetting.coins -= 1;
    GameManager::sharedGameManager()->saveGameSetting();
    updateCoins();
    
    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
    text_info->setString(GameManager::sharedGameManager()->getLocalizedString("bombUsed"));
    
    GameManager::sharedGameManager()->logEvent("Item: bomb");
    
    CCCallFunc* updateRemainingBlocks = CCCallFunc::create(this, callfunc_selector(GameLayer::updateRemainingBlocks));
    CCDelayTime* delay2 = CCDelayTime::create(duration);
    CCCallFunc* refillBlocks = CCCallFunc::create(this, callfunc_selector(GameLayer::refillBlocks));
    
    CCCallFunc* finishPoping = CCCallFunc::create(this, callfunc_selector(GameLayer::finishPoping));
    CCSequence* fullAction = CCSequence::create(updateRemainingBlocks, delay2, refillBlocks, finishPoping,NULL);
    this->runAction(fullAction);

}
void GameLayer::popBlocks() {
    
    stopHint();


    this->setTouchEnabled(false);
    isPoping = true;
    int n= this->getConnectedCount();
    popOrder = 1;
    
    
    if (GameManager::sharedGameManager()->gameMode == 4) {
        networkScore += (n*n*5);
        GameManager::sharedGameManager()->sendScoreRT(networkScore);
    }
    
    CCCallFunc* popOne = CCCallFunc::create(this, callfunc_selector(GameLayer::popOneBlock));
    CCDelayTime* delay = CCDelayTime::create(0.08);
    CCSequence* actionOnce = CCSequence::createWithTwoActions(popOne, delay);
    CCRepeat* repeatAction = CCRepeat::create(actionOnce, n);
    
    
    CCCallFunc* updateRemainingBlocks = CCCallFunc::create(this, callfunc_selector(GameLayer::updateRemainingBlocks));
    CCDelayTime* delay2 = CCDelayTime::create(duration);
    CCCallFunc* refillBlocks = CCCallFunc::create(this, callfunc_selector(GameLayer::refillBlocks));
    
    
    CCCallFunc* finishPoping = CCCallFunc::create(this, callfunc_selector(GameLayer::finishPoping));
    CCSequence* fullAction = CCSequence::create(repeatAction, updateRemainingBlocks, delay2, refillBlocks, finishPoping,NULL);
    
    this->runAction(fullAction);
}

void GameLayer::popRemainingBlocks() {
    
    this->setTouchEnabled(false);
    isPoping = true;

    int n = this->getRemainingCount();
    
    CCCallFunc* popOne = CCCallFunc::create(this, callfunc_selector(GameLayer::popOneRemainingBlock));
    CCDelayTime* delay = CCDelayTime::create(0.08);
    CCSequence* actionOnce = CCSequence::createWithTwoActions(popOne, delay);
    CCRepeat* repeatAction = CCRepeat::create(actionOnce, n);
    
    CCCallFunc* updateRemainingBlocks = CCCallFunc::create(this, callfunc_selector(GameLayer::updateRemainingBlocks));

    CCSequence* fullAction = CCSequence::create(repeatAction, updateRemainingBlocks,NULL);
    this->runAction(fullAction);
}


void GameLayer::finishPoping() {
    selected = false;
    isPoping = false;
    
    switch (GameManager::sharedGameManager()->gameMode) {
        case 0:
            if(testStageEnd() == false) {
                this->scheduleOnce(schedule_selector(GameLayer::autoHint), kHintTime );
            }

            break;
        case 1:
        case 4:
            if (remainingTime == 0) {
                isGameOver = true;
                setTouchEnabled(false);
                // Delay in case the play score animation is still playing
                CCDelayTime* delay = CCDelayTime::create(0.81);
                CCCallFunc* popupGameOver = CCCallFunc::create(this, callfunc_selector(GameLayer::popupGameOver));
                CCSequence* fullAction = CCSequence::create(delay, popupGameOver, NULL);
                this->runAction(fullAction);
            }
            else {
                this->setTouchEnabled(true);
                this->scheduleOnce(schedule_selector(GameLayer::autoHint), kHintTime);
            }
            break;
        case 2:
        {
            remainingMoves --;
            CCString* stage_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelMove"), remainingMoves);
            
            if (remainingMoves < 5) {
                GameManager::sharedGameManager()->playSoundEffect("beep.wav");
                CCSprite* countdownSprite = CCSprite::create("countdown.png");
                countdownSprite->setScaleY(screenSize.height / 568.0);
                countdownSprite->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.5));
                this->addChild(countdownSprite);
                countdownSprite->scheduleOnce(schedule_selector(CCSprite::removeFromParent), 0.5);
            }
            
            CCLabelBMFont* text_stage = (CCLabelBMFont*)this->getChildByTag(kStageTag);
            text_stage->setString(stage_str->getCString());
            
            if (remainingMoves == 0) {
                isGameOver = true;
                setTouchEnabled(false);
                
                CCDelayTime* delay = CCDelayTime::create(0.81);
                CCCallFunc* popupGameOver = CCCallFunc::create(this, callfunc_selector(GameLayer::popupGameOver));
                CCSequence* fullAction = CCSequence::create(delay, popupGameOver, NULL);
                this->runAction(fullAction);
            }
            else {
                this->setTouchEnabled(true);
                this->scheduleOnce(schedule_selector(GameLayer::autoHint), kHintTime);
            }
            break;
        }
        case 3:
            if (subGameMode == 0) {
                remainingMoves --;
                
                CCString* move_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelMove"), remainingMoves);
                
                if ((remainingMoves < 5) && (cheered == false)) {
                    
                    GameManager::sharedGameManager()->playSoundEffect("beep.wav");
                    CCSprite* countdownSprite = CCSprite::create("countdown.png");
                    countdownSprite->setScaleY(screenSize.height / 568.0);
                    countdownSprite->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.5));
                    this->addChild(countdownSprite);
                    countdownSprite->scheduleOnce(schedule_selector(CCSprite::removeFromParent), 0.5);
                }
                
                CCLabelBMFont* text_move = (CCLabelBMFont*)this->getChildByTag(kTargetTag);
                text_move->setString(move_str->getCString());
            }
            
            
            
            
            
            if (cheered == true) {
                isGameOver = true;
                setTouchEnabled(false);

                
                CCDelayTime* delay = CCDelayTime::create(0.81);
                CCCallFunc* popupGameOver = CCCallFunc::create(this, callfunc_selector(GameLayer::popupAdventureGameOver));
                CCSequence* fullAction = CCSequence::create(delay, popupGameOver, NULL);
                this->runAction(fullAction);
                
                return;
            }
            
            
            if ((subGameMode == 0 && remainingMoves == 0) ||
                (subGameMode == 1 && remainingTime == 0)) {
                isGameOver = true;
                setTouchEnabled(false);
                
                //this->removeChildByTag(kStageTargetTag);

                CCDelayTime* delay = CCDelayTime::create(0.81);
                CCCallFunc* popupGameOver = CCCallFunc::create(this, callfunc_selector(GameLayer::popupAdventureGameOver));
                CCSequence* fullAction = CCSequence::create(delay, popupGameOver, NULL);
                this->runAction(fullAction);
            }
            else {
                this->setTouchEnabled(true);
                this->scheduleOnce(schedule_selector(GameLayer::autoHint), kHintTime);
            }
            break;
            
            

            
    }
    
}

void GameLayer::refillBlocks() {
    
    // Refill
    if (GameManager::sharedGameManager()->gameMode != 0) {
        CCArray* columnX;

        for (int x = 0; x < sizeX; x++) {
            
            if (x < columns->count()) {
                columnX = getCol(columns, x);
                for (int y = 0; y < sizeY; y++) {
                    if (y < columnX->count()) {
                        // go to next
                    }
                    else {
                        int initColor = rand() % kColorCount;
                        GameBlock* block = GameBlock::create(initColor);
                        block->setScale(0);
                        block->setXY(x,y);
                        block->setAnchorPoint(ccp(0,0));
                        block->setPosition( genPosition( x, y ));
                        this->getChildByTag(kBGMaskTag)->addChild(block);
                        columnX->addObject(block);
                        
                        
                        block->setPosition( ccpAdd(block->getPosition(), ccp(blockSize * 0.5, blockSize * 0.5)) ); 
                        CCScaleTo* scaleUp = CCScaleTo::create(duration, 1);
                        CCMoveBy* moveToCorner = CCMoveBy::create(duration, ccp(-blockSize * 0.5, -blockSize * 0.5));
                        CCSpawn* spawn = CCSpawn::createWithTwoActions(scaleUp, moveToCorner);
                        
                        
                        block->runAction(spawn);
                    }
                }
            }
            else { // empty column
                columns->addObject(CCArray::createWithCapacity(sizeY));
                for (int y = 0; y < sizeY; y++) {
                    int initColor = rand() % kColorCount;
                    GameBlock* block = GameBlock::create(initColor);
                    block->setScale(0);
                    block->setXY(x,y);
                    block->setAnchorPoint(ccp(0,0));
                    block->setPosition( genPosition( x, y ));
                    this->getChildByTag(kBGMaskTag)->addChild(block);
                    columnX = getCol(columns, x);
                    columnX->addObject(block);
                    
                    block->setPosition( ccpAdd(block->getPosition(), ccp(blockSize * 0.5, blockSize * 0.5)) );
                    CCScaleTo* scaleUp = CCScaleTo::create(duration, 1);
                    CCMoveBy* moveToCorner = CCMoveBy::create(duration, ccp(-blockSize * 0.5, -blockSize * 0.5));
                    CCSpawn* spawn = CCSpawn::createWithTwoActions(scaleUp, moveToCorner);
                    
                    
                    block->runAction(spawn);
                }
            }
        }
    }
}
void GameLayer::updateRemainingBlocks() {

    
    bool needPlayLandingSound = false;
    
    CCArray* columnX;
    GameBlock* blockM;
    
    // clear the empty columns
    for(int x = (columns->count() - 1); x >= 0; x--) {
        columnX = getCol(columns, x);
        if(columnX->count() == 0) {
            columns->removeObjectAtIndex(x);
        }
    }
    
    // move blocks to correct positons
    // merge move down & left
    if(columns->count() == 0) {
        return;
    }
    
    // move _|
    for(int x = (columns->count() - 1); x >= 0; x--) {
        columnX = getCol(columns, x);
        // j: index for the moveTo position of the upper blocks
        for (int y = 0; y < columnX->count(); y++) {
            blockM = getGameBlk(columnX, y);
            // if the current position match its index
            if((blockM->getX() == x) && (blockM->getY() == y)) {
            }
            else {
                if (blockM->getY() != y) {
                    // there is drop
                    needPlayLandingSound = true;
                }
                CCFiniteTimeAction* actionMoveDown = CCMoveTo::create(duration,
                                                                      genPosition( blockM->getX(), y ) );
                
                CCFiniteTimeAction* actionMoveLeft = CCMoveTo::create(duration,
                                                                      genPosition( x, y ) );
                blockM->runAction( CCSequence::create(actionMoveDown, actionMoveLeft, NULL) );
                blockM->setXY(x, y);
            }
        }
    }
    
    if (needPlayLandingSound) {
        this->scheduleOnce(schedule_selector(GameLayer::playLandingSound), duration);
    }
}


void GameLayer::playBonusScoreAnimation() {
    int score = genBonusScore(columns);
    
    if (score == 0) return;
    CCLabelBMFont* scoreLabel = CCLabelBMFont::create(to_string(score).c_str(), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    scoreLabel->setScale(1.6);
    scoreLabel->setPosition(screenSize.width * 0.5, screenSize.height * 0.5);
    scoreLabel->setTag(kScoreLabelTagBegin + score);
    this->addChild(scoreLabel);
    
    // CCMoveTo* moveToAction = CCMoveTo::create(0.8, text_score->getPosition());
    CCMoveTo* moveToAction = CCMoveTo::create(0.8, ccp(screenSize.width * 0.45, screenSize.height * kScoreY));
    CCScaleTo* scaleToAction = CCScaleTo::create(0.8, 0.3);
    CCSpawn* spawnAction = CCSpawn::createWithTwoActions(moveToAction, scaleToAction);
    CCCallFuncN * removeAction = CCCallFuncN::create(this, callfuncN_selector(GameLayer::removeScoreLabel));
    CCSequence* fullAction = CCSequence::createWithTwoActions(spawnAction, removeAction);
    scoreLabel->runAction(fullAction);
}

void GameLayer::updateTargetAnimation(GameBlock* block, int score) {
    int color = block->getBlockColor();
    if (currentStage.target[color] > 0) {
        currentStage.target[color]--;
        
        CCMenuItemImage* image = (CCMenuItemImage*)this->getChildByTag(kStageTargetTag)->getChildByTag(kTargetMenuTag)->getChildByTag(color);
        
        if (image) {
            CCLabelBMFont* countLabel = (CCLabelBMFont*)image->getChildByTag(kBlockCountTag);
            if (countLabel) {
                countLabel->setString(to_string(currentStage.target[color]).c_str());
                
                if (currentStage.target[color] == 0) {
                    countLabel->setColor(ccc3(128, 255, 128));
                }
            }
        }
    }
    if (currentStage.target[5] > 0) {
        totalScore += score * 10 - 5;
        
        CCMenuItemImage* image = (CCMenuItemImage*)this->getChildByTag(kStageTargetTag)->getChildByTag(kTargetMenuTag)->getChildByTag(5);
        
        if (image) {
            CCLabelBMFont* countLabel = (CCLabelBMFont*)image->getChildByTag(kBlockCountTag);
            if (countLabel) {
                countLabel->setString((to_string(totalScore) + "/" + to_string(currentStage.target[5])).c_str());
                
                if (totalScore >= currentStage.target[5]) {
                    countLabel->setColor(ccc3(128, 255, 128));
                }
            }
        }
    }
    
    if (cheered == false) {
        
        for (int i=0; i<5; i++) {
            if (currentStage.target[i] > 0) return;
        }
        
        if ((currentStage.target[5] > 0) && (totalScore < currentStage.target[5])) {
            return;
        }
        
        CCLabelBMFont* info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
        info->setString(GameManager::sharedGameManager()->getLocalizedString("stageClear"));
        
        cheered = true;
        GameManager::sharedGameManager()->playSoundEffect("cheer.wav");
        
        this->unschedule(schedule_selector(GameLayer::countDown));
        stopHint();
    }

}


void GameLayer::playScoreAnimation(GameBlock* block, int score) {
    // CCLabelBMFont* scoreLabel = CCLabelBMFont::create(to_string(score).c_str(), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"), 30);
    CCLabelBMFont* scoreLabel = CCLabelBMFont::create(to_string(score).c_str(), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    scoreLabel->setScale(1.2);
    
    
    CCLayerColor* mask = (CCLayerColor*)this->getChildByTag(kBGMaskTag);
    CCPoint maskPosition = mask->getPosition();
    CCPoint blockPosition = block->getPosition();
    
    scoreLabel->setPosition(ccpAdd(maskPosition, blockPosition));
    scoreLabel->setTag(kScoreLabelTagBegin + score);
    this->addChild(scoreLabel);
    
    // CCMoveTo* moveToAction = CCMoveTo::create(0.8, text_score->getPosition());
    CCMoveTo* moveToAction = CCMoveTo::create(0.8, ccp(screenSize.width * 0.45, screenSize.height * kScoreY));
    CCScaleTo* scaleToAction = CCScaleTo::create(0.8, 0.6);
    CCSpawn* spawnAction = CCSpawn::createWithTwoActions(moveToAction, scaleToAction);
    CCCallFuncN * removeAction = CCCallFuncN::create(this, callfuncN_selector(GameLayer::removeScoreLabel));
    CCSequence* fullAction = CCSequence::createWithTwoActions(spawnAction, removeAction);
    scoreLabel->runAction(fullAction);
}

void GameLayer::removeScoreLabel(CCNode *pSender) {
    totalScore += (pSender->getTag() - kScoreLabelTagBegin);
    updateScore();
    pSender->removeFromParent();
}

void GameLayer::updateCoins() {
    int coinsCount = GameManager::sharedGameManager()->gameSetting.coins;
    CCLabelBMFont* coinsLabel = (CCLabelBMFont*)this->getChildByTag(kCoinsTag);
    if (coinsLabel) {
        coinsLabel->setString(to_string(coinsCount).c_str());
    }
}






// Controls layout
void GameLayer::addCoinButtonTouched() {

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    return;
#endif
    if ((GameManager::sharedGameManager()->gameMode == 4) && (isGameOver == false)){
        return;
    }
    else {
        GameManager::sharedGameManager()->playSoundEffect("select.wav");
        CCDirector::sharedDirector()->pushScene(CCTransitionSlideInT::create(0.5, StoreScene::create()));
        return;
    }
}

void GameLayer::colorSelected(CCNode *pSender) {
    CCMenuItemImage* item = (CCMenuItemImage*)pSender;
    if (item) {
        CCLOG("item selected: %d", item->getTag());
        GameManager::sharedGameManager()->playSoundEffect("select.wav");

        CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);

        int color = item->getTag();
        if (color != -1) {
            changeColor(changeColorX, changeColorY, color);

            // TODO: use coin
        }
        else {
            if (GameManager::sharedGameManager()->gameMode == 3) {
                text_info->setString(defaultInfoText.c_str());
            }
            else {
                text_info->setString("");
            }
        }
        this->removeChildByTag(kPopupMenuTag);
        
        // Unselect the paint tool
        setItemSeleted(3, false);
        selectedItem = 0;
        

    }
    
    CCArray* columnX = getCol(columns, changeColorX);
    GameBlock* blockC = getGameBlk(columnX, changeColorY);
    
    blockC->stopAllActions();
    blockC->setPosition(genPosition(changeColorX, changeColorY));
    blockC->setScale(1.0);
    setTouchEnabled(true);
    
    if (GameManager::sharedGameManager()->gameMode == 0) {
        testStageEnd();
    }}

void GameLayer::setItemSeleted(int itemTag, bool selected) {
    
    CCMenuItemImage* item = (CCMenuItemImage*)this->getChildByTag(kItemMenuTag)->getChildByTag(itemTag);
    if (item) {
        item->stopAllActions();
        item->setScale(1.0);
        
        if (selected) {
            CCScaleBy* scaleUp = CCScaleBy::create(0.5, 1.25);
            CCScaleBy* scaleDown = CCScaleBy::create(0.5, 0.8);
            CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleUp, scaleDown);
            CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);
            
            item->runAction(repeatAction);
        }
    }
}

void GameLayer::cancelItemUse() {
    // For timed mode, at the end
    setItemSeleted(1, false);
    setItemSeleted(2, false);
    setItemSeleted(3, false);
    selectedItem = 0;
    
    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
    text_info->setString("");
    
    this->removeChildByTag(kPopupMenuTag);
    this->removeChildByTag(kExitBattleMenuTag);
}

void GameLayer::itemSelected(CCNode *pSender) {
    
    if (this->isTouchEnabled() == false) return;
    
    //stopHint();

    CCMenuItemImage* item = (CCMenuItemImage*)pSender;
    if (item) {
        //CCLOG("item selected: %d", item->getTag());
        
        //setItemSeleted(item, true);
        int currentSelectedItem = item->getTag();
        
        
        switch (currentSelectedItem) {
            case 0:
                // shouldn't happen
                CCLOG("Something wrong");
                break;
                
            case 1:
                // Use bomb
                if (GameManager::sharedGameManager()->gameSetting.coins < 1) {
                    this->addCoinButtonTouched();
                    return;
                }
                
                if ( selected ) {
                    recoverSelection();
                }
                if (selectedItem == currentSelectedItem) {
                    setItemSeleted(1, false);
                    selectedItem = 0;
                    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                    
                    if (GameManager::sharedGameManager()->gameMode == 3) {
                        text_info->setString(defaultInfoText.c_str());
                    }
                    else {
                        text_info->setString("");
                    }
                    
                }
                else {
                    // make sure other tool is unseleted
                    setItemSeleted(3, false);
                    setItemSeleted(1, true);
                    selectedItem = 1;
                    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                    text_info->setString(GameManager::sharedGameManager()->getLocalizedString("bombInfo"));
                }
                GameManager::sharedGameManager()->playSoundEffect("select.wav");
                

                break;
                
            case 3:
                // Use paint
                
                if (GameManager::sharedGameManager()->gameSetting.coins < 2) {
                    this->addCoinButtonTouched();
                    return;
                }
                
                if ( selected ) {
                    recoverSelection();
                }
                if (selectedItem == currentSelectedItem) {
                    setItemSeleted(3, false);
                    selectedItem = 0;
                    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                    if (GameManager::sharedGameManager()->gameMode == 3) {
                        text_info->setString(defaultInfoText.c_str());
                    }
                    else {
                        text_info->setString("");
                    }
                    
                }
                else {
                    // make sure other tool is unseleted
                    setItemSeleted(1, false);
                    setItemSeleted(3, true);
                    selectedItem = 3;
                    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                    text_info->setString(GameManager::sharedGameManager()->getLocalizedString("paintInfo"));
                }
                GameManager::sharedGameManager()->playSoundEffect("select.wav");
                
                
                break;
                
            case 2:
            {
        
                if (GameManager::sharedGameManager()->gameSetting.coins < 3) {
                    this->addCoinButtonTouched();
                    return;
                }
        

                setItemSeleted(3, false);
                setItemSeleted(1, false);
                selectedItem = 0;

                // Use hint, one time
                if ( selected ) {
                    recoverSelection();
                }
                
                stopHint();
                recolor();
                
                this->scheduleOnce(schedule_selector(GameLayer::autoHint), kHintTime);
                 
                GameManager::sharedGameManager()->playSoundEffect("select.wav");

                CCScaleTo* scaleUp = CCScaleTo::create(0.15, 1.2);
                CCScaleTo* scaleDown = CCScaleTo::create(0.15, 1.0);
                CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleUp, scaleDown);
                item->runAction(scaleUpAndDown);
                
                GameManager::sharedGameManager()->gameSetting.coins -= 3;
                GameManager::sharedGameManager()->saveGameSetting();
                updateCoins();
                
            
                CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                text_info->setString(GameManager::sharedGameManager()->getLocalizedString("hintUsed"));
            
                GameManager::sharedGameManager()->logEvent("Item: rainbow");

                break;
        
            }

            case 4: {
                
                if (GameManager::sharedGameManager()->gameSetting.coins < 1) {
                    this->addCoinButtonTouched();
                    return;
                }
                
                setItemSeleted(3, false);
                selectedItem = 0;
                
                GameManager::sharedGameManager()->playSoundEffect("select.wav");
                
                GameManager::sharedGameManager()->gameSetting.coins -= 1;
                GameManager::sharedGameManager()->saveGameSetting();
                updateCoins();
                
                if (subGameMode == 0) {
                    remainingMoves += 3;
                    itemUsed = true;
                
                    CCString* move_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelMove"), remainingMoves);
                
                    CCLabelBMFont* text_move = (CCLabelBMFont*)this->getChildByTag(kTargetTag);
                    text_move->setString(move_str->getCString());
                    
                    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                    text_info->setString(GameManager::sharedGameManager()->getLocalizedString("addMovesUsed"));
                    
                    GameManager::sharedGameManager()->logEvent("Item: addMoves");
                }
                else {
                    remainingTime += 5;
                    itemUsed = true;
                    
                    CCString* stage_str = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("labelTime"), remainingTime);
                    CCLabelBMFont* text_stage = (CCLabelBMFont*)this->getChildByTag(kTargetTag);
                    text_stage->setString(stage_str->getCString());
                    
                    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
                    text_info->setString(GameManager::sharedGameManager()->getLocalizedString("addTimeUsed"));
                    
                    GameManager::sharedGameManager()->logEvent("Item: addTime");
                }
                
                break;
            }
        }
    }
 
}

void GameLayer::fadeOutControls() {
    cancelItemUse();
    
    int tags[5] = {kStageTag, kTargetTag, kItemMenuTag, kPauseMenuTag, kStageClearTag};
    for (int i=0; i<5; i++) {
        CCNode* obj = this->getChildByTag(tags[i]);
        
        if (obj == NULL) continue;
        
        CCFadeOut* fadeout = CCFadeOut::create(0.5);
        obj->runAction(fadeout);
    }
    
    int tags2[2] = {kCoinsMenuTag, kCoinsTag};
    for (int i=0; i<2; i++) {
        CCNode* obj = this->getChildByTag(tags2[i]);
        CCFadeTo* fadein = CCFadeTo::create(0.5, 255);
        obj->runAction(fadein);
    }
}

void GameLayer::fadeInControls() {
    int tags[7] = {kStageTag, kTargetTag, kCoinsTag, kCoinsMenuTag, kItemMenuTag, kPauseMenuTag, kScoreTag};
    for (int i=0; i<7; i++) {
        
        
        if (GameManager::sharedGameManager()->gameMode == 4 && (tags[i] == kCoinsMenuTag || tags[i] == kCoinsTag)) {
            CCNode* obj = this->getChildByTag(tags[i]);
            CCFadeTo* fadein = CCFadeTo::create(0.5, 128);
            obj->runAction(fadein);
        }
        else {
         
            CCNode* obj = this->getChildByTag(tags[i]);
            CCFadeIn* fadein = CCFadeIn::create(0.5);
            obj->runAction(fadein);
        }
        
    }
}


void GameLayer::layoutControls() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CCMenuItemImage* addCoinButton = CCMenuItemImage::create("coins.png", "coins.png", this, menu_selector(GameLayer::addCoinButtonTouched));
#else

    CCMenuItemImage* addCoinButton = CCMenuItemImage::create("money_plus.png", "money_plus.png", this, menu_selector(GameLayer::addCoinButtonTouched));
#endif
    // addCoinButton->setScale(0.4);
    CCMenu* menu = CCMenu::create(addCoinButton, NULL);
    menu->setPosition(ccp(screenSize.width * kCoinsX, screenSize.height * kCoinsY));
    menu->setTag(kCoinsMenuTag);
    menu->setOpacity(0);
    this->addChild(menu);
    
    int coinsCount = GameManager::sharedGameManager()->gameSetting.coins;
    // CCLabelBMFont* coinsLabel = CCLabelBMFont::create(to_string(coinsCount).c_str(), kGameFont, 15);
    CCLabelBMFont* coinsLabel = CCLabelBMFont::create(to_string(coinsCount).c_str(), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    coinsLabel->setScale(0.7);
    coinsLabel->setAnchorPoint(ccp(1,0.5f));
    coinsLabel->setPosition(ccp(screenSize.width * kCCountX, screenSize.height * kCCountY));
    coinsLabel->setTag(kCoinsTag);
    coinsLabel->setOpacity(0);
    this->addChild(coinsLabel);
    
    
    // Items
    CCMenuItemImage* bomb = CCMenuItemImage::create("bomb.png", "bomb.png", this, menu_selector(GameLayer::itemSelected));
    CCMenuItemImage* hint = CCMenuItemImage::create("rainbow.png", "rainbow.png", this, menu_selector(GameLayer::itemSelected));
    CCMenuItemImage* paint = CCMenuItemImage::create("paint.png", "paint.png", this, menu_selector(GameLayer::itemSelected));
    
    CCMenuItemImage* addMove = CCMenuItemImage::create("addmoves.png", "addmoves.png", this, menu_selector(GameLayer::itemSelected));
    
    /*
    paint->setScale(0.4);
    bomb->setScale(0.4);
    hint->setScale(0.4);
    */
    
    bomb->setTag(1);
    hint->setTag(2);
    paint->setTag(3);
    addMove->setTag(4);

    
    // CCMenu* itemMenu = CCMenu::create(red, yellow, green, blue, purple, paint, bomb, hint, NULL);
    
    CCMenu* itemMenu = NULL;
    if (GameManager::sharedGameManager()->gameMode == 3) {
        itemMenu = CCMenu::create(addMove, paint, hint, NULL);
        lastSubGameMode = 0;
    }
    else {
        itemMenu = CCMenu::create(bomb, paint, hint, NULL);
    }
    
    itemMenu->setTag(kItemMenuTag);
    itemMenu->alignItemsHorizontallyWithPadding(0.0);
    itemMenu->setPosition(ccp(screenSize.width * kCheatX, screenSize.height * kCheatY));
    this->addChild(itemMenu);
    itemMenu->setOpacity(0);
    
    
    
    const char* pauseFileName = "pause.png";
    if (GameManager::sharedGameManager()->gameMode == 4) {
        pauseFileName = "exit.png";
    }
    CCMenuItemImage* pauseButton = CCMenuItemImage::create(pauseFileName, pauseFileName, this, menu_selector(GameLayer::pauseButtonTouched));
    pauseButton->setScale(0.8);
    pauseButton->setAnchorPoint(ccp(0.0, 0.5));
    CCMenu* pauseMenu = CCMenu::create(pauseButton, NULL);
    pauseMenu->setPosition(ccp(screenSize.width * kPauseX, screenSize.height * kPauseY));
    pauseMenu->setTag(kPauseMenuTag);
    this->addChild(pauseMenu);
    pauseMenu->setOpacity(0);
    
    
    CCLabelBMFont *text_stage = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *text_target = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *text_score = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *text_info = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    //CCLabelBMFont *text_best = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    
    text_stage->setScale(0.7);
    text_target->setScale(0.7);
    text_score->setScale(1.0);
    text_info->setScale(0.6);
    //text_best->setScale(0.3);
    
    text_stage->setAnchorPoint(ccp(0,0.5f));
    text_target->setAnchorPoint(ccp(0.5,0.5f));
    text_score->setAnchorPoint(ccp(0.5,0.5f));
    //text_best->setAnchorPoint(ccp(0,0.5f));
    
    text_stage->setPosition(ccp(screenSize.width * kStageX, screenSize.height * kStageY));
    text_target->setPosition(ccp(screenSize.width * kTargetX, screenSize.height * kTargetY));
    text_score->setPosition(ccp(screenSize.width * kScoreX, screenSize.height * kScoreY));
    text_info->setPosition(ccp(screenSize.width * kInfoX, screenSize.height * kInfoY));
    

    //text_best->setPosition(ccp(screenSize.width * kBestX, screenSize.height * kBestY));
    
    text_stage->setTag(kStageTag);
    text_target->setTag(kTargetTag);
    text_score->setTag(kScoreTag);
    text_info->setTag(kInfoTag);
    defaultInfoText = "";
    //text_best->setTag(kBestTag);
    
    this->addChild(text_stage);
    this->addChild(text_target);
    this->addChild(text_score, 5);
    this->addChild(text_info);
    
    text_stage->setOpacity(0);
    text_target->setOpacity(0);
    text_score->setOpacity(0);

    
    // TODO: hack for iphone X, move everything down 10
    if (screenSize.height == 693.0) {
        text_stage->setPositionY(text_stage->getPositionY() - 10.0);
        text_target->setPositionY(text_target->getPositionY() - 10.0);
        text_score->setPositionY(text_score->getPositionY() - 10.0);
        text_info->setPositionY(text_info->getPositionY() - 10.0);
        itemMenu->setPositionY(itemMenu->getPositionY() - 10.0);
        pauseMenu->setPositionY(pauseMenu->getPositionY() - 10.0);
        menu->setPositionY(menu->getPositionY() - 10.0);
        coinsLabel->setPositionY(coinsLabel->getPositionY() - 10.0);
    }
}

void GameLayer::pauseButtonTouched() {    
    if (this->isTouchEnabled() == false) return;
    // stopHint();
    if (GameManager::sharedGameManager()->gameMode == 4) {
        
        GameManager::sharedGameManager()->playSoundEffect("select.wav");
        popupExitBattleQuery();

    }
    else {
        GameManager::sharedGameManager()->playSoundEffect("select.wav");
        CCDirector::sharedDirector()->pushScene(CCTransitionSlideInT::create(0.5, PauseScene::create()));
    }
}





// Save and load

void GameLayer::saveStateAndScore() {
    saveState();
    saveScore();
}

void GameLayer::saveStateThenScore() {
    saveState();
    this->scheduleOnce(schedule_selector(GameLayer::saveScore), 0.81);
}

void GameLayer::saveScore() {
    GameManager* gm = GameManager::sharedGameManager();
    if (gm->gameMode != 0) {
        return;
    }
    gm->gameState.score = totalScore;
}

void GameLayer::saveState() {
    
    GameManager* gm = GameManager::sharedGameManager();
    if (gm->gameMode != 0) {
        return;
    }
    
    // Classic mode
    gm->gameState.stage = stage;
    gm->gameState.beginScore = stageBeginScore;
    
    for (int i=0; i<10; i++) {
        for (int j=0; j<10; j++) {
            if (i >= columns->count()) {
                gm->gameState.matrix[i][j] = 0;
            }
            else {
                CCArray* columnX = getCol(columns, i);
                if (j >= columnX->count()) {
                    gm->gameState.matrix[i][j] = 0;
                }
                else {
                    GameBlock* blockC = getGameBlk(columnX, j);
                    int color = blockC->getBlockColor() + 1;
                    gm->gameState.matrix[i][j] = color;
                }
            }
        }
    }
}


void GameLayer::sendScore() {
    int gameMode = GameManager::sharedGameManager()->gameMode;
    if (gameMode >= 3) return;
    
    
    const char* keys[3] = {kLeaderboardClassic, kLeaderboardTime, kLeaderboardMove};
    GameManager::sharedGameManager()->sendScore(totalScore, keys[gameMode]);
    
    // Classic mode, send the stage number, other mode, store local high score;
    if (gameMode == 0) {
        GameManager::sharedGameManager()->sendScore(stage, kLeaderboardStage);
    }

}


void GameLayer::sendAdventureScore() {
    
    int totalStars = 0;
    int stages = 0;
    
    int stageCount = GameManager::sharedGameManager()->getAdventureStageCount();
    int stars = 0;

    for (int i=0; i<stageCount; i++) {
        int stageNumber = i+1;
        std::string key = "star_" + to_string(stageNumber);
        stars = CCUserDefault::sharedUserDefault()->getIntegerForKey(key.c_str());
        totalStars += stars;
        
        if (stars == 0) {
            break;
        }
        else {
            stages = stageNumber;
        }
    }
    
    
    if ((totalStars == 0) || (stages == 0)) return;
    
    GameManager::sharedGameManager()->sendScore(totalStars, kLeaderboardAdvStar);
    GameManager::sharedGameManager()->sendScore(stages, kLeaderboardAdvStage);
}

void GameLayer::sendBattleScore(int rewardCoin) {
    int battle = CCUserDefault::sharedUserDefault()->getIntegerForKey("battlePlayed");
    int coin = CCUserDefault::sharedUserDefault()->getIntegerForKey("battleCoinWon");
    
    battle += 1;
    coin += rewardCoin;
    
    CCUserDefault::sharedUserDefault()->setIntegerForKey("battlePlayed", battle);
    CCUserDefault::sharedUserDefault()->setIntegerForKey("battleCoinWon", coin);
    CCUserDefault::sharedUserDefault()->flush();
    
    
    GameManager::sharedGameManager()->sendScore(battle, kLeaderboardBattle);
    GameManager::sharedGameManager()->sendScore(coin, kLeaderboardCoin);
}


int GameLayer::lookupReward(int score) {
    if (GameManager::sharedGameManager()->gameMode == 0) return 0;
    
    int level_easy[10] = {500, 1000, 2000, 3000, 4000, 5000, 6000, 8000, 10000, 12000};
    int level_hard[10] = {500, 1000, 2000, 4000, 6000, 8000, 10000, 13000, 16000, 20000};

    //hard int level[10] = {500, 1000, 2000, 3000, 5000, 7000, 10000, 13000, 17000, 21000};
    //easy int level[10] = {50, 100, 200, 300, 500, 700, 1000, 1300, 1700, 2100};
    
    if (GameManager::sharedGameManager()->getServerInterger("rewardLevel") == 0) {
        for (int i=9; i>=0; i--) {
            if (score >= level_hard[i])
                
                return (i/2) + 1;

        }
    }
    else {
        for (int i=9; i>=0; i--) {
            if (score >= level_easy[i])
                return (i/2) + 1;
        }
    }
    
    return 0;
}


void GameLayer::changeMyColor(CCNode* node) {
    GameBlock* block = (GameBlock*)node;
    block->setBlockColor(rand() % kColorCount);
}

void GameLayer::enableTouch() {
    setTouchEnabled(true);
}

void GameLayer::recolor() {
    
    setTouchEnabled(false);
    CCArray* columnX = NULL;
    GameBlock* blockC = NULL;
    //scheduleOnce(schedule_selector(GameLayer::enableTouch), 1.1);
    scheduleOnce(schedule_selector(GameLayer::testStageEnd), 1.1);
    
    
    CCFadeIn* cfi = CCFadeIn::create(0.25);
    CCDelayTime* d = CCDelayTime::create(0.5);
    CCFadeOut* cfo = CCFadeOut::create(0.25);
    CCSequence* rainbowAction = CCSequence::create(cfi, d, cfo, NULL);
    CCSprite* rainbow = CCSprite::create("rainbow_bg.png");
    rainbow->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.5));
    this->addChild(rainbow);

    rainbow->runAction(rainbowAction);
    
    
    for(int x = (columns->count() - 1); x >= 0; x--) {
        columnX = getCol(columns, x);
        // j: index for the moveTo position of the upper blocks
        for (int y = 0; y < columnX->count(); y++) {
            
            blockC = getGameBlk(columnX, y);
            
            CCDelayTime* delay = CCDelayTime::create(rand() % 5 / 10.0);
            
            CCScaleTo* scaleDown = CCScaleTo::create(0.25, 0);
            CCMoveBy* moveDown = CCMoveBy::create(0.25, ccp(blockSize * 0.5, blockSize * 0.5));
            CCSpawn* down = CCSpawn::createWithTwoActions(scaleDown, moveDown);
            CCScaleTo* scaleUp = CCScaleTo::create(0.25, 1);
            CCMoveBy* moveUp = CCMoveBy::create(0.25, ccp(-blockSize * 0.5, -blockSize * 0.5));
            CCSpawn* up = CCSpawn::createWithTwoActions(scaleUp, moveUp);
            CCCallFuncN* changeColor = CCCallFuncN::create(this, callfuncN_selector(GameLayer::changeMyColor));
            CCSequence* action1 = CCSequence::create(delay, down, changeColor, up, NULL);
            
            CCFadeOut* fadeout = CCFadeOut::create(0.25);
            CCFadeIn* fadein = CCFadeIn::create(0.25);
            CCSequence* action2 = CCSequence::create(delay, fadeout, changeColor, fadein, NULL);

            blockC->runAction(action1);
        }
    }
}






void GameLayer::rightCornerSelected(CCNode* pNode) {
    
    
    const char* url = GameManager::sharedGameManager()->getServerString("promoUrl");
    
    if (( url == NULL ) || (strlen(url) == 0)){
        GameManager::sharedGameManager()->openURL("http://fleetseven.com");
    }
    else if (strcmp(url, "wall") == 0) {
        GameManager::sharedGameManager()->showWall();
    }
    else if ((url[0] <= '9') && (url[0] >= '0')) {
        GameManager::sharedGameManager()->presendAppstore(url);
    }
    else {
        GameManager::sharedGameManager()->openURL(url);
    }
    
    pNode->removeFromParent();

}

void GameLayer::updateRightCorner() {
    if (getChildByTag(kRCMenuTag) != NULL) return;
    
    const char* promoUrl = GameManager::sharedGameManager()->getServerString("promoUrl");
    if ((promoUrl == NULL) || (strlen(promoUrl) == 0)) return;
    
    
    if (GameManager::sharedGameManager()->getServerBoolean("promoInGame2")) {
        
        const char* rcfn = "badge.png";
        CCMenuItemImage* rcImg = CCMenuItemImage::create(rcfn, rcfn, this, menu_selector(GameLayer::rightCornerSelected));
        rcImg->setAnchorPoint(ccp(1.0f, 0.0f));
        rcImg->setTag(kRCImgTag);
        
        
        CCScaleBy* scaleUpRC = CCScaleBy::create(1.5, 1.25);
        CCScaleBy* scaleDownRC = CCScaleBy::create(1.5, 0.8);
        CCSequence* scaleUpAndDownRC = CCSequence::createWithTwoActions(scaleUpRC, scaleDownRC);
        CCRepeatForever* repeatActionRC = CCRepeatForever::create(scaleUpAndDownRC);
        rcImg->runAction(repeatActionRC);
        
        
        
        
        CCMenu* rcMenu = CCMenu::create(rcImg, NULL);
        rcMenu->setAnchorPoint(ccp(1.0f, 0.0f));
        rcMenu->setPosition(ccp(screenSize.width , 0));
        rcMenu->setTag(kRCMenuTag);
        rcMenu->setOpacity(0);
        this->addChild(rcMenu);
        
        CCFadeIn* fadeIn = CCFadeIn::create(1.0);
        rcMenu->runAction(fadeIn);
    }
}


void GameLayer::layoutGCScore() {
    
    CCLabelBMFont* text_info = (CCLabelBMFont*)this->getChildByTag(kInfoTag);
    if (text_info) text_info->setVisible(false);

    int playerCount = GameManager::sharedGameManager()->getPlayerCount();
    for (int i=0; i<playerCount; i++) {
        
        CCSprite* player = CCSprite::create("button_clear.png");
        
        const char* playerName = GameManager::sharedGameManager()->getPlayerName(i);

        CCSprite* playerPhoto = GameManager::sharedGameManager()->getPicture(i);
        int score = GameManager::sharedGameManager()->getScore(i);
        CCLabelTTF* nameLabel = CCLabelTTF::create(playerName, "HelveticaNeue-CondensedBold", 10);
        
        CCLabelBMFont* scoreLabel = CCLabelBMFont::create("0", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        scoreLabel->setScale(0.5);
        scoreLabel->setAnchorPoint(ccp(0.0, 0.5));
        nameLabel->setAnchorPoint(ccp(0.0, 0.5));
        playerPhoto->setAnchorPoint(ccp(0.0, 0.5));
        
        if (i == playerCount -1) {
            nameLabel->setColor(kGreenColor);
            scoreLabel->setColor(kGreenColor);
        }
        
        /*else if (i == playerCount - 2) {
            nameLabel->setColor(kRedColor);
            scoreLabel->setColor(kRedColor);
        }
        else if (i == playerCount - 3) {
            nameLabel->setColor(kYellowColor);
            scoreLabel->setColor(kYellowColor);
        }
        else {
            nameLabel->setColor(kBlueColor);
            scoreLabel->setColor(kBlueColor);

        }
         */
        

        playerPhoto->setPosition(ccp(player->getContentSize().width * (-0.1), player->getContentSize().height * 0.55));
        nameLabel->setPosition(ccp(player->getContentSize().width * (-0.1), player->getContentSize().height * 0.1));
        scoreLabel->setPosition(ccp(player->getContentSize().width * 0.4, player->getContentSize().height * 0.5));
        player->addChild(playerPhoto);
        player->addChild(nameLabel);
        player->addChild(scoreLabel);
        scoreLabel->setTag(kGCPlayerScoreLabelTag);
        
        if (playerCount == 4) {
            player->setPosition(ccp(screenSize.width * (0.125 + (0.25 * i)), screenSize.height * kGCScoreY));
        }
        else if (playerCount == 3) {
            player->setPosition(ccp(screenSize.width * (0.25 + (0.25 * i)), screenSize.height * kGCScoreY));
        }
        else { // 2 player
            player->setPosition(ccp(screenSize.width * (0.375 + (0.25 * i)), screenSize.height * kGCScoreY));
        }
        this->addChild(player);
        player->setTag(kGCScoreLayerTagStart + i);
    }
}


void GameLayer::moveGCScore() {
    

    
    int playerCount = GameManager::sharedGameManager()->getPlayerCount();
    for (int i=0; i<playerCount; i++) {
        CCSprite* player = (CCSprite*)this->getChildByTag(kGCScoreLayerTagStart + i);
        int rank = GameManager::sharedGameManager()->getRank(i);
        if (!player) continue;
        
        CCMoveTo* move = NULL;
        CCScaleTo * scale = NULL;
        
        
        if (playerCount == 4) {
            move = CCMoveTo::create(0.5, ccp(screenSize.width * 0.35, screenSize.height * (0.62 - (0.08 * rank))));
            scale = CCScaleTo::create(0.5,1.1);
        }
        else if (playerCount == 3) {
            move = CCMoveTo::create(0.5, ccp(screenSize.width * 0.35, screenSize.height * (0.60 - (0.1 * rank))));
            scale = CCScaleTo::create(0.5,1.3);

        }
        else {
            move = CCMoveTo::create(0.5, ccp(screenSize.width * 0.35, screenSize.height * (0.56 - (0.12 * rank))));
            scale = CCScaleTo::create(0.5,1.5);

        }

        player->stopAllActions();
        CCSpawn* spawn = CCSpawn::createWithTwoActions(move, scale);
        player->runAction(spawn);

    }
        /*
    CCLayer* gcScoreLayer = (CCLayer*)this->getChildByTag(kGCScoreLayerTag
         );
    if (gcScoreLayer) {
        CCArray* children = gcScoreLayer->getChildren();
        for (int i=0; i<children->count(); i++) {
            CCNode* obj = (CCNode*)children->objectAtIndex(i);
            if (obj == NULL) continue;
            
            CCMoveBy* move = CCMoveBy::create(0.5, ccp(screenSize.width * 0.4, screenSize.height * (-0.4)));
            obj->runAction(move);
        }
             

         */
}




void GameLayer::updateGCScore() {
    
    
    bool needPlayJumpSound = false;
    GameManager::sharedGameManager()->updateRank();
    
    int playerCount = GameManager::sharedGameManager()->getPlayerCount();
    for (int i=0; i<playerCount; i++) {
        CCSprite* player = (CCSprite*)this->getChildByTag(kGCScoreLayerTagStart + i);
        if (!player) continue;
        CCLabelBMFont* scoreLabel = (CCLabelBMFont*)player->getChildByTag(kGCPlayerScoreLabelTag);
        if (!scoreLabel) continue;
        
        int score = GameManager::sharedGameManager()->getScore(i);
        scoreLabel->setString(to_string(score).c_str());
        
        int rank = GameManager::sharedGameManager()->getRank(i);
        
        
        float newX = 0.0;
        
        if (playerCount == 4) {
            newX = screenSize.width * (0.125 + (0.25 * rank));
            //player->setPosition(ccp(screenSize.width * (0.125 + (0.25 * rank)), screenSize.height * kGCScoreY));
        }
        else if (playerCount == 3) {
            newX = screenSize.width * (0.25 + (0.25 * rank));

            //player->setPosition(ccp(screenSize.width * (0.25 + (0.25 * rank)), screenSize.height * kGCScoreY));
        }
        else if (playerCount == 2) {
            newX = screenSize.width * (0.375 + (0.25 * rank));

            //player->setPosition(ccp(screenSize.width * (0.375 + (0.25 * rank)), screenSize.height * kGCScoreY));
        }

        CCAction* move;
        if (newX < player->getPosition().x) {
            

            


            
            move = CCJumpTo::create(0.4, ccp(newX, screenSize.height * kGCScoreY), screenSize.height * 0.06, 1);
            
            if (i == playerCount -1) {
                needPlayJumpSound = true;
            }
            
            /*
            CCSprite* tail = CCSprite::create("tail.png");
            tail->setOpacity(0);
            player->addChild(tail);
            tail->setPosition(ccp(player->getContentSize().width, player->getContentSize().height * 0.5));
            
            CCFadeIn* fadeIn = CCFadeIn::create(0.1);
            CCDelayTime * delay = CCDelayTime::create(0.2);
            CCFadeOut* fadeOut = CCFadeOut::create(0.1);
            tail->runAction(CCSequence::create(fadeIn, delay, fadeOut, NULL));
            */
            
            /*
            CCMotionStreak* streak = CCMotionStreak::create(0.5,0.1,20, ccWHITE, "block.png");
            this->addChild(streak);
            streak->setPosition(player->getPosition());
             */
            //streak->schedule(schedule_selector(GameLayer::updateStreak), 0.1, 4, 0.0);
            //streak->schedule(schedule_selector(GameLayer::removeStreak));
            
            
           // streak->setPosition(ccp(newX, screenSize.height * kGCScoreY));
            

        }
        else {
            move = CCMoveTo::create(0.4, ccp(newX, screenSize.height * kGCScoreY));
        }
        player->runAction(move);
        
        if (needPlayJumpSound) {
            GameManager::sharedGameManager()->playSoundEffect("jump.wav");
        }
                                       
        //player->setPosition(ccp(screenSize.width * (0.125 + (0.25 * rank)), screenSize.height * kGCScoreY));

        
    }
    

        
        
        
        
    
    /*
    this->removeChildByTag(kGCScoreLayerTag, true);
    
    CCLayer* gcScoreLayer = CCLayer::create();
    int playerCount = GameManager::sharedGameManager()->getPlayerCount();
    for (int i=0; i<playerCount; i++) {
        //const char* playerId = GameManager::sharedGameManager()->getPlayerID(i);
        
        const char* playerName = GameManager::sharedGameManager()->getPlayerName(i);
        CCSprite* playerPhoto = GameManager::sharedGameManager()->getPicture(i);
        int score = GameManager::sharedGameManager()->getScore(i);
        
        
        // force to 20x20
        
        
        
        playerPhoto->setPosition(ccp(screenSize.width * 0.05, screenSize.height * (0.97 - (i* 0.05))));
        
        CCLabelTTF* nameLabel = CCLabelTTF::create(playerName, "Tahoma", 10);
        CCLabelTTF* scoreLabel = CCLabelTTF::create(to_string(score).c_str(), "Tahoma", 10);
        
        nameLabel->setAnchorPoint(ccp(0.0, 0.5));
        scoreLabel->setAnchorPoint(ccp(0.0, 0.5));
        
        nameLabel->setPosition(ccp(screenSize.width * 0.10, screenSize.height * (0.98 - (i*0.05))));
        scoreLabel->setPosition(ccp(screenSize.width * 0.10, screenSize.height * (0.96 - (i*0.05))));

        
        gcScoreLayer->addChild(playerPhoto);
        gcScoreLayer->addChild(nameLabel);
        gcScoreLayer->addChild(scoreLabel);
        
        
        
    }
    
    this->addChild(gcScoreLayer);
    gcScoreLayer->setTag(kGCScoreLayerTag);
    */
    
}

GameLayer::~GameLayer() {

    CCLOG("GameLayer::~() called-------------------");

    this->columns->release();

}

