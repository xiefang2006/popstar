//
//  PauseLayer.cpp
//  SimpleGame
//
//  Created by SimplyGame on 9/6/13.
//
//

#include "PauseLayer.h"
#include "MenuScene.h"
#include "GameScene.h"
#include "GameManager.h"
#include "StageScene.h"

#define kRCImgTag 551
#define kRCMenuTag 553
#define kBottomMenuTag 555

void PauseLayer::buttonTouched(CCNode *pNode) {
    
    // Using this requires customized CCDirector class
    CCMenuItemImage* item = (CCMenuItemImage*)pNode;
    if (item) {
        int tag = item->getTag();
        
        switch (tag) {
            case 0:
            {
                bool isOff = GameManager::sharedGameManager()->gameSetting.isSoundOff;
                isOff = !isOff;
                
                CCSprite* newImage;
                if (isOff) {
                    newImage = CCSprite::create("no_sound.png");
                }
                else {
                    newImage = CCSprite::create("sound.png");
                }
                
                item->setNormalImage(newImage);
                GameManager::sharedGameManager()->gameSetting.isSoundOff = isOff;
            }
                break;
            case 1:
            {
                bool isOff = GameManager::sharedGameManager()->gameSetting.isMusicOff;
                isOff = !isOff;
                
                CCSprite* newImage;
                if (isOff) {
                    newImage = CCSprite::create("no_music.png");
                }
                else {
                    newImage = CCSprite::create("music.png");
                }
                
                item->setNormalImage(newImage);
                GameManager::sharedGameManager()->gameSetting.isMusicOff = isOff;
                if (isOff) {
                    GameManager::sharedGameManager()->stopMusic();
                }
                else {
                    GameManager::sharedGameManager()->continueMusic();
                }
            }
                break;
                
            case 5:
            {
                bool isOff = GameManager::sharedGameManager()->gameSetting.isSingleTapOff;
                isOff = !isOff;
                
                CCSprite* newImage;
                if (isOff) {
                    newImage = CCSprite::create("tap_double.png");

                }
                else {
                    newImage = CCSprite::create("tap_single.png");

                }
                
                item->setNormalImage(newImage);
                GameManager::sharedGameManager()->gameSetting.isSingleTapOff = isOff;
            }
                break;
                
            case 2:
            {
                // remove the badge
                removeChildByTag(kRCMenuTag);
                
                CCScene *prevScene = CCDirector::sharedDirector()->previousScene();
                CCTransitionSlideInB* transition = CCTransitionSlideInB::create(0.5, prevScene);
                CCDirector::sharedDirector()->popScene(transition);
                GameManager::sharedGameManager()->startBanner();
                
                GameScene *gameScene = dynamic_cast<GameScene*>(prevScene);
                if (gameScene) {
                    gameScene->scheduleOnce(schedule_selector(GameScene::updateCoins), 0.1);
                }
                
            }
                break;
            case 3:
                break;
                //CCDirector::sharedDirector()->replaceScene(CCTransitionSlideInB::create(0.5, GameScene::create()));
                //break;
            case 4:
                
                if (GameManager::sharedGameManager()->gameMode == 3) {
                    CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, StageScene::create()));
                }
                else {
                    CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, MenuScene::create()));
                }
                break;
                
        }
    }
    GameManager::sharedGameManager()->playSoundEffect("select.wav");
    GameManager::sharedGameManager()->saveGameSetting();
}

void PauseLayer::layoutMenu() {
    CCMenuItemImage* sound;
    if (GameManager::sharedGameManager()->gameSetting.isSoundOff) {
        sound = CCMenuItemImage::create("no_sound.png", "no_sound.png", this, menu_selector(PauseLayer::buttonTouched));
        
    }
    else {
        sound = CCMenuItemImage::create("sound.png", "sound.png", this, menu_selector(PauseLayer::buttonTouched));
    }
    CCMenuItemImage* music;
    if (GameManager::sharedGameManager()->gameSetting.isMusicOff) {
        music = CCMenuItemImage::create("no_music.png", "no_music.png", this, menu_selector(PauseLayer::buttonTouched));
    }
    else {
        music = CCMenuItemImage::create("music.png", "music.png", this, menu_selector(PauseLayer::buttonTouched));
    }
    
    CCMenuItemImage* tap;
    if (GameManager::sharedGameManager()->gameSetting.isSingleTapOff) {
        tap = CCMenuItemImage::create("tap_double.png", "tap_double.png", this, menu_selector(PauseLayer::buttonTouched));
        
    }
    else {
        tap = CCMenuItemImage::create("tap_single.png", "tap_single.png", this, menu_selector(PauseLayer::buttonTouched));
        
    }
    
    
    CCMenuItemImage* back;
    if (GameManager::sharedGameManager()->gameMode == 3) {
        back = CCMenuItemImage::create("stage.png", "stage.png", this, menu_selector(PauseLayer::buttonTouched));
    }
    else {
        back = CCMenuItemImage::create("exit.png", "exit.png", this, menu_selector(PauseLayer::buttonTouched));
    }
    CCMenuItemImage* retry = CCMenuItemImage::create("retry.png", "retry.png", this, menu_selector(PauseLayer::buttonTouched));
    
    //CCMenuItemImage* rank = CCMenuItemImage::create("ranking.png", "ranking.png", this, menu_selector(PauseLayer::buttonTouched));
    CCMenuItemImage* play = CCMenuItemImage::create("continue.png", "continue.png", this, menu_selector(PauseLayer::buttonTouched));
    play->setScale(1.5);
    
    
    sound->setTag(0);
    music->setTag(1);
    play->setTag(2);
    retry->setTag(3);
    back->setTag(4);
    
    tap->setTag(5);
    
    //retry->setOpacity(0);
    
    CCScaleBy* scaleUp = CCScaleBy::create(0.5, 1.25);
    CCScaleBy* scaleDown = CCScaleBy::create(0.5, 0.8);
    CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleDown, scaleUp);
    CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);
    play->runAction(repeatAction);
    
    
    
    CCMenu* menu = CCMenu::create(play, back, sound, music, tap, NULL);
    
    menu->alignItemsHorizontallyWithPadding(screenSize.width * 0.05);
    menu->setPosition(screenSize.width * 0.5, screenSize.height * 0.1);
    menu->setTag(kBottomMenuTag);
    this->addChild(menu);
    
}

void PauseLayer::layoutItemsForAdventure() {
    float lineScale = 0.6;
    
    CCLabelBMFont *line1 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line2 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line3 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line4 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line5 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line6 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line7a = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line7b = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));

    CCLabelBMFont *line8 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line9 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *lineExample = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));

    
    line1->setScale(lineScale * 1.2);
    line2->setScale(lineScale);
    line3->setScale(lineScale);
    line4->setScale(lineScale);
    line5->setScale(lineScale);
    line6->setScale(lineScale * 1.2);
    line7a->setScale(lineScale);
    line7b->setScale(lineScale);

    line8->setScale(lineScale);
    line9->setScale(lineScale);
    lineExample->setScale(lineScale);
    
    line1->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine1a"));
    line2->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine2"));
    line3->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine3"));
    line4->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine4a"));
    line5->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine5a"));
    line6->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine6"));
    line7a->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine7a"));
    line7b->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine7b"));

    line8->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine8"));
    line9->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine9"));
    lineExample->setString(GameManager::sharedGameManager()->getLocalizedString("stageTargetExample"));
    line1->setAnchorPoint(ccp(0,0.5f));
    line2->setAnchorPoint(ccp(0,0.5f));
    // line3->setAnchorPoint(ccp(0,0.5f));
    lineExample->setAnchorPoint(ccp(0.0f,0.5f));
    line4->setAnchorPoint(ccp(0,0.5f));
    line5->setAnchorPoint(ccp(0,0.5f));
    line6->setAnchorPoint(ccp(0,0.5f));
    line7a->setAnchorPoint(ccp(0,0.5f));
    line7b->setAnchorPoint(ccp(0,0.5f));
    line8->setAnchorPoint(ccp(0,0.5f));
    line9->setAnchorPoint(ccp(0,0.5f));
    line1->setPosition(ccp(screenSize.width * 0.06, screenSize.height * 0.94));
    line2->setPosition(ccp(screenSize.width * 0.08, screenSize.height * 0.88));
    // line3->setPosition(ccp(screenSize.width * 0.06, screenSize.height * 0.00));
    line4->setPosition(ccp(screenSize.width * 0.08, screenSize.height * 0.82));
    line5->setPosition(ccp(screenSize.width * 0.08, screenSize.height * 0.76));
    lineExample->setPosition(ccp(screenSize.width * 0.08, screenSize.height * 0.70));
    line6->setPosition(ccp(screenSize.width * 0.06, screenSize.height * 0.47)); // boosters
    line7a->setPosition(ccp(screenSize.width * 0.16, screenSize.height * 0.41)); // bomb
    line7b->setPosition(ccp(screenSize.width * 0.16, screenSize.height * 0.35)); // bomb
    line8->setPosition(ccp(screenSize.width * 0.16, screenSize.height * 0.29)); // hint
    line9->setPosition(ccp(screenSize.width * 0.16, screenSize.height * 0.23)); // paint
    
    this->addChild(line1);
    this->addChild(line2);
    this->addChild(lineExample);
    this->addChild(line4);
    this->addChild(line5);
    this->addChild(line6);
    this->addChild(line7a);
    this->addChild(line7b);
    this->addChild(line8);
    this->addChild(line9);
    
    CCSprite *sampleTarget = CCSprite::create("sampletarget.png");
    sampleTarget->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.62));
    this->addChild(sampleTarget);
    
    /*
     
    
    CCSprite *connected = CCSprite::create();
    connected->initWithFile("connected.png");
    connected->setPosition(ccp(screenSize.width * 0.28, screenSize.height * 0.62));
    this->addChild(connected);
    
    CCSprite *connectedNot = CCSprite::create();
    connectedNot->initWithFile("connected_not.png");
    connectedNot->setPosition(ccp(screenSize.width * 0.72, screenSize.height * 0.62));
    this->addChild(connectedNot);
    */
    
    CCSprite *addMoves = CCSprite::create("addmoves.png");
    CCSprite *addTime = CCSprite::create("addtime.png");
    addMoves->setScale(0.8);
    addTime->setScale(0.8);
    addMoves->setPosition(ccp(screenSize.width * 0.11, screenSize.height * 0.41));
    addTime->setPosition(ccp(screenSize.width * 0.11, screenSize.height * 0.35));
    this->addChild(addMoves);
    this->addChild(addTime);

    
    CCSprite *hint = CCSprite::create();
    hint->initWithFile("paint.png");
    hint->setScale(0.8);
    hint->setPosition(ccp(screenSize.width * 0.10, screenSize.height * 0.29));
    this->addChild(hint);
    
    CCSprite *paint = CCSprite::create();
    paint->initWithFile("rainbow.png");
    paint->setScale(0.8);
    paint->setPosition(ccp(screenSize.width * 0.10, screenSize.height * 0.23));
    this->addChild(paint);
    

}

void PauseLayer::layoutItems() {


    float lineScale = 0.6;

    CCLabelBMFont *line1 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line2 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line3 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line4 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line5 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line6 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line7 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line8 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *line9 = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *lineConnected = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    CCLabelBMFont *lineConnectedNot = CCLabelBMFont::create("", GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));

    line1->setScale(lineScale * 1.2);
    line2->setScale(lineScale);
    line3->setScale(lineScale);
    line4->setScale(lineScale);
    line5->setScale(lineScale);
    line6->setScale(lineScale * 1.2);
    line7->setScale(lineScale);
    line8->setScale(lineScale);
    line9->setScale(lineScale);
    lineConnected->setScale(lineScale);
    lineConnectedNot->setScale(lineScale);

    line1->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine1"));
    line2->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine2"));
    line3->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine3"));
    line4->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine4"));
    line5->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine5"));
    line6->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine6"));
    line7->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine7"));
    line8->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine8"));
    line9->setString(GameManager::sharedGameManager()->getLocalizedString("pauseLine9"));
    lineConnected->setString(GameManager::sharedGameManager()->getLocalizedString("pauseConnected"));
    lineConnectedNot->setString(GameManager::sharedGameManager()->getLocalizedString("pauseConnectedNot"));

    line1->setAnchorPoint(ccp(0,0.5f));
    line2->setAnchorPoint(ccp(0,0.5f));
    // line3->setAnchorPoint(ccp(0,0.5f));
    lineConnected->setAnchorPoint(ccp(0.5f,0.5f));
    lineConnectedNot->setAnchorPoint(ccp(0.5f,0.5f));
    line4->setAnchorPoint(ccp(0,0.5f));
    line5->setAnchorPoint(ccp(0,0.5f));
    line6->setAnchorPoint(ccp(0,0.5f));
    line7->setAnchorPoint(ccp(0,0.5f));
    line8->setAnchorPoint(ccp(0,0.5f));
    line9->setAnchorPoint(ccp(0,0.5f));

    line1->setPosition(ccp(screenSize.width * 0.06, screenSize.height * 0.94));
    line2->setPosition(ccp(screenSize.width * 0.08, screenSize.height * 0.88));
    // line3->setPosition(ccp(screenSize.width * 0.06, screenSize.height * 0.00));
    line4->setPosition(ccp(screenSize.width * 0.08, screenSize.height * 0.82));
    line5->setPosition(ccp(screenSize.width * 0.08, screenSize.height * 0.76));
    lineConnected->setPosition(ccp(screenSize.width * 0.28, screenSize.height * 0.5));
    lineConnectedNot->setPosition(ccp(screenSize.width * 0.72, screenSize.height * 0.5));

    line6->setPosition(ccp(screenSize.width * 0.06, screenSize.height * 0.41)); // boosters
    line7->setPosition(ccp(screenSize.width * 0.16, screenSize.height * 0.35)); // bomb
    line8->setPosition(ccp(screenSize.width * 0.16, screenSize.height * 0.29)); // hint
    line9->setPosition(ccp(screenSize.width * 0.16, screenSize.height * 0.23)); // paint

    this->addChild(line1);
    this->addChild(line2);
    // this->addChild(line3);
    this->addChild(lineConnected);
    this->addChild(lineConnectedNot);
    this->addChild(line4);
    this->addChild(line5);
    this->addChild(line6);
    this->addChild(line7);
    this->addChild(line8);
    this->addChild(line9);
   
    CCSprite *connected = CCSprite::create();
    connected->initWithFile("connected.png");
    connected->setPosition(ccp(screenSize.width * 0.28, screenSize.height * 0.62));
    this->addChild(connected);

    CCSprite *connectedNot = CCSprite::create();
    connectedNot->initWithFile("connected_not.png");
    connectedNot->setPosition(ccp(screenSize.width * 0.72, screenSize.height * 0.62));
    this->addChild(connectedNot);
 

    CCSprite *bomb = CCSprite::create();
    bomb->initWithFile("bomb.png");
    bomb->setScale(0.8);
    bomb->setPosition(ccp(screenSize.width * 0.11, screenSize.height * 0.35));
    this->addChild(bomb);
    
    CCSprite *hint = CCSprite::create();
    hint->initWithFile("paint.png");
    hint->setScale(0.8);
    hint->setPosition(ccp(screenSize.width * 0.10, screenSize.height * 0.29));
    this->addChild(hint);
 
    CCSprite *paint = CCSprite::create();
    paint->initWithFile("rainbow.png");
    paint->setScale(0.8);
    paint->setPosition(ccp(screenSize.width * 0.10, screenSize.height * 0.23));
    this->addChild(paint);

}


bool PauseLayer::init() {
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    setKeypadEnabled(true);

    
    GameManager::sharedGameManager()->hideBanner();
    
    if (GameManager::sharedGameManager()->getServerBoolean("noInterInPause")==false) {
        GameManager::sharedGameManager()->showInterstitial();
    }
    
    screenSize = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite *bg = CCSprite::create();
    
#ifdef __Fruit__
    bg->initWithFile("iphone_bg.png");
#else
    bg->initWithFile("up_bg.png");
#endif
    bg->setPosition(ccp(screenSize.width * 0.5,screenSize.height * 0.5));
    bg->setScaleY(screenSize.height / 568.0);
    this->addChild( bg , -10 );
    
    //CCLayerColor* backgroundLayer = CCLayerColor::create(ccc4(0, 0, 0, 192));
    //this->addChild(backgroundLayer, -5);
    
    this->layoutMenu();
    if (GameManager::sharedGameManager()->gameMode == 3) {
        this->layoutItemsForAdventure();
    }
    else {
        this->layoutItems();
    }

    this->scheduleOnce(schedule_selector(PauseLayer::updateRightCorner), 0.5);

    return true;
}



void PauseLayer::rightCornerSelected(CCNode* pNode) {
    
    
    const char* url = GameManager::sharedGameManager()->getServerString("promoUrl");
    
    if (( url == NULL ) || (strlen(url) == 0)){
        GameManager::sharedGameManager()->openURL("http://fleetseven.com");
    }
    else if (strcmp(url, "wall") == 0) {
        GameManager::sharedGameManager()->showWall();
    }
    else if ((url[0] <= '9') && (url[0] >= '0')) {
        GameManager::sharedGameManager()->presendAppstore(url);
    }
    else {
        GameManager::sharedGameManager()->openURL(url);
    }
    
    pNode->removeFromParent();

}

void PauseLayer::updateRightCorner() {
    if (getChildByTag(kRCMenuTag) != NULL) return;
    
    const char* promoUrl = GameManager::sharedGameManager()->getServerString("promoUrl");
    if ((promoUrl == NULL) || (strlen(promoUrl) == 0)) return;
    
    
    if (GameManager::sharedGameManager()->getServerBoolean("promoInPause2")) {
        
        const char* rcfn = "badge.png";
        CCMenuItemImage* rcImg = CCMenuItemImage::create(rcfn, rcfn, this, menu_selector(PauseLayer::rightCornerSelected));
        rcImg->setAnchorPoint(ccp(1.0f, 0.0f));
        rcImg->setTag(kRCImgTag);
        
        
        CCScaleBy* scaleUpRC = CCScaleBy::create(1.5, 1.25);
        CCScaleBy* scaleDownRC = CCScaleBy::create(1.5, 0.8);
        CCSequence* scaleUpAndDownRC = CCSequence::createWithTwoActions(scaleUpRC, scaleDownRC);
        CCRepeatForever* repeatActionRC = CCRepeatForever::create(scaleUpAndDownRC);
        rcImg->runAction(repeatActionRC);
        
        
        
        
        CCMenu* rcMenu = CCMenu::create(rcImg, NULL);
        rcMenu->setAnchorPoint(ccp(1.0f, 0.0f));
        rcMenu->setPosition(ccp(screenSize.width , 0));
        rcMenu->setTag(kRCMenuTag);
        rcMenu->setOpacity(0);
        this->addChild(rcMenu);
        
        CCFadeIn* fadeIn = CCFadeIn::create(1.0);
        rcMenu->runAction(fadeIn);
    }
}

void PauseLayer::keyBackClicked() {
    buttonTouched(getChildByTag(kBottomMenuTag)->getChildByTag(2));
}
