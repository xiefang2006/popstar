//
//  GameScene.h
//  SampleGame
//
//  Created by SimplyGame on 7/25/13.
//  Copyright (c) 2013 Bullets in a Burning Box, Inc. All rights reserved.
//

#ifndef __SampleGame__GameScene__
#define __SampleGame__GameScene__

#include "cocos2d.h"
#define kGameLayerTag 10

using namespace cocos2d;

class  GameScene : public CCScene
{
public:
    
    void rewardBonusCoin();
    void updateCoins();
    void updateGCScore();
    
    void popupExitBattleAlert();
    
    virtual bool init();
    void initGameLayer();
    CREATE_FUNC(GameScene);
};

#endif /* defined(__SampleGame__GameScene__) */
