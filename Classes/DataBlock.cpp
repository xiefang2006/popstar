#include "cocos2d.h"
#include "DataBlock.h"

using namespace cocos2d;

bool DataBlock::init(){

	// place holder...

	return true;
}

void DataBlock::setXY(int x, int y) {
    this->x = x;
    this->y = y;
}

void DataBlock::setX(int x) {
    this->x = x;
}

void DataBlock::setY(int y) {
    this->y = y;
}

int DataBlock::getX() {
    return x;
}

int DataBlock::getY() {
    return y;
}

void DataBlock::setBlockColor(int c) {
    this->color = c;
}

int DataBlock::getBlockColor() {
    return color;
}

DataBlock* DataBlock::create ( int x, int y, int color ) {
    DataBlock *cBlk = new DataBlock();
    cBlk->setBlockColor( color );
    cBlk->setXY( x, y );
    if ( cBlk ) {
        cBlk->autorelease();
        return cBlk;
    }
    CC_SAFE_DELETE(cBlk);
    return NULL;
}

       
