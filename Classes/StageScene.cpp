//
//  StageScene.cpp
//  SimpleGame
//
//  Created by Game on 14-1-9.
//
//

#include "StageScene.h"
#include "StageLayer.h"
#include "GameManager.h"


bool StageScene::init() {
    if ( !CCScene::init() )
    {
        return false;
    }
    
    screenSize = CCDirector::sharedDirector()->getWinSize();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    GameManager::sharedGameManager()->playMusic("music0.wav");
#else
    GameManager::sharedGameManager()->playMusic("music3.mp3");
#endif

    StageLayer* stageLayer = StageLayer::create();
    stageLayer->setTag(kStageLayerTag);
    this->addChild(stageLayer);
    
    CCSprite *bg = CCSprite::create();
    bg->initWithFile("iphone_bg.png");
    bg->setPosition(ccp(screenSize.width * 0.5,screenSize.height * 0.5));
    bg->setScaleY(screenSize.height / 568.0);
    this->addChild( bg , -10 );
    
    return true;
}
