//
//  GameManager.h
//  SimpleGame
//
//  Created by SimplyGame on 6/11/13.
//
//

#ifndef __SimpleGame__GameManager__
#define __SimpleGame__GameManager__

#define kGameFont "DFHaiBaoW12-GB"
#include "cocos2d.h"

#include "HttpRequest.h"
#include "HttpResponse.h"
#include "HttpClient.h"
using namespace cocos2d::extension;

using namespace cocos2d;

typedef struct {
    CCString* playerID;
    CCString* playerName;
    CCSprite* playerPhoto;
    int score;
    int rank;
} GamePlayer;

typedef struct {
    int stage; // starting from 1
    int target[6]; // red, yellow, green, blue, purple, score
    int moves[3]; // one, two, three star
    int time[3]; // one, two, three star
} AdventureStage;

typedef struct {
    unsigned int highScore;
    unsigned int bestScoreTime;
    unsigned int bestScoreMove;
    int coins;
    bool isMusicOff;
    bool isSoundOff;
    bool isSingleTapOff;
} GameSetting;

typedef struct {
    unsigned int stage;
    unsigned int score;
    unsigned int beginScore;
    unsigned int matrix[10][10];
} GameState;


class  GameManager : cocos2d::CCObject
{    
private:
    int playerCount;
    GamePlayer players[4];
    
    
    char currentMusic[64];
    
    static GameManager* m_pInstance;
    GameManager();
    
    // Member variables
    cocos2d::CCDictionary* localizedStrings;
    cocos2d::CCDictionary* serverSetting;

public:
    static GameManager* sharedGameManager();
    ~GameManager();
    
    GameSetting gameSetting;
    GameState gameState;
    
    bool hasBonusScene;
    
    int gameMode;   // 0, classic; 1, time; 2, move; 3, adventure; 4. multiplayer
    int selectedStage;
    
    bool inBattle;
    
    // Audio
    void setupAudioEngine();
    int playSoundEffect(const char* soundEffectName);
    void stopSoundEffect(int soundId);
    
    void playMusic(const char* musicName);
    void stopMusic();
    void continueMusic();
    
    // Game states
    void loadGameSetting();
    void saveGameSetting();
    bool loadGameState();
    void saveGameState();
    
    // Server setting
    void loadServerSetting();
    void httpLoaded(CCHttpClient *sender, CCHttpResponse *data);
    bool getServerBoolean(const char* key);
    int getServerInterger(const char* key);
    const char* getServerString(const char* key);
    void onServerSettingLoaded();
    

    
    // Localization
    void setupLocalization();
    const char* getLocalizedString(const char* key);
    
    // Advertising
    void startBanner(); //Start the banner, if it's already started, make sure it's showing
    void showBanner();  //Show the banner
    void hideBanner(); // Hide the banner, when going to scene that should be ad free
    
    void cacheInterstitial();
    void showInterstitial();
    
    void showWall();
    void showVideoAd();

    
    // InAppPurchase
    void loadInAppStore();
    void purchaseProuct(const char* productId);
    const char* getProductPrice(const char* productId);
    void restorePurchases();
    
    // GameCenter
    void loadGameCenter();
    void sendScore(int score, const char* scoreId);
    void showGameCenter(const char* socreId = NULL);
    
    // Social
    void shareSocialNetwork(int score);

    // OpenURL
    void openURL(const char* url);
 
    void logEvent(const char* info);
    
    
    
    void presendAppstore(const char* appId);
    void presendCoinAlert();
    void presendPromoPopAlert(bool rate);
    
    
    AdventureStage getAdventureStage(unsigned int stage);
    int getAdventureStageCount();

    
    void updateCoins();
    
    
    
    // Match making
    void hostMatch();
    void disconnectMatch();
    void sendScoreRT(int score);
    
    
    void removeAllPlayers();
    void addPlayer(int i, const char* playerId, const char* playerName, CCSprite* playerPhoto);
    
    int getScore(int playerIndex);
    void setScore(const char* playerId, int score);
    void setScore(int playerIndex, int score);
    int getRank(int playerIndex);
    
    int getPlayerCount();
    const char* getPlayerName(int playerIndex);
    CCSprite* getPicture(int playerIndex);
    
    const char* getPlayerID(int playerIndex);
    
    
    void debugPlayers();
    void updateGCScore();
    void updateRank();
    
    
    float getScreenScale();
    
};

inline
char const* B2S(bool x) {
    return (x ? "T" : ".");
}

inline
std::string to_string(int i) {
    char s[256];
    sprintf( s, "%d", i );
    return s;
}


#endif /* defined(__SimpleGame__GameManager__) */
