//
//  BonusLayer.h
//  SimpleGame
//
//  Created by SimplyGame on 10/1/13.
//
//

#ifndef __SimpleGame__BonusLayer__
#define __SimpleGame__BonusLayer__

#include "cocos2d.h"

using namespace cocos2d;

class  BonusLayer : public CCLayer
{
private:
    CCSize screenSize;

    
public:
    virtual bool init();
    
    void removeScene();
    
    void openBonus(CCMenuItem* item);
    
    void playRewardCoinsAnimation(int reward);

    void playCoinSound();
    void removeCoin(CCNode* coin);
    void playCoinsInSound();
    
    void updateCoins();
    
    CREATE_FUNC(BonusLayer);
};
#endif /* defined(__SimpleGame__BonusLayer__) */
