//
//  PauseScene.cpp
//  SimpleGame
//
//  Created by SimplyGame on 9/6/13.
//
//

#include "PauseScene.h"
#include "PauseLayer.h"

bool PauseScene::init() {
    if ( !CCScene::init() )
    {
        return false;
    }
    
    //GameManager::sharedGameManager()->playMusic("music.wav");
    PauseLayer* pauseLayer = PauseLayer::create();
    pauseLayer->setTag(kPauseLayerTag);
    this->addChild(pauseLayer);
    
    return true;
}