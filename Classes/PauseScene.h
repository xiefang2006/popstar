//
//  PauseScene.h
//  SimpleGame
//
//  Created by SimplyGame on 9/6/13.
//
//

#ifndef __SimpleGame__PauseScene__
#define __SimpleGame__PauseScene__


#include "cocos2d.h"
#define kPauseLayerTag 10

using namespace cocos2d;

class  PauseScene : public CCScene
{
public:
    virtual bool init();
    CREATE_FUNC(PauseScene);
};

#endif /* defined(__SimpleGame__PauseScene__) */
