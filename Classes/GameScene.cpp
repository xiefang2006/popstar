//
//  GameScene.cpp
//  SampleGame
//
//  Created by SimplyGame on 7/25/13.
//  Copyright (c) 2013 Bullets in a Burning Box, Inc. All rights reserved.
//

#include "GameScene.h"
#include "GameLayer.h"
#include "StoreScene.h"
#include "GameManager.h"
#include "Popstar.h"

void GameScene::initGameLayer() {
    GameLayer* gameLayer = GameLayer::create();
    gameLayer->setTag(kGameLayerTag);
    this->addChild(gameLayer);
    
}
bool GameScene::init() {
    if ( !CCScene::init() )
    {
        return false;
    }
    
    
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CCString* musicfilename = CCString::create("music0.wav");
#else
    CCString* musicfilename = CCString::createWithFormat("music%d.wav", GameManager::sharedGameManager()->gameMode); 
#endif

    if (GameManager::sharedGameManager()->gameMode != 3) {
        GameManager::sharedGameManager()->playMusic(musicfilename->getCString());
    }

    
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

    CCSprite *bg = CCSprite::create();
    bg->initWithFile("iphone_bg.png");
    bg->setPosition(ccp(screenSize.width * 0.5,screenSize.height * 0.5));
    bg->setScaleY(screenSize.height / 568.0);
    this->addChild( bg , -10 );
    
    this->scheduleOnce(schedule_selector(GameScene::initGameLayer), 0.6);
    GameManager::sharedGameManager()->startBanner();
    return true;
}

void GameScene::updateCoins() {
    GameLayer* gameLayer = (GameLayer*)this->getChildByTag(kGameLayerTag);
    if (gameLayer) {
        gameLayer->updateCoins();
    }
}



void GameScene::updateGCScore() {
    GameLayer* gameLayer = (GameLayer*)this->getChildByTag(kGameLayerTag);
    if (gameLayer) {
        gameLayer->updateGCScore();
    }
}


void GameScene::rewardBonusCoin() {
    GameLayer* gameLayer = (GameLayer*)this->getChildByTag(kGameLayerTag);
    if (gameLayer) {
        gameLayer->rewardBonusCoin();
    }
}

void GameScene::popupExitBattleAlert() {
    GameLayer* gameLayer = (GameLayer*)this->getChildByTag(kGameLayerTag);
    if (gameLayer) {
        gameLayer->popupExitBattleAlert();
    }
}
