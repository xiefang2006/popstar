//
//  StoreLayer.cpp
//  SimpleGame
//
//  Created by SimplyGame on 8/21/13.
//
//

#include "Popstar.h"


#include "StoreLayer.h"
#include "GameManager.h"
#include "GameScene.h"
#include "MenuScene.h"

// 0-100 reserved for buy coin buttons
#define kCoinsTag 1001
#define kRateButtonTag 2001
#define kRateLabelTag 2002

#define kFreeCoinMenuTag 501



// void StoreLayer::backButtonTouched() {
void StoreLayer::backButtonTouched(CCNode *pNode) {
    /*
    GameScene* gameScene = (GameScene*)this->getParent();
    if (gameScene != NULL) {
        gameScene->hideStoreLayer();
    }
     
     CCScene *prevScene = CCDirector::sharedDirector()->previousScene();
     CCTransitionSlideInB* transition = CCTransitionSlideInB::create(1.0, prevScene);
     CCDirector::sharedDirector()->popScene(transition);
    */
    
    CCScene *prevScene = CCDirector::sharedDirector()->previousScene();
    
    // Should use dynamic cast as the prevScene could be GameScene or MenuScene
    GameScene *gameScene = dynamic_cast<GameScene*>(prevScene);
    if (gameScene) {
        gameScene->scheduleOnce(schedule_selector(GameScene::updateCoins), 0.5);
        GameManager::sharedGameManager()->startBanner();
    }
    
    MenuScene *menuScene = dynamic_cast<MenuScene*>(prevScene);
    if (menuScene) {
        menuScene->scheduleOnce(schedule_selector(MenuScene::updateCoins), 0.5);
    }
    
    GameManager::sharedGameManager()->playSoundEffect("select.wav");

    CCTransitionSlideInB* transition = CCTransitionSlideInB::create(0.5, prevScene);
    CCDirector::sharedDirector()->popScene(transition);
    
}

void StoreLayer::buyButtonTouched(CCObject* pSender) {
    CCMenuItemImage* button = (CCMenuItemImage*)pSender;
    int tag = button->getTag();
    
    if (tag == kRateButtonTag) {
        

        
        // rate us
        GameManager::sharedGameManager()->openURL(kRateUrl);
        button->setEnabled(false);
        button->setOpacity(127);
        
        CCLabelTTF* rateLabel = (CCLabelTTF*) this->getChildByTag(kRateLabelTag);
        if (rateLabel) {
            rateLabel->setOpacity(127);
        }
        
        CCUserDefault::sharedUserDefault()->setBoolForKey("didRating25", true);
        GameManager::sharedGameManager()->gameSetting.coins += 5;
        GameManager::sharedGameManager()->saveGameSetting();
        
        this->scheduleOnce(schedule_selector(StoreLayer::updateCoins), 0.5);
    }
    else {
        const char*productId[5] = {kInAppPurchaseCoin1Id, kInAppPurchaseCoin2Id, kInAppPurchaseCoin3Id, kInAppPurchaseCoin4Id, kInAppPurchaseCoin5Id};
        GameManager::sharedGameManager()->purchaseProuct(productId[button->getTag()]);
        GameManager::sharedGameManager()->playSoundEffect("select.wav");

    }
}

void StoreLayer::layoutItems() {
    

    
    const char* haveCoinsStr = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("haveCoins"), GameManager::sharedGameManager()->gameSetting.coins)->getCString();
    CCLabelBMFont* haveCoinsLabel = CCLabelBMFont::create(haveCoinsStr, GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    haveCoinsLabel->setScale(1.0);
    haveCoinsLabel->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.9));
    haveCoinsLabel->setTag(kCoinsTag);
    this->addChild(haveCoinsLabel);
    
    int items[5] = {30, 80, 180, 380, 780};
    const char*productId[5] = {kInAppPurchaseCoin1Id, kInAppPurchaseCoin2Id, kInAppPurchaseCoin3Id, kInAppPurchaseCoin4Id, kInAppPurchaseCoin5Id};
    
    bool showRating = false;
    float gap = 0.12;
    
    
    if ((GameManager::sharedGameManager()->getServerBoolean("askRating") == true) &&
        (CCUserDefault::sharedUserDefault()->getBoolForKey("didRating25") == false))
    {
        showRating = true;
        gap = 0.12;
    }
    
    int numberOfIAP = 5;
    if (showRating == true) {
        numberOfIAP = 4;
    }
    
    for (int i=0; i<numberOfIAP; i++) {
        const char* labelStr = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("coins"), items[i])->getCString();
        CCLabelBMFont* label = CCLabelBMFont::create(labelStr, GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        label->setScale(0.7);
        CCSprite* coins = CCSprite::create("coins.png");
        // coins->setScale(0.4);
        CCMenuItemImage* buyButton = CCMenuItemImage::create("buy_button.png", "buy_button.png", this, menu_selector(StoreLayer::buyButtonTouched));
        buyButton->setTag(i);
        CCMenu* buyMenu = CCMenu::create(buyButton, NULL);
        
        const char* price = GameManager::sharedGameManager()->getProductPrice(productId[i]);
        
        CCLabelTTF* priceLabel = CCLabelTTF::create(price, "Optima-Bold", 16);
        
        /*
        CCLabelBMFont* priceLabel = CCLabelBMFont::create(price, "mm.fnt");
        priceLabel->setScale(0.3);
        */
        
        coins->setPosition(ccp(screenSize.width * 0.12, screenSize.height * (0.25 + (i * gap))));
        label->setPosition(ccp(screenSize.width * 0.34, screenSize.height * (0.25 + (i * gap))));
        buyMenu->setPosition(ccp(screenSize.width * 0.72, screenSize.height * (0.25 + (i * gap))));
        priceLabel->setPosition(ccp(screenSize.width * 0.77, screenSize.height * (0.25 + (i * gap))));
        
        this->addChild(label);
        this->addChild(coins);
        this->addChild(buyMenu);
        this->addChild(priceLabel);
    }
    
    if (showRating == true) {
        const char* labelStr = GameManager::sharedGameManager()->getLocalizedString("free");
        CCLabelBMFont* label = CCLabelBMFont::create(labelStr, GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        label->setScale(0.7);
        CCSprite* coins = CCSprite::create("coins.png");
        // coins->setScale(0.4);
        CCMenuItemImage* buyButton = CCMenuItemImage::create("buy_button.png", "buy_button.png", this, menu_selector(StoreLayer::buyButtonTouched));
        buyButton->setTag(kRateButtonTag);
        CCMenu* buyMenu = CCMenu::create(buyButton, NULL);
        
        const char* price = GameManager::sharedGameManager()->getLocalizedString("rate");
        
        CCLabelTTF* priceLabel = CCLabelTTF::create(price, "Optima-Bold", 16);
        
        /*
         CCLabelBMFont* priceLabel = CCLabelBMFont::create(price, "mm.fnt");
         priceLabel->setScale(0.3);
         */
        
        coins->setPosition(ccp(screenSize.width * 0.12, screenSize.height * (0.25 + (4 * gap))));
        label->setPosition(ccp(screenSize.width * 0.34, screenSize.height * (0.25 + (4 * gap))));
        buyMenu->setPosition(ccp(screenSize.width * 0.72, screenSize.height * (0.25 + (4 * gap))));
        priceLabel->setPosition(ccp(screenSize.width * 0.77, screenSize.height * (0.25 + (4 * gap))));
        priceLabel->setTag(kRateLabelTag);
        
        this->addChild(label);
        this->addChild(coins);
        this->addChild(buyMenu);
        this->addChild(priceLabel);
    }
    
    
    CCMenuItemImage* play = CCMenuItemImage::create("continue.png", "continue.png", this, menu_selector(StoreLayer::backButtonTouched));
    play->setScale(1.5);
    
    CCScaleBy* scaleUp = CCScaleBy::create(0.5, 1.25);
    CCScaleBy* scaleDown = CCScaleBy::create(0.5, 0.8);
    CCSequence* scaleUpAndDown = CCSequence::createWithTwoActions(scaleDown, scaleUp);
    CCRepeatForever* repeatAction = CCRepeatForever::create(scaleUpAndDown);
    play->runAction(repeatAction);
    
    CCMenu* exitMenu = CCMenu::create(play, NULL);
    exitMenu->setPosition(ccp(screenSize.width * 0.5, screenSize.height * 0.1));
    this->addChild(exitMenu);
    
}

void StoreLayer::updateCoins() {
    
    GameManager::sharedGameManager()->playSoundEffect("coinsin.wav");

    CCString* coinsStr = CCString::createWithFormat(GameManager::sharedGameManager()->getLocalizedString("haveCoins"), GameManager::sharedGameManager()->gameSetting.coins);
    //CCLOG("Coins: %d", GameManager::sharedGameManager()->gameSetting.coins);
    
    CCLabelBMFont* coinsLabel = (CCLabelBMFont*)this->getChildByTag(kCoinsTag);
    if (coinsLabel) {
        //CCLOG("%s", coinsStr->getCString());
        coinsLabel->setString(coinsStr->getCString());
    }
}
bool StoreLayer::init() {
    if ( !CCLayer::init() )
    {
        return false;
    }
    GameManager::sharedGameManager()->hideBanner();

    screenSize = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite *bg = CCSprite::create();
#ifdef __Fruit__
    bg->initWithFile("iphone_bg.png");
#else
    bg->initWithFile("up_bg.png");
#endif    /*
    bg->setAnchorPoint(ccp(0,0));
    bg->setPosition(ccp(0,0));
    this->addChild( bg , -10 );
    */
    bg->setPosition(ccp(screenSize.width * 0.5,screenSize.height * 0.5));
    bg->setScaleY(screenSize.height / 568.0);
    this->addChild( bg , -10 );

    
    /*
    CCLayerColor* backgroundLayer = CCLayerColor::create(ccc4(0, 0, 0, 192));
    this->addChild(backgroundLayer, -5);
    */
    
    this->layoutItems();
    
    this->updateRightCorner();
    
    return true;
}

void StoreLayer::updateRightCorner() {
    if (getChildByTag(kFreeCoinMenuTag) != NULL) return;
    
    /*
    const char* promoUrl = GameManager::sharedGameManager()->getServerString("promoUrl");
    if ((promoUrl == NULL) || (strlen(promoUrl) == 0)) return;
    */
    
    if (GameManager::sharedGameManager()->getServerBoolean("videoInStore") == true) {
        
        // Most likely we don't show out of China
        ccLanguageType currentLanguage = CCApplication::sharedApplication()->getCurrentLanguage();
        if (currentLanguage != kLanguageChinese) {
            if (GameManager::sharedGameManager()->getServerBoolean("videoNonChina") == false) {
                return;
            }
        }
        
        
        const char* rcfn = "free_button.png";
        CCMenuItemImage* rcImg = CCMenuItemImage::create(rcfn, rcfn, this, menu_selector(StoreLayer::rightCornerSelected));
        rcImg->setAnchorPoint(ccp(1.0f, 0.0f));
        rcImg->setScale(0.9);
        
        
        CCLabelBMFont* freeCoinsLabel = CCLabelBMFont::create(GameManager::sharedGameManager()->getLocalizedString("freeCoins"), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
        freeCoinsLabel->setScale(0.8);
        freeCoinsLabel->setPosition(rcImg->getContentSize().width * 0.5, rcImg->getContentSize().height * 0.5);
        rcImg->addChild(freeCoinsLabel);
        
        
        /*
        CCScaleBy* scaleUpRC = CCScaleBy::create(1.0, 1.25);
        CCScaleBy* scaleDownRC = CCScaleBy::create(1.0, 0.8);
        CCSequence* scaleUpAndDownRC = CCSequence::createWithTwoActions(scaleUpRC, scaleDownRC);
        CCRepeatForever* repeatActionRC = CCRepeatForever::create(scaleUpAndDownRC);
        rcImg->runAction(repeatActionRC);
        */
        

        
        
        
        CCMenu* rcMenu = CCMenu::create(rcImg, NULL);
        rcMenu->setAnchorPoint(ccp(1.0f, 0.0f));
        rcMenu->setPosition(ccp(screenSize.width , 0));
        rcMenu->setTag(kFreeCoinMenuTag);
        rcMenu->setOpacity(0);
        this->addChild(rcMenu);
        
        CCFadeIn* fadeIn = CCFadeIn::create(1.0);
        rcMenu->runAction(fadeIn);
        
        //isRCOn = false;
        
    }
}

void StoreLayer::rightCornerSelected(CCNode* node) {
    GameManager::sharedGameManager()->showVideoAd();
    return;
}
