#include "cocos2d.h"
using namespace cocos2d;

class DataBlock : public CCObject
{
private:

public:
    int x, y;
    int color; 

    // #-color
    // -------
    // 0-blue
    // 1-green
    // 2-purple
    // 3-red
    // 4-yellow

    virtual bool init();
    static DataBlock* create(int x, int y, int color);
    void setXY(int x, int y);
    void setX(int x);
    void setY(int y);
    int getX();
    int getY();
    int getBlockColor();
    void setBlockColor(int c);
    
    CREATE_FUNC(DataBlock);
};
