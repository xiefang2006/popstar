#include "Popstar.h"
#include "AppDelegate.h"
#include "GameScene.h"
#include "GameManager.h"
#include "MenuScene.h"
#include "SimpleAudioEngine.h"

#include "BonusScene.h"


USING_NS_CC;



AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {

    
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    


    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
    
    CCSize screenSize = CCEGLView::sharedOpenGLView()->getFrameSize();
    CCSize designSize = CCSizeMake(320, 480);
    std::vector<std::string> searchPaths;
    
#ifdef __Fruit__
    searchPaths.push_back("sound-fruit");
#else
    searchPaths.push_back("sound");
#endif
    
    /*
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if (screenSize.width > 720) {    // Android HD = 720p
#else
    if (GameManager::sharedGameManager()->getScreenScale() > 1.0) {
    //if (screenSize.width > 640) {    // iPad retina 1536
#endif
     */

#ifdef __Fruit__
        //searchPaths.push_back("ipad-fruit");
        //searchPaths.push_back("fonts-ipad-hkhb");
        //pDirector->setContentScaleFactor(1280.0/designSize.width);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

    searchPaths.push_back("iphone-fruit");
    searchPaths.push_back("fonts-iphone-hkhb");
    pDirector->setContentScaleFactor(640.0/designSize.width);
#else //iOS
    searchPaths.push_back("ipad-fruit");
    searchPaths.push_back("fonts-ipad-hkhb");
    pDirector->setContentScaleFactor(1280.0/designSize.width);
#endif
    
#else
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    searchPaths.push_back("iphone-star");
    searchPaths.push_back("fonts-iphone-hkhb");
    pDirector->setContentScaleFactor(640.0f/designSize.width);
#else // iOS
    searchPaths.push_back("ipad-star");
    searchPaths.push_back("fonts-ipad-hkhb");
    pDirector->setContentScaleFactor(1280.0f/designSize.width);
#endif
    
#endif


    /*
        
        //searchPaths.push_back("halloween");
        //pDirector->setContentScaleFactor(640.0f/designSize.width);
    }
    else {      // iPhone 320, 640, origianl iPad 768
                // Android 720p or less

#ifdef __Fruit__
        searchPaths.push_back("iphone-fruit");
#else
        searchPaths.push_back("iphone-star");
#endif
        searchPaths.push_back("fonts-hkhb");
        pDirector->setContentScaleFactor(640.0f/designSize.width);
    }
    
     */
    
    
    CCFileUtils::sharedFileUtils()->setSearchPaths(searchPaths);
    
    
    
    // Get save data, IAP, game center, init audio as soon as we start the game
    GameManager::sharedGameManager()->loadGameSetting();
    GameManager::sharedGameManager()->loadGameState();
    GameManager::sharedGameManager()->loadInAppStore();
    GameManager::sharedGameManager()->loadGameCenter();
    GameManager::sharedGameManager()->setupAudioEngine();
    GameManager::sharedGameManager()->loadServerSetting();
    
    // 2018: init interstitial no matter what server status
    GameManager::sharedGameManager()->cacheInterstitial();
    
    
    
    if ((screenSize.height / screenSize.width) >= 1.5) { // iPhone 3.5 or taller, fixed width
        CCEGLView::sharedOpenGLView()->setDesignResolutionSize(designSize.width, designSize.height,kResolutionFixedWidth);
    }
    else { // Fatter screen, for example iPad, OK to have black on the size
        CCEGLView::sharedOpenGLView()->setDesignResolutionSize(designSize.width, designSize.height,kResolutionFixedWidth); //kResolutionShowAll);
    }

    // turn on display FPS
    //pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);
    
    
    if (GameManager::sharedGameManager()->hasBonusScene == true) {
        pDirector->runWithScene(BonusScene::create());
    }
    else {
        pDirector->runWithScene(MenuScene::create());
    }
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    CCDirector::sharedDirector()->stopAnimation();
    
    GameManager::sharedGameManager()->saveGameState();
    // if you use SimpleAudioEngine, it must be pause
    
    if (GameManager::sharedGameManager()->gameSetting.isMusicOff == false) {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    }
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();
    
    // if you use SimpleAudioEngine, it must resume here
    if (GameManager::sharedGameManager()->gameSetting.isMusicOff == false) {

        CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    }
}



