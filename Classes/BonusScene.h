//
//  BonusScene.h
//  SimpleGame
//
//  Created by SimplyGame on 10/1/13.
//
//

#ifndef __SimpleGame__BonusScene__
#define __SimpleGame__BonusScene__

#include "cocos2d.h"
#define kBonusLayerTag 10

using namespace cocos2d;

class  BonusScene : public CCScene
{
public:
    virtual bool init();
    CREATE_FUNC(BonusScene);
};


#endif /* defined(__SimpleGame__BonusScene__) */
