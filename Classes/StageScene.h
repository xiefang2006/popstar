//
//  StageScene.h
//  SimpleGame
//
//  Created by Game on 14-1-9.
//
//

#ifndef __SimpleGame__StageScene__
#define __SimpleGame__StageScene__

#include "cocos2d.h"
#define kStageLayerTag 10

using namespace cocos2d;

class  StageScene : public CCScene
{
private:
    CCSize screenSize;

public:
    virtual bool init();
    CREATE_FUNC(StageScene);
};

#endif /* defined(__SimpleGame__StageScene__) */
