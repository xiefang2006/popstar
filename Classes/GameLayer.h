//
//  GameLayer.h
//  SampleGame
//
//  Created by SimplyGame on 7/25/13.
//  Copyright (c) 2013 Bullets in a Burning Box, Inc. All rights reserved.
//

#ifndef __SampleGame__GameLayer__
#define __SampleGame__GameLayer__

#include "cocos2d.h"
#include "DataBlock.h"
#include "GameBlock.h"
#include "stdio.h"
#include "GameManager.h"

// CCSprite tags




#define REC_DEPTH 1
#define ADX 0.00
#define ADY 0.115

using namespace cocos2d;

class  GameLayer : public CCLayer
{
private:
    int lastSubGameMode;
    bool isScoreShared;
    
    // For replay the stage
    int stageBeginScore;
    
    bool cheered;
    int lastPopSound;
    bool initFromSavedGame;
    
    CCSize screenSize;
    CCArray *bestSolution;
    
    int selectedItem;

    int sNum;        // solveMap excuted times
    bool selected;   // blocks are been selected

    int stage;
    int targetScore;
    int totalScore;
    int bestScore;

    // Avoid sending scores too often
    int networkScore;
    
    int recNum;

    int blockSize;
    float duration;

    int sizeX;
    int sizeY;

    cocos2d::CCArray *columns; // columns of blocks
    bool tcm[10][10]; // touch connection map [row][column]

    // Label in starting animation

    std::string fontName;
    int fontSize;

    int adX;    // addition X space for ads
    int adY;    // addition Y space for ads
    
    // Pitch will go higher as more blocks is cleared.
    //int soundPitch;
    int popOrder;

    // position for the block of best score
    int bestX;
    int bestY;
    
    // position for painting
    int changeColorX;
    int changeColorY;

    int searchDepth;
    
    int remainingTime;
    int remainingMoves;
    
    bool isPoping;
    bool isGameOver;
    
    float zoomRatio;

    int hintTimer;
    bool hintOn;
    bool needFadeIn;
    
    
    
    // Advanture game mode
    //int targetBlocks[6];
    AdventureStage currentStage;
    int subGameMode; // 0 for moves, 1 for time, 2 for both
    std::string defaultInfoText;
    bool itemUsed;

public:
    virtual bool init();
    virtual void registerWithTouchDispatcher(void);
    virtual bool ccTouchBegan (CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved (CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchEnded (CCTouch *pTouch, CCEvent *pEvent);
    virtual void keyBackClicked();

    void initStage ();
    void updateColumns ();
    void updateColumnsData ( CCArray *columns, bool **tcm );
    bool findConnection ( CCArray *columns, int x, int y, int touchColor );
    int testConnection ( CCArray *columns, int x, int y, int testColor, bool **cCM );
    void clearTCM ();
    void printCM ();
    // void printCM ( bool **tcm );                            // print connection map
    void printCM ( bool **tcm, int sizeX, int sizeY );      // print connection map
    // void printSS ( int **s );                              // print solvedScore
    void printSS ( int **s, int sizeX, int sizeY );        // print solvedScore
    void printColumns ( CCArray *columns );                              // print solvedScore
    int solveMap ( CCArray *columns, int depth );
    int getMaxX ( CCArray *columns );
    int getMaxY ( CCArray *columns );
    void copyCM ( bool **a, bool **b, int sizeX, int sizeY ); // copy connection map from a to b
    CCArray* cloneColumns( CCArray *columns );
    CCArray* cloneGameColumns( CCArray *columns );
    void deleteColumns( CCArray *columns );
    void clearColumns ();
    int genBonusScore ( CCArray *columns );
    int genBonusScore ( int num );
    int genScore ();
    int genScore ( bool **cm, int sizeX, int sizeY );

    void updateSelection ();
    void recoverSelection ();

    void updateScore (); 
    bool testStageEnd (); 

    int genTargetScore ( int stage );
    int genTargetScore2 ( int stage );

    int countBlocks();
    void setSearchDepth( int cnt );
    
    void layoutStageTarget();
    
    
    
    

    
    // Animations
    void playStartAnimation(int currentStage);
    void playEndAnimation(int remainingBlocks);
    
    void initBlocks();
    void finishStage();
    

    // CCPoint genPosition ( int x, int y ); 
    CCPoint genPosition ( float x, float y ); 

    int getTouchedX ( float x ); 
    int getTouchedY ( float y ); 

    
    void playPopSound();
    void playPopSoundOnce();
    
    int getConnectedCount();
    int getRemainingCount();
    
    void popBlocks();
    void popOneBlock();
    void popBlock(int order, int column, int row);
    void updateRemainingBlocks();
    void finishPoping();
    
    void popRemainingBlocks();
    void popOneRemainingBlock();
    
    void playScoreAnimation(GameBlock *block, int score);
    void removeScoreLabel(CCNode* pSender);
    
    void hearts ( GameBlock *blk );

    void layoutControls();
    void addCoinButtonTouched();
    void updateCoins();
    
    
    void itemSelected(CCNode* pSender);
    
    
    void bombBlocks(int column, int row);
    void popupColorSelection( int column, int row);
    void changeColor(int column, int row, int color);
    void colorSelected(CCNode* pSender);
    void setItemSeleted(int itemTag, bool selected);
    
    void refillBlocks();

    void countDown();
    
    void pauseButtonTouched();
    
    void popupGameOver();
    void enableGameOverMenu();

    void gameOverItemTouched(CCNode* pSender);
    
    void playReadyGo();
    void playLandingSound();

    void playBonusScoreAnimation();
    void playGameOverAnimation();

    void fadeOutControls();
    void fadeInControls();

    
    
    // Save and load stage. Save after every pop, but write to file only at exiting
    void saveState();
    void saveScore();
    void saveStateThenScore();
    void saveStateAndScore();
    void sendScore();
    void sendAdventureScore();

    
    // Reward for getting high score
    int lookupReward(int score);
    
    void cancelItemUse();

    void rewardBonusCoin();
    void rewardCoins();
    void playCoinSound();
    void removeCoin(CCNode* coin);
    void playCoinsInSound();
    
    
    void playCollectCoinsAnimation();
    
    void fadeInMenu();

    void restartGame();
    
    
    void recolor();
    void changeMyColor(CCNode* node);
    void enableTouch();
    
    
    
    void popupRestartQuery();
    void continueSelected(CCNode* pNode);

    void popupExitBattleQuery();
    void popupExitBattleAlert();

    void exitBattleSelected(CCNode* pNode);
    


    
    void updateRightCorner();
    void rightCornerSelected(CCNode* pNode);


    void playRewardCoinsAnimation(int reward);


    void autoHint();
    void showHint();
    void stopHint();

    virtual ~GameLayer();

    
    
    void updateTargetAnimation(GameBlock *block, int score);
    void popupAdventureGameOver();
    void adventureGameOverItemTouched(CCNode* pSender);
    
    
    void shareTouched(CCNode* pSender);
    
    
    
    void updateGCScore();
    void moveGCScore();
    void layoutGCScore();
    void rewardBattleCoins();
    
    void playRewardCoinsAnimation2(float delay, int reward, float x, float y);
    void removeCoin2(CCNode* coin);

    void sendBattleScore(int rewardCoin);

    
    CREATE_FUNC(GameLayer);
};

inline
CCArray* getCol(CCArray *columns, int x) {
    return (dynamic_cast<CCArray*>(columns->objectAtIndex(x)));
}

inline
DataBlock* getBlk(CCArray *column, int y) {
    return (dynamic_cast<DataBlock*>(column->objectAtIndex(y)));
}

inline
GameBlock* getGameBlk(CCArray *column, int y) {
    return (dynamic_cast<GameBlock*>(column->objectAtIndex(y)));
}




#endif /* defined(__SampleGame__GameLayer__) */


