//
//  MenuScene.h
//  SimpleGame
//
//  Created by SimplyGame on 9/3/13.
//
//

#ifndef __SimpleGame__MenuScene__
#define __SimpleGame__MenuScene__

#include "cocos2d.h"
#define kMenuLayerTag 10

using namespace cocos2d;

class  MenuScene : public CCScene
{
public:
    virtual bool init();
    void updateCoins();
    CREATE_FUNC(MenuScene);
};

#endif /* defined(__SimpleGame__MenuScene__) */
