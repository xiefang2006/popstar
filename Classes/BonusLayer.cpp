//
//  BonusLayer.cpp
//  SimpleGame
//
//  Created by SimplyGame on 10/1/13.
//
//


#include "BonusLayer.h"
#include "GameManager.h"
#include "MenuScene.h"
#include "GameScene.h"
#include "StoreScene.h"


#define kCoinsX     0.92
#define kCoinsY     0.95
#define kCCountX    0.85
#define kCCountY    0.95
#define kCoinsTag 1001
#define kCoinsMenuTag 1002
#define kSunBusrtTag 560
#define kBonusBagTag 570

void BonusLayer::updateCoins() {
    int coinsCount = GameManager::sharedGameManager()->gameSetting.coins;
    CCLabelBMFont* coinsLabel = (CCLabelBMFont*)this->getChildByTag(kCoinsTag);
    if (coinsLabel) {
        coinsLabel->setString(to_string(coinsCount).c_str());
    }
}

void BonusLayer::playCoinSound() {
    GameManager::sharedGameManager()->playSoundEffect("coin.mp3");
}


void BonusLayer::playCoinsInSound() {
    GameManager::sharedGameManager()->playSoundEffect("coinsin.wav");
}


void BonusLayer::removeCoin(CCNode* coin) {
    GameManager::sharedGameManager()->gameSetting.coins ++;
    GameManager::sharedGameManager()->saveGameSetting();
    updateCoins();
    coin->removeFromParent();
}

void BonusLayer::openBonus(CCMenuItem* item) {
    
    item->stopAllActions();
    
    CCScaleTo* scale = CCScaleTo::create(0.5, 1.0);
    item->runAction(scale);
    
    item->setEnabled(false);
    
    
    // reward coins
    int reward = rand() % 6 + 5;
    playRewardCoinsAnimation(reward);
    
    float delay = reward * 0.2 + 1.0;
    
    this->scheduleOnce(schedule_selector(BonusLayer::removeScene), delay);

    
}

void BonusLayer::playRewardCoinsAnimation(int reward) {
    for (int i=0; i<reward; i++) {
        CCSprite* coin = CCSprite::create("coin.png");
        coin->setPosition(ccp((i + 1) * screenSize.width / (reward + 1), screenSize.height * 0.4));
        coin->setOpacity(0);
        //coin->setTag(kRewardCoinTagBegin + i);
        this->addChild(coin, -1);
        
        CCFadeIn* fadein = CCFadeIn::create(0.5);
        CCDelayTime* delay = CCDelayTime::create(i * 0.2);
        CCCallFunc* sound = CCCallFunc::create(this, callfunc_selector(BonusLayer::playCoinSound));
        CCDelayTime* delay2 = CCDelayTime::create((reward-i) * 0.15);
        CCMoveTo* move = CCMoveTo::create(0.5, ccp(screenSize.width * kCoinsX, screenSize.height * kCoinsY));
        CCCallFuncN* remove = CCCallFuncN::create(this, callfuncN_selector(BonusLayer::removeCoin));
        CCSequence* action = NULL;
        
        CCCallFunc* sound2 = CCCallFunc::create(this, callfunc_selector(BonusLayer::playCoinsInSound));
        
        if (i== 0) {
            action = CCSequence::create(delay, sound, fadein, delay2, move, remove, sound2, NULL);
        }
        else {
            action = CCSequence::create(delay, sound, fadein, delay2, move, remove, NULL);
            
        }
        
        
        coin->runAction(action);
    }
    
    
}

void BonusLayer::removeScene() {
    
    if (GameManager::sharedGameManager()->hasBonusScene) {
        CCDirector::sharedDirector()->replaceScene(CCTransitionFadeBL::create(0.7, MenuScene::create()));
        GameManager::sharedGameManager()->hasBonusScene = false;
    }
    else {
        CCScene *prevScene = CCDirector::sharedDirector()->previousScene();
        CCTransitionFadeBL* transition = CCTransitionFadeBL::create(0.7, prevScene);
        CCDirector::sharedDirector()->popScene(transition);
        
        GameScene *gameScene = dynamic_cast<GameScene*>(prevScene);
        if (gameScene) {
            gameScene->scheduleOnce(schedule_selector(GameScene::updateCoins), 0.1);
        }
        
        MenuScene *menuScene = dynamic_cast<MenuScene*>(prevScene);
        if (menuScene) {
            menuScene->scheduleOnce(schedule_selector(MenuScene::updateCoins), 0.1);
        }
        
        StoreScene *storeScene = dynamic_cast<StoreScene*>(prevScene);
        if (storeScene) {
            storeScene->scheduleOnce(schedule_selector(StoreScene::updateCoins), 0.1);
        }
    }
}


bool BonusLayer::init() {
    
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    screenSize = CCDirector::sharedDirector()->getWinSize();
    
    
    CCSprite *bg = CCSprite::create("iphone_bg.png");
    bg->setPosition(ccp(screenSize.width * 0.5,screenSize.height * 0.5));
    bg->setScaleY(screenSize.height / 568.0);
    this->addChild( bg , -10 );
    
    
#ifdef __Fruit__
#else
    CCSprite* sunburst = CCSprite::create("bg_sunburst.png");
    sunburst->setPosition(ccp (screenSize.width * 0.5, screenSize.height * 0.6));
    sunburst->setScale(1.0);
    sunburst->setOpacity(192);
    //sunburst->setTag(kGameOverBackgroundTag);
    this->addChild(sunburst, -1);
    sunburst->setTag(kSunBusrtTag);
    CCRotateBy* rotate = CCRotateBy::create(5.0, 10.0);
    CCRepeatForever* repeat = CCRepeatForever::create(rotate);
    sunburst->runAction(repeat);
#endif
    
    
    CCMenuItemImage* bonus = CCMenuItemImage::create("coins3x.png", "coins3x.png", this, menu_selector(BonusLayer::openBonus));
    bonus->setTag(kBonusBagTag);
    CCScaleBy* scaleUp = CCScaleBy::create(0.5, 1.25);
    CCScaleBy* scaleDown = CCScaleBy::create(0.5, 0.8);
    CCSequence* scale = CCSequence::createWithTwoActions(scaleUp, scaleDown);
    CCRepeatForever* repeat2 = CCRepeatForever::create(scale);
    bonus->runAction(repeat2);
    
    CCMenu* menu = CCMenu::create(bonus, NULL);
    menu->setPosition(ccp (screenSize.width * 0.5, screenSize.height * 0.6));
    this->addChild(menu);
    
    
    
    CCMenuItemImage* addCoinButton = CCMenuItemImage::create("coins.png", "coins.png", this, NULL);
    // addCoinButton->setScale(0.4);
    CCMenu* addCoinMenu = CCMenu::create(addCoinButton, NULL);
    addCoinMenu->setPosition(ccp(screenSize.width * kCoinsX, screenSize.height * kCoinsY));
    addCoinMenu->setTag(kCoinsMenuTag);
    //addCoinMenu->setOpacity(0);
    this->addChild(addCoinMenu);
    
    int coinsCount = GameManager::sharedGameManager()->gameSetting.coins;
    // CCLabelBMFont* coinsLabel = CCLabelBMFont::create(to_string(coinsCount).c_str(), kGameFont, 15);
    CCLabelBMFont* coinsLabel = CCLabelBMFont::create(to_string(coinsCount).c_str(), GameManager::sharedGameManager()->getLocalizedString("mm.fnt"));
    coinsLabel->setScale(0.7);
    coinsLabel->setAnchorPoint(ccp(1,0.5f));
    coinsLabel->setPosition(ccp(screenSize.width * kCCountX, screenSize.height * kCCountY));
    coinsLabel->setTag(kCoinsTag);
    //coinsLabel->setOpacity(0);
    this->addChild(coinsLabel);
    
    
    return true;
    
    
}
