//
//  StoreLayer.h
//  SimpleGame
//
//  Created by SimplyGame on 8/21/13.
//
//

#ifndef __SimpleGame__StoreLayer__
#define __SimpleGame__StoreLayer__

#define kStoreLayerTag 20

#include "cocos2d.h"

using namespace cocos2d;
class StoreLayer : public CCLayer {
private:
    CCSize screenSize;
public:
    virtual bool init();
    void layoutItems();
    // void backButtonTouched();
    void backButtonTouched(CCNode *pNode);
    void buyButtonTouched(CCObject* pSender);
    void updateCoins();
    
    void updateRightCorner();
    void rightCornerSelected(CCNode* node);
    CREATE_FUNC(StoreLayer);
};

#endif /* defined(__SimpleGame__StoreLayer__) */
