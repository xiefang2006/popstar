#include "cocos2d.h"
#include "GameBlock.h"

using namespace cocos2d;

bool GameBlock::init(){

	// place holder...

	return true;
}

GameBlock* GameBlock::create(int color) {
    const char *pszFileName;
    switch(color) {
        case 0: 
            pszFileName = "red.png";
            break;
        case 1: 
            pszFileName = "blue.png";
            break;
        case 2: 
            pszFileName = "green.png";
            break;
        case 3: 
            pszFileName = "orange.png";
            break;
        case 4: 
            pszFileName = "purple.png";
            break;
        default:
            pszFileName = "red.png";
    }

    GameBlock *pobSprite = new GameBlock();
    pobSprite->color = color;
    if (pobSprite && pobSprite->initWithFile(pszFileName))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void GameBlock::setXY(int x, int y) {
    this->x = x;
    this->y = y;
}

void GameBlock::setX(int x) {
    this->x = x;
}

void GameBlock::setY(int y) {
    this->y = y;
}

int GameBlock::getX() {
    return x;
}

int GameBlock::getY() {
    return y;
}

void GameBlock::setBlockColor(int c) {
    this->color = c;
    
    /*
    const char *pszFileName = (color2str(c) + ".png").c_str();
    this->setTexture(CCTextureCache::sharedTextureCache()->addImage(pszFileName));
    */
    std::string ss = color2str(c) + ".png";
    this->setTexture(CCTextureCache::sharedTextureCache()->addImage(ss.c_str()));
}

int GameBlock::getBlockColor() {
    return color;
}

std::string GameBlock::getColorStr() {
    switch(this->color) {
        case 0: 
            return("red");
        case 1: 
            return("blue");
        case 2: 
            return("green");
        case 3: 
            return("orange");
        case 4: 
            return("purple");
    }
}

std::string GameBlock::color2str(int color) {
    switch(color) {
        case 0: 
            return("red");
        case 1: 
            return("blue");
        case 2: 
            return("green");
        case 3: 
            return("orange");
        case 4: 
            return("purple");
    }
}


       
