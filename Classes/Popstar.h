//
//  GameLayer.h
//  SampleGame
//
//  Created by SimplyGame on 7/25/13.
//  Copyright (c) 2013 Bullets in a Burning Box, Inc. All rights reserved.
//

#ifndef __PopStar__
#define __PopStar__

#include "cocos2d.h"




#define __Star__


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#ifdef __Fruit__
#include "../Classes_Android/Fruit_Android.h"
#else
#include "../Classes_Android/Star_Android.h"
#endif

#else // __IOS__

#ifdef __Fruit__
#include "Fruit.h"
#else
#include "Star.h"
#endif

#endif






#endif /* defined(__PopStar__) */


