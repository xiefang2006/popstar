//
//  StageLayer.h
//  SimpleGame
//
//  Created by Game on 14-1-9.
//
//

#ifndef __SimpleGame__StageLayer__
#define __SimpleGame__StageLayer__

#include "cocos2d.h"
#include "CCScrollView.h"

using namespace cocos2d;
using namespace cocos2d::extension;



class  StageLayer : public CCLayer, public CCScrollViewDelegate
{
private:
    CCSize screenSize;
    float buttonSizeW;
    float buttonSizeH;
    CCScrollView* scrollView;
    int lastEnabledStage;
    int totalStars;
    
public:
    virtual bool init();
    void layoutButtons();
    void layoutControls();
    
    void exitTouched(CCNode* node);
    void gameCenterTouched(CCNode* node);
    void stageSelected(CCNode* node);
    
    virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view) {};
    virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view) {};
    
    void keyBackClicked();

    
    CREATE_FUNC(StageLayer);
};


#endif /* defined(__SimpleGame__StageLayer__) */
