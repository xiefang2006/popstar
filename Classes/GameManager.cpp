//
//  GameManager.cpp
//  minesweeperx
//
//  Created by SimplyGame on 8/3/13.
//
//

#include "Popstar.h"
#include "GameManager.h"
#include "SimpleAudioEngine.h"
#include <stdlib.h>

#include "StoreScene.h"
#include "GameScene.h"
#include "MenuScene.h"

#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

static unsigned int stages[][12] = {
    {0,0,0,0,0,0,0,0,0,0,0,0},
    // 1
    {2,0,0,0,0,0,       1,2,3,0,0,0},
    {0,0,0,0,0,120,     1,3,5,0,0,0},
    {0,0,3,3,0,0,       2,4,6,0,0,0},
    {5,5,0,0,0,200,     4,6,8,0,0,0},
    
    {0,0,0,8,0,0,       0,0,0,6,10,12},
    {0,0,0,0,0,300,     0,0,0,6,10,12},
    {5,5,5,0,0,0,       0,0,0,10,15,20},
    {8,0,0,8,0,400,     0,0,0,10,20,30},

    // 9
    {18,18,0,0,0,0,     15,25,30,0,0,0},
    {0,0,0,0,0,500,     6,10,12,0,0,0},
    {0,0,12,12,12,0,    15,25,30,0,0,0},
    {0,0,0,15,15,1500,  15,25,30,0,0,0},
    
    {0,0,20,20,0,0,     0,0,0,15,25,30},
    {0,0,0,0,0,1000,    0,0,0,12,18,22},
    {12,12,12,0,0,0,    0,0,0,13,20,25},
    {0,0,0,15,15,1500,  0,0,0,13,20,25},
    
    // 17
    {30,0,0,0,0,0,      20,35,40,0,0,0},
    {0,0,0,0,0,1200,    10,18,22,0,0,0},
    {25,0,25,0,0,0,     20,35,40,0,0,0},
    {0,20,20,20,0,2500, 22,36,42,0,0,0},
    
    {25,25,0,0,0,0,     0,0,0,20,35,40},
    {0,0,0,0,0,1800,    0,0,0,12,20,24},
    {15,0,15,0,15,0,    0,0,0,16,28,36},
    {20,20,0,0,20,2500, 0,0,0,16,30,40},
    
    // 25
    {0,0,40,0,0,0,      25,50,60,0,0,0},
    {0,0,0,0,0,2000,    16,32,40,0,0,0},
    {25,0,25,0,25,0,    30,55,70,0,0,0},
    {0,40,40,0,0,4000,  45,66,80,0,0,0},
    
    {0,0,0,0,35,0,      0,0,0,20,35,45},
    {0,0,0,0,0,2500,    0,0,0,16,28,34},
    {25,0,0,25,25,0,    0,0,0,30,50,60},
    {0,40,0,0,40,3000,  0,0,0,45,60,70},
    
    // 33
    {0,0,30,0,30,0,     30,55,65,0,0,0},
    {0,0,0,0,0,5000,    38,64,80,0,0,0},
    {0,25,25,25,25,0,   40,60,80,0,0,0},
    {0,15,25,35,0,4000, 40,60,80,0,0,0},
    
    {0,0,35,0,35,0,     0,0,0,35,55,70},
    {0,0,0,0,0,5000,    0,0,0,32,55,70},
    {25,25,25,25,0,0,   0,0,0,35,55,70},
    {15,0,25,0,35,4000, 0,0,0,40,55,70},
    
    // 41
    {50,0,0,0,0,0,      30,55,70,0,0,0},
    {0,0,0,0,0,6000,    40,70,85,0,0,0},
    {0,50,0,0,50,0,     50,70,85,0,0,0},
    {0,60,0,0,0,5000,   40,65,85,0,0,0},
    
    {0,0,0,0,50,0,      0,0,0,35,50,70},
    {0,0,0,0,0,6000,    0,0,0,36,60,75},
    {40,0,30,20,0,0,    0,0,0,40,60,75},
    {0,60,0,0,0,5000,   0,0,0,40,60,75},
    
    // 49
    
    {40,0,40,0,0,0,     35,60,80,0,0,0},
    {0,0,0,0,0,6500,    45,75,85,0,0,0},
    {0,40,40,0,40,0,    45,75,90,0,0,0},
    {0,55,0,55,0,6000,  55,75,90,0,0,0},
    
    {40,0,0,0,40,0,     0,0,0,40,60,75},
    {0,0,0,0,0,6500,    0,0,0,40,65,80},
    {40,0,40,40,0,0,    0,0,0,50,70,85},
    {0,55,55,0,0,6500,  0,0,0,55,75,90},
    
    // 57
    {0,60,0,0,0,0,      44,68,80,0,0,0},
    {0,0,0,0,0,7000,    50,80,90,0,0,0},
    {35,35,35,35,0,0,   45,75,90,0,0,0},
    {0,40,40,40,0,6500, 55,77,98,0,0,0},
    
    {60,0,0,0,0,0,      0,0,0,40,60,75},
    {0,0,0,0,0,7000,    0,0,0,45,70,85},
    {35,0,35,35,35,0,   0,0,0,50,70,85},
    {40,40,40,0,0,6500, 0,0,0,55,75,90},
    
    // 65
    {0,0,0,50,50,0,     45,70,90,0,0,0},
    {0,0,0,0,0,8000,    55,80,95,0,0,0},
    {35,40,45,50,0,0,   55,85,95,0,0,0},
    {0,0,80,0,0,7000,   60,80,95,0,0,0},
    
    {0,0,45,0,55,0,     0,0,0,55,75,90},
    {0,0,0,0,0,8000,    0,0,0,50,75,90},
    {35,0,40,45,50,0,   0,0,0,55,75,90},
    {0,0,75,0,0,7000,   0,0,0,60,80,95},
    
    // 73
    {0,0,0,72,0,0,      50,72,90,0,0,0},
    {0,0,0,0,0,8500,    55,80,100,0,0,0},
    {20,40,20,40,20,0,  55,85,95,0,0,0},
    {40,50,60,0,0,7500, 65,85,100,0,0,0},
    
    {0,0,75,0,0,0,      0,0,0,55,75,90},
    {0,0,0,0,0,8500,    0,0,0,55,80,95},
    {40,40,20,20,20,0,  0,0,0,55,75,90},
    {60,0,40,0,50,7500, 0,0,0,60,80,95},
    
    // 81
    {0,55,0,55,0,0,     50,75,95,0,0,0},
    {0,0,0,0,0,9000,    60,85,98,0,0,0},
    {55,0,55,55,0,0,    60,90,98,0,0,0},
    {0,0,0,90,0,8000,   65,85,98,0,0,0},
    
    {0,0,55,0,55,0,     0,0,0,55,75,92},
    {0,0,0,0,0,9000,    0,0,0,55,80,95},
    {55,0,0,55,55,0,    0,0,0,60,80,95},
    {0,0,0,85,0,8000,   0,0,0,65,85,98},
    
    // 89
    {0,90,0,0,0,0,      55,75,95,0,0,0},
    {0,0,0,0,0,9500,    60,85,100,0,0,0},
    {20,30,40,50,60,0,  60,85,100,0,0,0},
    {50,0,60,0,70,9000, 70,90,100,0,0,0},
    
    {0,0,0,0,90,0,      0,0,0,60,80,95},
    {0,0,0,0,0,9500,    0,0,0,60,85,100},
    {60,50,40,30,20,0,  0,0,0,60,80,100},
    {70,70,0,70,0,9000, 0,0,0,65,85,100},

    // 97
    {0,0,70,70,0,0,     50,75,95,0,0,0},
    {0,0,0,0,0,10000,   60,85,98,0,0,0},
    {55,55,55,55,55,0,   60,90,98,0,0,0},
    {0,100,0,0,0,10000,  65,85,98,0,0,0},
    
    {70,70,0,0,0,0,     0,0,0,55,75,92},
    {0,0,0,0,0,10000,   0,0,0,55,80,95},
    {0,60,60,60,60,0,    0,0,0,60,80,95},
    {100,0,0,0,0,10000,  0,0,0,65,85,98},
    
    // 105
    {0,0,0,0,120,0,      60,85,100,0,0,0},
    {0,0,0,0,0,12000,    60,85,100,0,0,0},
    {40,60,80,80,80,0,  60,85,100,0,0,0},
    {0,0,60,80,100,12000, 70,90,100,0,0,0},
    
    {0,0,120,0,0,0,      0,0,0,60,85,100},
    {0,0,0,0,0,12000,    0,0,0,60,85,100},
    {60,50,60,70,70,0,  0,0,0,60,80,100},
    {0,0,90,90,90,12000, 0,0,0,65,85,100},
    
    // 113
    {0,160,0,0,0,0,      60,100,120,0,0,0},
    {0,0,0,0,0,15000,    60,100,120,0,0,0},
    {80,80,80,80,80,0,   60,100,120,0,0,0},
    {0,0,0,0,140,14000, 60,100,120,0,0,0},
    
    {0,0,0,0,150,0,      0,0,0,60,100,100},
    {0,0,0,0,0,16000,    0,0,0,60,100,120},
    {100,0,100,0,100,0,  0,0,0,60,100,100},
    {0,120,0,120,0,16000, 0,0,0,60,100,100},
};





GameManager* GameManager::m_pInstance = NULL;

GameManager* GameManager::sharedGameManager()
{
    if (!m_pInstance)
        m_pInstance = new GameManager();
    
    return m_pInstance;
}

GameManager::GameManager() {
	localizedStrings = NULL;
	serverSetting = NULL;
    currentMusic[0] = '\0';
    gameMode = 0;
    selectedStage = 0;
    hasBonusScene = false;
    playerCount = 0;
    inBattle = false;
}

GameManager::~GameManager() {
    if (localizedStrings != NULL) {
        localizedStrings->release();
    }
    
    if (serverSetting != NULL) {
        serverSetting->release();
    }
}

void GameManager::setupAudioEngine() {
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("clear.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("pop.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("select.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("beep.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("readygo.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("coin.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("coinsin.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("cheer.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("landing.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("jump.wav");




    
    CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(0.6);
}

int GameManager::playSoundEffect(const char* soundEffectName) {
    if (gameSetting.isSoundOff == false) {
        return CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(soundEffectName);
    }
    
    return 0;
}


void GameManager::stopSoundEffect(int soundId) {
    if (gameSetting.isSoundOff == false) {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(soundId);
    }
}

void GameManager::playMusic(const char *musicName) {
    if (gameSetting.isMusicOff == false) {
        if (strcmp(musicName, currentMusic) != 0) {
            CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
            CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(musicName, true);
        }
    }
    strcpy(currentMusic, musicName);
}

void GameManager::stopMusic() {
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
}

void GameManager::continueMusic() {
    if (GameManager::sharedGameManager()->gameSetting.isMusicOff == false) {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(currentMusic, true);
    }
}


void GameManager::loadGameSetting() {
    CCUserDefault* userDefault = CCUserDefault::sharedUserDefault();
    gameSetting.highScore = userDefault->getIntegerForKey("highScore");
    gameSetting.bestScoreTime = userDefault->getIntegerForKey("bestScoreTime");
    gameSetting.bestScoreMove = userDefault->getIntegerForKey("bestScoreMove");
    gameSetting.isMusicOff = userDefault->getBoolForKey("isMusicOff");
    gameSetting.isSoundOff = userDefault->getBoolForKey("isSoundOff");
    gameSetting.isSingleTapOff = userDefault->getBoolForKey("isSingleTapOff");

    gameSetting.coins = userDefault->getIntegerForKey("coins");
    //gameMode = userDefault->getIntegerForKey("gameMode");
    
    // Give the first time bonus
    if (gameSetting.coins <= 0) {
        bool firstTimeBonusGiven = userDefault->getBoolForKey("firstTimeBonusGiven");
        if (!firstTimeBonusGiven) {
            
            
#ifdef __PW__
            gameSetting.coins = 150;
#else
            gameSetting.coins = 30;
#endif
            
            userDefault->setBoolForKey("firstTimeBonusGiven", true);
            userDefault->setIntegerForKey("coins", gameSetting.coins);
            userDefault->flush();
        }
    }
}

void GameManager::saveGameSetting() {
    CCUserDefault* userDefault = CCUserDefault::sharedUserDefault();
    userDefault->setIntegerForKey("highScore", gameSetting.highScore);
    userDefault->setIntegerForKey("bestScoreTime", gameSetting.bestScoreTime);
    userDefault->setIntegerForKey("bestScoreMove", gameSetting.bestScoreMove);
    userDefault->setBoolForKey("isMusicOff", gameSetting.isMusicOff);
    userDefault->setBoolForKey("isSoundOff", gameSetting.isSoundOff);
    userDefault->setBoolForKey("isSingleTapOff", gameSetting.isSingleTapOff);
    userDefault->setIntegerForKey("coins", gameSetting.coins);
    //userDefault->setIntegerForKey("gameMode", gameMode);
    
    userDefault->flush();
}

bool GameManager::loadGameState() {
    CCUserDefault* userDefault = CCUserDefault::sharedUserDefault();
    gameState.stage = userDefault->getIntegerForKey("stage");
    if (gameState.stage == 0)
        return false;

    gameState.score = userDefault->getIntegerForKey("score");
    gameState.beginScore = userDefault->getIntegerForKey("beginScore");
    for (int i=0; i<10; i++) {
        for (int j=0; j<10; j++) {
            std::string key = "matrix_" + to_string(i) + "_" + to_string(j);
            gameState.matrix[i][j] = userDefault->getIntegerForKey(key.c_str());
        }
    }
    return true;
}

void GameManager::saveGameState() {
    CCUserDefault* userDefault = CCUserDefault::sharedUserDefault();
    userDefault->setIntegerForKey("stage", gameState.stage);
    userDefault->setIntegerForKey("score", gameState.score);
    userDefault->setIntegerForKey("beginScore", gameState.beginScore);
    for (int i=0; i<10; i++) {
        for (int j=0; j<10; j++) {
            std::string key = "matrix_" + to_string(i) + "_" + to_string(j);
            userDefault->setIntegerForKey(key.c_str(), gameState.matrix[i][j]);
        }
    }
    
    userDefault->flush();
}


void GameManager::setupLocalization() {
    ccLanguageType currentLanguage = CCApplication::sharedApplication()->getCurrentLanguage();
    localizedStrings = CCDictionary::create();    
    localizedStrings->retain();

#ifdef __ChineseOnly__
    currentLanguage = kLanguageChinese;
#endif
    
    if (currentLanguage == kLanguageChinese) {
        
        localizedStrings->setObject(CCString::create("微信"), "wechat");
        localizedStrings->setObject(CCString::create("朋友圈"), "timeline");
        
#ifdef __Fruit__
        localizedStrings->setObject(CCString::create("水果爆爆乐"), "appname");
#else
#ifdef __POPPER__
        localizedStrings->setObject(CCString::create("StarPopper"), "appname");
#else
        localizedStrings->setObject(CCString::create("PopStar"), "appname");
#endif
#endif
        

        
        localizedStrings->setObject(CCString::create("第%d关"), "stage");
        localizedStrings->setObject(CCString::create("目标 %d"), "target");
        localizedStrings->setObject(CCString::create("剩余 %d"), "remain");
        localizedStrings->setObject(CCString::create("奖励 %d"), "bonus");
        localizedStrings->setObject(CCString::create("+ %d 金币"), "coins");
        localizedStrings->setObject(CCString::create("你现在拥有 %d 金币"), "haveCoins");
        
        localizedStrings->setObject(CCString::create("第%d关"), "labelStage");
        localizedStrings->setObject(CCString::create("目标  %d"), "labelTarget");
        localizedStrings->setObject(CCString::create("%d"), "labelScore");
        localizedStrings->setObject(CCString::create("最高 %d"), "labelBest");
        
        localizedStrings->setObject(CCString::create("做好准备"), "getReady");
        localizedStrings->setObject(CCString::create("限时%d秒"), "timeText");
        localizedStrings->setObject(CCString::create("限定%d步"), "moveText");
        localizedStrings->setObject(CCString::create("时间 %d"), "labelTime");
        localizedStrings->setObject(CCString::create("步数 %d"), "labelMove");
        
        localizedStrings->setObject(CCString::create("炸掉3x3方块..."), "bombInfo");
        localizedStrings->setObject(CCString::create("选择需要变色的方块..."), "paintInfo");
        localizedStrings->setObject(CCString::create("%d个方块 : %d"), "selectInfo");
        localizedStrings->setObject(CCString::create("选择想要的颜色..."), "selectColorInfo");
        
        localizedStrings->setObject(CCString::create("play_zh.png"), "play.png");
        
#ifdef __Fruit__
        localizedStrings->setObject(CCString::create("title_zh.png"), "title.png");
#else
#ifdef __POPPER__
        localizedStrings->setObject(CCString::create("title_popper.png"), "title.png");
#else
#ifdef __PW__
        localizedStrings->setObject(CCString::create("title_cn.png"), "title.png");
#else
        localizedStrings->setObject(CCString::create("title.png"), "title.png");
#endif
#endif
#endif
        
        localizedStrings->setObject(CCString::create("mm_zh.fnt"), "mm.fnt");

        
        localizedStrings->setObject(CCString::create("60秒"), "timeInfo");
        localizedStrings->setObject(CCString::create("30步"), "moveInfo");
        localizedStrings->setObject(CCString::create("经典"), "targetInfo");
        localizedStrings->setObject(CCString::create("冒险"), "adventureInfo");
        localizedStrings->setObject(CCString::create("对战"), "vsInfo");


        localizedStrings->setObject(CCString::create("经典过关 - 第%d关"), "targetInfo2");
        
        
        localizedStrings->setObject(CCString::create("使用5个金币, 奖励1到10个金币"), "timeInfoDetail");
        localizedStrings->setObject(CCString::create("使用5个金币, 奖励1到10个金币"), "moveInfoDetail");
        localizedStrings->setObject(CCString::create("满足目标分数即可过关"), "targetInfoDetail");
        localizedStrings->setObject(CCString::create("满足每一关的过关条件, 争取3颗星"), "adventureInfoDetail");
        localizedStrings->setObject(CCString::create("对战用5个金币, 奖励最高10个金币"), "vsInfoDetail");
        localizedStrings->setObject(CCString::create("拥有金币不足, 对战用5个金币, 奖励最高10个金币"), "vsInfoDetail2");


        
        localizedStrings->setObject(CCString::create("上次玩到第%d关, 是否继续?"), "continueQuery");
        localizedStrings->setObject(CCString::create("新游戏"), "new");
        localizedStrings->setObject(CCString::create("继续游戏"), "continue");
        
        localizedStrings->setObject(CCString::create("炸弹花费1个金币"), "bombUsed");
        localizedStrings->setObject(CCString::create("颜料花费2个金币"), "paintUsed");
#ifdef __Fruit__
        localizedStrings->setObject(CCString::create("花费3个金币"), "hintUsed");
#else
        localizedStrings->setObject(CCString::create("彩虹花费3个金币"), "hintUsed");
#endif
        localizedStrings->setObject(CCString::create("加5秒花费1个金币"), "addTimeUsed");
        localizedStrings->setObject(CCString::create("加3步花费1个金币"), "addMovesUsed");

        
        localizedStrings->setObject(CCString::create("基本规则 :"), "pauseLine1");
        localizedStrings->setObject(CCString::create("冒险模式 :"), "pauseLine1a");
        localizedStrings->setObject(CCString::create("- 选中并消除相连的方块"), "pauseLine2");
        localizedStrings->setObject(CCString::create("- 方块上下左右方向相连, 斜向不相连"), "pauseLine3");
        localizedStrings->setObject(CCString::create("- 消除的相连方块越多, 分数越高"), "pauseLine4");
        localizedStrings->setObject(CCString::create("- 剩余方块少于10, 有额外奖励"), "pauseLine5");
        localizedStrings->setObject(CCString::create("- 获取目标方块或分数过关"), "pauseLine4a");
        localizedStrings->setObject(CCString::create("- 步数或时间越少, 评级越高"), "pauseLine5a");
        localizedStrings->setObject(CCString::create("- 关卡目标例子"), "stageTargetExample");
        
        localizedStrings->setObject(CCString::create("道具 :"), "pauseLine6");
        localizedStrings->setObject(CCString::create("- 炸掉3x3方块 - 1个金币"), "pauseLine7");
        localizedStrings->setObject(CCString::create("- 自选方块颜色 - 2个金币"), "pauseLine8");
        localizedStrings->setObject(CCString::create("- 更新所有方块颜色 - 3个金币"), "pauseLine9");
        localizedStrings->setObject(CCString::create("- 加3次移动步数 - 1个金币"), "pauseLine7a");
        localizedStrings->setObject(CCString::create("- 加5秒时间 - 1个金币"), "pauseLine7b");
        localizedStrings->setObject(CCString::create("相连"), "pauseConnected");
        localizedStrings->setObject(CCString::create("不相连"), "pauseConnectedNot");
        
        
        localizedStrings->setObject(CCString::create("目标达成"), "stageClear");
        
        localizedStrings->setObject(CCString::create("想再试一次第%d关吗 (5个金币)?"), "restartQuery");
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        localizedStrings->setObject(CCString::create("分享你的分数, 和小伙伴们比赛!"), "shareScore");
#else
        localizedStrings->setObject(CCString::create("分享你的分数, 获得额外金币"), "shareScore");
#endif
        localizedStrings->setObject(CCString::create("从AppStore下载, 试试看你能得多少分"), "beatMe");
        
        
        localizedStrings->setObject(CCString::create("评分"), "rate");
        localizedStrings->setObject(CCString::create("+ 5 免费金币"), "free");

        localizedStrings->setObject(CCString::create("收集以下颜色方块"), "collectColors");
        localizedStrings->setObject(CCString::create("达到目标分数"), "reachScore");
        localizedStrings->setObject(CCString::create("收集以下颜色并达到目标分数"), "colorAndScore");

        localizedStrings->setObject(CCString::create("恭喜获胜!"), "youWon");
        localizedStrings->setObject(CCString::create("再试一次!"), "tryAgain");
        localizedStrings->setObject(CCString::create("你用了%d步"), "usedMoves");
        localizedStrings->setObject(CCString::create("你用了%d秒"), "usedSeconds");
        localizedStrings->setObject(CCString::create("步数用完了"), "outOfMoves");
        localizedStrings->setObject(CCString::create("时间用完了"), "outOfTime");
        
        localizedStrings->setObject(CCString::create("使用过加时间, 一星"), "addTimeItemUsed");
        localizedStrings->setObject(CCString::create("使用过加步数, 一星"), "addMovesItemUsed");


        localizedStrings->setObject(CCString::create("选择关卡"), "selectStage");
        localizedStrings->setObject(CCString::create("免费金币"), "freeCoins");
        localizedStrings->setObject(CCString::create("更多金币"), "moreCoins");

        localizedStrings->setObject(CCString::create("我得到%d分,你能超过我吗? "), "shareText");
        
        localizedStrings->setObject(CCString::create("赏个评价, 免费解锁冒险模式"), "askRate");

        localizedStrings->setObject(CCString::create("确认退出本次对战吗?"), "exitBattleQuery");
        localizedStrings->setObject(CCString::create("退出游戏导致对战终止"), "exitBattleAlert");

    }
    else { // Default to English

        localizedStrings->setObject(CCString::create("WeChat"), "wechat");
        localizedStrings->setObject(CCString::create("Timeline"), "timeline");
        
        

#ifdef __Fruit__
        localizedStrings->setObject(CCString::create("PopFruit"), "appname");
#else
#ifdef __POPPER__
        localizedStrings->setObject(CCString::create("StarPopper"), "appname");
#else
        localizedStrings->setObject(CCString::create("PopStar"), "appname");
#endif
#endif
        
        


        
        localizedStrings->setObject(CCString::create("Stage %d"), "stage");
        localizedStrings->setObject(CCString::create("Target %d"), "target");
        localizedStrings->setObject(CCString::create("%d remaining"), "remain");
        localizedStrings->setObject(CCString::create("Bonus %d"), "bonus");
        localizedStrings->setObject(CCString::create("+ %d Coins"), "coins");
        localizedStrings->setObject(CCString::create("You have %d Coins"), "haveCoins");
        
        localizedStrings->setObject(CCString::create("Stage %d"), "labelStage");
        localizedStrings->setObject(CCString::create("Target %d"), "labelTarget");
        localizedStrings->setObject(CCString::create("%d"), "labelScore");
        localizedStrings->setObject(CCString::create("Best %d"), "labelBest");
        
        localizedStrings->setObject(CCString::create("Get Ready"), "getReady");
        localizedStrings->setObject(CCString::create("%d Seconds Left"), "timeText");
        localizedStrings->setObject(CCString::create("%d Moves Left"), "moveText");
        localizedStrings->setObject(CCString::create("Time %d"), "labelTime");
        localizedStrings->setObject(CCString::create("Moves %d"), "labelMove");
        
        localizedStrings->setObject(CCString::create("Bomb 3x3 blocks off..."), "bombInfo");
        localizedStrings->setObject(CCString::create("Select a block to change color..."), "paintInfo");
        localizedStrings->setObject(CCString::create("%d blocks : %d"), "selectInfo");
        localizedStrings->setObject(CCString::create("Select desired color..."), "selectColorInfo");
        
        localizedStrings->setObject(CCString::create("play.png"), "play.png");
        
        

#ifdef __POPPER__
        localizedStrings->setObject(CCString::create("title_popper.png"), "title.png");
#else
#ifdef __PW__
        localizedStrings->setObject(CCString::create("title_cn.png"), "title.png");
#else
        localizedStrings->setObject(CCString::create("title.png"), "title.png");
#endif
#endif
        
        localizedStrings->setObject(CCString::create("mm_en.fnt"), "mm.fnt");

        

        localizedStrings->setObject(CCString::create("60 SECONDS"), "timeInfo");
        localizedStrings->setObject(CCString::create("30 MOVES"), "moveInfo");
        
        localizedStrings->setObject(CCString::create("CLASSIC"), "targetInfo");
        localizedStrings->setObject(CCString::create("CLASSIC - Stage %d"), "targetInfo2");
        localizedStrings->setObject(CCString::create("ADVENTURE"), "adventureInfo");
        localizedStrings->setObject(CCString::create("ONLINE"), "vsInfo");

        
        
        localizedStrings->setObject(CCString::create("5 coins to play, win up to 10 coins"), "timeInfoDetail");
        localizedStrings->setObject(CCString::create("5 coins to play, win up to 10 coins"), "moveInfoDetail");
        localizedStrings->setObject(CCString::create("Meet the stage target"), "targetInfoDetail");
        localizedStrings->setObject(CCString::create("Meet target conditions, earn stars"), "adventureInfoDetail");
        localizedStrings->setObject(CCString::create("5 coins to play online, win up to 10"), "vsInfoDetail");
        localizedStrings->setObject(CCString::create("5 coins to play online, win up to 10. You need more coins."), "vsInfoDetail2");


        
        localizedStrings->setObject(CCString::create("Continue your last game at stage %d ?"), "continueQuery");
        localizedStrings->setObject(CCString::create("Restart"), "new");
        localizedStrings->setObject(CCString::create("Continue"), "continue");
        
        
        localizedStrings->setObject(CCString::create("Bomb costs 1 coin"), "bombUsed");
        localizedStrings->setObject(CCString::create("Paint costs 2 coins"), "paintUsed");
#ifdef __Fruit__
        localizedStrings->setObject(CCString::create("Splash costs 3 coins"), "hintUsed");
#else
        localizedStrings->setObject(CCString::create("Rainbow costs 3 coins"), "hintUsed");
#endif
        localizedStrings->setObject(CCString::create("Adding 5 seconds costs 1 coin"), "addTimeUsed");
        localizedStrings->setObject(CCString::create("Adding 3 moves costs 1 coin"), "addMovesUsed");
        
        
        localizedStrings->setObject(CCString::create("Basic rules :"), "pauseLine1");
        localizedStrings->setObject(CCString::create("Adventure mode :"), "pauseLine1a");
        localizedStrings->setObject(CCString::create("- Select and clear connected blocks"), "pauseLine2");
        localizedStrings->setObject(CCString::create("- Blocks are connected vertically and horizontally, not diagnally."), "pauseLine3");
        localizedStrings->setObject(CCString::create("- More connected,  higher score"), "pauseLine4");
        localizedStrings->setObject(CCString::create("- Get bonus when less than 10 remained"), "pauseLine5");
        localizedStrings->setObject(CCString::create("- Get target blocks and/or score to advance"), "pauseLine4a");
        localizedStrings->setObject(CCString::create("- Less moves or time used, higher rating"), "pauseLine5a");
        localizedStrings->setObject(CCString::create("- Stage target example"), "stageTargetExample");


        localizedStrings->setObject(CCString::create("Items :"), "pauseLine6");
        localizedStrings->setObject(CCString::create("- Bomb off 3x3 blocks - 1 coin"), "pauseLine7");
        localizedStrings->setObject(CCString::create("- Change the color of a block - 2 coins"), "pauseLine8");
        localizedStrings->setObject(CCString::create("- Recolor all blocks - 3 coins"), "pauseLine9");
        localizedStrings->setObject(CCString::create("- Add 3 moves - 1 coin"), "pauseLine7a");
        localizedStrings->setObject(CCString::create("- Add 5 seconds - 1 coin"), "pauseLine7b");


        
        localizedStrings->setObject(CCString::create("Connected"), "pauseConnected");
        localizedStrings->setObject(CCString::create("Not connected"), "pauseConnectedNot");
        
        localizedStrings->setObject(CCString::create("Stage Clear"), "stageClear");
        
        localizedStrings->setObject(CCString::create("Try stage %d again ( 5 coins ) ?"), "restartQuery");
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        localizedStrings->setObject(CCString::create("Share your score , challenge your friends"), "shareScore");
#else
        localizedStrings->setObject(CCString::create("Share your score and get extra coins"), "shareScore");
#endif
        localizedStrings->setObject(CCString::create("Can you do better? Download for FREE from AppStore"), "beatMe");
        
        localizedStrings->setObject(CCString::create("Rate"), "rate");
        localizedStrings->setObject(CCString::create("+ 5 Free Coins"), "free");
        
        
        // Mode 3
        localizedStrings->setObject(CCString::create("Collect the colors"), "collectColors");
        localizedStrings->setObject(CCString::create("Reach score target"), "reachScore");
        localizedStrings->setObject(CCString::create("Collect colors and reach score target"), "colorAndScore");

        localizedStrings->setObject(CCString::create("You won!"), "youWon");
        localizedStrings->setObject(CCString::create("Try again!"), "tryAgain");
        localizedStrings->setObject(CCString::create("Used %d moves"), "usedMoves");
        localizedStrings->setObject(CCString::create("Used %d seconds"), "usedSeconds");
        localizedStrings->setObject(CCString::create("Run out of moves"), "outOfMoves");
        localizedStrings->setObject(CCString::create("Run out of time"), "outOfTime");
        
        localizedStrings->setObject(CCString::create("Add time used, 1 star"), "addTimeItemUsed");
        localizedStrings->setObject(CCString::create("Add moves used, 1 star"), "addMovesItemUsed");

        localizedStrings->setObject(CCString::create("Select Stage"), "selectStage");
        localizedStrings->setObject(CCString::create("Free Coins"), "freeCoins");
        localizedStrings->setObject(CCString::create("More Coins"), "moreCoins");

        localizedStrings->setObject(CCString::create("I got %d. Can you do better? "), "shareText");
        
        localizedStrings->setObject(CCString::create("Rate the game to unlock this mode"), "askRate");
        localizedStrings->setObject(CCString::create("Quit current battle?"), "exitBattleQuery");
        localizedStrings->setObject(CCString::create("You are disconnected"), "exitBattleAlert");


    }
}


const char* GameManager::getLocalizedString(const char* key) {
    if (localizedStrings == NULL) {
        this->setupLocalization();
    }
    CCString* value = (CCString*)(localizedStrings->objectForKey(key));
    if (value != NULL) {
        return value->getCString();
    }
    else {
        return key;
    }
}



bool GameManager::getServerBoolean(const char* key) {
    const char* theString = getServerString(key);
    if (theString == NULL) {
        return 0;
    }
    return atoi(theString);
}

int GameManager::getServerInterger(const char* key) {
    const char* theString = getServerString(key);
    if (theString == NULL) {
        return 0;
    }
    return atoi(theString);
}

const char* GameManager::getServerString(const char* key) {
    if (serverSetting == NULL) {
        return NULL;
    }
    
    CCString* value = (CCString*)(serverSetting->objectForKey(key));
    if (value == NULL) {
        return NULL;
    }
    else {
        return value->getCString();
    }
}

void GameManager::loadServerSetting() {
    
    CCHttpRequest* request = new CCHttpRequest();
    
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if ((screenSize.height / screenSize.width) < 1.5) { // iPad
        request->setUrl(kServerSettingURLHD);
    }
    else {
        request->setUrl(kServerSettingURL);
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if ( screenSize.width > 720 ) { // for android devices screen larger than retina display
        request->setUrl(kServerSettingURLHD);
    }
    else {
        request->setUrl(kServerSettingURL);
    }
#else
    request->setUrl(kServerSettingURL);
#endif
    
    request->setRequestType(CCHttpRequest::kHttpGet);
    request->setResponseCallback(this, httpresponse_selector(GameManager::httpLoaded));
    request->setTag("GET Data");
    CCHttpClient::getInstance()->send(request);
    request->release();
    
}


void GameManager::httpLoaded(CCHttpClient *sender, CCHttpResponse *data) {
    CCHttpResponse *response = (CCHttpResponse*)data;
    
    if (!response)
    {
        return;
    }
    
    if (!response->isSucceed())
    {
        CCLog("NETWORK: response failed");
        CCLog("NETWORK: error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
    std::vector<char> *buffer = response->getResponseData();
    std::string path = CCFileUtils::sharedFileUtils()->getWritablePath();
    std::string bufstring(buffer->begin(), buffer->end());
    path += "server.plist";

    CCLOG("NETWORK: buffer: \n %s", bufstring.c_str());
    CCLOG("NETWORK: filepath: %s", path.c_str());

    FILE *fp = fopen(path.c_str(), "wb+");
    fwrite(bufstring.c_str(), 1, buffer->size(), fp);
    fclose(fp);
    
    serverSetting = CCDictionary::createWithContentsOfFile(path.c_str());
    serverSetting->retain();
    
    
    // Cache the interstitial ad
    onServerSettingLoaded();
    
}





void GameManager::onServerSettingLoaded() {
    
    // 2018: always cache interstitial
    
    /*
    if (getServerInterger("interType2") != 0) {
        // 64 bit conflict full screen 
        cacheInterstitial();
    }
     */
}



AdventureStage GameManager::getAdventureStage(unsigned int stage) {
    
    
    AdventureStage result = {0};
    result.stage = stage;
    
    // max stage is ArraySize - 1, (16 right now)
    if (stage == 0) return result;
    if (stage >= ARRAY_SIZE(stages)) return result;
    
    for (int i=0; i<6; i++) {
        result.target[i] = stages[stage][i];
    }
    
    for (int i=0; i<3; i++) {
        result.moves[i] = stages[stage][i+6];
    }
    
    for (int i=0; i<3; i++) {
        result.time[i] = stages[stage][i+9];
    }
    return result;
}


int GameManager::getAdventureStageCount() {
    return ARRAY_SIZE(stages) - 1;
}

void GameManager::updateCoins() {
    
    CCScene* currentScene = CCDirector::sharedDirector()->getRunningScene();
    
    StoreScene* storeScene = dynamic_cast<StoreScene*>(currentScene);
    if (storeScene) {
        storeScene->updateCoins();
    }

    GameScene *gameScene = dynamic_cast<GameScene*>(currentScene);
    if (gameScene) {
        gameScene->updateCoins();
    }
    
    MenuScene *menuScene = dynamic_cast<MenuScene*>(currentScene);
    if (menuScene) {
        menuScene->updateCoins();
    }
    
}






void GameManager::removeAllPlayers() {
    for (int i=0; i<playerCount; i++) {
        players[i].playerID->release();
        players[i].playerName->release();
        players[i].playerPhoto->release();
    }
    playerCount = 0;
}
void GameManager::addPlayer(int i, const char* playerId, const char* playerName, CCSprite* playerPhoto) {

    
    players[i].playerID = CCString::create(playerId);
    players[i].playerName = CCString::create(playerName);
    players[i].playerPhoto = playerPhoto;

    
    players[i].playerID->retain();
    players[i].playerName->retain();
    players[i].playerPhoto->retain();
    
    players[i].score = 0;
    players[i].rank = i;
    
    playerCount ++;
    
}

int GameManager::getScore(int playerIndex) {
    return players[playerIndex].score;

}

void GameManager::setScore(const char* playerId, int score) {
    for (int i=0; i<playerCount; i++) {
        if (strcmp(playerId, players[i].playerID->getCString()) == 0) {
            setScore(i, score);
        }
    }
}

void GameManager::setScore(int playerIndex, int score) {
    players[playerIndex].score = score;

}

int GameManager::getPlayerCount() {
    return playerCount;
}

int GameManager::getRank(int playerIndex) {
    return players[playerIndex].rank;
}

const char* GameManager::getPlayerName(int playerIndex) {
    return players[playerIndex].playerName->getCString();
}

CCSprite* GameManager::getPicture(int playerIndex) {
    return players[playerIndex].playerPhoto;

}

const char* GameManager::getPlayerID(int playerIndex) {
    return players[playerIndex].playerID->getCString();
}

void GameManager::debugPlayers() {
    for (int i=0; i<playerCount; i++) {
        CCLog("%s: %s: %d", players[i].playerID->getCString(), players[i].playerName->getCString(), players[i].score);
    }
    
}

void GameManager::updateRank() {
    for (int i=0; i<playerCount; i++) {
        players[i].rank = 0;
    }
    
    for (int i=1; i<playerCount; i++) {
        for (int j=0; j<i; j++) {
            if (players[i].score <= players[j].score) {
                players[i].rank++;
            }
            else players[j].rank++;
        }
    }
    
}

void GameManager::updateGCScore() {
    
    CCScene* currentScene = CCDirector::sharedDirector()->getRunningScene();
    
    
    GameScene *gameScene = dynamic_cast<GameScene*>(currentScene);
    if (gameScene) {
        gameScene->updateGCScore();
    }
    

}
