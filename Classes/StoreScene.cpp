//
//  PauseScene.cpp
//  SimpleGame
//
//  Created by SimplyGame on 9/7/13.
//
//

#include "StoreScene.h"
#include "StoreLayer.h"

bool StoreScene::init() {
    if ( !CCScene::init() )
    {
        return false;
    }
    
    //GameManager::sharedGameManager()->playMusic("music.wav");
    StoreLayer* storeLayer = StoreLayer::create();
    storeLayer->setTag(kStoreLayerTag);
    this->addChild(storeLayer);
    
    return true;
}

void StoreScene::updateCoins() {
    StoreLayer* storeLayer = (StoreLayer*)this->getChildByTag(kStoreLayerTag);
    if (storeLayer) {
        storeLayer->updateCoins();
    }
}