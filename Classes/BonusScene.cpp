//
//  BonusScene.cpp
//  SimpleGame
//
//  Created by SimplyGame on 10/1/13.
//
//

#include "BonusScene.h"
#include "BonusLayer.h"
#include "GameManager.h"


bool BonusScene::init() {
    if ( !CCScene::init() )
    {
        return false;
    }
    
    if (GameManager::sharedGameManager()->hasBonusScene == true) {
        GameManager::sharedGameManager()->playMusic("music.wav");
    }
    BonusLayer* bonusLayer = BonusLayer::create();
    bonusLayer->setTag(kBonusLayerTag);
    this->addChild(bonusLayer);
    
    return true;
}