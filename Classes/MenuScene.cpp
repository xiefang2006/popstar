//
//  MenuScene.cpp
//  SimpleGame
//
//  Created by SimplyGame on 9/3/13.
//
//

#include "MenuScene.h"
#include "MenuLayer.h"
#include "GameManager.h"

void MenuScene::updateCoins() {
    MenuLayer* menuLayer = (MenuLayer*)this->getChildByTag(kMenuLayerTag);
    if (menuLayer) {
        menuLayer->updateCoins();
    }
}


bool MenuScene::init() {
    if ( !CCScene::init() )
    {
        return false;
    }
    
    GameManager::sharedGameManager()->playMusic("music.wav");
    MenuLayer* menuLayer = MenuLayer::create();
    menuLayer->setTag(kMenuLayerTag);
    this->addChild(menuLayer);
    
    return true;
}