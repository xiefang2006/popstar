//
//  PauseLayer.h
//  SimpleGame
//
//  Created by SimplyGame on 9/6/13.
//
//

#ifndef __SimpleGame__PauseLayer__
#define __SimpleGame__PauseLayer__

#include "cocos2d.h"

using namespace cocos2d;

class  PauseLayer : public CCLayer
{
private:
    CCSize screenSize;

public:
    void layoutItemsForAdventure();
    void layoutItems();
    void layoutMenu();
    void buttonTouched(CCNode* pNode);
    virtual bool init();
    
    void updateRightCorner();
    void rightCornerSelected(CCNode* pNode);
    
    void keyBackClicked();

    CREATE_FUNC(PauseLayer);
};
#endif /* defined(__SimpleGame__PauseLayer__) */
