/****************************************************************************
 Copyright (c) 2010 cocos2d-x.org
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

#import <GoogleMobileAds/GADBannerView.h>

//#import <Parse/Parse.h>
#import "AppController.h"
#import "cocos2d.h"
#import "EAGLView.h"
#import "AppDelegate.h"

#import "RootViewController.h"
//#import "Flurry.h"

#include "GameManager.h"
#include "GameScene.h"
#include "BonusScene.h"
#include "MenuScene.h"

#include "Popstar.h"

@implementation AppController
@synthesize url;

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    alertType = 0;
    self.url = nil;
    
    /*
    [Parse setApplicationId:@"rKto8KniY7vXTMSuhC8sDKwzehAKVPD5x6F9W6Ok" clientKey:@"u8nqsODOvhpcgqm9v12xaW8FB9Ngd8fQG0dSJgLK"];
*/
    
    // Flurry
    //[Flurry startSession:kFlurryId];
    
    // Override point for customization after application launch.
    //[WXApi registerApp:kWechatId];

    // Remote notification
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    


    
    // Handle notification
    
    if (application.applicationIconBadgeNumber == 1) {
        [self processNotificationInApp:application withNotification:nil isStartingApp:YES];
    }
    else if (launchOptions != nil) {
        NSDictionary *remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        
        if (remoteNotification != nil) {
            [self processNotificationInApp:application withNotification:remoteNotification isStartingApp:YES];
        }
        
        //UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        
        /*
        else if (localNotification != nil) {
            [self processNotificationInApp:application withNotification:nil isStartingApp:YES];
        }
         */

    }
    
    
    
    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    EAGLView *__glView = [EAGLView viewWithFrame: [window bounds]
                                        pixelFormat: kEAGLColorFormatRGBA8
                                        depthFormat: GL_DEPTH_COMPONENT16
                                 preserveBackbuffer: NO
                                                                                 sharegroup:nil
                                                                          multiSampling:NO
                                                                    numberOfSamples:0];
    
    // Use RootViewController manage EAGLView 
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = __glView;

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:viewController];
    }
    
    [window makeKeyAndVisible];

    [[UIApplication sharedApplication] setStatusBarHidden: YES];
    
    cocos2d::CCApplication::sharedApplication()->run();
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */

    
    // If server side allowExit, we don't force terminate the battle
    bool allowExit = GameManager::sharedGameManager()->getServerBoolean("allowExit");
    bool inBattle = GameManager::sharedGameManager()->inBattle;
    
    if (inBattle && !allowExit) {
        CCScene *currentScene = CCDirector::sharedDirector()->getRunningScene();
        GameScene *gameScene = dynamic_cast<GameScene*>(currentScene);
        if (gameScene) {
            gameScene->scheduleOnce(schedule_selector(GameScene::popupExitBattleAlert), 0.1);
        }
        
        //GameManager::sharedGameManager()->disconnectMatch();
        //CCDirector::sharedDirector()->replaceScene(CCTransitionTurnOffTiles::create(0.5, MenuScene::create()));
    }
    cocos2d::CCDirector::sharedDirector()->pause();
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    cocos2d::CCDirector::sharedDirector()->resume();

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    
    
    // Schedule bonus notification if less than 5 coins
    /*
    if (GameManager::sharedGameManager()->gameSetting.coins <= 5) {
        int interval = GameManager::sharedGameManager()->getServerInterger("bonusInterval");
        if (interval == 0)
            interval = 60 * 60 * 24;
    
        if (interval != -1) {
            [self schedulBonusNotificationWithTimeIntervalSinceNow:interval];
        }
    }
     */
    
    int coins = GameManager::sharedGameManager()->gameSetting.coins;
    int days = coins / 5;   // 0 for 0,1,2,3,4 coins, 1 for < 10 coins
    if (days > 6) days = 6;
    
    
    
    if (true) {
        int interval = GameManager::sharedGameManager()->getServerInterger("bonusInterval");
        if (interval == 0)
            interval = 60 * 60 * 24 * (days + 1);
        
        if (interval != -1) {
            [self schedulBonusNotificationWithTimeIntervalSinceNow:interval];
        }
    }
    
    cocos2d::CCApplication::sharedApplication()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    
    if (application.applicationIconBadgeNumber == 1) {
        [self processNotificationInApp:application withNotification:nil isStartingApp:NO];
    }
    
    cocos2d::CCApplication::sharedApplication()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [super dealloc];
}

-(UIViewController*)getViewController {
    return viewController;
}


#pragma mark -
#pragma mark Weixin sharing

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    // 64 bit conflict
    return false; //[WXApi handleOpenURL:url delegate:self];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    // 64 bit conflit
    return false; //[WXApi handleOpenURL:url delegate:self];
}


/*
-(void) onReq:(BaseReq*)req
{
    NSLog(@"OnReq called");
    
}

-(void) onResp:(BaseResp*)resp
{
    NSLog(@"OnResp called");
  */
    /* 64 bit conflict
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        if (resp.errCode == 0) {
            
            NSString* info = [NSString stringWithFormat:@"Social: %@", @"Weixin"];
            GameManager::sharedGameManager()->logEvent([info UTF8String]);
            
            GameScene* gameScene = dynamic_cast<GameScene*>(CCDirector::sharedDirector()->getRunningScene());
            if (gameScene != NULL) {
                gameScene->rewardBonusCoin();
            }
            
        }
    }
     */
//}



- (void)schedulBonusNotificationWithTimeIntervalSinceNow:(int)seconds {
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    if (localNotification == nil)
        return;
    
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:seconds];
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = NSLocalizedString(@"bonus", nil);
    localNotification.soundName = @"notif.caf";
    localNotification.applicationIconBadgeNumber = 1;
    localNotification.userInfo = nil; //[adConfig objectForKey:@"notification"];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    [localNotification release];
    
    //NSLog([[NSDate date] description]);
    //NSLog([localNotification.fireDate description]);
    
    
    // We will use the badge number to know what kind of notification it is, so that's the only useful info
}

- (void)processNotificationInApp:(UIApplication *)application withNotification:(NSDictionary *)notification isStartingApp:(BOOL)starting{
    
    // Clear pending notificaiton
    application.applicationIconBadgeNumber = 1;
    application.applicationIconBadgeNumber = 0;
    [application cancelAllLocalNotifications];
    
    
    if (notification) {
        
        /*
         
        if ([notification objectForKey:@"url"]) {
            NSString* str = [notification objectForKey:@"url"];
            GameManager::sharedGameManager()->setNotificationUrl([str UTF8String]);
        }
         */
        
        /* format 
         
         {"alert": "New PopStar. 全新对战版消灭星星","badge": "2","sound": "notif.caf","type": "promo","pTitle": "All new PopStar. 全新对战版消灭星星","pText": "Get it now? 现在下载？","pUrl": "928037439"}
         
                  {"alert": "Play online mode and earn free coins","badge": "2","sound": "notif.caf","type": "promo","pTitle": "Update Available","pText": "Get coins to play with millions in online battle mode?","pUrl": "928037439"}
         
         {"alert": "Play online mode and earn FREE coins","badge": "2","sound": "notif.caf","type": "promo","pTitle": "Update Available","pText": "Get FREE coins and challenge millions in online battle mode?","pUrl": "928037439"}
         
         {"alert": "玩网络对战，送金币礼包","badge": "2","sound": "notif.caf","type": "promo","pTitle": "立刻更新","pText": "玩全新网络对战，送拼手气金币礼包","pUrl": "928037439"}
         
         
         {"alert": "玩网络对战,得免费金币","badge": "2","sound": "notif.caf","type": "promo","pTitle": "立刻更新","pText": "获得免费金币并加入风靡数百万人的消灭星星网络对战吧!","pUrl": "928037439"}
         
        {"alert": "Free Coins 送金币了","badge": "1","sound": "notif.caf","type": "promo","pTitle": "Free coins? 免费金币","pText": "Want free coins? 获取免费金币？？","pUrl": "httpnull"}
         */
        
        if ([[notification objectForKey:@"type"] isEqualToString:@"promo"]) {
        
            // Handle remote notification? UIAlert
            NSString* pTitle = [notification objectForKey:@"pTitle"];
            NSString* pText = [notification objectForKey:@"pText"];
            NSString* pUrl = [notification objectForKey:@"pUrl"];
            
            // No need to show alert if no url
            if (pUrl == nil) return;
            else (self.url = pUrl);
            alertType = 1;
        
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:pTitle message:pText delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"download", nil), nil];
        
            [alert show];
            [alert release];
        }
        return;
        
    }
    

    if (starting) {
        // App is starting
        GameManager::sharedGameManager()->hasBonusScene = true;
    }
    else {
        // App is running
        [self presentBonusScene];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString* promoUrl = [NSString stringWithString:self.url];
    self.url = nil;
    alertType = 0;
    
    if (buttonIndex == 0) return;
    else {
        if ([promoUrl hasPrefix:@"http"]) {
            // Open url
            GameManager::sharedGameManager()->openURL([promoUrl UTF8String]);
            [self presentBonusScene];
        }
        else {
            // url is appId
            [self presentAppStoreForID:promoUrl];
        }
    }
    
}

- (void)presentBonusScene {
    CCScene* pScene = BonusScene::create();
    CCDirector::sharedDirector()->pushScene(CCTransitionFadeTR::create(0.7, pScene));
}


- (void)presentAppStoreForID:(NSString *)appStoreID
{
    if(NSClassFromString(@"SKStoreProductViewController")) { // Checks for iOS 6 feature.
        
        SKStoreProductViewController *storeController = [[SKStoreProductViewController alloc] init];
        storeController.delegate = self; // productViewControllerDidFinish
        
        NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier : appStoreID };
        
        
        [storeController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *error) {
            if (result) {

                [[self getViewController] presentViewController:storeController animated:YES completion:nil];
            } else {
                // Gracefully do nothing
                //[[[UIAlertView alloc] initWithTitle:@"Uh oh!" message:@"There was a problem displaying the app" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            }
        }];
        
        
    } else { // Before iOS 6, we can only open the URL
        NSString* fullUrl = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", appStoreID];
        GameManager::sharedGameManager()->openURL([fullUrl UTF8String]);
        [self presentBonusScene];

    }
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)storeViewController {
    [[self getViewController] dismissViewControllerAnimated:YES completion:^{
        [storeViewController release];
        [self presentBonusScene];
    }];
}













//
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    
    /*
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
     */
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    // This will pop up an alert
    //[PFPush handlePush:userInfo];
    
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        //opened from a push notification when the app was on background
        [self processNotificationInApp:application withNotification:userInfo isStartingApp:NO];
    }
}

/*
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  ) {
            [self processNotificationInApp:application withNotification:nil isStartingApp:NO];
    }
}
 */

@end

