//
//  Star.h
//  SimpleGame
//
//  Created by Game on 14-8-13.
//
//

#ifndef SimpleGame_Star_h
#define SimpleGame_Star_h

//#define __Xmas__

// Platforms, pick one of them, should match app id
#define __Platform_iOS__
//#define __Platform_Mac__
//#define __Platform_Android__

#define kHintTime 3
#define kVSCoinNeed 5




// Platforms, pick one of them
#ifdef __Platform_iOS__

#ifdef __Star__
#define kInAppPurchaseCoin1Id "com.sg.popstar.coin1"
#define kInAppPurchaseCoin2Id "com.sg.popstar.coin2"
#define kInAppPurchaseCoin3Id "com.sg.popstar.coin3"
#define kInAppPurchaseCoin4Id "com.sg.popstar.coin4"
#define kInAppPurchaseCoin5Id "com.sg.popstar.coin5"

#define kServerSettingURL "https://www.simplygame.net/web/sgpopstar.plist"
#define kServerSettingURLHD "https://www.simplygame.net/web/sgpopstarhd.plist"
#define kAppUrl "https://itunes.apple.com/app/id685462046"
#define kRateUrl "itms-apps://itunes.apple.com/app/id685462046?action=write-review"
#define kFlurryId @"47JJ8VWNX9GT7CFRSD85"
#define kWechatId @"wx2da1eead1eb2e7e3"
#define kAdsmogoIdPhone @"5c424ddd2836419ab2933b9bb9b5642d"
#define kAdsmogoIdPad @"77d0769e23dd4fb3a072e8461f938388"
// FOR AFP (alimama)
#define kAdsAFPBannerPhone @"67408965"
#define kAdsAFPBannerPad @"67428527"
#define kAdsAFPInterPhone @"67426584"
#define kAdsAFPInterPad @"67434434"
#define kGuomobId @"cd7uy5tk01t1357"
#define kChartBoostId @"5268171b16ba47f23f000001"
#define kChartBoostSignature @"2b3c0e49d037c91a98d87cece0080cb73ebdb22a"
#define kLeaderboardClassic "grp.com.sg.popstar.classicmode"
#define kLeaderboardStage "grp.com.sg.popstar.stage"
#define kLeaderboardTime "grp.com.sg.popstar.timedmode"
#define kLeaderboardMove "grp.com.sg.popstar.movesmode"
#define kLeaderboardAdvStar "grp.com.sg.popstar.advstar"
#define kLeaderboardAdvStage "grp.com.sg.popstar.advstage"
#define kLeaderboardCoin "grp.com.sg.popstar.coin"
#define kLeaderboardBattle "grp.com.sg.popstar.battle"

#define kAdmobIdPhone @"a15203ccf34cbc7"
#define kAdmobIdPad @"a1528254c91e3b3"

//#define kDomobId @"96ZJ39xwzegKfwTA/L"
#define kDomobId @"96ZJ37CQzenkLwTB4o"




#endif

#endif



#ifdef __Platform_Mac__

#ifdef __Star__
#define kInAppPurchaseCoin1Id "com.sg.popstarmac.coin1"
#define kInAppPurchaseCoin2Id "com.sg.popstarmac.coin2"
#define kInAppPurchaseCoin3Id "com.sg.popstarmac.coin3"
#define kInAppPurchaseCoin4Id "com.sg.popstarmac.coin4"
#define kInAppPurchaseCoin5Id "com.sg.popstarmac.coin5"

#define kServerSettingURL "https://www.simplygame.net/web/sgpopstarmac.plist"
#define kRateUrl "macappstore://itunes.apple.com/app/id720910624?mt=12"
#define kLeaderboardClassic "grp.com.sg.popstar.classicmode"
#define kLeaderboardStage "grp.com.sg.popstar.stage"
#define kLeaderboardTime "grp.com.sg.popstar.timedmode"
#define kLeaderboardMove "grp.com.sg.popstar.movesmode"
#define kLeaderboardAdvStar "grp.com.sg.popstar.advstar"
#define kLeaderboardAdvStage "grp.com.sg.popstar.advstage"

#endif

#endif





#ifdef __Platform_Android__

#ifdef __Star__
#define kInAppPurchaseCoin1Id "com.sg.androidpopstar.coin1"
#define kInAppPurchaseCoin2Id "com.sg.androidpopstar.coin2"
#define kInAppPurchaseCoin3Id "com.sg.androidpopstar.coin3"
#define kInAppPurchaseCoin4Id "com.sg.androidpopstar.coin4"
#define kServerSettingURL "https://www.simplygame.net/web/sgpopstarandroid.plist"
#define kServerSettingURLHD "https://www.simplygame.net/web/sgpopstarandroidhd.plist"
#define kRateUrl "" // "macappstore://itunes.apple.com/app/id725833455?mt=12"       // no rate for android now.
#define kLeaderboardClassic "" // "grp.com.f7.popstar.classicmode"
#define kLeaderboardStage "" // "grp.com.f7.popstar.stage"
#define kLeaderboardTime "" // "grp.com.f7.popstar.timedmode"
#define kLeaderboardMove "" // "grp.com.f7.popstar.movesmode"


#endif

#endif



#endif
